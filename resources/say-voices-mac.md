| Language         | ISO Code | Voice Name                    | Phrase                           |
|------------------|----------|-------------------------------|----------------------------------|
| Arabic           | ar_001   | Majed                         | مرحبًا! اسمي ماجد.               |
| Bulgarian        | bg_BG    | Daria                         | Hello! My name is Daria.         |
| Cantonese        | zh_HK    | Sinji                         | 你好！我叫善怡。                  |
| Catalan          | ca_ES    | Montse                        | Hola! Em dic Montse.             |
| Chinese (Mandarin) | zh_CN  | Tingting                      | Hi my name is Tingting           |
| Chinese (Taiwan) | zh_TW    | Meijia                        | 你好，我叫美佳。                  |
| Croatian         | hr_HR    | Lana                          | Bok, zovem se Lana.              |
| Czech            | cs_CZ    | Zuzana                        | Hi my name is Zuzana             |
| Danish           | da_DK    | Sara                          | Hej! Jeg hedder Sara.            |
| Dutch (Belgium)  | nl_BE    | Ellen                         | Hallo! Mijn naam is Ellen.       |
| Dutch (Netherlands) | nl_NL | Xander                        | Hallo! Mijn naam is Xander.      |
| English (Australia) | en_AU | Karen                         | Hi my name is Karen              |
| English (India)  | en_IN    | Rishi                         | Hello! My name is Rishi.         |
| English (Ireland) | en_IE   | Moira                         | Hello! My name is Moira.         |
| English (South Africa) | en_ZA | Tessa                     | Hello! My name is Tessa.         |
| English (UK)     | en_GB    | Daniel                        | Hello! My name is Daniel.        |
| English (UK)     | en_GB    | Eddy                          | Hello! My name is Eddy.          |
| English (UK)     | en_GB    | Flo                           | Hello! My name is Flo.           |
| English (UK)     | en_GB    | Grandma                       | Hello! My name is Grandma.       |
| English (UK)     | en_GB    | Reed                          | Hello! My name is Reed.          |
| English (UK)     | en_GB    | Rocko                         | Hello! My name is Rocko.         |
| English (UK)     | en_GB    | Sandy                         | Hello! My name is Sandy.         |
| English (UK)     | en_GB    | Shelley                       | Hello! My name is Shelley.       |
| English (US)     | en_US    | Albert                        | Hello! My name is Albert.        |
| English (US)     | en_US    | Bad News                      | Hello! My name is Bad News.      |
| English (US)     | en_US    | Bahh                          | Hello! My name is Bahh.          |
| English (US)     | en_US    | Bells                         | Hello! My name is Bells.         |
| English (US)     | en_US    | Boing                         | Hello! My name is Boing.         |
| English (US)     | en_US    | Bubbles                       | Hello! My name is Bubbles.       |
| English (US)     | en_US    | Cellos                        | Hello! My name is Cellos.        |
| English (US)     | en_US    | Flo                           | Hello! My name is Flo.           |
| English (US)     | en_US    | Fred                          | Hello! My name is Fred.          |
| English (US)     | en_US    | Good News                     | Hello! My name is Good News.     |
| English (US)     | en_US    | Grandma                       | Hello! My name is Grandma.       |
| English (US)     | en_US    | Grandpa                       | Hello! My name is Grandpa.       |
| English (US)     | en_US    | Jester                        | Hello! My name is Jester.        |
| English (US)     | en_US    | Junior                        | Hello! My name is Junior.        |
| English (US)     | en_US    | Kathy                         | Hello! My name is Kathy.         |
| English (US)     | en_US    | Organ                         | Hello! My name is Organ.         |
| English (US)     | en_US    | Ralph                         | Hello! My name is Ralph.         |
| English (US)     | en_US    | Reed                          | Hello! My name is Reed.          |
| English (US)     | en_US    | Rocko                         | Hello! My name is Rocko.         |
| English (US)     | en_US    | Samantha                      | Hello! My name is Samantha.      |
| English (US)     | en_US    | Sandy                         | Hello! My name is Sandy.         |
| English (US)     | en_US    | Shelley                       | Hello! My name is Shelley.       |
| English (US)     | en_US    | Superstar                     | Hello! My name is Superstar.     |
| English (US)     | en_US    | Trinoids                      | Hello! My name is Trinoids.      |
| English (US)     | en_US    | Whisper                       | Hello! My name is Whisper.       |
| English (US)     | en_US    | Wobble                        | Hello! My name is Wobble.        |
| English (US)     | en_US    | Zarvox                        | Hello! My name is Zarvox.        |
| Finnish          | fi_FI    | Eddy                          | Hei! Nimeni on Eddy.             |
| Finnish          | fi_FI    | Flo                           | Hei! Nimeni on Flo.              |
| Finnish          | fi_FI    | Grandma                       | Hei! Nimeni on Grandma.          |
| Finnish          | fi_FI    | Grandpa                       | Hei! Nimeni on Grandpa.          |
| Finnish          | fi_FI    | Reed                          | Hei! Nimeni on Reed.             |
| Finnish          | fi_FI    | Rocko                         | Hei! Nimeni on Rocko.            |
| Finnish          | fi_FI    | Sandy                         | Hei! Nimeni on Sandy.            |
| Finnish          | fi_FI    | Shelley                       | Hei! Nimeni on Shelley.          |
| French (Canada)  | fr_CA    | Amélie                        | Bonjour! Je m’appelle Amélie.    |
| French (Canada)  | fr_CA    | Eddy                          | Bonjour! Je m’appelle Eddy.      |
| French (Canada)  | fr_CA    | Flo                           | Bonjour! Je m’appelle Flo.       |
| French (Canada)  | fr_CA    | Grandma                       | Bonjour! Je m’appelle Grandma.   |
| French (Canada)  | fr_CA    | Grandpa                       | Bonjour! Je m’appelle Grandpa.   |
| French (Canada)  | fr_CA    | Reed                          | Bonjour! Je m’appelle Reed.      |
| French (Canada)  | fr_CA    | Rocko                         | Bonjour! Je m’appelle Rocko.     |
| French (Canada)  | fr_CA    | Sandy                         | Bonjour! Je m’appelle Sandy.     |
| French (Canada)  | fr_CA    | Shelley                       | Bonjour! Je m’appelle Shelley.   |
| French (France)  | fr_FR    | Eddy                          | Bonjour, je m’appelle Eddy.      |
| French (France)  | fr_FR    | Flo                           | Bonjour, je m’appelle Flo.       |
| French (France)  | fr_FR    | Grandma                       | Bonjour, je m’appelle Grandma.   |
| French (France)  | fr_FR    | Grandpa                       | Bonjour, je m’appelle Grandpa.   |
| French (France)  | fr_FR    | Jacques                       | Bonjour, je m’appelle Jacques.   |
| French (France)  | fr_FR    | Reed                          | Bonjour, je m’appelle Reed.      |
| French (France)  | fr_FR    | Rocko                         | Bonjour, je m’appelle Rocko.     |
| French (France)  | fr_FR    | Sandy                         | Bonjour, je m’appelle Sandy.     |
| French (France)  | fr_FR    | Shelley                       | Bonjour, je m’appelle Shelley.   |
| French (France)  | fr_FR    | Thomas                        | Bonjour, je m’appelle Thomas.    |
| German (Germany) | de_DE    | Anna                          | Hallo! Ich heiße Anna.           |
| German (Germany) | de_DE    | Eddy                          | Hallo! Ich heiße Eddy.           |
| German (Germany) | de_DE    | Flo                           | Hallo! Ich heiße Flo.            |
| German (Germany) | de_DE    | Grandma                       | Hallo! Ich heiße Grandma.        |
| German (Germany) | de_DE    | Grandpa                       | Hallo! Ich heiße Grandpa.        |
| German (Germany) | de_DE    | Reed                          | Hallo! Ich heiße Reed.           |
| German (Germany) | de_DE    | Rocko                         | Hallo! Ich heiße Rocko.          |
| German (Germany) | de_DE    | Sandy                         | Hallo! Ich heiße Sandy.          |
| German (Germany) | de_DE    | Shelley                       | Hallo! Ich heiße Shelley.        |
| Greek            | el_GR    | Melina                        | Χαίρετε! Το όνομά μου είναι «Μελίνα». |
| Hebrew           | he_IL    | Carmit                        | שלום, שמי כרמית.                 |
| Hindi            | hi_IN    | Lekha                         | नमस्ते, मेरा नाम लेखा है।        |
| Hungarian        | hu_HU    | Tünde                         | Üdvözlöm! A nevem Tünde.         |
| Indonesian       | id_ID    | Damayanti                     | Halo! Nama saya Damayanti.       |
| Italian (Italy)  | it_IT    | Alice                         | Ciao! Mi chiamo Alice.           |
| Italian (Italy)  | it_IT    | Eddy                          | Ciao! Mi chiamo Eddy.            |
| Italian (Italy)  | it_IT    | Flo                           | Ciao! Mi chiamo Flo.             |
| Italian (Italy)  | it_IT    | Grandma                       | Ciao! Mi chiamo Grandma.         |
| Italian (Italy)  | it_IT    | Grandpa                       | Ciao! Mi chiamo Grandpa.         |
| Italian (Italy)  | it_IT    | Rocko                         | Ciao! Mi chiamo Rocko.           |
| Italian (Italy)  | it_IT    | Sandy                         | Ciao! Mi chiamo Sandy.           |
| Italian (Italy)  | it_IT    | Shelley                       | Ciao! Mi chiamo Shelley.         |
| Japanese         | ja_JP    | Kyoko                         | こんにちは! 私の名前はKyokoです。 |
| Korean           | ko_KR    | Yuna                          | 안녕하세요. 제 이름은 유나입니다. |
| Malay            | ms_MY    | Amira                         | Hi my name is Amira              |
| Norwegian        | nb_NO    | Nora                          | Hei! Jeg heter Nora.             |
| Polish           | pl_PL    | Zosia                         | Hi my name is Zosia              |
| Portuguese (Brazil) | pt_BR | Eddy                          | Olá, meu nome é Eddy.            |
| Portuguese (Brazil) | pt_BR | Flo                           | Olá, meu nome é Flo.             |
| Portuguese (Brazil) | pt_BR | Grandma                       | Olá, meu nome é Grandma.         |
| Portuguese (Brazil) | pt_BR | Grandpa                       | Olá, meu nome é Grandpa.         |
| Portuguese (Brazil) | pt_BR | Luciana                       | Olá, meu nome é Luciana.         |
| Portuguese (Brazil) | pt_BR | Reed                          | Olá, meu nome é Reed.            |
| Portuguese (Brazil) | pt_BR | Rocko                         | Olá, meu nome é Rocko.           |
| Portuguese (Brazil) | pt_BR | Sandy                         | Olá, meu nome é Sandy.           |
| Portuguese (Brazil) | pt_BR | Shelley                       | Olá, meu nome é Shelley.         |
| Portuguese (Portugal) | pt_PT | Joana                       | Olá! Chamo‑me Joana.             |
| Romanian         | ro_RO    | Ioana                         | Salut! Numele meu este Ioana.    |
| Russian          | ru_RU    | Milena                        | Здравствуйте! Меня зовут Милена. |
| Slovak           | sk_SK    | Laura                         | Ahoj, volám sa Laura.            |
| Spanish (Mexico) | es_MX    | Eddy                          | ¡Hola! Me llamo Eddy.            |
| Spanish (Mexico) | es_MX    | Flo                           | ¡Hola! Me llamo Flo.             |
| Spanish (Mexico) | es_MX    | Grandma                       | ¡Hola! Me llamo Grandma.         |
| Spanish (Mexico) | es_MX    | Grandpa                       | ¡Hola! Me llamo Grandpa.         |
| Spanish (Mexico) | es_MX    | Paulina                       | ¡Hola! Me llamo Paulina.         |
| Spanish (Mexico) | es_MX    | Reed                          | ¡Hola! Me llamo Reed.            |
| Spanish (Mexico) | es_MX    | Rocko                         | ¡Hola! Me llamo Rocko.           |
| Spanish (Mexico) | es_MX    | Sandy                         | ¡Hola! Me llamo Sandy.           |
| Spanish (Mexico) | es_MX    | Shelley                       | ¡Hola! Me llamo Shelley.         |
| Spanish (Spain)  | es_ES    | Eddy                          | ¡Hola! Me llamo Eddy.            |
| Spanish (Spain)  | es_ES    | Flo                           | ¡Hola! Me llamo Flo.             |
| Spanish (Spain)  | es_ES    | Grandma                       | ¡Hola! Me llamo Grandma.         |
| Spanish (Spain)  | es_ES    | Grandpa                       | ¡Hola! Me llamo Grandpa.         |
| Spanish (Spain)  | es_ES    | Mónica                        | ¡Hola! Me llamo Mónica.          |
| Spanish (Spain)  | es_ES    | Reed                          | ¡Hola! Me llamo Reed.            |
| Spanish (Spain)  | es_ES    | Rocko                         | ¡Hola! Me llamo Rocko.           |
| Spanish (Spain)  | es_ES    | Sandy                         | ¡Hola! Me llamo Sandy.           |
| Spanish (Spain)  | es_ES    | Shelley                       | ¡Hola! Me llamo Shelley.         |
| Swedish          | sv_SE    | Alva                          | Hej! Jag heter Alva.             |
| Thai             | th_TH    | Kanya                         | สวัสดี! ฉันชื่อกันยา             |
| Turkish          | tr_TR    | Yelda                         | Merhaba, benim adım Yelda.       |
| Ukrainian        | uk_UA    | Lesya                         | Привіт! Мене звуть Леся.         |
| Vietnamese       | vi_VN    | Linh                          | Xin chào! Tên tôi là Linh.       |

