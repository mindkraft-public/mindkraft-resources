# Swedish / Norwegian Pitch Accent and the Danish Stod

# Swedish 

***NOTE*** 

> Taken from youtube channel 'Academia Cervena' 
> 
> https://www.youtube.com/watch?v=lXp7_Sjgm34 (Understanding Swedish Pitch Accent Part One)
> and
> https://www.youtube.com/watch?v=6_60Oec-8pg (Swedish Pitch Accent Revisited)


## Dialectical Variation

### Single and Twin Peaks

When speaking about Swedish Pitch Accent, we hear the term 'peaks' used referring to the way the pitch works across two key syllables.  Note that while these syllables may be adjacent to each other, these Accent patterns may 'split' across multisyllable blocks depending on the word / phrase.  


|          | Syllable 1       | Syllable 2       |
|----------|------------------|------------------|
| Accent 1 | ˧˦˨              | ˨˨               |
| Accent 2 | ˦˦˧              | ˥˨               |


In this clip, speaking about East Central Swedish (Standard)- Stockholm / Upsalla

Accent 1 and Accent 2 Delineated as:

| Accent Type | Tone Pattern                     | Swedish Example | English Translation |
|-------------|----------------------------------|-----------------|---------------------|
| Accent 1    | ˨˧ (Low Rising), ˩ (Low)         | ánden           | duck                |
| Accent 2    | ˦˨ (High Falling), ˥˩ (High Low) | ãnden           | spirit              |

> ***Note on Using Chao Tone Pegs***
> 
> The Chao tone numbers are a system for representing tone contours in speech, developed by the linguist Yuen Ren Chao. They are often used to describe the tonal languages of East Asia, like Mandarin Chinese, where the pitch of a syllable can change the meaning of a word. The numbers range from 1 to 5, representing the relative pitch levels, with 1 being the lowest pitch and 5 the highest. Here is a table converting Chao tone numbers to IPA tone symbols:


***Chao Tone Pegs At a Glance*** 


| Chao Tone Number | IPA Tone Symbol | Voice Pitch    |
|------------------|-----------------|----------------|
| 1                | ˩               | Lowest pitch   |
| 2                | ˨               | Low pitch      |
| 3                | ˧               | Mid pitch      |
| 4                | ˦               | High pitch     |
| 5                | ˥               | Highest pitch  |


> In tonal languages, these numbers are used to mark the pitch contour of each syllable, which is critical because different contours can change the meaning of the word. For example, in Mandarin, the word "ma" can mean "mother," "hemp," "horse," or "to scold," depending on the tone.
> 
> When applied to pitch accent languages like Swedish and Norwegian, the Chao tone numbers can be used to map the pitch contours that characterise the accentual patterns of these languages. Although they are not tonal languages in the same way that Mandarin is, Swedish and Norwegian use pitch in a similar way to distinguish between meanings of words that are otherwise identical in terms of vowels and consonants, much like Mandarin does with tones.
> 
> The pitch accent in Swedish and Norwegian can be described as a pitch movement that occurs on the stressed syllable of a word and can continue onto the following syllables. The contour of this movement—whether it is rising, falling, or has a more complex movement—helps to distinguish between words that would otherwise be homophones.


## Difference Between Accents

***Accent 1*** 

˨˧ ˩

| Swedish   | English    |
|-----------|------------|
| ánden     | the duck   |
| kómmer    | comes      |
| Éngland   | England    |
| ö́ken      | desert     |
| sáken     | the thing  |
| órdet     | the word   |
| bína      | the bees   |
| dágis     | daily      |

***Accent 2*** 

Characterised by a Double Peak in Pitch. 

˦˨ ˥˩

| Swedish | English    |
|---------|------------|
| ãnden   | the spirit |
| kõmma   | come       |
| kũnnor  | can        |
| põjke   | boy        |
| kãtter  | cats       |
| vĩsste  | knew       |
| slũtat  | finished   |
| rõlig   | funny      |


### Pairs for Practice

| Meaning (Accent 1) | Accent 1 (Word) | Accent 2 (Word) | Meaning (Accent 2) |
|--------------------|-----------------|-----------------|--------------------|
| the duck           | ánden           | ãnden           | the spirit         |
| the cage           | búren           | bũren           | carried            |
| Poland             | Pólen           | pãlen           | the pole           |
| the plot           | tómten          | tõmten          | the gnome          |
| marries            | gífter          | gĩfter          | poisons            |
| the sounds         | ljúden          | júden           | the Jew            |


## Rules for What Accent Goes with What Word

### Accent 1

***Monosyllabic words:***

| Swedish       | English         |
|---------------|-----------------|
| húnd → húnden | dog → the dog   |
| kátt → kátten | cat → the cat   |
| fót → fótten  | foot → the foot |


***Adjectives ending in 'isk':***

| Swedish    | English   |
|------------|-----------|
| mágisk     | magic     |
| grékisk    | greek     |
| hátisk     | hateful   |
| fantástisk | fantastic |

***Two syllable words with unique syllables (not traditional Swedish word)***

| Swedish | English |
|---------|---------|
| húmmus  | hummus  |
| pólio   | polio   |
| pájas   | fool    |
| gódis   | candy   |

***Words without stress on first syllable***

| Swedish    | English   |
|------------|-----------|
| télefon    | telephone |
| bétala     | pay       |
| sérvera    | serve     |
| egéntligen | actually  |

*Exception*

**Nouns with penultimate stress**

| Swedish | English       |
|---------|---------------|
| väñinna | female friend |
| kõpia   | copy          |

### Accent 2

***Two syllable words with recognisable endings***

| Swedish | English |
|---------|---------|
| põjke   | boy     |
| hõppa   | jump    |
| kvĩnnor | women   |
| rõlig   | funny   |
| fẽmton  | fifteen |
| flũgit  | flown   |

***Compound Words***

| Swedish  | English    |
|----------|------------|
| lãstbil  | truck      |
| sjũkhus  | hospital   |
| ö̃rngott  | pillowcase |
| brãndman | fireman    |


### Problematic Words: -el, -en, -er


***Nouns*** 

***Some nouns can have Either Accent***


| Accent 1        | Accent 2         |
|-----------------|------------------|
| cýkel (bike)    | spĩndel (spider) |
| fǻgel (bird)    | nyc̃kel (key)     |
| vínter (winter) | sỹster (sister)  |

***Adjectives are Rulebound*** 

> - Adjectives in -el and -er, en take on Accent 1
> - Adjectives in -en take on Accent 2

| Accent 1           | Accent 2     |
|--------------------|--------------|
| énkel (easy)       | ẽgen (own)   |
| símpel (simple)    | lẽdsen (sad) |
| vácker (beautiful) | ṏppen (open) |


***Verbs***

> - Present Tense Verbs in -er Always have Accent 1
> - Present Tense Verbs in -ar can have Accent 2

| Accent 1        | Accent 2       |
|-----------------|----------------|
| sprínger (runs) | hõppar (jumps) |
| kómmer (comes)  | kãllar (calls) |
| léker (plays)   |                |
| býter (changes) |                |


***Plurals with Vowel Change***

> Plurals where the core vowel changes - e.g. 'and' to 'änd+er' will take on **Accent 1** where unchanged (most nouns) take **Accent 2**.

| Accent 1         | Accent 2       |
|------------------|----------------|
| ä́nder (ducks)    | kãtter (cats)  |
| bö́nder (farmers) | fĩlmer (films) |
| stä́nger (rods)   |                |
| fö́tter (feet)    |                |


## Accents Change within a Single Paradigm

So with all of this, we can see that accents will shift within a single paradigm:

***Example***

| Swedish | English Equivalent  | Reason                                      |
|---------|---------------------|---------------------------------------------|
| lä̃sa    | to read, infinitive | More than 1 syllable - recognisable ending. |
| lä́ser   | reads, present      | Present tense verb ending on standard 'er'. |
| lä̃ste   | read, past          | More than 1 syllable - recognisable ending. |
| lä́st    | read, supine        | One syllable word.                          |


| Swedish Sentence       | English Equivalent                  |
|------------------------|-------------------------------------|
| Jag vill lä́se en bok.  | I want to read a book. (infinitive) |
| Han lä́ser en bok.      | He reads a book. (present)          |
| Han lä̃ste en bok igår. | He read a book yesterday. (past)    |
| Han har lä́st boken.    | He has read the book. (supine)      |


### Supine Tense (Past Participle) Vs. Past Tense
The difference between "past" and "supine" in Swedish (and in general linguistic terms) pertains to their use in expressing different aspects of verbal action, particularly regarding time and aspect of completion.

> - **Past tense** indicates that an action occurred at a specific time in the past. It's a straightforward expression of past activity without necessarily implying completion from the perspective of the present.

Vs. 

> - **Supine** is a bit more complex and is specific to certain languages, including Swedish. It is used primarily to form the perfect and pluperfect tenses in combination with an auxiliary verb, usually "har" (have) or "hade" (had), indicating that an action has been completed relative to the present or another point in time.

### Examples in Swedish and English:

**Past:**
 
> - Swedish: Han lä̃ste boken. (He read the book.)
>   - This simple past tense suggests that he read the book at some point in the past.
> 
> - English: "I walked to the store."
>   - The action happened in the past, with no implication about its completion regarding the present.


 **Supine:**
 
> - Swedish: Han har läst boken. (He has read the book.)
>   - The use of "har" (has) with the supine form "läst" (read) indicates that the action of reading the book is complete as of now.
> 
> - English: "I have walked to the store."
>   - Similar to the Swedish example, this uses the present perfect tense to show that the action of walking to the store has been completed at some point before now, with relevance to the present.

The supine in Swedish is crucial for forming compound tenses that reflect completed actions, much like the past participle in English. However, the term "supine" is not typically used in English grammar; instead, we refer to this form as the past participle when discussing perfect tenses.

***More Examples Where Accent Changes on Same Word Root***

Using the root 'kom' - to come. 

| Swedish | English Translation     | Reason                                                                             |
|---------|-------------------------|------------------------------------------------------------------------------------|
| kõmma   | to come (infinitive)    | More than 1 syllable, recognisable ending.                                         |
| kómmer  | comes (present)         | Standard '-er' verb ending.                                                        |
| kóm     | came (past)             | One syllable word.                                                                 |
| kõmmit  | has/have come (supine)  | More than 1 syllable, recognisable ending.                                         |

Using the root word 'kat' - 'Cat'. 

| Swedish  | English Translation | Explanation                                         |
|----------|---------------------|-----------------------------------------------------|
| én katt  | a cat               | Monosyllabic word.                                  |
| kátten   | the cat             | Monosyllabic word using definite article.           |
| kãtter   | cats                | Standard plural ending in '-er'.                    |
| kãtterna | the cats            | Standard plural definite article ending in '-erna'. |


## In Real Life - Accent Not Always Pronounced

In general, the pitch accent will only apply when the word is stressed in a sentence. 

It is all up to the prosody of the language

> Jag kommer hém.
> 
> /jakomme *HEM*/

or 

> Jag äter kỹckling
> 
> /jaäter *KYCKLING*/

Note that 'prosody' and 'stress' may vary - so you may emphasise a different word to shift the meaning. 

# Swedish Pitch Accent Revisited: Dialectal Variation

We will look at key varieties of Swedish:
1. East Central Swedish
2. Northern Swedish
3. Dala Swedish
4. Finland Swedish
5. Gota Swedish
6. Gotland Swedish
7. Southern Swedish


East Central Swedish (Standard)
Accent 1 anden (duck) ˧˦˨ 
Accent 2 anden (evil spirit) ˦˥˥˧ ˥˩

## Pitch Accent Types

These are generalisations of specific traits and each group has a lot of variation. 



| Accent Type | Accent I | Accent II | Regional Variety of Swedish |
|-------------|----------|-----------|-----------------------------|
| 0           | ˦˧       | ˦˧        | Finland                     |
| 1A          | ˥˧       | ˧˥˧       | South                       |
| 1B          | ˧˥˨      | ˧˧˥˨      | Gotland/Dala-Bergslagen     |
| 2A          | ˧˧˥˨     | ˧˥˧       | North/Middle East (Svea)    |
| 2B          | ˧˨˥      | ˥˨˥       | Middle West (Göta)          |



### Type 0

This is lacking pitch accent all together:
- Finland
- Estonia
- Other non Swedish places that speak Swedish

Nowadays this group are starting to take on normal modern Swedish prosody. 

West Finland now more like Sweden

## Type 1 vs. Type 2

| Language Group Type | Accent 1 Pattern          | Accent 2 Pattern         |
|---------------------|---------------------------|--------------------------|
| Type 1              | Single Peak: ˧˦˨          | Double Peak: ˦˥˥˧, ˥˩    |
| Type 2              | Single Peak (earlier): ˦˨ | Single Peak (later): ˧˦˨ |

## Type 1 Comparison

| Accent Type | Accent 1 Pattern | Accent 2 Pattern |
|-------------|------------------|------------------|
| 1a          | 523 (˧˦˨)       | 3423 (˨˧˦˨)     |
| 1b          | 342 (˨˧˦)       | 3342 (˧˧˨˧)     |



- The peaks in type 1a come earlier than in 1b, and that is the sole distinguishing factor. 
- In type 1a, the peak on Accent 1 is immediate, where Accent 2 has a more gradujjal rise to it. 
- Rises in both accents, peaking on stressed vowel in Accent 1 and just after in in accent 2. 
- Pitch keeps falling after reaching the peak, unlike 1a that eventually levels out. 

## Type 2 Comparison

| Accent Type | Accent 1 Pattern | Accent 2 Pattern |
|-------------|------------------|------------------|
| 2a          | 342 (˧˦˨)       | 5342 (˥˧˦˨)     |
| 2b          | 325 (˧˨˥)       | 525 (˥˩˥)       |

This type has a single pitch in 1 and 2 distinguished by timing. 

# Modern Shifts

In old Stockholm Mälar type: 
- Monosyllabic take Accent 1
- Polysyllabic take Accent 2 

This was a standard feature, but during 20th century disappeared - only a few have it now. 

### Compound Words

Compounds generally take accent 2 - However

***Type 2a***

Compound Stress is key - accent 2 syllable following stressed
Compounds - 2nd peak syllable carrying secondary stress, in 2nd part of compound

Type 2b 
Always puts 2nd peak at end. 

In many south Swedish dialects, compounds can shift. 

For example in Scania:

| Accent 1                  | Accent 2                     |
|---------------------------|------------------------------|
| vítlök (garlic)           | mãtsal (dining hall)         |
| ljusblǻ (light blue)      | sõmmarlov (summer vacation)  |
| cýkelnyckel (bicycle key) | fĩngervante (fabric glove) |

### Foreign Words May Take Either

- ananas
- blåbär
- morfar
- persika
- sallad
- Arvid
- Axel
- Elin 
- Martin 
- Oskar
- Simon 


