A. S. zyxwLiberman zyxw
This paper is concerned with four Scandinavian prosodemes: acc. z1,  

SCANDINAVIAN  
ACCENTS IN THEIR  

RELATION TO ONE ANOTHER  
ace. 2, sterd, and no st~d. Its aim is to establish the function of each of  them. The much-discussed questions of whether the Swedish and  Norwegian accents are dynamic or musical in nature and whether it is  possible to detect tonal ingredients in the realizations of the Danish  stprd and no stprd are of secondary importance. What interests us here is  the role all those entities play in the system, not the details of their  manifestation.  
1.  
As was clear to the very first investigators of Swedish and Norwegian  stress, acc. 1 occurs mainly in the words which had one syllable in Old  Icelandic (01). Hence the tradition to call acc. 1 monosyllabic and ace.  2 polysyllabic. This tradition holds, on the whole, for the modern  languages too: monosyllabic words have acc. 1, and in polysyllabic  words ace. 2 is felt as more natural. C.4. Elert (1964: 31-33, and  several times later) noticed that the main function of acc. 2 is purely  connective. If we compare the Swedish utterances vdr ‘sven slea ‘flagga  (our page will run up the flag’ and vdr ’svenska ’flagga (our Swedish  flag’, we shall see at once that in the syntagm svenska (from the second  example) the syllables sven and ska are understood as parts of one word  
may be one word and a group of two words (cf. Hd11 zyen minut utanfor  
because they constitute the domain of acc. 2. When we hear a string of  syllables with acc. 2, we know that there is no word boundary within  it; thus, acc. 2 performs an anti-delimitative (connective) function. On  the other hand, the tone of ace. 1 syllables carries no information about  their structure. Acc. 1, as well as ace. 2, is devoid of any delimitative  power. The syllable hdll, for example, need not be a final syllable in a  word (cf. hill ‘distance’ and hdllen pp. of hdlla ‘keep’); similarly hdllen  
porten ‘stay a minute outside the gate’). But it has no task corresponding 

zy
cf. z
‘kommer-med dampbdten - til zHamburg ‘He will catch a steam-boat for  

SCANDINAVIAN  
ACCENTS  
IN  
THEIR  
RELATION  
TO  
ONE ANOTHER  

the connective  
function  
of acc.  
To sum up:  2.  
acc.  
2 signals  
the absence  

of a word boundary within its domain, while ace. 1 takes no part in  bdten - zyxwvuti1 Hamburg ‘he is going to Hamburg by steam-boat’ and Hann  

word  
delimitation.  
Ace.  
2 can connect  
parts  
of  
a  
compound word:  

en  
stormans  
drakt  
[En  
‘stu:rman:s  
‘drokt]  
‘an  
outfit  
for  
a magnate’,  

en stor  
mansdrakt  
[En  
‘stu:r ‘man:sdrekt]  
‘a  
big  
man’s  
outfit’,  
en stor 

munsdrakt [En ‘stu:rman:sdr~kt] ‘a magnate’s outfit’.  
The connective role of acc. 2 is also quite manifest within a word and ’ta pd zyxwvunoe ‘touch something’ will be rendered in Standard Swedish  
group. A syntagm with acc. 2 is more unified and idiomatic than the  same syntagm with ace. 1: cf. the Norwegian Hann ‘kommer-med damp 
Hamburg’ (with emphasis laid on the verb). Ace. 2 turns kommer med  into a compound verb, and acc. 1 preserves med as preposition (Broch  1937: 315-316). The Norwegian phrases ’tap& noe ‘put on something’  
as ’ta pd ndgot and ta ’pd ndgot. But in the Uppland dialects of Sweden  the accents are used in the sentence according to the East-Norwegian  rules (Witting 1968: 92).  
Since acc. 2 is called upon to connect the components of different  units and in a word such components are syllables, the question arises,  whether the Swedish-Norwegian accents 1 and 2 are syllable- or word accents. B. Malmberg, J. Kurylowicz, and s. Ohman (to mention just  a few names) analyze them as word accents. N. S. Trubetzkoy, E.  Haugen, M. Kloster-Jensen, S. D. Kacnel’son, and many others treat  them as syllable accents. But it is hardly possible to defend the second  interpretation other than by phonetic or formal arguments (it is said,  for example, that the initial syllable of the Swedish ‘unden ‘the spirit’  is sufficient for the recognition of the accent or that assigning accents  to the syllable permits setting up a more elegant system, and so on).  But the functional approach to the matter shows that the opposite  solution is true: at least acc. 2 belongs most definitely to the whole  word.  
It has been recently said by several investigators that the phono logical locus of the Swedish and Norwegian accents is the second  syllable. This view is really quite old. K. J. Lyngby saw the only  difference between the Swedish words ‘axel ‘shoulder’ and ‘axel ‘axis’  in their second syllables (mentioned by A. Kock 1878: 43). R. Iversen  wrote (1913: 24) that the monosyllabic accent in the northern Norwegian  dialects falls when the beginning of the second syllable is reached,  whereas the disyllabic accent falls in the second syllable itself, so that  
25 

A. zyxwv
zyxw
since in the Norwegian forms like ’tanken zyx(tank-en) ‘the tank‘ zN ‘tanken  S. LIBERMAN  

all distinction between them  
is  
concentrated  
in the second syllable.1  

But modern phonologists came to Lyngby’s and Iversen’s conclusions  anew. Of special interest are J. Rischel’s works. Rischel argues that  
(tanke-n) ‘the thought’ the tonal contour is distributed over both  syllables, there must be a tonal distinction dependent on the second  syllable. Cf. also huuset [’h~:sa] ‘the house’ N hwe phtt:sa] ‘houses’  (pl.). According to Rischel, the carrier of different accents is added to  the tonally neutral stem (1960: 178, 179, 183; 1963: 155-157). This  treatment was very favourably commented upon by J. Jasanoff (1966)  and P. Garde (1968: 166).  
I think we must concur with Rischel’s analysis. It is true that acc. 2  extends over the whole word and the second syllable is all-important  for its existence (it will be remembered that the material under in vestigation in most articles devoted to our subject is restricted entirely  to mono- and disyllabic words). That acc. 1 is a word accent as well is  much less obvious, but acc. 1 does not determine the content of the  accentual contrast, for it is the unmarked member of the opposition.  The markedness of acc. 2 follows from its role: it has a certain function  which acc. 1 does not possess. All the other considerations (treatment  of borrowings, comparison with non-Scandinavian languages, fre 
quency of occurrence, and so on) are beyond the point: they demon strate the markedness of acc. 2 but do not explain it, for they are its  consequences, not the cause. The tonal contour of acc. 2 is ideally suited  to its function, especially in Standard Swedish and South-West Nor wegian, where the second top (the levis of Kock’s classification) is very  strong.  
Let us now turn to the accent of monosyllabic words. Theoretically,  it is possible to consider three treatments of them: they may have  either acc. 1 or an archiaccent (i.e. neither acc. 1, nor acc. 2, but the  product of their neutralization), or a special accent, peculiar only to  tonelaget i nordlandsk zyxwv. . . stikker i behandlingen av tonen i anden stavelse.  
them. The second treatment goes back to C. Borgstrram’s famous  paper (1938) and is rather popular among phonologists. But it is most  probably wrong. Borgstrram pointed out that in monosyllabic words  there is no accentual contrast, and so the opposition acc. 1 ~acc. 2 is  neutralized. But neutralization must be a point of intersection of two  
“Den vEsentligste musikalske forskjel mellem enstavelses- og tostavelses 
Kortest og klarest kan denne forskjel uttrykkes saaledes: enstavelses-tonelaget  synker ti1 anden stavelse, tostavelses-tonelaget pas anden stavelse”.  
26 

zy
mally kept apart zyxwv(Bad and Bat). For the same reason there is no  

SCANDINAVIAN  
ACCENTS  
IN THEIR  
RELATION  
TO  
ONE ANOTHER  

meaningful  
units. The  
German  
form [ra:T]  
is the  
result  
of neutraliza 

words zyxwvlys ‘light’ (9.) or mann ‘man’ contain no ambiguity comparable  
tion not because a German word “cannot end in a voiced plosive” but  because in it are submerged the meanings of two words, which are nor 
Swedish and Norwegian monosyllabic words (as there is no zIs/ N z/z/  
neutralization at the beginning of the English word speak. An archiunit  can always be dissolved into the forms which have been neutralized, and  if the restoration of the neutralized members is impossible (as in speak),  we can be sure that there has been no neutralization. The Norwegian  
with that in the German [ra:T]. There are no accentual contrasts in  
opposition before /p/ or /p/ N /b/ opposition after /s/ in English), but  nothing is neutralized in them.  
The first treatment is traditional and is primarily based on the similar  auditory impression made by the words like hdll and the initial syllables  of such words as hdllen. But A. Vanvik, the only proponent of the third  solution (Vanvik 1961, and many times later), emphasizes the differ 
ences in the tonal curves of monosyllables and stressed syllables of acc.  1 words. His facts are drawn mainly from the Trondheim dialect. He  discovered that land er ‘land is’, landet ‘the land’, and lande ‘land’ (v.)  had different curves and set up three tonemes in Norwegian. Comment 
ing on Vanvik’s theory, Borgstrerm (1962) put forward two objections  against it. He said that land er and lande did not form a minimal pair  (a word-group vs. a word), so that the even tone on them was a bound ary rather than a toneme. Besides, he observed that Vanvik had  extended the Trondheim norms to the whole of Norwegian. Borgstram’s  first objection is irrefutable. The second remark may also be true. In  K. Fintoft’s experiments the listeners usually identified the contour of  monosyllabic words as that of ace. 1 (if the speaker and the listeners  represented the same dialect); the only exception was Trondheim  (Fintoft 1970: 318). Besides, it can hardly be due to chance that the  necessity to set up a third toneme in Norwegian has been advocated  twice: each time by a native of Trerndelag (Vanvik’s predecessor was  E. Mo).  
But Vanvik’s analysis is also quite indefensible on structural grounds.  In phonology we meet with three types of positions: in the first the  elements are opposed (an obvious case); in the second they are neu tralized (the case of [ra:T]); in the third they are not opposed but not  neutralized (the case of speak). Contrary to the widely accepted view,  a sound unit can keep its individuality in a certain context, even if it  
27 
is not opposed there to some  
of  
its  
but a maximum num 

counterparts;  
able that there should be the phonemes /s/ zyxand 1.1 in English and a third  
ber of units will be of course found in the contexts permitting all the  phoneme (neither Is/, zyxwnor /z/) in speak. A special toneme in Norwegian  
possible oppositions, and it is only there that primary identification  can take place. Though a sound unit can retain all its distinctive fea tures in a position of minimal contrast, the existence of the features  presupposes that somewhere the units are allowed to clash. The con text of the speak type will never yield a new phoneme, because there is  no chance for a new set of features to reveal itself there. It is unthink 
monosyllables is equally improbable.  
And a last remark on toneme 3. If we assert that there is no neutrali zation in speak before /p/ we are expected to identify the initial  phoneme as one of the general set. This is no place to discuss the prob lem in its entirety. Suffice it to say that the phoneme is identified by its  
Monosyllables have only ace. 1, other words may have both. Ace. z2  
distinctive features. But accents form a simpler system than phonemes  and lend themselves to analysis much more easily. Thus, if we know that  there is no ace. 3 in Norwegian and Swedish monosyllables and, since  ace. 2 is excluded from them by definition, we are left with no other  choice but to identify their accent as ace. 1.  
We may now summarize. There are two special prosodemes in  Swedish and Norwegian, which must be qualified as word accents.  
is the marked member of the opposition. It signals the absence of word  boundaries within its domain. The second syllable of a disyllabic word  is the real locus of the accentual distinction. Consequently, the accents  are inseparably bound with the number of syllables.  
mic length. He transcribes sol zyxw[so’l] ‘sun’ as /sool/, andsole [so:la] ‘expose  
2.  
The function of st0d and no st0d in Standard Danish is more difficult  to ascertain than that of the accents, for their analysis entails two  factors: the basis and the number of syllables. We shall begin with the  basis. P. Andersen suggests (1958: 85-86) that stcad indicates biphone 
to the sun’ as /soola/. Analogous transcriptions may be found in many  modern Danish dialectological works. But long before Andersen a very  similar treatment of the Danish material was advanced by Trubetzkoy  (1939: 173,195), according to whom the function of st0d is to divide the  syllabic nucleus into two parts and thus turn the vowel into a geminate,  
28 

zy
tion of his view in the fact that the opposition stad zy- no stad could be  

SCANDINAVIAN  
ACCENTS  
IN THEIR RELATION  
TO  
ONE  
ANOTHER  

i.e. a combination  
of  
two moras.  
Trubetzkoy saw  
the  
main  
confirma 

detected both in the long vowels and diphthongs and also in the groups  of a vowel + a resonant. The truth of this statement seems to me  incontestable. Stsd would not have been a tool of mora-counting, if it  simply cut a vowel into two halves (and phonetically they are of course  
(in hus z
far from being halves). If some vowels were pronounced with a closure  in the middle, this would mean nothing at dl from a phonological  point of view. We do indeed from time to time come across such doubt ful stsds, as in some Sormland dialects described by 0. Gjerdman (1918:  183)) but one hesitates to recognize them as such. In any case, they  belong to usage, not to the system.  
Stsd makes the equation: a long vowel = a vowel + a sonorous  

consonant-an obvious  
fact,  
for example  
the  
equation  
of  
[u:]  

[hu’s] ‘house’) to [un] (in  hund  
[hun’] ‘dog’).  But  
it must  
be  
borne  
in  

mind that a consonantal stad belongs to the consonant not in the same  way as a vocalic stad belongs to its vowel. In hus Is/ remains outside  the syllabic nucleus, and the stsd would have been possible without it  as well: cp. by ‘town’ (with strad). But hund has strad on the group /un/,  not just on In/, for In/ alone cannot form a syllabic after a vowel. Even  from a phonetic point of view it is located between /u/ and In/ (cf.  Lauritsen 1968: 7). So I think, Andersen is wrong in analysing hus as  /huus/ and hund as /hunn/.  
If we admit that the vowels with stad are bimoric, we lose the right  to say that some word has no stsd because its vowel is short. Such  statements pretend to explain the absence of stad, but they are quite  tautological, for if there is no stsd in a word, it means that the stressed  vowel contains only one mora, i.e. the vowel is short by definition.  
Disyllables present almost no additional difficulties. In those with  a second open syllable stsd is rare, and only two types prevail: 1)  [baxra] bare ‘bear’ (v.), [sdu:a] stue ‘room’, [hu:sa] huse ‘houses’ (s.,  pl.) and 2) [lsga] Isgge ‘lay’ (inf.), [tzela] tdle ‘count’, [tyba] type  
house’ type. zyx
‘type’. Since [lagal, [tda], and the like are syllabified as the English  [bega] beggar, [tela] teller (cf. Jespersen 1934: 123-124)) they must  be juxtaposed within the correlation of syllable-cut (Xilbenschnitt).  But in Danish the Silbenschnitt correlation is more complicated than  in English, for besides the usual opposition of checked and free vowels,  the [hu’sat] zyxwvutsrqpLinauislica  
there is an opposition within the free vowels, which can be smooth or  divided. The latter covers the occurrence of stsd in the disyllables of  huset ‘the  

3-Studia  
XXIX . 1975  
29 

zyxw
A. zyxwS. LIBERMAN  
is supported by numerous pairs like zyxhus zyxN huse (pl.), god zy‘good’ N  gode (pl.) and such quasi-homonyms as skallen ‘the shell’ (= zyskall + en)  

Though stsd is possible both in  
monosyllabic  
and disyllabic words,  

it also depends on the number of syllables. In Danish this dependence  
and skallen ‘the skull’ (= skalle + n). In his book A. Hansen (1943)  strives to prove that the main function of strad is to signal monosyllabi  city. But in Danish the number-of-syllables rule is destroyed from  two sides: there are disyllabic words with stsd (including the definite  forms of monosyllabic nouns) and there are monosyllabic words with  no stsd: ven ‘friend’, s0n ‘son’, f01 ‘foal’, nut ‘night’, kop ‘cup’, flask  ‘grease’, etc. In ven and the like Hansen sets up a latent stsd, which  comes to light in the definite forms. But it is hardly a happy construc 
tion, and a latent stard remains a fictitious entity. Besides, in natten,  koppen, flcesket there is no stsd at all. Unless we are trying to prove  some preconceived idea, we are bound to admit that strad serves the  number-of-syllables rule rather imperfectly, only insofar as its principal  task of mora-counting allows it to do so. That is why in the opposition  sts d N no strad, stsd alone is the carrier of the main function and the  marked member, while no strad is its unmarked shadow.  
According to a tradition of very long standing, the opposition acc.  1 N acc. 2 is viewed as an analogue of the opposition strad N no stsd.  But as a matter of fact, there are more points of difference between the  Swedish-Norwegian and Danish prosodemes than there are points of  
resemblance. The accents belong to the word, whereas stad and no  st0d belong to the syllable. The marked accent (i.e. acc. 2) is not ad mitted to monosyllabic words, while stsd avoids disyllables. The  accents serve almost exclusively the number-of-syllables rule. Stsd, on  the contrary, is primarily needed as an instrument of mora-counting,  and the necessity to indicate the number of syllables is subsidiary to it.  There is a phonetic similarity between strad and acc. 1, but no stard lacks  
the connective function and in no way resembles acc. 2.  But driven by the force of the old tradition, phonologists always try  to prove that in Swedish and Norwegian everything happens as in  Danish. Consider Hansen’s attempt to reduce the function of strad to  the indication of monosyllabicity. Consider also Trubetzkoy’s analysis  of Norwegian (1939: 197). Having discovered the mora-counting  character of Danish, Trubetzkoy began to look for similar evidence in  Norwegian. When he failed to detect any strad-like phenomena there,  he resorted to an insignificant phonetic detail in the realization of the  Norwegian tones and came to most unconvincing results. Trubetzkoy  
30 

z
the locus of the tone was either a long vowel or a short vowel z+ any  

SCANDINAVIAN  
ACCENTS  
IN THEIR  
RELATION  
TO ONE ANOTHER  

cites  
Borgstrram’s assertion  
that  
when  
a short vowel  
is followed by  
a  

voiceless consonant, the contour of the tone is only indicated, though  the juxtaposition is preserved; when the postvocalic consonant is  voiced, the consonant is sustained into it. Trubetzkoy concluded that  
consonant. He took the tone for a strad, because he thought he had  found an equation of a long vowel to the group VC. He even decided  that the Norwegian tone was a strad to a greater measure than the  Danish strad, because it made no distinction between the sonorous and  non-sonorous bases. But the Norwegian tone (= ace. 2) does not only  extend to the implosion of the postvocalic consonant: it needs two  syllables for its manifestation. Trubetzkoy probably did not realize  that; hence his idea to identify ace. 2 and stud.  
The Scandinavian data allow us to give the following phonological  definition of the accents and strad. The accents are units of word  prosody, their function is to indicate polysyllabicity as opposed to  monosyllabicity. Strad is a syllabic prosodeme which performs mora 
counting. In Swedish and Norwegian the accents carry out their func tion not quite consistently, because ace. 1 is allowed to occur in poly syllabic words. Neither does stsd fulfil its task to the end, for it is not  admitted to non-sonorous bases. Besides, stud and no strad have an  additional role: they indicate the number of syllables; consequently,  stud is not only what it is always taken for but also an accent. There is  a great variety of prosodemes in the Scandinavian dialects, but all of  them emerge either as pure accents, or accents burdened with strad  functions, or strad and no strad partly serving as accents.  
The main cases when accents are at the same time strad and no strad  will be met with in the dialects with the jamviktsaccent. But they need  special treatment, so we shall confine ourselves to the facts which lie  outside the problems of short-syllable words.  
In the Norwegian dialect of Flekkefjord (Vest-Agder, Srarland) there  are two accents distributed according to the usual rule. But in numer ous monosyllabic words we find ace. 2 instead of ace. 1, because ace. 1  does not occur before a long voiceless consonant or a group of voiceless  consonants: [‘guttl-[‘guttn] ‘boy’, rhattl-rhattrr] ‘hat’, [‘raettl-[‘raettn]  ‘right, law’ (the nouns are given in two forms: indefinite and definite),  [’drikkal-[’drikka] ‘drink-drinks’, [‘hest]-[‘hestn] ‘horse’, [‘vae~kl- 

[‘vaerka] ‘work’(s), and so zyxwon (Larsen  zyxw
A. zyxwS. LIBERMAN  
‘light (not heavy)’, though the variants [’my:gt] zyN [‘mykt] ‘soft’ and  

1970: 9-12).  
Diphthongs behave  

as long vowels, i.e. in monosyllabic words they have ace. 1: [’haust] and  [’haustn] host-hosten ‘autumn’. The forms [‘haust], [‘haustn] are rare,  and in all of them the diphthong is shortened. But neuter adjectives  prefer ace. 2 in spite of the diphthong: cf. [‘braeitt] ‘broad’, [‘leitt]  strad, as in the Danish zyxwflcesk, kop. The main difference between the  
the like can sometimes be heard.  
Even the few examples given above reveal a striking similarity  usual Norwegian correlation of syllable length (VC zy- Vc), which  
between the Flekkefjord ace. 1 and strad. Ace. 1 marks off sonorous  bases, and when an accent ought to be chosen for a monosyllabic  word, the structure of the basis is more important than monosyllabicity,  and we have ‘gutt, ‘hutt. Moreover, an ace. 2 which is allowed to occur  in a monosyllabic word, is, by definition, not an ace. 2 at all, but no  
Flekkefjord and the Danish systems is that in Flekkefjord there is a  
prevents the Danish variety of mora-counting. The Flekkefjord strad  can barely force its way through this correlation: cf .vowel-shortening  in huzcst with ace. 2. Though ace. 1 is phonetically an abrupt accent,  and ace. 2 is smooth, the Flekkefjord ace. 1 on a long vowel is present  
to indicate the moras, and it is quite natural that its basis remains  long. Flekkefjord has the prosodic system which Trubetzkoy ascribed  to all Norwegian. In this dialect even the details betray a strad camou flaged as an accent. Thus, in Standard Danish, just as in Flekkefjord,  neuter adjectives have no strad: cf. [ly’s] ‘light’ and [lyst].  
E. G. Larsen emphasizes the unique character of the accentual  distribution in Flekkefjord, but it is unique from the Norwegian, not  from the Scandinavian point of view. Norwegian dialectologists have  long ago noticed the so-called Srarland strad and registered it on a rather  large territory (as far as Lister), but they recognized it not by its func tion but by its realization: for them an accent is a unit which can be  described as tone, and a strad is a break in the process of phonation.  But as pointed out above, we must be ready to meet with prosodemes  serving the number-of-syllables rule (i.e. with an ace. 2 having a pecu liar second top) and at the same time suited to mora-counting (i.e.  
with an ace. 1 occurring only on long-vocalic or sonorous bases). In  the borderline dialects of this type realizations are very unsafe indica tors of the prosodemes. It is symptomatic that A. B. Larsen heard a  weak strad in Mandal and Lister (more often in women’s speech, see  Johnsen 1942: 31), and J. Storm found a strad-like accent in Kristian- 

SCANDINAVIAN  
ACCENTS  
IN THEIR  
RELATION TO  
ONE ANOTHER  
zy

sand (Storm  
The authors  
of subsequent  

1908: 49).  
detailed  
descriptions  

did not confirm Larsen’s and Storm’s observat,ions (cf. Seip 1915: 22-  23, Johnsen 1942: 31; no mention of stad in: Swenning 1917-37),  but the testimony of two such dialectologists can hardly be explained  away as mere errors.  
Outside Denmark ace. 1 and ace. 2 are at the same time stad and no  stod only in West Sorland. But a hint at a similar distribution of  functions can be found in some of the vernaculars of Dalarna (Sweden).  In one of the parishes of the Dala-Bergslag dialect, where the num ber-of-syllables rule has its ordinary form, words with the stem ending  in bb, dd, gg, and palatalized gg have acc. 1 independently of the num ber of syllables (Levander 1925: 54, Enwall 1930: 62). Similarly, when  old short-syllable words, which originally had no accent at all, under went the lengthening of the intervocalic consonants, they acquired acc.  2 together with the other disyllables, but in some places (Grytnas,  Hedemora) they have ace. 1 before dd, gg (Enwall 1930: 65). This  anomaly is of almost no importance for the phonological interpretation  of the accents in the dialect, but it reveals the ability of the Dala Bergslag ace. 1 to behave like a stpld, i.e. to be subject not to the  monosyllabicity of the form but to the structure of the phonematic  basis.  
The situation, rare in Sweden and Norway, is a very common  occurrence in Denmark. A classic example of a half-accent, half-stad  has been registered in Marstal (Bra) (Kroman 1947: 68-107). In  Marstal there are two accents: ace. 1 with a rising-falling contour and  acc. 2, usually falling-rising. Dynamically they are also somewhat  different. In its most pronounced form (e.g. in the compounds like  [‘gamaldaws] gammeldags ‘old, old-fashioned’) ace. 2 has such a strong  additional top that stress seems to fall on the second component  (Andersen 1958: 48, 1965: 101). Monosyllables always have acc. 1, but  
vowel zyxwv+ /r/ + a consonant ([.jarma] hjerne ‘brain’, [’l~erga] l~erke  
in disyllables either of the two accents is possible. But if in Standard  Swedish and Norwegian ace. 1 in simple disyllables occurs almost  
z
exclusively before -el, -er, -en, in Marstal there are numerous acc.  1 words ending in -e. Kroman discovered that acc. 1 usually marks  long-vocalic and ace. 2 short-vocalic stems: cp. [’bla.ma] blomme  ‘plum’, [’byanal bmne ‘bean’ [\asgal aske ‘ashes’, [‘baga] bakke ‘hill’,  and so on. Besides, ace. 1 is found in words, whose stems end in a short  
‘lark’) and words with 1 < Id, n < nd, and the reflexes of ng. Words  with 1 < 11, lp and n < np have ace. 2. But the connection of the  
3 3  

A. zyxwS.  
LIBERMAN  
zyxw

accents with different 111’s and  
/n/’s is  
important  
only for reconstruc 

tion. The modern dialect simply has [’h&.la] holde ‘hold’ (01 halda)  strad. The members of the opposition [‘h&.la] N zy[‘hae.la] are related  
and [‘hae.la] hdde ‘pour’ (01 hella), [‘ble.~] blande ‘mix’ (01 blanda)  and re.^] rinde ‘run’ (01 renna). In disyllables ending in a resonant  
‘skins’ (pl. of zyxwvhud) or [sgri’vaR] skriver ‘(he) writes’ N [sgri:vaR]  
both accents may occur.  
The Marstal accents are to a greater extent strad and no strad than  the Smland acc. 1 and acc. 2. In disyllables acc. 1 is confined to long  vowels and the combinations of short vowels with a resonant, i.e.  signals the division into two moras very much like the Standard Danish  
exactly as the literary [hu’sa] huset ‘the house’ N [hu:i)aR] huder  . zyxwv
skriver ‘scribe’. To sum up, in a monosyllabic word the choice of accent  in Marstal does not depend on the basis, but in disyllables there is  
complementary. Thus, the Standard Danish zywen, zyxwfOl have no strad, the  
nothing new in comparison with the norm. But in Marstal no strad is  also an unquestionable acc. 2, for it is not admitted to monosyllabic  words and has a strong second top, i.e. it performs a connective func tion, and herein lies the main value of this dialect for Scandinavian  accentology  

One must  
constantly bear in mind that the function of strad and no  

strad and the function of the accents are at cross-purposes rather than  
Srarland gutt, hatt are pronounced with acc. 2, the Marstal [bl&*ma],  [h&.la] have acc. 1; all those forms are unnatural if viewed in the light  of the number-of-syllables rule but quite natural for the strad N no  strad opposition. The realization of Scandinavian prosodemes depends  
acc. zyxwv1, though clear to a phonetician, is much more obvious to a  
on the extent to which no strad possesses the function of the marked  member of the opposition, i.e. on the degree to which it is an ace. 2.  In Standard Danish where the principal domain of stcad is monosyllabic  words, it is an acc. 1 by definition, while no strad is an acc. 2. But no  stod performs the function of acc. 2 passively: being the strad’s counter 
part, it has no choice, so to speak. That is why in the Danish norm  st0d is realized as a break in the syllabic nucleus, and its affinity to an  
phonologist. In Flekkefjord and Marstal, on the other hand, acc. 2 is  a marked word prosodeme, so there ace. 1 is not a real strad, but rather  its echo. Phonetically, any variety of acc. 1 may be identified as strad,  but it is acc. 2 and not acc. 1 that determines the phonological content  of the prosodic system.  
Concluding our discussion of the Marstal dialect, we should like to  31 

SCANDINAVIAN ACCENTS IN THEIR RELATION TO zyxONE ANOTHER  syllable (Andersen 1958: 47, 1965: 100). This means that the zBr0-  
call attention to a characteristic detail. This detail, seen in its true  light, will turn a theoretical discussion about stads and accents into  something palpable and concrete. In the Bra-Langeland accentual  
between the two ideal types of prosodemes. z
area acc. 2 is not necessarily characterized by a falling-rising contour  with a strong second top. Sometimes it is represented by a simple  rising tone, though in this case it also reaches its top in the second  
Langeland acc. 2, which is almost in equal measure an accent and a  stad, has an unstable realization: sometimes that of a marked acc. 2,  sometimes that of no stad. The Bra-Langeland accents indeed stand  half-way  
4.  
A much more complicated prosodic system has been described by P.  Andersen (1958: 34-123) for East Funen (EF). The EF dialect is in  the state of free apocope, but we shall abstain from any discussion of  apocopated forms. In the original monosyllables there are several  accents, which Andersen calls acc. 1, acc. 2, and stcad. The EF stad is  
z
very weak; its final phase is realized as a slow braking or a wave-like  movement within the vowel. The tonal curve rises rapidly, stays for a  while at the top, and after the final phase (if it is present) descends  again. The stad in the parish of Vinding is characterized by Andersen  as tone-stad. Ace. 1 is rising or (if the contour is blurred) simply high;  acc. 2 is falling or relatively low. Ace. 1 shortens the vowel and the  subsequent consonant. If acc. 1 hits an open syllable, the vowel is  

especially  
short,  
closed vowels  
being  
followed  
by a  
voiceless  
off-glide:  

2 produces half-length and a wide glide: /el zy< lil, 101 < Iyl. The  

[q] or  
[h]. Ace.  
lengthens  
the vowel;  
the  
resultant  
2  
drawl is  
so  
natural 

that  
very often  
a  
diphthong is  
heard.  
In the  
words  
of Andersen’s acc.  

1 and acc. 2 are distinguished accordingly, as they shorten or lengthen  the stem vowel. However, in the central area of EF the accentual  opposition is more often realized by tone, though even here it may be  replaced by qualitative differences: acc. 1 produces a diphthong with a  narrow glide and a voiceless epenthetic consonant at the end, while acc.  
farther to the west, the weaker are the tonal distinctions. Andersen  also adds that acc. 1 is really a one-phase stad2 and that though the  tonal component is always easier to perceive and register, the dynamic  
According to S. Smith, stad in its full form has three phases; when reduced,  it has only two. One phase is not enough to identify stad.  
3 5 

zyxw
zyxwS.  LIBERMAN  
A.  

movement may very well be  
the  
most  
important  
feature of the accent.  

The EF stsd contains a sharp dynamic and tonal rise and a fall, so that  from the Brra-Langeland rising tone (the one that varies with zace. 2).  
the second phase is sometimes absent. Acc. 1 is a variant of the same  dynamic accent, but comprising only the rise without the fall. This is a  very interesting explanation, but as we have pointed out, an ace. 1 can  always be taken for a weak stsd.  
Ace. 1 usually occurs on high vowels and 101: zyx’bi ‘bee’, ‘ny ‘new’, ’fin  
The EF disyllables often demonstrate a rising intonation which  resembles ace. 1, but it must not be taken for acc. 1, for thelatterisonly  
open vowels: ‘sravn ‘sleep’ (s.), zyxwhavn ‘harbor’, etc. Ace. 2 is possible on  
admitted to monosyllables. This rising intonation is indistinguishable  
Perhaps this similarity, or a natural reluctance to assign an ace. 1 to  disyllabic words made Andersen qualify only the monosyllabic rising  tone as acc. 1.  
baere ‘bear’ N zyxw[baer] baer ‘berry’, [ba.’r] bar ‘bare’ - [bar] bOr  
‘fine’, ’mund ‘mouth’, ’sol ‘sun’, though there are some examples with  
all vowels: rbi(.)] bid ‘bit’, [‘ns(.)d] nsd ‘nut’, rbar] bor ‘borer, gimlet’,  and so on.  
Besides ace. 1, acc. 2, and strad, monosyllables also have no strad, but  its distinctive features are not clear. The transcriptions show that  only st0d has a long basis, while the basis of no st0d is short: cf. [bae.’r]  
‘stretcher’. There are minimal or almost minimal pairs illustrating the  relevance of each of the four prosodemes.  
It is far from easy to give a definitive interpretation of the facts  presented by Andersen. But some conclusions are rather clear, for what  we have before us is not an isolated dialect but a fragment of the  Scandinavian area, and many things, enigmatic as they may appear,  clear up when viewed against a familiar background. We shall begin  with a short theoretical digression. Let us suppose that we have a  language whose vocabulary consists only of monosyllabic words and  that there are presumbly four tones in it. The distribution of the tones  is not regulated by any rules, i.e. they perform no other function except  that %a (a word with tone l), 2ta, 3ta, and %a are different words.  How must these tones be interpreted phonologically? The answer is  self-evident: the four tones do not belong to the prosody of the lan guage, for there is nothing to bring out their suprasegmental nature.  tion, and there is no phonological difference between the series lta -  
They must be classified as phonemes with a peculiar mode of realiza N 3ta N %a and the series ta N pa N ka N sa. 

z
case: there the accents and st0d zyx- no stnd have a clear function. There stereotyped features which usually distinguish an acc. 1 from an acc. z2,  

SCANDINAVIAN ACCENTS IN THEIR RELATION  
TO ONE ANOTHER  

Scandinavian accentual systems  
in no way resemble  
this imaginary  

fore any interpretation of the EF data must start with a search for the  
st0d from no stgd, an acc. 1 from sttad, and an acc. 2 from no st~d. If  we follow this way of investigation, we shall immediately conclude that  Andersen’s acc. 2 is something quite different from a usual ace. 2. It is  rather the Sorland variety of ace. 2, i.e. no sterd, because ace. 2, by  definition, is not admitted to monosyllables (it will be remembered that  
to take acc. 1 and stard for the variants of one unit, the more zyso that in  
apocopated words are left out of our discussion). Andersen too some times identifies no sterd and ace. 2.  
The distributional differences between st0d and acc. 1 seem to depend  on the length of the basis: acc. 1 falls on short vowels and st0d is  registered on long ones. But acc. 1 has a shortening effect, and it seems  that lack of length is no more than its phonetic concomitant. Andersen  is not quite consistent in his treatment of acc. 1, but he is almost ready  
some places they are interchangeable. I. Ejskjax too, in a work written  much later than Andersen’s book (1969: 21), calls the EF st~d and the  rising tone (i.e. acc. 1) realizations of one accent. But Andersen never  says so himself, because there are words where stad and acc. 1 are  opposed, and also because acc. 1 and stard behave differently in the  second components of compound words: stad is retained in this posi 
tion, while ace. 1 is not. But the idea of identifying ace. 1 with sterd has  a sound foundation. They may be considered variants of one prosodeme,  because it is impossible to detect any phonologically relevant differ ences in their bases, and only the norm makes the speaker choose one  or the other for each particular word.  
Thus, if we ignore some moments of minor importance, we may say  that monosyllables in EP have either stprd or no st0d, both being  represented by two phonetic variants. Such a conclusion would mean  that the EF monosyllables obey the same system of prosodic relation 
ships as Standard Danish. There are indeed some symptoms bearing  out this hypothesis. To begin with, all four accents are connected with  some sort of basis and depend on the length of the vowel and degree of  aperture. There are also differences between the types of bases.  Andersen classifies his material according to the groups: a vowel +  
a plosive, a vowel + a voiceless fricative, a vowel + a nasal, a vowel  + /j/, /w/, a vowel in an open syllable. But though it is rather easy to  reduce the four accents of the EF monosyllables to st0d and no st~d,  
it  
7 
3  

zyxw
A. zyxwS. LIBERMAN  
must not be done, for one cannot  
help seeing  
the other side of the mat 
ter, viz. that the dialect has the number-of-syllables rule.  
Andersen emphasizes that acc. 1 occurs only in monosyllables. He  warns that the rising tone in disyllables should not be taken for acc. 1.  
Andersen says (1965: 100) that when he hears the words zykatte ‘cats,)  
But in disyllables ending in -e one does not find Andersen’s acc. 2 either.  scekke ‘sacks’, hatte ‘hats’ (pl. of kat, scelc, zyxwhat) pronounced by a native  
Acc. 2 is a falling or a low tone, lengthening its basis, and the accent of  disyllables is a sharply rising or high tone on a very short vowel. As far  as I can judge of the dialect by the published descriptions, the difference  between the monosyllabic acc. 1 and the rising tone of disyllables lies  not in the form of the curve. The contour of the disyllabic accent does  resemble that of acc. 1 and that of the North-West-Funen strad.  
of Langeland, they sometimes strike him as usual Br0 words, but  sometimes as if they were Funen words. The Br0 variant is the falling rising or even-rising tone, the Funen variant is simply rising. Andersen  gives the curve of the rising tone. As we mentioned in our discussion of  the Marstal data, the culmination point of this tone lies very far at the  end of the second syllable, practically in the same place where it would  have been if the first phase were present, and it is very probable that  everything depends just on the point of culmination. The EF acc. 1  plays itself out in one syllable, whereas the disyllabic accent needs two  syllables for its manifestation. This is certainly not the distinct two peaked acc. 2 of Swedish and Norwegian, and still it is a variety of an  acc. 2. In comparison with the Marstal acc. 2 it looks more like no strad,  but in the capacity of a peculiar disyllabic accent it may be treated as  a real acc. 2. This accent is allowed only in such disyllables in -e as have  a voiceless intervocalic consonant following a short vowel.  
Andersen does not mark accents in the other disyllables (i.e. those  ending in an open syllable with a long stem vowel or containing a  vowel +/a/), but there is almost no choice there. Acc. 1 and acc. 2 (I  mean Andersen’s acc. 2) occur only in monosyllables. The accent which  is realized as a rising tone (and which is accent 2 proper) is confined to  short vowels followed by a voiceless consonant. Unless we are ready to  meet with a completely new accent, only strad and no stad remain.  
3 x zyxwvut
According to Andersen, EF disyllables in -e cannot have stard, but his  own material shows that it is not exactly true, for the so-called West  Jutland st0d occurs in [so‘ba] suppe ‘soup’ and the like. Besides, some  forms are indeterminate: one cannot say whether they have strad or not.  In disyllabic forms of nouns st~d may have one or two phases. This  
an accent which can be identified both as the EF acc. zy1 and the EF  
SCANDINAVIAN ACCENTS IN THEIR RELATION TO ONE ANOTHER  ending in -e is length: cf. /laeta/ laegte ‘lath‘ N zy/lseset~/ lette ‘ease,  
strad is optional and irrelevant. Judging by the description, it looks like  
sterd. So, if we disregard this doubtful strad and the few forms of the  [so’ba] type, the only prosodeme of the disyllables in -e must be no  strad. And so it redly is. Andersen refuses to recognize an accent even in  the disyllabic tone of the words with an intervocalic voiceless conso nant. In his opinion, the only relevant prosodic entity of the disyllables  
alleviate’ (or the weak form of the adjective let ‘light, not heavy’).  well as that of the monosyllables, is, in principle, the same as zin Standard  
Andersen does not say anything of syllable-division in EF, but it is  probably the same as everywhere in Dansish. The forms /laeta/, /laesta/  must be related in the same way as the literary /bena/ binde ‘bind’ to  /be:na/ bene (af) ‘make off’. Both in Standard Danish and in EF, an  unchecked vowel of a disyllable ending in /a/ usually has no strad.  
As regards the words in -el, -en, -er, they often have strad.  
It will be easily seen that the accentuation of the EF disyllables, as  
Danish (for a similar conclusion, though drawn from partly different  disyllabic words). zyxwv
premises, cf. Kacnel’son 1965: 174) and there can be no doubt that in  EF mora-counting is correlated with the number-of-syllables rule.  So far we have met with the accents functioning as st~d and no st~d  (Flekkefjord, partly Dalarna), and with strad and no stud playing the  role of the accents (Standard Danish, Zrra-Langeland). The most  striking feature of EF is that it has all the four separate prosodemes:  strad, no strad, acc. 1, and acc. 2 (by the latter I mean the rising tone of  acc. 1 zyxwvut- acc. 2 and st0d - no strad. Acc. 1 belongs mostly to mono 
the  
6.  
We shall also cast a cursory glance at that part of Sjdand, where,  according to Kroman (1947: 49-68), there also exist two oppositions:  
contracted forms: jar zyxwv‘father’, mor ‘mother’, bror ‘brother’) have acc. 1.  
syllabic words and has a rising-falling contour. Acc. 2 occurs only in  disyllabic words and is characterized by a falling-rising contour (both  realizations appear to be roughly the same as in Marstal). The top of  acc. 1 merges with the ictus. In acc. 2 the low point is on the ictus of the  first syllable, and the rise takes the end of the first syllable and the  whole of the second. All the monosyllables (with the exception of the  
Those among them which have a certain type of basis (a long vowel, a  3 9 
diphthong zyxwv+ a short consonant;  zyxw
A. zyxwvS. LIBERMAN  

a short vowel  
+ a voiced consonant  

(+ one or more consonants); sometimes a short vowel if it goes back to  a long one; a short vowel + a consonant, if the word is a borrowing) in  addition to acc. 1 have stad: either on the vowel or on the consonant.  
and no sterd with ace. 2: cf. zyxwsl& ‘strike!’ with sterd, ski ham ‘strike him’  
Thus, in monosyllables acc. 1 and sterd correspond. But in disyllables  the accent depends on the sterd. Sterd is possible only in some disyllables  ending in a resonant, and it is invariably accompanied by acc. 1. If the  disyllabic word lacks the sterd-basis, it acquires ace. 2. Only under  extraordinary circumstances acc. 1 in a disyllabic word can be divorced  from stud. For example, when used with strong emphasis, a word with  acc. 2 gets a metatonic acc. 1. Sometimes sterd alternates with acc. 1,  
with ace. 1; rcek ‘reach!’ with no stud, rcek mig stoklcen ‘hand me the  stick’ with ace. 2.  
The Sjaelland and the El? systems are quite unlike one another, but  if Kroman’s facts are reliable, they are as interesting as those from EF,  because we have before us a unique dialect, where different prosodemes  not only coexist, but can co-occur in one and the same word. According  to Kroman, acc. 2 and sterd are incompatible, but sterd and ace. 1 are  linked, and in the sphere of polysyllables acc. 1 is clearly dependent on  the sterd. I have allowed myself to doubt the reliability of Kroman’s  data, because Andersen says (1965: 99) that different contours (figures),  are not all distributed in Sjaelland according to Kroman’s rules and that  the tone with a rise at the end (i.e. acc. 2) is not confined to sterdless  words (e.g. it is present both in sommer ‘summer’ with sterd and in  winter ‘winter’ without sterd) and is phonologically redundant. But this  objection, though it does question Kroman’s accuracy, is hardly able  to undermine his principal thesis about the existence of tones and strad  in the dialect under investigation. Kroman also admits the redundancy  of ace. 2, for it is only a phonetic realization of no stud in disyllables.  Acc. 2 may be encroaching on other words which end in resonants, but  if so, then contrary to Andersen’s opinion, it is gaining ground as a  relevant prosodeme. But this is hardly the case, and in general this is  less important, than the fact that the contour of ace. 2 never occurs in  monosyllabic words.  
A review of Danish dialects (including those not mentioned here)  proves that stads are inseparably bound to the number-of -syllables  rule. Consequently, stud and no strad are everywhere also accents of the  Swedish-Norwegian type. The problem of markedness in the st0d area  
40 

Jensen 1944: 20), zyxwvbut they are oriented towares different systems: sterd  
SCANDINAVIAN ACCENTS IN THEIR RELATION TO ONE ANOTHER  
is more complicated than the same problem in relation to the Swedish Norwegian area. Potentially, stod and no sterd are both marked (cf.  
towards mora-counting, no stod towards the number of syllables. When  zyxw
we come across a very weak stod, its realization may be interpreted not  only diachronically (as a last stage on the way to disappearance) but  also as synchronic evidence of the loss of markedness. It is not due to  chance alone that stod in Funen is all but imperceptible: lack of  
articulatory and auditory distinctness is the price for and the outward  manifestation of the loss of peculiar stod functions. Pure sterds do not  exist, but pure accents are common: it is an accent with an admixture  

of  
“sterdness”  
that  
is  
rare.  

A  
bird’s-eye  
view  
of  
Scandinavian 
accents shows two things: sterds  

ANDERSEN, P. 1958. zyxwvuFonemsystemet zyxwi Bsifynsk. Pd grundlag af dialekten i  
conforming to the role of accents, and stods already superceded by  accents. It is precisely these things that must become a cornerstone of  
zyxwvu
historical reconstruction.  
Bibliography  
tidsskrgt for zyxwvusprogvidenskap IX. 250-273.  Revninge Sogn. Udvalg for folkemaals publikationer. Serie A. 14. K0bcn-  

- havn.  1965. 
Dialektforskning. Det danske sprogs udforskning i 20. Grhundrede.  

Udgivet af Selskab for nordisk filologi. 85-126. Kehenhavn.  
BORGSTROM, C. 1938. Zur Phonologie der norwegischen Schriftsprache. Nordisk  - 1962. Tonemes and Phrase Intonation in South-Eastern Standard Norwe gian. Studia Linguistica XVI, 1. 34-37.  
BROCH, 0. 1937. Begriffsunterschied durch Intonationsunterschied in dem  Ostnorwegischen. Acta Jutlandica IX [ = M6langes Linguistiques offerts b  M. Holger Pedersen B l’occasion de son soixante-dixii?me anniversaire. 7  avril 19371. 308-322.  
EJSKJZR, I. 1969. St0d i andet sammensaetningsled i typen fortis-semifortis i  GARDE, P. 1968. L’Accent. SUP. Le Linguiste 5. zyxPresses Universitaires do France.  
dansk 0mAl. Acta philologica Scandinavica 27. 19-67.  
ELERT, C.-C. 1964. Phonologic Studies of Quantity in Swedish. Monografier ut givna av Stockholms Kommunalforvaltning. Uppsala.  
ENWALL, P.1930. Dala- Bergslagsmdlet. Dialekthistorisk och dialektgeografisk  oversikt. Uppsala.  
FINTOFT, K. 1970. Acoustical Analysis and Perception of Tonemes in Some Nor wegian Dialects. Universitetsforlaget. Oslo-Bergen-Tromse.  
Paris.  
GJERDMAN, 0. 1918. Studier over de sormlandska stadsmdlens kvalitativa ljd Ziira. Uppsala.  
41 

zyxw
A. zyxwvS.  
HANSEN, A. 1943. zyxwvutSt~det i zyxwvDanak.  J. H. . zyxwv1966. Remarks on the Scandinavian Word Tones. Lingua 16,  LIBERMAN  

Det  
Kgl. Danske  
Videnskabernes Selskab.  

Historisk-filologiske Meddelelser XXIX, 5. Kobenhavn.  
IVERSEN, R. 1913. Senjen-maalet. Lydverket i hmeddraq. Skrifter utgit av  JOHNSEN, A. 1942. zyxwvutKristiansands bymdl. Uttgitt av BymAlslaget. lh. Oslo.  
Videnskapsselskapet i Kristiania 1912. Historisk-filosofisk Klasse 1912, 4.  Kristiania  
JASANOFF,  
KOCK, A. 1878. Sprdkhistoriska undersokningar zyxom svenak akcent. I. Lund.  
1. 71-81.  
JENSEN, E. 1944. Houlbjergmdlet. Bidrag ti1 beskrivelse af en ostjysk dialekt.  Udvalg for folkemaals pablikationer. Serie A. 6. Kobenhavn.  
JESPERSEN, 0. 1934. Modersmdlets fonetik. 3. udg. Nordisk forlag. Kobenhavn.  
KACNEL’SON, S. D. 1966. Sravnitel’naya akcentologiya germanskix yazykou. Nau ka. Moskva-Leningrad.  
KROMAN, E.1947. Musikalsk akcent i Dansk. Kebenhavn.  
LARSEN, E. G. 1970. Pormverket i Plekkefjord bymdl. Universitetsforlaget.  LAURITSEN, M. A. 1968. Phonetic Study of the DanGh Sted. Project on Linguistic  Analysis. Second Series, 7: June 1968. Phonology Laboratory, Department  of Linguistics. University of California, Berkeley.  
LEVAKDER, L. 1925. Dalmdlet. Beskrivning och historia. I. Uppsala.  RISCHEL, J. 1960. Uber die phonematische und morphonematische Funktion  der sogenannten Worttone im Norwegischen. Zeitschrift fur Phonetik und  allgemeine Sprachwissenschaft 13, H. 2, 177-185.  
- 1963. Morphemic Tone and Word Tone in Eastern Norwegian. Phonetica  10, 3-4. 154-164.  
STORM, J. 1908. Norsk lydskrift. Med omrids of fonetiken. Norwegia I. 19-179.  SWENNING, J. 1917-37. Polkmdlet i Listera Hard. Nyare bidrag till kannedom  om de svenska landsmiilen och svenskt folkliv 36. Stockholm.  
TRUBETZKOY, N. S. 1939. Grundziige der Phonologie. Travaux du Cercle Linguis tique de Prague 7. Prague.  
VANVIK, A. 1961. Three Tonemes in Norwegian? Studia Linguistics XV, 1,  22-28.  
WITTING, C. 1968. On Acute and Grave Contours in Central Swedish Dialectal  Speech (Uppland). An Audio-phonetic study. Svenska Landsmdl och  Svenskt Folkliv 65. Stockholm.  
Institute of Linguistics  
University Embankment 5  
Leningrad  
42 

