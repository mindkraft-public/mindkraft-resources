Super Useful Expressions, Part 1  

(Intermediate level)
 
1) ถูกเผงเลย (tùuk pěng ləəi) --- Spot on!; Exactly!; You hit the nail on the head!  

Note: 

-ถูกต้อง(เลย) (tùuk dtɔ̂ng; tùuk dtɔ̂ng ləəi) --- has a similar meaning but is a bit  less emphatic.  

2) นั่นแหละ (nân-lɛ̀) --- Exactly.; That's it/right.; There you are then.; That's... (or  emphasizes preceding part of sentence). 

3) (ก็)นั่นน่ะสิ (nân-nâ-sì; gɔ̂ɔ nân-nâ-sì) --- Quite right!; Yes...; Exactly!; That's just it!  

4) จะไปแล้ว (jà bpai lɛ́ɛo) --- (I) gotta get going; I'm gonna go now.; I gotta run.  5) ไปไหน (bpai nǎi) --- Where are you going?  

6) พูดเล่น (pûut-lên) --- Just joking!; I'm kidding.  

7) อย่าพูดเล่น (yàa pûut-lên) --- Don't joke around.  

8) โอ้โฮ (ôo-hǒo) --- Wow; Oh!; Gee!; Holy Cow!; Whoa! (shows surprise in a  kind of cute, comical way)  

9) ตายแล้ว (dtaai-lɛ́ɛo) --- Goodness!; Shoot!; Shit! Oh my!  

10) ทุกอย่างจะดีขึ้นเอง (túk-yàang jà dii kʉ̂n eeng) --- Everything will be okay/  work out.; It will all work out.  

11) อีกแล้วหรือ (ìik lɛ́ɛo rʉ̌ʉ) --- Again?!; It happened again?!; You did it again?!  12) ไม่ใช่ครั้งแรก (mâi-châi kráng-rɛ̂ɛk) --- This/That/It's not the first time.  

13) คุณว่าอะไรนะ (kun wâa àrai ná) --- What did you say?  

Note: 

-คุณพูดว่าอะไรนะ (kun pûut wâa àrai ná) --- is a bit longer and less casual way to  say this. 

14) ใครว่า... (krai wâa...) --- Who says...?; Who said...?  

15) โธ่เอ๋ย/โธ่เอ๊ย (tôo-əə̌i/tôo-əə́i) --- Oh man!; Come on man!; Oh dear!; Shit!;  (Oh), damn!, etc.  

16) อะไรก็ได้ (à-rai gɔ̂ɔ dâi) --- Anything's fine.; Whatever you want.; whatever;  anything 

17) หุบปาก (hùp-bpàak) --- Shut up!; Keep quiet!; Shush!; Zip it!  18) บอกแล้วไง(...) (bɔ̀ɔk lɛ́ɛo ngai...) --- I told you...; I told you so.  

19) อย่ามายุ่ง(กับ...) (yàa maa yûng; yàa maa yûng gàp...) --- Don't mess with  (me)!; Leave me alone!; Bug off!; Stay out of it!  

20) พูดง่ายแต่ทำยาก (pûut ngâai dtɛ̀ɛ tam yâak) --- Easier said than done!; Easy  to say but difficult to do.  

Super Useful Short Expressions, Part 2  

(Intermediate level) 

1) ไม่เข้าใจจริงๆ (mâi-kâo-jai jing-jing) 

I don't understand at all! / I really don't understand.  

2) ไม่เห็นด้วยเลย (mâi hěn-dûai ləəi)

I don't agree at all. / I disagree!  

3) อย่างนี้นี่เอง (yàang-níi nîi eeng) 

(Oh,) I see. / Aha. / Oh, I got it. / Oh, that's how it is.  

4) (ก็)แล้วแต่(คุณ) (gɔ̂ɔ) lɛ́ɛo dtɛ̀ɛ (kun) 

It's up to you. / Up to you.  

5) มันเรื่องอะไรกัน (man rʉ̂ang àrai gan) 

What's going on?! / What's this all about?!  

6) มันไม่ได้เป็นอย่างนั้น (man mâi-dâi bpen yàang-nán) 

It's not like that. / That's not how it is. / That's not the case. / That's not true.  

7) อะไรทำนองนั้น (à-rai tam-nɔɔng nán) 

Something like that. / Something along those lines.  

8) คงยังงั้น/คงงั้นมัง/คงยังงั้นมัง/คงงั้นมั้ง INFORMAL  

(kong yang-nán / kong ngán mang / kong yang-ngán mang / kong ngán máng)  I guess so. / I think so. / Probably.  

9) ไม่มีอะไร (mâi mii à-rai) 

It's nothing. / Nothing. / There is nothing.  

10) เอาล่ะ (ao-lâ) 

Okay...(let's start); Alright then... 

11) เอาซิ/เอาดิ (ao-sí / ao-dì) INFORMAL 

Come on (do it)! / Bring it on! / Sure! / Do it!  

12) ไม่เอาน่า (mâi ao nâa) 

Come on! (Are you crazy?/really?) / No...! 

13) อย่านะ (yàa ná) INFORMAL  

Don't! / Don't do that! / Please!  

14) อย่ามาทำเป็น... (yàa tam bpen...) INFORMAL  

Don't (act like)... ! 

15) อย่าเลย (yàa ləəi) 

You'd better not. / Please don't. / Don't do it. 

16) คิดอะไรของคุณ (kít à-rai kɔ̌ɔng kun) INFORMAL  

What were you thinking?! (Are you crazy / Why did you do that?)  

17) มีปัญหาอะไร (mii bpan-hǎa à-rai) 

What's the problem? / What's the matter?  

18) อยากรู้ใช่ไหม (yàak rúu châi mǎi) 

You wanna know, don't you?  

19) ใจคอไม่ดี (jai-kɔɔ mâi-dii) 

I have a bad feeling. (something bad will happen)  

20) ไม่ต้องห่วงหรอกนะ (mâi-dtɔ̂ng hùang rɔ̂ɔk ná) 

Don't worry about it. / There's no need to worry about it at all. 

Super Useful Short Expressions, Part 3  (low-intermediate level) 

1) หิวหรือยัง (hǐu rʉ̌ʉ yang) 

Are you hungry (yet)?  

2) ไม่ได้กินอะไร(มา)ตั้งแต่เช้า (mâi dâi gin à-rai (maa) dtâng-dtɛ̀ɛ cháao)  I haven't eaten anything since this morning.  

3) นี่อะไร(ครับ/คะ) (nîi à-rai; nîi à-rai kráp; nîi à-rai kà)  What's this?  

4) ฝีมือผมเอง/ทำเอง (fǐi-mʉʉ pǒm eeng / tam-eeng) 

I made it myself.  

5) ระวังนะ มันร้อน (rá-wang ná man rɔ́ɔn) 

Be careful---it's hot. 

6) เก่งนี่ (gèng nîi) 

Wow, you're very good at that! / Great job! / Wow, you're very good at that! /  Brilliant!  

7) เรื่องมาก(จัง) (rʉ̂ang mâak; rʉ̂ang mâak jang) 

You're so fussy/high-maintenance/demanding! 

8) ใช่แล้ว (châi lɛ́ɛo)  

That's right!  

9) รู้ได้ยังไง (rúu dâi yang-ngai) 

How do you know?  

10) ตกลงครับ (dtòk-long kráp) 

Okay, (I agree). / Agreed. / Alright.  

11) รู้ไหม (rúu-mǎi)  

 Do/Did you know...?  

12) (ผม)ลืมไป (pǒm lʉʉm bpai; lʉʉm bpai) 

I forgot.  

13) เดี๋ยวนะคะ (dǐao ná-ká)  

Wait. / Wait a second. / Hold on.  

14) เจอแล้ว (jəə lɛ́ɛo)  

I found it/him/her.  

15) ยังไม่ง่วง (yang mâi ngûang) 

I'm not sleepy yet. 

16) มาแล้ว (maa lɛ́ɛo)  

It's here. / She's here. / Here he comes (He's here.)  

17) อีกแล้วเหรอ (iìk lɛ́ɛo rə̌ə) 

Again?! / Not again! / Him again?! / Another... ?! 

18) แล้วคุณล่ะ (lɛ́ɛo kun lâ) 

What about you? / And you?  

19) หึงหรือ (hʉ̌ng rʉ̌ʉ) 

Are you jealous?  

20) เกือบไปแล้ว (gʉ̀ap bpai lɛ́ɛo) 

That was close! / Close one! / Close call!  

Super Useful Short Expressions, Part 4  (low-intermediate level) 

1) ทำง่าย (tam ngâai) 

Easy to make!  

2) อร่อยจังเลย (à-rɔ̀i jang ləəi) 

It's delicious! / This is great!  

3) ไม่มีอะไรเหลือ (mâi mii àrai lʉ̌a)  

There's nothing left. / It's all gone.  

4) ต้องอย่างนั้น(สิ) (dtɔ̂ng yàang nán; dtɔ̂ng yàang nán sì)  Now you're talking! / That's it! / That's what I'm talking about!  

5) ไม่มีทาง (mâi mii taang) 

No way!  

6) ดูแลตัวเองดีๆนะ (duu-lɛɛ dtua-eeng dii-dii ná [kráp/ká])  Take good care of yourself. / Please take care.  

7) ไม่ชอบขี้หน้าเขา SLANG (mâi chɔ̂ɔp kîi-nâa kǎo)  I can't stand him! / I hate the sight of him!

8) สวยไม่ใช่เล่น SLANG (sǔai mâi châi lên) 

She's really beautiful! / She's pretty darn good lookin'!  

9) คุณต้องการอะไร (kun dtɔ̂ng-gaan à-rai) 

What do you want? / What do you need?  

10) ตัดใจไม่ได้ (dtàt-jai mâi-dâi) 

I can't get over her. / I can't forget her. / I can't let her go. / I can't stop thinking  about her. 

11) ทำไมไม่บอก(ผม) (tam-mai mâi bɔ̀ɔk; tam-mai mâi bɔ̀ɔk pǒm)  Why didn't you tell me?  

12) เป็นครั้งแรกในชีวิต (bpen kráng-rɛ̂ɛk nai chii-wít) 

It's the first time in my life. / This is the first time (I've done this) in my life.  

13) จะเข้าห้องน้ INFORMAL (jà kâo hɔ̂ng-nám) 

I'm gonna go to the bathroom/restroom.  

14) เอาไงดี INFORMAL (ao ngai dii) 

Now what? / What should we do now?  

Note: ไง (ngai) is short for อย่างไร (yàang-rai) 

15) คอยดูนะ (kɔɔi duu ná) 

Wait and see! / You'll see! / You watch...  

16) ตื่นแล้วเหรอ (dtʉ̀ʉn lɛ́ɛo rə̌ə) 

Oh, you're awake? / You're up? / Are you awake? 

17) ให้ตายเถอะ INFORMAL (hâi dtaai tə̀) 

For God's sake! / Damn it! / Bloody hell! / Shit!  

18) อย่าเพิ่ง/อย่าเพิ่ง... (yàa pə̂ng / yàa pə̂ng...)  Don't! / Don't.... (do something yet)  

19) อย่ามาขวาง INFORMAL/INSULTING (yàa maa kwǎang)  Get out of my way! / Don't block my way!  

20) ใช้เวลาให้เป็นประโยชน์ (chái wee-laa hâi bpen bprà-yòot)  Make good use of your time. / Spend your time wisely.  

Super Useful Short Expressions, Part 5  (high-beginner level) 

1) ถูกใจไหม (tùuk-jai mǎi) 

Do you like it? / Is it to your liking?  

2) สุดยอดเลย (sùt-yɔ̂ɔt ləəi) 

It's awesome/super/cool!  

3) เก่งมาก (gèng mâak) 

Very good! / Nice job! / You're good at this!  

4) ยินดีด้วย (yin-dii dûai) 

Congrats! / Congratulations!  

5) ชนแก้ว (chon gɛ̂ɛo) 

Cheers! 

6) ทำใจดีๆ (tam-jai dii-dii) 

Hang in there! / Keep up your courage. / Hold on! / Cheer up!  

7) พักผ่อนเถอะ (pák-pɔ̀n tə̀) 

Get some rest.  

8) ก็ใช่น่ะสิ INFORMAL (gɔ̂ɔ châi nâ sí) 

That's right! / Yeah, I am! / Exactly! / Of course!  

9) ดีแล้ว (dii lɛ́ɛo) 

Good. / That's good. / Good for you.  

10) เชิญครับ (chəən kráp) 

Go ahead. / After you. / Please... / Come on in.  

11) แป๊บนึง (bpɛ́ɛp-nʉng) 

Just a moment. / Just a second.  

12) ขอโทษค่ะ (kɔ̌ɔ-tôot kà) 

I'm sorry. / Excuse me. / Pardon me.  

13) สนุกจะตาย (sà-nùk jà-dtaai) 

This is soooo much fun! / It's a blast!  

14) ไง INFORMAL (ngai) 

Hey. (Hi) / What's up?  

15) มาดูเร็ว (maa duu reo) 

Come and have a look!  

16) พูดเล่นนะ (pûut-lên ná) 

I'm joking! 

17) ดูนั่นสิ (duu nân sí) 

Look at that! / Look! 

18) หอมจังเลย (hɔ̌ɔm jang ləəi) 

It smells so good! 

19) จริงด้วย (jing dûai) 

That's right! / That's true!  

20) ใครๆก็ชอบ... (krai-krai gɔ̂ɔ chɔ̂ɔp) 

Everyone likes/loves...  

Super Useful Short Expressions, Part 6  (low-intermediate level) 

1) พอได้แล้ว (pɔɔ dâi lɛ́ɛo) 

That's enough! (stop it!) / Enough! / Break it up! / Knock it off!  

2) เลิกคิดได้แล้ว (lə̂ək kít dâi lɛ́ɛo) 

Quit thinking about it! / Get it out of your head!  

3) ไม่ต้องโกหก (mâi dtɔ̂ng goo-hòk) 

You don't have to lie. / Don't lie (to me).  

4) อย่าทำอะไรผม(เลย) (yàa tam à-rai pǒm (ləəi) Don't hurt me. / Don't do anything to me.  

5) ระวังไว้ให้ดี(เถอะ) (rá-wang wái hâi dii tə̀) 

Watch your back. / Be very careful. / Look out.  

6) ถอยไป (tɔ̌ɔi bpai) 

Get back! / Get away! / Back off! / Stay away from me!  

7) อยู่เฉยๆ (yùu chə̌əi-chə̌əi) 

Stay still. / Don't move. / Stay right there. / Sit tight.  

8) เบาๆหน่อย (bao-bao nɔ̀i) 

Softly! / Not so rough. / Easy! / Not so loud! 

9) อย่าดิ้น (yaa dîn) 

Don't move! / Don't try to escape! / Don't squirm!  

10) ปล่อยฉันเดี๋ยวนี้ (bplɔ̀i chǎn dǐao-níi) 

Let go of me (right now)!  

11) พูดมาซิ (pûut maa sí) 

Tell me! / Say it!  

12) ไหนบอก(ว่า)... (nǎi bɔ̀ɔk (wâa)...) 

Hey, I thought you said...  

13) ฉิบหาย (chìp-hǎai) 

Damn! / Shit!  

14) เวรกรรม (ween-gam) 

Damn! / Damn it! / Oh, crap! / What bad luck! (This also means karma or What  goes around, comes around.)  

15) ทำไมมาช้าจัง (tam-mai maa cháa jang) 

Why are you so late?  

16) ไม่น่าล่ะ / มิน่าล่ะ (mâi-nâa-lâ / mi-nâa-lâ) 

No wonder... ! / No wonder! / It's no wonder... ! / That's why... !  

17) ไม่ใช่เด็กๆแล้วนะ (mâi-châi dèk-dèk lɛ́ɛo ná) 

You're not a kid/child (anymore)!  

18) ไม่ดีเหรอ (mâi-dii rə̌ə) 

Is that (so) bad? / What's wrong with that?  

19) ไม่คุ้มค่า (mâi kúm-kâa) 

It's not worth it.  

20) ปล่อยให้(มัน)เป็นไป (bplɔ̀i hâi (man) bpen-bpai) 

Let it go. / Let it be. / So be it. 

Super Useful Short Expressions, Part 7  (high-beginner level) 

1) หิวน้ (hǐu-náam) 

I'm thirsty.  

2) หิวข้าว (hǐu-kâao) 

I'm hungry.  

3) เหนื่อยจัง (nʉ̀ai jang) 

I'm so tired.  

4) ง่วงแล้ว (ngûang lɛ́ɛo) 

I'm sleepy.  

5) ผมเมามาก (pom mao mâak) 

I'm really drunk.  

6) น่าอิจฉาจัง (nâa-ìt-chǎa jang) 

I'm so jealous/envious!  

7) เปลี่ยนใจแล้ว (bplìan-jai lɛ́ɛo) 

I changed my mind. / I've changed my mind.  

8) คิดถึงจังเลย (kít-tʉ̌ng jang ləəi) 

I really miss you. / I really miss her.  

9) เที่ยวให้สนุก (tîao-hâi sà-nùk) 

Have a good trip. / Enjoy your trip.  

10) รอด้วย (rɔɔ dûai) 

Wait. / Wait for me. / Wait up. 

11) สุขสันต์วันเกิด(นะคะ) sùk-sǎn wan-gə̀ət (ná ká) Happy Birthday! 

12) เชิญนั่ง(ก่อน)(ครับ) chəən nâng (gɔ̀ɔn) (kráp) Have a seat.  

13) (มัน)อันตรายมาก (man an-dtà-raai mâak) (It)'s very dangerous.  

14) อย่าทิ้งฉัน (yàa tíng chǎn) 

Don't leave me! / Don't throw me away!  

15) เรียบร้อยแล้ว (rîap-rɔ́ɔi lɛ́ɛo) 

It's all done. / I'm finished.  

16) มันซับซ้อน (man sáp-sɔ́ɔn) 

It's complicated. 

17) ไม่มีอะไรเกิดขึ้น (mâi mii à-rai gə̀ət-kʉ̂n) Nothing happened.  

18) (ก็)คิดอยู่ (gɔ̂ɔ) kít-yùu 

I'm thinking about it. 

19) (ฉัน)ดูไม่ออก (chǎn) duu mâi-ɔ̀ɔk 

I can't tell. / I can't make out who it is. / I can't make it out.  

20) ไม่เคยเจอกัน (mâi kəəi jəə-gan) 

We've never met (before). / I've never met her (before).  

Super Useful Short Expressions, Part 8  (low-intermediate level)

1) เป็นอะไร (bpen à-rai) 

Are you okay? / What's wrong? / What's going on?  

2) มีใครไปบ้าง (mii krai bpai bâang) 

Who's going?  

3) บอกว่าอะไร(คะ/ครับ) bɔ̀ɔk wâa à-rai (ká/kráp) What did he say?  

4) พูดถึงอะไร (pûut-tʉ̌ng à-rai) 

What are you talking about?  

5) คิดอะไรอยู่ (kít à-rai yùu) 

What are you thinking? / What are you thinking about?  

6) ไปเอามาจากไหน (bpai ao maa jàak nǎi) 

Where did you get it/that?  

7) ทำอะไรอยู่ / ทำอะไรอ่ะ INFORMAL (tam à-rai yùu / tam à-rai à) What are you doing?  

8) ช่วยหน่อย INFORMAL (chûai nɔ̀i) 

Help me. / Can you help me?  

9) ว่ายังไงบ้างครับ (wâa yang-ngai bâang kráp) 

What did she say? 

10) กลัวอะไร (glua à-rai) 

What are you afraid of?  

11) จำไม่ได้หรือ (jam mâi-dâi rə̌ə) 

You don't remember? / Don't you remember? 

12) นานแค่ไหนแล้วที่... (naan kɛ̂ɛ-nǎi lɛ́ɛo tîi...) 

How long (has it been since)... ?

13) ใครจะไปรู้ (krai jà bpai rúu) 

Who knows? / It's anyone's guess. / You never know.  

14) จะซื้ออะไรให้ดี (jà sʉ́ʉ à-rai hâi dii) 

What should I get (him)? (for example: for his birthday)  

15) (...) จะว่ายังไง (...) jà wâa yang-ngai 

What would (...) say/think? What do (you) say? / 

What do (you) have to say to that? / What do (you) think? 

16) เอาอย่างนี้/งี้ (ao yàang níi/ngíi) 

How about this...? / I tell you what... / Okay, then... (giving suggestion)  

17) เคยไปหรือ (kəəi bpai rə̌ə) 

Oh, have you been (there) before? / Oh, you've been there before?  

18) ดีขึ้นไหม (dii-kʉ̂n mǎi) 

Are you better now? / Is it better now?  

19) มันผิดตรงไหน (man pìt dtrong-nǎi) 

What's wrong with that/it?  

20) เขาทำอะไรคุณ (kao tam à-rai kun) 

What did (she) do to you?  

Super Useful Short Expressions, Part 9  (mid-intermediate level) 

1) น่าเสียดาย (nâa-sǐa-daai) 

That's too bad. / What a pity. / What a shame.  

2) ฉันเป็นห่วงเธอ (chǎn bpen hùang təə) 

I'm worried about you. 

3) บังเอิญจริงๆ (bang-əən jing-jing) 

What a coincidence!  

4) ถ้าไม่รังเกียจ... (tâa mâi rang-gìat...) 

If you don't mind... / If it's okay with you...  

5) มาช้าดีกว่าไม่มา* (maa cháa dii gwàa mâi maa) 

Better late than never.  

6) เอาเป็นว่า... (ao bpen wâa...) 

(Used when concluding something): Well/Okay/Then... / ...then. / Okay, let's do  it this way... / It turns out/In the end... / Let's just say...  

7) สุขสันต์วันเกิดย้อนหลัง (sùk-sǎn wan-gə̀ət yɔ́ɔn-lǎng) 

Happy Belated Birthday!  

Note: 

-ย้อนหลัง (yɔ́ɔn-lǎng) means in retrospect.  

8) ยังหาไม่ได้เลย (yang hǎa mâi dâi ləəi) 

I still can't find one. / I still can't find it.  

9) ตอนแรกไม่เชื่อ (dtɔɔn-rɛ̂ɛk mâi-chʉ̂a) 

At first, I didn't believe it.  

10) นึกแล้วเชียว (nʉ́k lɛ́ɛo chiao) 

I knew it! / I thought so! / Just as I thought!  

Note: 

-เชียว (chiao) is an intensifier used for emphasis here. 

11) ผมก็ไม่ว่า(อะไร) pǒm gɔ̂ɔ mâi-wâa (à-rai) 

It's fine with me. / Feel free. / I don't mind. (If you do it...)  

12) ไปกันเถอะ (bpai gan təə) 

Let's go. 

13) บอกตามตรง... (bɔ̀ɔk dtaam-dtrong) 

To be honest... / I'll be honest with you... / Honestly...  

14) ผมไม่ใช่คนโง่ (pǒm mâi-châi kon-ngôo) 

I'm not stupid! / I'm not a fool!  

15) ต้องจองล่วงหน้า (dtɔ̂ng jɔɔng lǔang-nâa) 

You have to make advance reservations. / You need to reserve/book (it) in  advance.  

16) ก็ดีเหมือนกัน (gɔ̂ɔ dii mʉ̌an-gan) 

Fine. / Okay. / (That) sounds good. (I like that idea.)  

17) ไม่ว่ายังไง... (b) 

No matter what... / No matter what happens...  

18) คงไม่หรอก (kong mâi rɔ̀ɔk) 

It's not likely. / I don't think so. / Probably not.  

19) (พี่)คิดมาก (pîi) kít-mâak 

Don't think about it so much---relax. / You're overthinking things/it.  

20) เขาไม่รู้ด้วยซ้ำว่า... (kǎo mâi rúu dûai-sám wâa...) 

He doesn't even know (that)...  

Super Useful Short Expressions, Part 10  (mid-intermediate level) 

1) ความจริงก็คือ... (kwaam-jing gɔ̂ɔ kʉʉ...) 

The truth is...  

2) ฉันรู้แล้ว(ค่ะ) chǎn rúu lɛ́ɛo (kà) 

I know (already). 

3) (มัน)สุดๆไปเลย (man) sùt-sùt bpai ləəi 

This is totally awesome! / Splendid! 

4) ฉันเกือบตาย (chǎn gʉ̀ap dtaai) 

I almost died.  

5) ไม่ได้สังเกตว่า... (mâi-dâi sǎng-gèet wâa...) 

I didn't notice...  

6) ยังไม่ได้บอกเลยว่า... (yang mâi-dâi bɔ̀ɔk ləəi wâa...) 

You haven't told me yet... / You still haven't told me... / 

I haven't told you yet... / I still haven't told you...  

7) ไม่ค่อยไว้ใจ(เขา) mâi kɔ̂i wái-jai (kǎo) 

I don't (really) trust (him). / I don't trust (him) so much.  

8) ไม่มีอะไรน่าสนใจหรอก (mâi mii à-rai nâa-sǒn-jai rɔ̀ɔk) 

It's nothing special. / There's nothing of interest (there). / There's nothing to see.  

9) ไม่เอา(น่า/นะ) mâi ao (nâa/ná) 

Don't. / I don't want it. / Come on!  

10) เดี๋ยวใครเห็นเข้า (dǐao krai hěn kâo) 

Careful/watch out--- someone will see us!  

11) ผมถึงได้บอก(ไง)ว่า... (chǎn tʉ̌ng dâi bɔ̀ɔk (ngai) wâa...) That's why I said/told you...  

12) ไม่รู้จะทำยังไงแล้ว (mâi rúu jà tam yang-ngai lɛ́ɛo) 

I don't know what (else) to do! / I don't even know what to do.  

13) เขาเคยเล่าว่า... (kǎo kəəi lâo wâa) 

He once told me... / He told me before that...  

14) ถ้าจำไม่ผิด... (tâa jam mâi pìt...) 

If I'm not mistaken... / If I recall correctly... / If my memory serves me well... 

15) (ไป)นอนก่อน(นะ) (bpai) nɔɔn gɔ̀ɔn (ná) 

I'm going to bed.  

16) เตือนแล้วนะ(ครับ)ว่า... dtʉan lɛ́ɛo ná (kráp) wâa... I warned you (that)...  

17) ปกติฉันตื่น(เจ็ด)โมงเช้า bpòk-gà-dtì chǎn dtʉ̀ʉn (jèt) moong cháao I usually get up at (7:00).  

18) กินไม่ลง/ทานไม่ลง (gin mâi long/ taan mâi long) 

I (just) can't eat this.  

19) อ่านไม่รู้เรื่อง (àan mâi-rúu-rʉ̂ang) 

I can't understand this at all. / I can't read it. 

20) ฉันไม่รู้เรื่อง (chǎn mâi-rúu-rʉ̂ang) 

I don't know anything about it! 

Super Useful Short Expressions, Part 11  (low-intermediate level) 

1) ลืมอะไรเหรอ (lʉʉm à-rai rə̌ə) 

Did you forget something?  

2) ลืมบอก(ไป)ว่า... lʉʉm bɔ̀ɔk (bpai) wâa... 

I forgot to tell you...  

3) ผมจะไม่มีวันลืม (pǒm jà mâi mii wan lʉʉm ) 

I will never forget! / I'll always remember!  

4) จริงเหรอ (jing rə̌ə) 

Really?/ Seriously? 

5) นี่คุณเมาหรือเปล่า (nîi kun mao rʉ̌ʉ-bplàao) 

Are you drunk?  

6) ไม่ไหวแล้ว (mâi-wǎi lɛ́ɛo) 

I can't take it anymore! / I can't stand this anymore! / I can't do this anymore!  

7) ใจเย็นๆ (jai yen-yen) 

Relax. / Calm Down. / Take it easy.  

8) ทำไปได้ยังไง (tam bpai dâi yang-ngai) 

How could you do that?!  

9) ผมคิดว่า... (pǒm kít wâa...) 

I think...  

10) ผมว่า... (pǒm wâa...) 

INFORMAL I think... 

11) มันไม่ง่าย (man mâi ngâai) 

It's not easy. / It won't be easy.  

12) อย่าเข้ามานะ (yàa kâo maa ná) 

Don't come any closer. / Don't come in here.  

13) ไปให้พ้น (bpai hâi pón) 

Get lost. / Get out of here. / Go away!  

14) โกรธเหรอ (gròot rə̌ə) 

Are you angry? 

15) จะพยายาม (jà pá-yaa-yaam) 

I'll try. 

16) ช่วยด้วย (chûai-dûai) 

Help me! / Help! 

17) แน่ใจนะว่า... (nɛ̂ɛ-jai ná wâa...) 

Are you sure (that)...?  

18) ผมไม่รู้ / ผมไม่ทราบ (pǒm mâi rúu / pǒm mâi sâap) I don't know. [Note: ทราบ (sâap) is more formal]  

19) อย่ามอง (yàa mɔɔng) 

Don't look.  

20) ไม่เกี่ยวกับคุณ (mâi gìao gàp kun) 

It's none of your business. / It's not about you. / It's no concern of yours.  

Super Useful Short Expressions, Part 12  (low-intermediate level) 

1) ไม่ต้องกลัว (mâi dtɔ̂ng glua) 

No need to be afraid. / Don't worry. / Don't be scared.  

2) อย่าลืมวันเกิดผม (yàa lʉʉm wan-gə̀ət pǒm) 

Don't forget my birthday. 

3) ฉันจำวันเกิดของคุณได้ (chǎn jam wan-gə̀ət kɔ̌ɔng kun dâi) I remember when your birthday is. / I can remember when your birthday is.  

4) หมดอายุแล้ว (mòt aa-yú lɛ́ɛo) 

It's expired. / It's reached its expiration date.  

5) เข้มงวดมาก (kêm-ngûat mâak) 

She's so strict! / It's very strict.  

6) พูดอะไรอย่างงั้น INFORMAL (pûut à-rai yàang-ngán) What are you talking about?! (Are you kidding?!)

7) (มัน)ไม่จริง(หรอก) (man) mâi jing (rɔ̀ɔk) That's not true (at all).  

8) ไม่เคยมาที่นี่ (mâi kəəi maa tîi-nîi) 

I've never been here (before).  

9) เคยได้ยินมา (kəəi dâi-yin maa) 

I've heard it before.  

10) ทำอาหารไม่เป็น(เลย) tam-aa-hǎan mâi-bpen (ləəi) I can't cook (at all).  

11) ทำอะไรไม่ได้ (tam à-rai mâi-dâi) 

(I) can't do anything. / There's nothing I can do.  

12) ไม่รู้ว่าทำไม (mâi rúu wâa tam-mai) 

I don't know why.  

13) ไม่เข้าใจว่าทำไม (mâi kâo-jai wâa tam-mai) I don't understand why.  

14) บอกตามตรง... (bɔ̀ɔk dtaam-dtrong) 

To be honest... / I'll be honest with you... / Honestly...  

15) ดีใจที่ได้คุยกัน (dii-jai tîi dâi kui-gan) 

Nice talking to you. / It was nice talking with you. 

16) ดีใจที่ได้พบกัน (dii-jai tîi dâi pop-gan) It was nice meeting you.  

17) ดึกแล้ว (dʉ̀k lɛ́ɛo) 

It's (getting) late. / It's really late. 

18) หยุดเดี๋ยวนี้ (yùt dǐao-níi) 

Stop it! / Stop!  

19) แปลว่าอะไร(คะ) bplɛ̀ɛ wâa à-rai (ká)

What does (it) mean? / What does (this) mean?  

20) แปลว่า... (bplɛ̀ɛ wâa...) 

That/it means...  

Super Useful Short Expressions, Part 13  

(low-intermediate level) 

1) แยกไม่ออก (yɛ̂ɛk-mâi-ɔ̀ɔk) 

I can't tell them apart. / I can't see the difference (between them).  

2) แทบไม่เชื่อสายตา (tɛ̂ɛp mâi-chʉ̂a sǎai-dtaa) 

I could hardly believe my eyes!  

3) ยินดีช่วยเหลือ (yin-dii chûai-lʉ̌a) 

Happy to help. / Glad to be of assistance.  

4) จะอยู่ได้ยังไงถ้าไม่มีเขา (jà yùu dâi yang-ngai tâa mâi mii kǎo) How will I live/survive without him?  

5) ช่วยเซ็น(ชื่อ)ตรงนี้หน่อย(ครับ/ค่ะ) chûai sen (chʉ̂ʉ) dtrong-níi nɔ̀i (kráp/kà) Please sign here.  

6) พอกันที (pɔɔ-gan tii) 

(That's) enough! / I've had enough!  

7) น่าสมเพชจริงๆ SEMI-LITERARY (nâa-sǒm-pêet jing-jing) How pathetic! / How pitiful!  

8) ไม่เป็นอะไรหรอก (mâi bpen-à-rai rɔ̀ɔk) 

She'll be okay. / Nothing's going to happen to her. / It'll be alright.  

9) เหมือนเป็นคนละคนเลย (mʉ̌an bpen kon-lá kon ləəi) 

She's like a completely different person. 

10) ในสายตาผม... (nai sǎai-dtaa pǒm...) 

In my eyes... / From my point of view... / To me... / (You don't look...) to me. 

11) หากต้องการข้อมูลเพิ่มเติม... FORMAL (hàak dtɔ̂ng-gaan kɔ̂ɔ-muun pə̂əm-dtəəm...) 

For more information... / If you need further information...  

12) ผมก็ไม่ว่า(อะไร) pǒm gɔ̂ɔ mâi-wâa (à-rai) 

It's fine with me. / Feel free. / I don't mind. (if you do it)  

13) ทีแรกคิดว่า... (tii-rɛ̂ɛk kít-wâa...) 

At first I thought...  

14) คงไม่หรอก (kong mâi rɔ̀ɔk) 

It's not likely. / I don't think so. / Probably not.  

15) ไม่ใช่เรื่องบังเอิญ (mâi-châi rʉ̂ang bang-əən) 

It's not a coincidence. / It's not just by chance.  

16) ถ้ายังงั้น... (tâa yang-ngán...) 

In that case... / So... / Well then...  

17) ทุกอย่างเปลี่ยนไป (túk-yàang bplìan bpai) 

Everything has changed. / Everything is different. / Everything changes.  

18) มันทำใจลำบาก (man tam-jai lam-bàak) 

It's really hard! / This is really hard on me! (breaking up with someone, etc.)  

19) คุณทำเพื่ออะไร (kun tam pʉ̂a-à-rai) 

What did you do it for? / Why did you do it?  

Note: เพื่ออะไร (pʉ̂a-à-rai) means 'for what reason' or 'why'.  

20) จ(เขา)ได้ไหม jam (kǎo) dâi mǎi 

Do you remember (him)? 

Super Useful Short Expressions, Part 14  (low-intermediate level) 

1) ไม่เชื่อหรอก (mâi chʉ̂a rɔ̀ɔk) 

I don't believe it! / I don't believe you!  

2) อย่าไปเชื่อ(...) yàa bpai chʉ̂a(...) 

Don't believe it/him! / Don't believe...  

3) จะไม่มีใครเชื่อ (jà mâi mii krai chʉ̂a...) 

No one's gonna believe you! / No one would believe you!  

4) ผมจัดการเอง (pǒm jàt-gaan eeng) 

I'll take care of it myself. / I'll do it myself. 

5) แค่อยากให้คุณรู้ว่า... (kɛ̂ɛ yàak hâi kun rúu wâa...) 

I just want you to know...  

6) มีโอกาส(สูง)มากที่... mii oo-gàat (sǔung) mâak tîi... 

There's a good/great chance that... / It's very likely that...  

7) อย่ารู้เลย (yàa rúu ləəi) 

It's better if you don't know. / You don't wanna know! 

8) ...อะไรประมาณนี้ (...à-rai bprà-maan nií) 

...(or)something (like that).  

9) มาพอดีเลย (maa pɔɔ-dii ləəi) 

You're just in time. / Speak of the devil---here she is/comes.  

10) ตกลง... INTERJECTION (dtòk-long...) 

So... / Okay... / Alright... (like 'Tell me...' when making a decision/conclusion)  11) ฉันยกโทษให้ (chǎn yók-tôot-hâi)

I forgive you.  

12) คุณก็รู้ว่าผมคิดยังไง (kun gɔ̂ɔ rúu wâa pǒm kít yang-ngai) You know how I feel (about you). 

13) ไม่ต้องก็ได้ (mâi-dtɔ̂ng gɔ̂ɔ-dâi) 

You don't have to. / No need to. / Don't bother.  

14) ไม่รู้จะซื้ออะไรดี (mâi-rúu jà sʉ́ʉ à-rai dii) 

I don't what to buy. / I don't know what I should buy.  

15) ทั้งวันทั้งคืน (táng wan táng kʉʉn) 

All day and all night / Night and day / Day and night  

16) อย่าลืมซื้อของฝาก (yàa lʉʉm sʉ́ʉ-kɔ̌ɔng fàak) 

Don't forget to buy me something. / Don't forget to buy me a souvenir.  

17) เพราะฉันใช่ไหม (prɔ́ chǎn châi-mǎi) 

Because of me, right? / Because of me?  

18) เทียบไม่ได้เลย / เทียบไม่ได้กับ... (tîap mâi-dâi ləəi / tîap mâi-dâi gàp...) It doesn't compare at all. / I'm no match for (him). / That's nothing compared to...  

19) คุณไม่มีสิทธิ์... (kun mâi mii sìt...) 

You have no right to... / You can't just...  

20) ไม่อยากเชื่อเลย (mâi yàak chʉ̂a ləəi) 

Unbelievable! / It's unbelievable.  

Super Useful Short Expressions, Part 15  

(high-beginner level) 

1) อย่าทำแบบนี้ (yàa tam bɛ̀ɛp-níi) 

Don't do this. 

2) อย่าทำแบบนั้น (yàa tam bɛ̀ɛp-nán) 

Don't do that.  

3) อย่าทำแบบนี้กับฉัน (yàa tam bɛ̀ɛp-níi gàp chǎn) Don't do this to me.  

4) อย่าทำแบบนี้อีก (yàa tam bɛ̀ɛp-níi ìik) Don't do it again. 

5) เงียบมาก (ngîap mâak) 

It's so quiet!  

6) เสียงดังมาก (sǐang-dang mâak) 

It's so loud!  

7) โชคดีนะ(ค่ะ/ครับ) chôok-dii ná (kà / kráp) Good luck. / Take care.  

8) ขอบใจ (kɔ̀ɔp-jai) 

Thanks. (use older to younger or between close friends)  

9) ออกไป (ɔ̀ɔk bpai) 

Get out of here! / Get out!  

10) ไม่ต้องพูด (mâi dtɔ̂ng pûut) 

Don't tell me (about it). / No need to mention that.  

11) ไม่อยากฟัง (mâi yàak fang) 

I don't wanna hear (it). 

12) เป็นไปไม่ได้ (bpen-bpai mâi-dâi) 

Impossible!; That's impossible!  

13) เขาเป็นเพื่อนฉัน (kǎo bpen pʉ̂an chǎn) He's my friend. 

14) นี่ไง (nîi ngai) 

Here it is. 

15) นี่ครับ/ค่ะ (nîi kráp / nîi kà) 

Here you are. (hands something to the listener)  

16) ยินดีเสมอ (yin-dii sà-mə̌ə) 

My pleasure. / You're more than welcome. / It's always a pleasure.  

17) มีเงินเก็บ (mii ngən-gèp) 

I have some money saved up.  

18) ปกติแล้ว... (bpòk-gà-dtì lɛ́ɛo...) 

Normally... / Usually... / Generally... / Typically...  

19) ไม่ต้องทอนนะ (mâi-dtɔ̂ng tɔɔn ná) 

Keep the change.  

20) รับอะไรเพิ่มไหม (ráp à-rai pə̂əm mǎi) 

Would you like anything else?  

Super Useful Short Expressions, Part 16  (low-intermediate level) 

1) ทำไม่เป็น (tam mâi-bpen) 

I don't know how (to do it).  

2) มีอะไรหรือเปล่า INFORMAL (mii à-rai rʉ̌ʉ-bplàao) 

What's the matter? / What's up? / What is it?  

3) เป็นอะไรของคุณ INFORMAL (bpen-à-rai kɔ̌ɔng kun) What's wrong with you?! / What happened to you?! / What's your problem?!  

4) จะให้ผมทำยังไง (jà hâi pǒm tam yang-ngai)

What do you want me to do? 

5) เขารับไม่ได้ (kǎo ráp mâi-dâi) 

She can't accept it. / She can't handle it. / She can't live with it.  

6) ไม่รู้ได้ยังไง INFORMAL (mâi rúu dâi yang-ngai) 

How could you not know? 

7) ก็ยังดีกว่า... (gɔ̂ɔ yang dii gwàa...) 

Well, that's (still) better than... / It beats...  

8) (แล้ว)จะเอายังไงต่อ (lɛ́ɛo) jà ao yang-ngai dtɔ̀ɔ 

(So) what now? / (So) what do we do now? / Now what?  

9) ไม่ต้องไปสนใจ(มัน) INFORMAL mâi-dtɔ̂ng bpai sǒn-jai (man) Don't let her bother you. / Don't pay any attention (to what she says). / Just  ignore her.  

Note: The pronoun มัน (man) is used in a derogatory way here, but it can also be casual.  

10) จะเอาเงินที่ไหน... (jà ao ngən tîi-nǎi...) 

Where are we gonna get the money to...? / How are we gonna afford...?  

11) ไม่ค่อยได้ใช้ (mâi-kɔ̂i dâi chái) 

I hardly ever use it. / I haven't used it very much. / I barely ever use it.  

12) เชื่อกันว่า... (chʉ̂a gan wâa...) 

It's believed that...  

13) ไม่ต้องสงสัย(ว่า...) mai-dtɔ̂ng sǒng-sǎi (wâa...) 

Obviously... / Without a doubt... / It's obvious (that...)  

14) แม่ขา (mɛ̂ɛ kǎa) 

Mommy... / Mom... (this is a cute way of a child addressing his/her mother)  15) ไม่อยากอยู่แล้ว (mâi yàak yùu lɛ́ɛo)

I want to die.  

16) มันผ่านไปแล้ว (man pàan bpai lɛ́ɛo) 

It's over. / It's in the past (now).  

17) เกิดอะไรขึ้น (gə̀ət à-rai kʉ̂n) 

What happened?  

18) นี่มันอะไรกัน STRONG (nîi man à-rai gan) 

What on earth is this?! / What the hell is this?!  

19) เขาเรียกตัวเองว่า... (kǎo rîak dtua-eeng wâa...) He calls himself "....".  

20) พวกเขาเรียกตัวเองว่า... (pûak-kǎo rîak dtua-eeng wâa...) They call themselves "....".  

Super Useful Short Expressions, Part 17  (high-beginner level) 

1) คุณดูดี (kun duu-dii) 

You look good.  

2) คุณดูดีมาก (kun duu-dii mâak) 

You look great.  

3) ขับรถดีๆ (นะ) (kàp-rót dii-dii ná) 

Drive safely.  

4) คุณพระช่วย (kun-prá-chûai) 

Oh, my god! / Goodness! / Oh no! / Gosh! 

5) ผมไปส่ง (pǒm bpai sòng) 

I'll see you out. / I'll walk you out. / I'll see you to the (door, car, etc. ). 

6) ฉันได้ยินมาว่า... (chǎn dâi-yin maa wâa...) 

I heard that... / I hear that...  

7) ไม่รู้มาก่อนว่า... (mâi-rúu maa-gɔ̀ɔn wâa...) 

I didn't know (before) that... / I never knew...  

8) จะไปไหนกัน (jà bpai nǎi gan) 

Where are you guys going? / Where are we going?  

9) ไม่เข้าใจ (mâi-kâo-jai) 

I don't understand.  

10) สวัสดีค่ะ / สวัสดีครับ (sà-wàt-dii kà / sà-wàt-dii kráp) Good-bye (or Hello)  

11) ตอนที่เป็นเด็ก... / ตอนเด็ก...* (dtɔɔn-tîi bpen dèk... / dtɔɔn dèk-dèk...*) When I was a kid/child/boy/girl...  

*Note: this is very casual.  

12) รอสักครู่(ครับ) rɔɔ-sàk-krûu (kráp) 

(Please) wait a moment/minute. / Just a moment (please).  

13) กรุณารอสักครู่(ค่ะ) FORMAL gà-rú-naa rɔɔ-sàk-krûu (kà)  (Please) wait a moment/minute. / Just a moment (please).  

14) เดี๋ยวฉันกลับมา(ค่ะ) dǐao chǎn glàp-maa (kà) 

I'll be right back.  

15) เสียงอะไร(คะ) (sǐang à-rai ká) 

What's that sound? / What was that?  

16) แตกไปแล้ว (dtɛ̀ɛk bpai lɛ́ɛo) 

It's broken. / It shattered.  

17) ใครๆก็รู้(...) (krai-krai gɔ̂ɔ rúu...)

Everyone knows (that...)  

18) ฉันชอบ... เหมือนกัน (chǎn chɔ̂ɔp... mʉ̌an-gan) 

I like... too.  

19) ไม่มีปัญหาหรอก(ค่ะ) mâi mii bpàn-hǎa rɔ̀ɔk (kà) 

It's not a problem at all. / I don't have a problem with it. / No problem at all.  

20) เราสนิทกันมาก (rao sà-nìt gan mâak) 

We're very close (good friends).  

Super Useful Short Expressions, Part 18  (low-intermediate level) 

1) พูดเล่นใช่ไหม (pûut-lên châi-mǎi) 

You're joking, right?  

2) ไม่ได้พูดเล่นนะ (mâi-dâi pûut-lên ná) 

I'm not joking.  

3) แกเคยพูดว่า... (gɛɛ kəəi pûut wâa...) 

You said (before) (that)... / You once said (that)...  

4) เสียใจด้วยจริงๆ(ค่ะ) sǐa-jai dûai jing-jing (kà) 

I'm so sorry to hear (about) that. (bad news) / My condolences.  

5) มันเป็นอุบัติเหตุ (man bpen ù-bpà-dtì-hèet) 

It was an accident.  

6) ผมจะทำให้ดีที่สุด (pǒm jà tam hâi dii-tîi-sùt) 

I'll do my best.  

7) เธอท้องได้ (3) เดือน(แล้ว) təə tɔ́ɔng dâi (sǎam) dʉan (lɛ́ɛo) She's (three) months pregnant. 

8) ขนาดนั้นเลยหรอ (kà-nàat nán ləəi rə̌ə) 

That much?! 

9) (นาน)ขนาดนั้นเลยหรอ (naan)kà-nàat nán ləəi rə̌ə That (long)?!  

10) โธ่เอ๋ย (tôo-ə̌əi) 

Oh man! / Oh shit! / Damn!  

11) อย่าเข้าใจผิดนะ (yàa kâo-jai-pìt ná) 

Don't misunderstand (me).  

12) ได้โปรดอย่าเข้าใจผิด FORMAL (dâi-bpròot yàa kâo-jai-pìt) Please don't misunderstand.  

13) คุณสัญญากับฉันแล้ว(ว่า)... kun sǎn-yaa gàp chǎn lɛ́ɛo (wâa)... You promised me. / You promised (that) you...  

14) ผมไม่ได้สัญญา(อะไร) pǒm mâi-dâi sǎn-yaa (à-rai) I didn't promise (anything).  

15) เราเหมือนกันมาก (rao mʉ̌an-gan mâak) 

We're very similar. / We're so much alike.  

16) หน้าตาเหมือนกันมาก (nâa-dtaa mʉ̌an-gan mâak) (They) look very similar. / (They) look so much alike.  

17) ฉันซื้อ(ขนม)มาฝาก chǎn sʉ́ʉ (kà-nǒm) maa fàak I bought some (sweets) for you.  

18) ครั้งเดียวไม่(เคย)พอ kráng-diao mâi (kəəi) pɔɔ Once is not (never) enough.  

19) อย่ามาโทษฉัน (yàa maa tôot chǎn) 

Don't blame me! 

20) อย่าโทษตัวเอง (yàa tôot dtua-eeng) 

Don't blame yourself.  

Super Useful Short Expressions, Part 19  (low-intermediate level) 

1) ว่าไง INFORMAL (wâa-ngai) 

What's up?  

Note: This has a lot of other meanings.  

2) ช่วยไม่ได้ (chûai mâi-dâi) 

It can't be helped. / I can't help it. / There's nothing I can do about it.  Note: This also means 'I can't help.'  

3) เขาหนีไปแล้ว (kǎo nǐi bpai lɛ́ɛo) 

He ran away (already) / He's escaped. / He's gone (ran away/escaped).  

4) ไปเลย INFORMAL/ROUGH (bpai ləəi) 

Go! / Go on! / Go ahead. 

5) เหม็น (měn) 

It stinks! / It smells bad.  

6) (...)(ก็)แค่นั้นเอง (...)(gɔ̂ɔ)kɛ̂ɛ-nán eeng 

That's all it is. / That's all. / ...is all.  

7) ฉันเคยถามเธอว่า... (chǎn kəəi tǎam təə wâa...) 

I('ve) asked you before.../ I used to ask you... 

8) เธอจะบอกว่า... (təə jà bɔ̀ɔk wâa...) 

Are you gonna tell me...? / Are you saying...?

9) ถึงเวลา(ที่)ต้อง... tʉ̌ng wee-laa (tîi) dtɔ̂ng... It's time to... / The time has come to...  

10) ไม่เคยฟัง (mâi kəəi fang) 

You never listen. / You never listen to me. 

11) รู้สึกไม่ค่อยสบาย (rúu-sʉ̀k mâi-kɔ̂i sà-baai) I don't feel so well. / I'm not feeling well. 

12) เป็นแค่งานอดิเรก (bpen kɛ̂ɛ ngaan-à-dì-rèek) It's just a hobby.  

13) ไว้ใจใครไม่ได้ (wái-jai krai mâi-dâi) 

You can't trust anyone.  

14) ขอตัวก่อนนะ(ครับ) FORMAL kɔ̌ɔ-dtua gɔ̀ɔn ná (kráp) (Please) excuse me (I have to go). / Goodbye.  

15) คิดไปเอง (kít bpai eeng) 

You're imagining things. / It's all in your head.  

16) ผมพูดความจริง (pǒm pûut kwaam-jing) I'm telling the truth.  

17) อย่าหาเรื่องนะ (yàa hǎa-rʉ̂ang ná) 

Don't make trouble.  

18) ขอเวลาส่วนตัว (kɔ̌ɔ wee-laa sùan-dtua) Please give (us/me) some time alone. 

19) ผมผิดเอง (pǒm pìt eeng) 

It was my fault. / It was my mistake. / My bad. 

20) ต่อไปนี้ (dtɔ̀ɔ-bpai-níi) 

From now on... 

Super Useful Short Expressions, Part 20  (high-beginner level) 

1) (น้อย)อยู่ไหน (nɔ́ɔi) yùu nǎi 

Where's (Noi )?  

2) เธอโสด (təə sòot) 

She's single. 

3) เธอเป็นแฟนคนแรก(ของผม) təə bpen fɛɛn kon-rɛ̂ɛk (kɔ̌ɔng pǒm) She was my first girlfriend.  

4) เธอเป็นคนขี้อาย(มาก) təə bpen kon kîi-aai (mâak) She's (really) shy. 

5) เธอก็เลย... (təə gɔ̂ɔ ləəi...) 

So you/she... 

6) นั่นอะไร(ครับ) nân à-rai (kráp) 

What's that?  

7) ฉันกลัวผีอ่ะ chǎn glua pǐi (à) 

I'm afraid of ghosts. 

Note: อ่ะ (à) is an informal, shortened form of ล่ะ (là).  

8) ผมไม่กลัวผี(หรอก) pǒm mâi glua pǐi (rɔ̀ɔk) 

I'm not afraid of ghosts (at all).  

9) ฉัน(ก็)ไม่คิดว่า... chǎn (gɔ̂ɔ) mâi kít wâa... 

I didn't think (that)... / I don't think (that)...  

10) มันมากเกินไป (man mâak-gəən-bpai) 

That's/It's too much. 

11) จำไม่ได้ (jam mâi-dâi) 

I can't remember.  

12) ผมเห็นกับตา (pǒm hěn-gàp-dtaa) 

I saw it myself. / I saw it with my own eyes.  

13) ยังไม่เคยไป(...) yang mâi-kəəi bpai (...) 

I've never been there. / I haven't been to... yet.  

14) ไม่น่าเชื่อ (ว่า...) mâi-nâa-chʉ̂a (wâa...) 

Unbelievable! / Incredible! / It's hard to believe...  

15) จะไปไหนกันดี (jà bpai-nǎi gan dii) 

Where should we go? / Where do you wanna go?  

16) ผม(ก็)ตัดสินใจแล้ว pǒm (gɔ̂ɔ) dtàt-sǐn-jai lɛ́ɛo I've decided. / I've made up my mind.  

17) พูดจริงเหรอ (pûut jing rə̌ə) 

Are you serious? (about what you are saying/what you told me)  

18) (โอเค)... คุณชนะ (oo-kee)... kun chá-ná (Okay)...You win. (I lose; I was wrong, etc.)  

19) (ผม)จะโทรไป(หา) (pǒm) jà too-bpai (hǎa) I'll call you.  

20) ตอนนี้เลย (dtɔɔn-níi ləəi) 

Right now. / Now!  

Super Useful Short Expressions, Part 21  (low-intermediate level) 

1) ลองเข้าไปดู (lɔɔng kâo bpai duu)

Go and have a look. / Go in and check it out.  

2) ไม่ต้องห่วงเรื่อง... (mâi dtɔ̂ng hùang rʉ̂ang...) 

Don't worry about... / You don't need to worry about... 

3) ไม่มีใครฟังฉัน(เลย) mâi mii krai fang chǎn (ləəi) No one listens to me. / No one's listening.  

4) ห้ามปฏิเสธ (hâam bpà-dtì-sèet) 

You can't say no. / You can't refuse. 

5) ฉันก็เหมือนกัน (chǎn gɔ̂ɔ mʉ̌an-gan) 

Me too. / So do I. / I do, too. 

6) (คุณ)ลืมอะไรไปหรือเปล่า (kun) lʉʉm à-rai bpai rʉ̌ʉ-bplàao Have you forgotten something, (or what)? / Did you forget something?  

7) ที่ไหนดี(ล่ะ) tîi-nǎi dii (lâ) 

Where's good? / Where do you want to (go)?  

8) ไม่จริงใช่ไหม (mâi-jing châi-mǎi) 

It's not true, right? / That's not true, is it?  

9) ไปรู้มาจากไหน (bpai rúu maa-jàak nǎi) 

Where did you find out? / Where did you learn (that)?  Note: This expression is a bit strong.  

10) จะกลับแล้วหรือ(ครับ) jà glàp lɛ́ɛo rʉ̌ʉ (kráp) 

Are you leaving? / Are you going back (now)?  

11) จะเอาไปทำไม (jà ao bpai tam-mai ) 

What are you gonna do with it? / What do you need it for?  

12) ทำไมทำกับฉันอย่างนี้ (tam-mai tam gàp chǎn yàang-níi) Why are you doing this to me? / Why are you treating me like this? 

13) ไม่ให้เกียรติ(ผู้หญิง) mâi-hâi gìat (pûu-yǐng) 

He doesn't respect (women).  

Note: The ร(r) in เกียรติ(gìat) is silent.  

14) สุดทนแล้ว (sùt ton lɛ́ɛo) 

I can't stand it any more! / This is unbearable!  

15) ไม่น่าเลย INFORMAL (mâi-nâa ləəi) 

It/This shouldn't have happened! / (He) shouldn't have done that! (meaning is  close to 'Unbelievable!') / Doggone it!  

16) เป็นเรื่องของครอบครัว (bpen rʉ̂ang kɔ̌ɔng krɔ̂ɔp-krua) It's a family matter.  

17) เข้าเค้า(นะ) IDIOM kâo-káo (ná) 

That makes sense. / That sounds reasonable. (=it matches the facts)  

18) แปลกจัง (bplɛ̀ɛk-jang) 

That's odd. / That's strange.  

19) พิสูจน์แล้ว (pí-sùut lɛ́ɛo) 

(He/They) proved it. / They've already proven (it).  

20) เท่ดี(นะ) INFORMAL têe-dii(ná) 

Cool! / Awesome! / Nice!  

Super Useful Short Expressions, Part 22  (high-intermediate level) 

1) ห้ามใจไม่ได้ (hâam-jai mâi-dâi) 

(He) couldn't resist. (He) couldn't help himself.

Note: This is often used for temptations.  

2) รวบรวมสติอน่อย/ไว้ (rûap-ruam sà-dtì nɔ̀ì/wái) 

Pull yourself together! / Compose yourself! / Try to gather your senses! /  Concentrate!  

3) ชอบยุ่งเรื่องชาวบ้าน (chɔ̂ɔp yûng rʉ̂ang chaao-bâan) He likes to mess in other people's business!  

4) ต้องเป็นฝีมือ (Dave) dtɔ̂ng bpen fǐi-mʉʉ (Dave) 

It had to be (Dave) (who did it)! / It must have been (Dave)!  Note: This is often used for mischief or sneaky actions. 

5) เราผ่านอะไรมาเยอะ (rao pàan à-rai maa yə́) 

We've been through a lot.  

6) ยากเหลือเกินที่จะ... SEMI-LITERARY/POETIC (yâak lʉ̌a-gəən tîi jà...) It's so difficult to...  

7) ตาต่อตาฟันต่อฟัน (dtaa dtɔ̀ɔ dtaa fan dtɔ̀ɔ fan) 

an eye for any eye, a tooth for a tooth / tit for tat  

8) คุณคนเดียวเท่านั้นที่... (kun kon-diao tâo-nán tîi...) 

You're the only one who...  

9) รัก(เขา)สุดหัวใจ SEMI-LITERARY/POETIC rák (kǎo) sùt hǔa jai I love her with all my heart.  

10) คุณพิสูจน์ตัวเองแล้ว(ว่า)... kun pí-sùut dtua-eeng lɛ́ɛo (wâa)... You've proven yourself (that)...  

11) แก้ไขไม่ได้แล้ว (gɛ̂ɛ-kǎi mâi-dâi lɛ́ɛo)  

It's irreparable. / It can't be fixed. 

12) ยิ่งไปกว่านั้น... SEMI-FORMAL (yîng bpai gwàa nán...) Moreover... / In addition... / Besides... / More than that... / What's more... /

On top of that...  

13) (มัน)ไม่ใช่อย่างที่คุณคิด(นะ) man mâi-châi yàang-tîi kun kít (ná) It's not like you think. / It's not what you think. / It's not what it looks like.  

14) ทุกคนมีสิทธิ์ที่จะเลือก (túk kon mii-sìt tîi jà lʉ̂ak) 

Everyone has the right to choose.  

15) ถ้าขาดเหลืออะไรบอกฉันได้เลยนะคะ (tâa kàat-lʉ̌a à-rai bɔ̀ɔk chǎn dâi leei ná-ká) 

If you need anything else, just let me know.  

16) ขอแสดงความเสียใจ(ด้วย)(นะ) FORMAL kɔ̌ɔ sà-dɛɛng kwaam-sǐa-jai (dûai) (ná) 

My (deepest) condolences. / I'm so sorry to hear about your loss.  

17) มากกว่าสิ่งใด LITERARY (mâak-gwàa sìng-dai) 

More than anything. 

18) เราเคยรู้จักกันมาก่อนหรือเปล่า (rao kəəi rúu-jàk-gan maa-gɔ̀ɔn rʉ̌ʉ bplàao) 

Have we met before?  

19) คงไม่(ครับ/ค่ะ) kong mâi (kráp/kà) 

I don't think so. / Probably not.  

20) ว่ากันว่า... (wâa-gan-wâa...) 

They say (that)... / It's said that...  

Super Useful Short Expressions, Part 23 

(low-intermediate level) 

1) คุณพูดอะไร (kun pûut à-rai) 

What are you talking about? / What are you saying? 

2) จะเอาอะไร INFORMAL (jà ao à-rai) 

What do you want?  

3) รู้ไหมทำไม(...) rúu mǎi tam-mai(...) 

Do you know why(...)?  

4) กินข้าวรึยัง (gin-kâao rʉ́-yang) 

Have you eaten yet?  

5) ทานข้าวรึยัง SEMI-FORMAL (taan-kâao rʉ́-yang) Have you eaten yet?  

6) คุณรู้เรื่องนี้ได้ยังไง (kun rúu-rʉ̂ang níi dâi yang-ngai) How did you find out? / How do you know about it/that? 

7) แล้วฉันล่ะ (lɛ́ɛo chǎn lâ) 

What about me?!  

Note: This shows surprise and has the effect of "Hey/Well/Wait..."  

8) เป็นยังไงบ้างลูก (bpen-yang-ngai bâang lûuk) How are you honey/sweetie?  

Note: This is said by a mother or father to their child.  

9) มันทำได้ยาก (man tam-dâi-yâak) 

It's hard to do.  

10) อาจเป็น... ก็ได้ (aat bpen... gɔ̂ɔ-dâi) 

It might/could be...  

11) ไม่มีเงินเลย (mâi mii ngən ləəi) 

I don't have any money (at all).  

12) อยากจะอ้วก (yàak jà ûak) 

I wanna throw up. / I'm gonna throw up. 

13) ขอบใจที่มา INFORMAL (kɔ̀ɔp-jai tîi maa) 

Thanks for coming. 

Note: Used casually to a person younger than you or to a child.  

14) ทำตัวตามสบายนะ (tam-dtua dtaam-sà-baai ná) 

Make yourself at home. / Make yourself comfortable.  

15) ฉันจะพาคุณไปเที่ยว (chǎn jà paa kun bpai-tîao) 

I'll take you around. / I'll show you around (=give you a tour). / I'll take you out.  

16) เธอคือคนที่ใช่ (təə kʉʉ kon-tîi-châi) 

She's the one (for me). 

17) (ฉัน)ออกเวรแล้ว (chǎn) ɔ̀ɔk-ween lɛ́ɛo 

My shift's over. / I'm off (now).  

18) เขารอผมอยู่ (kǎo rɔɔ pǒm yùu) 

He's waiting for me. / She's waiting for me.  

19) หมดเวลา(...)แล้ว mòt-wee-laa(...)lɛ́ɛo 

Time's up. / (X) is over.  

20) ฉันแทบไม่รู้จักเขา (chǎn tɛ̂ɛp mâi-rúu-jàk kǎo) 

I barely/hardly know him.  

Super Useful Short Expressions, Part 24  (high-intermediate level) 

1) เอาเข้าจริง... IDIOM (ao-kâo-jing...) 

When it comes down to it... / In actual practice.../ When you really do/try it.../  Really... / Actually... 

2) อย่ามาแตะต้องตัวฉัน (yàa maa dtɛ̀-dtɔ̂ng dtua chǎn) 

Don't touch me! / Keep your hands off me! / Don't Fxxxing touch me!  Note: this is quite strong and derogatory.  

3) (เรา)มีอะไรหลายๆอย่างเหมือนกัน (rao) mii à-rai lǎai-lǎai yàang mʉ̌an-gan) (We) have a lot in common.  

4) นี่อาจจะเป็นครั้งสุดท้าย(ที่)... nîi àat-jà bpen kráng-sùt-táai (tîi)... This might be the last time (that)...  

5) อย่างน้อยก็... (yàang-nɔ́ɔi gɔ̂ɔ...) 

(Well,) at least...  

6) ต้องไปแล้วจริงๆ (dtɔ̂ng bpai lɛ́ɛo jing-jing) 

I really got to go. / I really need to leave.  

7) อยู่ต่อไม่ได้เหรอ(คะ) yùu dtɔ̀ɔ mâi-dâi rə̌ə (ká) 

Can't you stay longer?  

8) ผมถูกกำหนดมา... (pǒm tùuk gam-nòt maa...) 

I was destined to...  

9) ไม่ดีเท่าที่ควร (mâi-dii tâo-tîi kuan) 

Not as good as it should be. 

10) เรารู้จักกันตั้งแต่สมัยเรียน (rao rúu-jàk-gan dtâng-dtɛ̀ɛ sà-mǎi rian) We've known each other since high school/college (etc.)  

11) ขอแนะนำว่า... (kɔ̌ɔ nɛ́-nam wâa...) 

I suggest (that)... / I recommend (that)... 

12) ผมก็รู้สึกเช่นนั้น FORMAL (pǒm gɔ̂ɔ rúu-sʉ̀k chên-nán) I feel the same way. / The feeling is mutual.  

13) (มัน)มีผลต่อ... (man) mii-pǒn-dtɔ̀ɔ... 

(It) affects/has an effect on... 

14) (น่ารัก)อะไรอย่างนี้ (nâa-rák) à-rai yàang-níi 

How (cute) you are! / What a (cute)...!  

15) พูดไม่ถูก (pûut mâi-tùuk) 

(I) don't know what to say. (at a loss for words) OR (I) didn't say it correctly.  

16) คิดไม่ออก (kít mâi-ɔ̀ɔk) 

(I) have no idea. / (I) can't think of it (right now).  

17) ป้องกันไม่ให้ (เกิดขึ้นอีก) bpɔ̂ng-gan mâi-hâi (gə̀ət-kʉ̂n ìik) Stop/prevent (it from happening again).  

18) ตลกสิ้นดี (dtà-lòk sîn-dii) 

That's ridiculous! / That's laughable! / What a joke! 

Note: this is a strong expression.  

19) (ผม)ตาสว่าง LITERARY (pǒm) dtaa-sà-wàang 

My eyes were opened. / I saw the light.  

20) นี่คือสาเหตุที่... SEMI-LITERARY (nîi kʉʉ sǎa-hèet tîi...) That's (the reason) why...  

Super Useful Short Expressions, Part 25  (low-intermediate level) 

1) คุณไม่เคยรักผมเลย (kun mâi-kəəi rák pǒm ləəi) 

You never loved me. / You were never in love with me.  

2) ฉันผิดไปแล้ว (chǎn pìt bpai lɛ́ɛo) 

I was wrong. / I make a mistake. 

3) เข้าไปเลย (kâo bpai ləəi)

Come (on) in. / Get in.  

4) ไม่รบกวนแล้ว(ค่ะ/ครับ) mâi róp-guan lɛ́ɛo (kà/kráp) 

I won't bother you anymore. / I'll let you get back to... (...what you were doing).  

5) (ฉัน)เชื่อเรื่อง... (chǎn) chʉ̂a rʉ̂ang... 

(I) believe in...  

6) (ฉัน)ไม่เชื่อเรื่อง... (chǎn) mâi chʉ̂a rʉ̂ang... 

(I) don't believe in...  

7) ขอบคุณอีกครั้ง(ครับ/ค่ะ) kɔ̀ɔp-kun ìik-kráng (kráp/kà) 

Thanks again. /Thank you again.  

8) ไม่มีใครรู้ (mâi-mii krai rúu) 

No one knows. / No one knew.  

9) (มัน)เป็นเรื่องเดียวกัน (man) bpen rʉ̂ang diao-gan 

It's the same thing (matter). / It's the same story.  

10) แค่แวะมาทักทาย (kɛ̂ɛ wɛ́ maa ták-taai) 

I just dropped/stopped by to say hello.  

11) ฉันจะพาไปเอง (chǎn jà paa bpai eeng) 

I'll take you (there) (myself).  

12) ต้องต่อคิว (dtɔ̂ng dtɔ̀ɔ kiu) 

You have to get/stand in line.  

13) ฉันเพิ่งกลับมาจาก(ญี่ปุ่น) chǎn pə̂ng glàp maa-jàak (yîi-bpùn) I just got back from (Japan).  

14) กลับกันได้แล้ว (glàp-gan dâi lɛ́ɛo) 

We should get going. / Let's go.  

15) ผมก็เลือกไปแล้ว (pǒm gɔ̂ɔ lʉ̂ak bpai lɛ́ɛo)

I've made my decision. / I've already chosen.  

16) นั่น(ก็)เพราะว่า... nân (gɔ̂ɔ) prɔ́-wâa... 

That's/It's because...  

17) ผมอดไม่ได้ที่จะ... (pǒm òt mâi-dâi tîi jà...) 

I can't help but...  

18) สมมุติว่า... (sǒm-mút wâa...) 

Suppose... / Let's say... 

19) ถ้าสมมุติว่า... (tâa sǒm-mút wâa...) 

Suppose if... / Let's say if... 

20) คุณ(กำลัง)มองหาใคร kun (gam-lang) mɔɔng-hǎa krai Who are you looking for?  

Super Useful Short Expressions, Part 26  (low-intermediate level) 

1) ผมเคยแอบชอบเธอ (pǒm kəəi ɛ̀ɛp-chɔ̂ɔp təə) 

I used to have a crush on her.  

Note: แอบปิ๊ง (ɛ̀ɛp-bpíng) is slang for 'have a crush on someone'.  

2) (หลง)รัก(เธอ)ตั้งแต่แรกเห็น (lǒng) rák (təə) dtâng-dtɛ̀ɛ rɛ̂ɛk hěn (I) fell in love with (her) at first sight/the first time I saw (her).  

3) เขานอกใจฉัน (kǎo nɔ̂ɔk-jai chǎn) 

He cheated on me.  

4) ต้องรู้ก่อนว่า... (dtɔ̂ng rúu gɔ̀ɔn wâa...) 

(I) (first) need to know... / (I) have to know... 

5) ฉัน(ก็)รู้สึกเหมือนกัน chǎn (gɔ̂ɔ) rúu-sʉ̀k mʉ̌an-gan 

I feel the same way.  

6) ไม่รู้เหมือนกัน (mâi-rúu mʉ̌an-gan) 

I don't know (either).  

7) ไม่ทราบเหมือนกันค่ะ FORMAL (mâi-sâap mʉ̌an-gan kà) I don't know (either).  

8) ไม่เก่งเท่าไหร่ (mâi gèng tâo-rài) 

I'm not so good at... / I'm not very good. (used for skills, sports, languages, etc.)  

9) ประมาณว่า... INFORMAL (bprà-maan wâa...) 

It's like... / Like... (appear or seem like... / be like...)  

10) ก็คงเป็น... (gɔ̂ɔ kong bpen...) 

(Well), it's probably/gotta be/must be...  

11) มีคำถามเกี่ยวกับ... (mii kam-tǎam gìao-gàp...) 

(I) have a question about...  

12) ใกล้จะจบแล้ว (glâi jà jòp lɛ́ɛo) 

(We) are almost finished. / It's almost over. / I'm graduating soon.  

13) เคล็ดลับคืออะไร (klét-láp kʉʉ à-rai) 

What's the trick/secret? (used for a special technique of doing something)  

14) เคล็ดลับ(ก็)คือ... klét-láp (gɔ̂ɔ) kʉʉ... 

The trick/secret is... 

15) ถ้ามีเวลาว่าง... (tâa mii wee-laa-wâang...) 

If (you) have free time...; If (you) are free/have spare time...  

16) ไม่ค่อยมีเวลา (mâi-kɔ̂i mii wee-laa) 

(I) don't have much time. / (I) didn't have much time. / (I)'m pressed for time. 

17) (มัน)ไม่ใช่สิ่งสำคัญ (man) mâi-châi sìng sǎm-kan It's not important. / It doesn't matter. / It's no biggie  

18) ...ไม่สมบูรณ์แบบ (...mâi sǒm-buun-bɛ̀ɛp) 

...isn't perfect. / ...is imperfect.  

19) (สวดมนต์)ก่อนนอนทุกคืน (sùat-mon) gɔ̀ɔn nɔɔn túk-kʉʉn (He prays) every night before going to bed. 

20) เขาได้รับรางวัลออสการ์ (kǎo dâi-ráp raang-wan-ɔ́ɔt-sà-gâa) He won an Oscar.  

Super Useful Short Expressions, Part 27  (high-beginner level) 

1) ฉันรักคุณ (chǎn rák kun) 

I love you. 

2) ผมก็รักคุณ (pǒm gɔ̂ɔ rák kun) 

I love you too.  

Note: ก็ (gɔ̂ɔ) here means also or too.  

3) ฉันรักเขา (chǎn rák kǎo) 

I love him. / I'm in love with him.  

4) ฉันกำลังมีความรัก (chǎn gam-lang mii kwaam-rák) I'm in love.  

5) ผมตกหลุมรักเธอ (pǒm dtòk-lǔm-rák təə) 

I fell in love with her.  

6) ไม่มีแฟน (mâi mii fɛɛn) 

(I) don't have a boyfriend/girlfriend. 

7) ไม่มีเจ้าของ (mâi mii jâo-kɔ̌ɔng) 

(He)'s not taken.  

8) ฉันยังไม่แต่งงาน (chǎn yang mâi dtɛ̀ng-ngaan) I'm not married (yet).  

9) ผมหย่าแล้ว (pǒm yàa lɛ́ɛo) 

I'm divorced.  

10) เขาแต่งงานแล้ว (kǎo dtɛ̀ng-ngaan lɛ́ɛo) 

He's married (already).  

11) แต่งงานกับผมนะ INFORMAL (dtɛ̀ng-ngaan gàp pǒm ná) Marry me. / Will you marry me?  

12) เราจะแต่งงานกัน (rao jà dtɛ̀ng-ngaan gan) We're getting married.  

13) ฉันเลิกกับเขาแล้ว (chǎn lə̂ək gàp kǎo lɛ́ɛo) I broke up with him.  

14) เราเลิกกัน(แล้ว) rao lə̂ək gan (lɛ́ɛo) 

We broke up.  

15) เขาทิ้งฉัน (kǎo tíng chǎn) 

He left me. / He dumped me.  

16) มันเป็นแค่ความฝัน (man bpen kɛ̂ɛ kwaam-fǎn) It was only/just a dream.  

17) มันเป็นฝันร้าย (man bpen fǎn-ráai) 

It was a nightmare.  

18) ทีละเล็ก(ทีละน้อย) tii-lá-lék (tii-lá-nɔ́ɔi) 

Little by little 

19) ไม่ยากเลย (mâi-yâak ləəi) 

It's not hard at all. / That wasn't (so) hard. 

20) ไม่เลวนะ (mâi-leeo ná) 

Not bad. / Not too bad.  

Super Useful Short Expressions, Part 28  (low-intermediate level) 

1) คุณเคย... (บ้าง)ไหม kun kəəi... (bâang) mǎi Have you ever... ?  

2) มีคนเคยบอกว่า... (mii kon kəəi bɔ̀ɔk wâa...) Someone once told me...  

3) เคยทำงานเป็น(แคชเชียร์) kəəi tam-ngaan bpen (kɛ́t-chia) I used to be a (cashier). / I used to work as a (cashier).  

4) ในชีวิตประจำวัน... (nai chii-wít bprà-jam-wan...) In everyday life... / In daily life...  

5) ...ในนาทีสุดท้าย (nai naa-tii sùt-táai) 

...at the last minute/moment.  

6) (มัน)ไม่ซับซ้อน (man) mâi sáp-sɔ́ɔn 

It's simple. / It's not complicated. / It's straightforward. 

7) เป็นเรื่องธรรมดา (bpen rʉ̂ang tam-má-daa) It's common. / It's normal. / It's natural.  

8) ไม่ธรรมดา (mâi tam-má-daa) 

(It's) uncommon/unusual/peculiar/strange.  

9) ไม่ได้มีแค่(ผม) mâi-dâi mii kɛ̂ɛ (pǒm)

It's not only (me).  

10) ไม่มีอะไรมาก (mâi mii à-rai mâak) 

Nothing much. / It's nothing (much).  

11) ดีใจด้วย INFORMAL (dii-jai dûai) 

Congrats!  

12) ลองอีกครั้ง(สิ) lɔɔng ìik-kráng (sì) 

Try (it) again. 

13) ฉันภูมิใจในตัว(เขา)มาก chǎn puum-jai nai dtua (kǎo) mâak I'm so proud of (him)!  

14) ฉันตื่นเต้นแทน(คุณ)(จริงๆ) chǎn dtʉ̀ʉn-dtên tɛɛn (kun) (jing-jing) I'm (so) excited for you! 

Note: แทน (tɛɛn) means 'in place of' or 'instead of'.  

15) ได้เวลาเสียที / ได้เวลา... เสียที (dâi wee-laa sǐa-tii / dâi wee-laa... sǐa-tii) About time! / It's about time (you)... !  

Note: เสียที (sǐa-tii) can be replaced with ซะที (sá-tii) to make it more casual. 

16) ไม่ต้องพูดถึง (...) mâi-dtɔ̂ng pûut-tʉ̌ng (...)  

Don't mention it. / No need to talk about that. / No need to talk about... 

17) เขาเสียชีวิตด้วย(โรคมะเร็ง) FORMAL/LITERARY kǎo sǐa-chii-wít dûai (rôok má-reng) 

He died of (cancer). / He passed away from (cancer).  

18) มันดึงดูดความสนใจ (man dʉng-dùut kwaam-sǒn-jai) 

It grabs your attention/interest.  

Note: ดึงดูด (dʉng-dùut) means attract, draw in, or allure.  

19) เต็มไปด้วย... (dtem-bpai-dûai...)

filled with... / full of... / loaded with... 

20) อย่ายอมแพ้ (yàa yɔɔm-pɛ́ɛ) 

Don't give up.  

Super Useful Short Expressions, Part 29  (low-intermediate level) 

1) จะพูดว่าอย่างไรดี (jà pûut wâa yàang-rai dii)  

What should I say? / How should I say it?  

2) ไม่รู้จะพูดว่าอย่างไร (mâi-rúu jà pûut wâa yàang-rai) (I) don't know what to say. / I didn't know what to say.  

3) ยากที่จะอธิบาย / อธิบายยาก (yâak tîi jà à-tí-baai / à-tí-baai yâak) It's difficult to explain.  

4) (...)อะไรอย่างนี้ INFORMAL (à-rai yàang-níi)  

Something like that. / ...something like that.  

5) (ก็)ไม่เชิง (gɔ̂ɔ) mâi-chəəng 

Not really. / Not exactly.  

6) ไม่ได้หมายความว่า... (mâi-dâi mǎai-kwaam wâa...) That/it doesn't mean (that)...  

7) ไม่ใช่(ค่ะ) mâi-châi (kà) 

No (it's not.) / Wrong. / Incorrect. / I don't think so.  

8) ผม(ก็)ไม่เข้าใจอยู่ดี pǒm (gɔ̂ɔ) mâi-kâo-jai yùu-dii I still don't get it. / I still don't understand.  

Note: อยู่ดี (yùu-dii) means still or anyway. 

9) ฟังไม่รู้เรื่อง (fang mâi-rúu-rʉ̂ang)  

(I) can't understand at all (what s/he's saying).  

10) (มัน)ย่อมาจาก... (man) yɔ̂ɔ-maa-jàak... 

It's short for... / It stands for... (example: UN=United Nations)  

11) (มัน)ขึ้นอยู่กับ(ว่า)... (man) kʉ̂n-yùu-gàp (wâa)... 

It depends on...  

12) ใช้เวลานานเท่าไหร่ (chái-wee-laa naan tâo-rai)  

How long does it take?  

13) ต้องใช้เวลา (dtɔ̂ng chái-wee-laa)  

It takes time.  

14) ใช้เวลานาน (chái-wee-laa naan)  

It takes a long time.  

15) จะใช้เวลาอีกประมาณ (5 นาที) jà chái-wee-laa ìik bprà-maan (hâa naa-tii) It'll take about (5) more (minutes).  

16) (มัน)ได้ผลจริงๆ (man) dâi-pǒn jing-jing 

It really works. / It really worked. / It was really effective.  

17) (...)เปลี่ยนอะไรไม่ได้ (...)bplìan à-rai mâi-dâi 

(You) can't change anything. / (You) can't make a difference. / ...isn't gonna  change anything.  

18) ไม่ใช่เรื่องยาก (mâi-châi rʉ̂ang yâak)  

It's not (that) hard. / It's a piece of cake.  

19) ทำอย่างนั้นทำไม (tam yàang-nán tam-mai)  

Why did you do that? / Why would you do that?  

20) ฟังดูประหลาด (fang-duu bprà-làat) 

It sounds strange/weird/peculiar. 

Super Useful Short Expressions, Part 30 

(high-beginner level) 

1) แน่นอน (nɛ̂ɛ-nɔɔn)  

Of course. / Sure. / Certainly.  

2) มานี่ (maa-nîi)  

Come here. / Come over here. 

Note: This is also used casually to mean 'Give it to me'.  

3) เร็วเข้า (reo-kâo)  

Come on! (hurry) / Hurry!  

4) ช้าๆ (cháa-cháa)  

Slowly! / Slow down. 

5) คุณชื่ออะไร(ครับ) kun chʉ̂ʉ à-rai (kráp) 

What's your name?  

6) ผมเกิด(เมื่อ)วันที่(5)(ธันวาคม) pǒm gə̀ət (mʉ̂a) wan-tîi (hâa) (tan-waa-kom) I was born on (December 5th). 

Note: With เมื่อ (mʉ̂a) the statement is more formal, and ธันวาคม (tan-waa kom) is just ธันวา (tan-waa) more casually.  

7) มีพี่ชาย(สาม)คน mii pîi-chaai (sǎam) kon  

I have (three) older brothers. 

8) มีพี่สาว(หนึ่ง)คน mii pîi-sǎao (nʉ̀ng) kon 

I have (one) older sister. 

9) มีน้องชาย(สอง)คน mii nɔ́ɔng-chaai (sɔ̌ɔng) kon 

I have (two) younger brothers. 

10) ตอนฉันอายุ(เจ็ด)ขวบ... dtɔɔn chǎn aa-yú (jèt) kùap When I was (seven) years old...  

Note: For ages over 10, use ปี (bpii) instead of ขวบ (kùap).  

11) เขายังเด็ก (kǎo yang dèk)  

He's still young. / He's just a kid.  

12) เรียนสาขาอะไร (rian sǎa-kǎa à-rai)  

What's your major? / What field are you studying?  

13) ฉันชอบอยู่คนเดียว (chǎn chɔ̂ɔp yùu kon-diao) I like being alone.  

14) หวัดดี INFORMAL (wàt-dii)  

Hi.  

15) ขอเดา(นะ) kɔ̌ɔ dao (ná)  

Let me guess.  

16) เดาไม่ถูก (dao-mâi-tùuk) 

(I) can't guess. / It's unpredictable.  

17) (มัน)เข้าใจยาก (man) kâo-jai yâak  

It's hard to understand.  

18) (มัน)เข้าใจง่าย (man) kâo-jai ngâai  

It's easy to understand.  

19) ราคาเท่าไหร่(ครับ) raa-kaa tâo-rài (kráp) How much is it? / How much is this?  

20) ...ไปทางไหนครับ/คะ (...bpai taang-nǎi kráp/ká) Which way is...? / How do I get to...? 

Super Useful Short Expressions, Part 31 

(low-intermediate level) 

1) ทำไมถึงชอบ(...) tam-mai tʉ̌ng chɔ̂ɔp (...)  

Why do you like (it/him, etc.)?  

2) ไม่ค่อยสนใจเรื่อง(ประวัติศาสตร์) mâi-kɔ̂i sǒn-jai rʉ̌ang (bprà-wàt-dtì-sàat) I'm not (so) interesting in (history).  

3) ไม่ดีเท่าไหร่ (mâi-dii tâo-rài)  

(It's) not so good. 

4) ไม่ไกลจาก(โรงแรม) mâi-glai jàak (roong-rɛɛm)  

(It's) not far from (the hotel). 

5) ใครจะ(มา)สนใจ INFORMAL krai jà (maa) sǒn-jai 

Who cares? / Who would be interested?  

Note: This expression is rather strong.  

6) มันเกี่ยวกับอะไร (man gìao-gàp à-rai)  

What's it about? (movie, book, etc.)  

7) ฉันยังสงสัยว่าทำไม (chǎn yang sǒng-sǎi wâa tam-mai)  

I still wonder why. 

8) ค่อนข้างเยอะ (kɔ̂n-kâang yə́)  

(There's) quite a few/pretty much/quite a lot.  

9) ช่างมัน(เถอะ) châng-man (tə̀)  

(Just) forget it. / So be it. / Never mind. / To hell with it.  

10) (แล้ว)ผลเป็นยังไง INFORMAL (lɛ́ɛo) pǒn bpen-yang-ngai What was the result? / How did it turn out? 

Note: More formal would be เป็นอย่างไร (bpen-yàang-rai).  11) ไม่รู้ว่าเขาไปไหน (mâi-rúu wâa kǎo bpai nǎi) I don't know where he went. 

12) ไม่มีใครบอกได้ว่า... (mâi mii krai bɔ̀ɔk dâi wâa...) No one can tell... / No one can say...  

13) ...ถ้าจำเป็น... (...tâa jam-bpen...)  

If necessary... / ...if needed. / If (I) have to...  

14) ยังไม่ได้ไป (yang mâi-dâi bpai)  

(He) hasn't been there yet. / (He) hasn't left/gone.  

15) เป็นยังไงบ้าง INFORMAL (bpen yang-ngai bâang) How are things going? / How is it? / How is she?  

16) เป็น(หนังสือ)ที่ขายดีที่สุด bpen (nǎng-sʉ̌ʉ) tîi kǎai dii-tîi-sùt It was a bestseller. / It was the bestselling (book).  

17) คนให้เข้ากัน (kon hâi-kâo-gan)  

Mix/blend/scramble them together. 

18) ปรุงด้วย(ซีอิ๊วขาว) bprung dûai (sii-íu-kǎao) Season with (light soy sauce). 

19) ห้ามสูบบุหรี่ (hâam sùup-bù-rìi)  

No smoking (allowed).  

20) เชื่อเถอะ (chʉ̂a-tə̀)  

Trust me. / Believe me.  

Super Useful Short Expressions, Part 32  (high-intermediate level)

1) อยู่ดีๆ... (yùu-dii-dii...)  

All of the sudden... / Suddenly... / Unexpectedly... / Then out of nowhere...  

2) ซึ่งแปลว่า... (sʉ̂ng bplɛɛ-wâa...)  

Which means...  

3) อย่างไรก็ตาม... FORMAL (yàang-rai-gɔ̂ɔ-dtaam)  

However... / Nevertheless... 

4) ดังนั้น... SEMI-FORMAL (dang-nán)  

Therefore... / So...  

5) เรียกง่ายๆ(ว่า)... rîak ngâai-ngâai (wâa)... 

...for short / Also called... / Otherwise known as...  

6) ไม่มีหลักฐานอะไร(เลย) mâi-mii làk-tǎan à-rai (ləəi)  

There's no proof/evidence (at all).  

7) ซ้ำร้าย... (sám-ráai...)  

(And) worse (yet)... / Moreover... / In addition...  

Note: This carries a negative meaning and isn't the same as a neutral  'moreover'.  

8) ขึ้นต้นด้วย (P) kʉ̂n-dtôn-dûai (P)  

It starts/begins with (the letter P).  

9) แต่มีข้อแม้(ว่า...) dtɛ̀ɛ mii kɔ̂ɔ-mɛ́ɛ (wâa...)  

But under one condition(...) / But there's one stipulation.  

10) เป็นสัญลักษณ์ของ... (bpen sǎn-yá-lák kɔ̌ɔng...)  

It's a symbol of... / It symbolizes... / It's a sign of...  

11) เขาได้รับแรงบันดาลใจจาก(แม่ชีเทเรซ่า) (kǎo dâi-ráp rɛɛng-ban-daan-jai  jàak (mɛ̂ɛ-chii tee-ree-sâa))  

She was inspired by (Mother Teresa). 

12) ไม่มีทางรักษา (mâi-mii taang rák-sǎa)  

There's no cure. / It's not treatable.  

13) มีไม่กี่คน(ที่)... SEMI-LITERARY mii-mâi-gìi kon (tîi)... Not many... / Few people... / There are (very) few who...  

14) (มัน)นานมาแล้ว(...) SEMI-LITERARY (man) naan-maa-lɛ́ɛo(...) (It) (was) a long time ago. / Long ago...  

15) ยิ่งไปกว่านั้น... SEMI-LITERARY (yîng-bpai-gwàa-nán...) Moreover... / In addition... / Besides... / Also...  

16) ...อีกด้วย (...ìik-dûai)  

...also/as well/too  

17) ไม่เห็นจะยาก (mâi hěn jà yâak)  

It's not so hard. / It can't be that hard, can it?  

18) เพิ่มสีสันให้(กับ)(ชีวิต) SEMI-LITERARY pə̂əm sǐi-sǎn hâi (gàp)(chii-wít) Add some color to (your life). / Brighten/spice up (your life).  

19) ปฏิเสธไม่ได้ว่า... (bpà-dtì-sèet mâi-dâi wâa...)  

I can't deny... / You can't deny...  

20) โดยส่วนตัว(ฉันไม่เห็นด้วย) dooi-sùan-dtua (chǎn mâi-hěn-dûai) Personally, (I don't agree).  

Super Useful Short Expressions, Part 33 

(high-beginner level) 

1) ไม่เป็นไร (mâi-bpen-rai)  

It's okay. / It's fine. / It's no problem. / It's all right. / You're welcome. / Never mind.  2) สบายดี (sa-baai-dii) 

I'm fine. / I'm good. / Fine. / Good. 

3) คุณล่ะ(คะ) kun lâ (ká)  

And you? / How about you?  

4) นี่ของคุณ (nîi kɔ̌ɔng kun)  

This is yours.  

5) นี่ของคุณใช่ไหม (nîi kɔ̌ɔng kun châi mǎi)  

Is this yours? / This is yours, right?  

6) คุณครับ... / คุณคะ... FORMAL (kun kráp.../ kun ká...) Excuse me (sir/madam)... / Sir? / Madam? (used to call attention to a stranger).  

7) ทำไม่ได้(ครับ) tam mâi-dâi (kráp)  

(I) can't (do it). / It can't be done.  

8) คุณจะแนะน(คอร์ส)ไหน(คะ) kun jà nɛ́-nam (kɔ́ɔt ) nǎi (ká) Which (course) do you recommend?  

9) มันไม่เหมือนกัน (man mâi mʉ̌an-gan)  

It's not the same. / It's different. 

10) ...บ่อยแค่ไหน (...bɔ̀i kɛ̂ɛ-nǎi)  

How often...?  

11) ยินดีต้อนรับค่ะ (yin-dii dtɔɔn-ráp kà)  

Welcome.  

12) หิวไหม (hǐu-mǎi)  

Are you hungry?  

13) นี่สำคัญ(มาก) nîi sǎm-kan (mâak) 

It's (very) important. / This is (very) important.  

14) ผมมีปัญหานิดหน่อย (pǒm mii bpan-hǎa nít-nɔ̀i)  

I have a bit of a problem. 

15) ฉันรู้สึกไม่ดี (chǎn rúu-sʉ̀k mâi-dii)  

I have bad feelings (about that). / I feel bad.  

16) อย่าขยับ INFORMAL (yàa kà-yàp)  

Don't move!  

17) ไม่ค่อยชอบ (mâi kɔ̀i chɔ̂ɔp)  

(I) don't like it so much. / (I) don't really like it.  

18) ฉันชินแล้ว (pǒm chin lɛ́ɛo)  

I'm used to it.  

19) ฝนตกหนัก (fǒn-dtòk nàk)  

It's raining hard.  

20) ไม่ต้องห่วง (mâi-dtɔ̂ng hùang)  

Don't worry. / No need to worry.  

Super Useful Short Expressions, Part 34  (low-intermediate level) 

1) รอสักครู่ (mâ)  

Just a minute. / Hold on a minute.  

2) กรุณารอสักครู่ครับ FORMAL (mâ)  

One moment please. / Hold on a moment please. 

3) ผมจะบอกอะไรบางอย่าง (mâ)  

I want to tell you something. 

4) กำลังรอให้... (mâ)  

I'm waiting for... 

5) หลังจากนั้นไม่นาน... (mâ)  

Shortly after that... / Not long after that...  

6) (มัน)ง่ายนิดเดียว INFORMAL (mâ)  

It's super easy. / It's very simple. / It's a cinch.  

7) เป็นมากกว่า(เพื่อน) (mâ)  

(He)'s more than (a friend).  

8) มันหายไปได้ยังไง INFORMAL (mâ)  

How could it just vanish/disappear?!  

9) ลืมไปได้ยังไง INFORMAL (mâ)  

How could I forget?!  

10) ฉันก็เป็นเหมือนกับคุณ (mâ)  

I'm the same as you. / I'm like you.  

11) เขาทำเงินได้ดี (mâ)  

He makes good money; He gets a good salary.  

12) เข้าใจแล้ว (mâ)  

I see. / I understand.  

13) เป็นสถานที่ท่องเที่ยว (mâ)  

It's a tourist attraction.  

14) ทำด้วย(ไม้) (mâ)  

It's made of (wood).  

15) รับอะไรดี(ครับ) (mâ)  

What would you like? / What can I get you? (at a restaurant, etc.) 

16) รวมทั้งหมด (450) บาด(ครับ) (mâ)  

That will be (450) baht altogether.  

17) (ถังขยะ)อยู่ตรงนั้น (mâ) 

(The trash can) is over there.  

18) ได้ยินไหม (mâ)  

Did you hear (that)? / Hear that?  

19) น่าทึ่งใช่ไหมล่ะ (mâ)  

Amazing, isn't it? / It's amazing! / Impressive, huh?  

20) เสียใจด้วย (mâ)  

I'm sorry to hear that.  

Super Useful Short Expressions, Part 35 

(low-intermediate level) 

1) ผมไม่แปลกใจ(เลย) pǒm mâi bplɛ̀ɛk-jai (ləəi)  

I'm not surprised (at all). 

2) พูดเป็นเล่น INFORMAL (pûut bpen lên)  

Are you kidding me?! / Are you joking? / You can't be serious. / Are you joshing  me?  

3) ฉันพูดกับตัวเอง(ว่า...) chǎn pûut gàp dtua-eeng (wâa...) I said to myself "....".  

4) เขาพยายามอย่างหนัก (kǎo pá-yaa-yaam yàang-nàk)  

He tried hard.  

5) ไม่ว่าคุณจะอยู่ที่ไหน (mâi-wâa kun jà yùu-tîi-nǎi)  

Wherever you are. / No matter where you are.  

6) ผมก็แค่รู้ว่า... (pǒm gɔ̂ɔ kɛ̂ɛ rúu-wâa...)  

I only know... / I just know... 

7) มีเรื่องอยากจะถาม(คุณ) mii rʉ̂ang yàak jà tǎam (kun) I have something I want to ask you (about).  

8) อย่ามาหลอกฉัน (yàa maa lɔ̀ɔk chǎn)  

Don't try to trick me. / Don't try to fool me. 

9) ไม่ได้เจอกันตั้งนาน (mâi-dâi jəə-gan dtâng naan) We haven't met for a long time. / I haven't see you in ages.  

10) (ช่วย)เปิดประตูหน่อย(ครับ) (chûai) bpìt bprà-dtuu nɔ̀i (kráp) Open the door, (please).  

11) ฉันดีใจมากที่(คุณกลับมา) chǎn dii-jai mâak tîi (kun glàp maa) I'm so glad that (you're back).  

12) ผมท(ไวน์)หก pǒm tam (wai) hòk 

I spilled (the wine).  

13) ชื่อคุณเพราะดีนะ (chʉ̂ʉ kun prɔ́-dii ná)  

Your name's beautiful. / I like the sound of your name.  

14) เคยสังเกตไหม(ว่า...) kəəi sǎng-gèet mǎi (wâa...) Have you ever noticed (...)?  

15) อีกครึ่งชั่วโมง (ìik krʉ̂ng chûa-moong)  

In half an hour.  

16) ผมก็เลยตัดสินใจว่า... (pǒm gɔ̂ɔ ləəi dtàt-sǐn-jai wâa...) So I decided to...  

17) (ตั้งแต่)เช้าถึงเย็น (dtâng-dtɛ̀ɛ) cháao tʉ̌ng yen (From) morning to night.  

18) มันน่าสนใจ(มาก) man nâa-sǒn-jai (mâak)  

That's/It's (very) interesting.  

19) ตื่นเถอะ INFORMAL (dtʉ̀ʉn tə̀) 

Wake up!  

20) ไปได้แล้ว INFORMAL (bpai dâi lɛ́ɛo)  

Time to go. / Let's go. / Let's get going. 

Super Useful Short Expressions, Part 36  (low-intermediate level) 

1) อย่าโกรธฉัน(เลยนะ) yàa gròot chǎn (ləəi ná) 

Don't get mad at me. / Please don't be angry.  

2) ผมจะดูให้ (pǒm jà duu hâi) 

I'll have a look. / I'll take a look.  

3) ขากลับ(นั่งแท็กซี่) kǎa-glàp (nâng tɛ́k-sîi) 

(I took a taxi) on the way back/on the return trip.  

4)  ...ไม่ต่างกัน(หรอก) mâi-dtàang-gan (rɔ̀ɔk) 

...are no different (at all). / ...aren't different (at all). 

5) เอาไงต่อ INFORMAL (ao-ngai-dtɔ̀ɔ) 

What's next? / What now?  

Note: ไง (ngai) is a shortened, more casual form of อย่างไร (yàang-rai).  

6) ปล่าว(ครับ/ค่ะ) bplàao (kráp/kà) 

No. (that's not why/that's not it, etc.) / Nope.  

7) เราทำแบบนี้ทำไม / เราทำอย่างนี้ทำไม (rao tam bɛ̀ɛp-níi tam-mai / rao tam yàang-níi tam-mai) 

Why are we doing this?  

8) มันเรื่องเข้าใจผิด(กัน) man rʉ̂ang kâo-jai-pìt (gan)

It was/is a misunderstanding.  

9) (อันนี้)ผมยอมรับ (an-níi) pǒm yɔɔm-ráp 

I admit it. / I admit that.  

10) เพราะกลัวว่า... (prɔ́ glua-wâa...) 

Because (I) was afraid (that)...  

11) (ร้าน)ปิดกิจการ(ไป)แล้ว (ráan) bpìt gìt-jà-gaan (bpai) lɛ́ɛo (The store) shut down/went out of business.  

12) ผมกำลังหางานท (pǒm gam-lang hǎa-ngaan tam) I'm looking for a job/work.  

13) มันกลายเป็นว่า... (man glaai-bpen wâa...) 

It turns out (that)... / Turns out...  

14) ไม่เกียวกับ...เลย (mâi-gìao-gàp...ləəi) 

It has nothing to do with... 

15) อยากเรียนต่อ (yàak rian-dtɔ̀ɔ) 

(I) want to go to college. / (I) want to continue (my) studies.  

16) เรียนไปด้วยทำงานไปด้วย (rian bpai-dûai tam-ngaan bpai-dûai) Work and go to school (at the same time). / Work while going to school.  

17) ถ้ามีโอกาส... (tâa mii oo-gàat...) 

If I have a chance... / If you get a chance..  

18) ใครบอกให้... (krai bɔ̀ɔk hâi...) 

Who told you...?  

19) อย่าท้อแท้ (yàa tɔ́ɔ-tɛ́ɛ) 

Don't be discouraged! / Don't give up!  

20) อธิบายไม่ถูก (à-tí-baai mâi-tùuk) 

I can't explain it. / It's hard to explain. 

Super Useful Short Expressions, Part 37  (low-intermediate level) 

1) ขอบคุณสำหรับทุกอย่าง(นะคะ) kɔ̀ɔp-kun sǎm-ràp túk-yàang (ná-ká) Thanks for everything. / Thank you for everything.  

2) ทำไมมอง(พ่อ)แบบนั้น tam-mai mɔɔng (pɔ̂ɔ) bɛ̀ɛp-nán Why are you looking at (me) like that? (as spoken by a father to his son) 

3) อย่าบอก(แม่)นะ yaa bɔ̀ɔk (mɛ̂ɛ) ná 

Don't tell (Mom).  

4) ปู่ย่าตายาย bpùu-yâa-dtaa-yaai 

grandparents (=grandfather+grandmother+grandfather+grandmother) 

5) เดี๋ยวก่อน dtǐao-gɔ̀ɔn 

Wait. / Wait a minute. / Just a minute.  

6) ฉันได้เจอเพื่อนใหม่ chǎn dâi jəə pʉ̂an mài 

I got to meet/make new friends.  

7) ไว้ค่อยเจอกัน INFORMAL wái kɔ̂i jəə-gan 

(I'll) see you later. / See you again.  

8) ดูเหมือนว่า... duu mʉ̌an wâa... 

It looks like... / It seems that...  

9) สวัสดีตอนบ่าย FORMAL/LITERARY sà-wàt-dii dtɔɔn-bàai Good afternoon.  

10) ตั้งใจเรียนนะ dtâng-jai rian ná 

Study hard. 

11) ไม่อยากฟัง mâi-yàak fang 

I don't want to hear (it). / I'm not listening.  

12) (ผม)อยู่กับเธอเสมอ (pǒm) yùu-gàp təə sà-mə̌ə 

(I)'m always with you. / (I)'ll always be with you. / (I)'ll always be there for you.  

13) (เซ็น)ตรงนี้ครับ sen dtrong-níi kráp 

(Sign) right here. 

14) กำลังจะหย่ากัน gam-lang jà yàa-gan 

(They)'re getting a divorce.  

15) ฉันไม่รู้(ว่า)คุณเป็นใคร chǎn mâi-rúu (wâa) kun bpen krai I don't know who you are.  

16) งั้นหรือ INFORMAL ngán-rʉ̌ʉ 

Is that so? / Really?  

Note: This is a shortened, informal form of อย่างนั้นหรือ (yàang-nán-rʉ̌ʉ). 

17) ใช่แล้ว châi-lɛ́ɛo 

That's right. / Yep. / Yeah (that's right).  

18) มีแค่นี้ mii kɛ̂ɛ-níi 

That's it. / This is it. / That's all (we) have.  

19) นั่นสินะ nân-sì ná 

That's right. / You're right. / Right.  

20) คุณเดือดร้อนแน่(ๆ) kun dʉ̀at-rɔ́ɔn nɛ̂ɛ-(nɛ̂ɛ) 

You're in big trouble! / You're in deep shit! 

Super Useful Expressions, Part 38 

(low-intermediate level)

1) ฉันหวังว่าคุณ(คง)เข้าใจ chǎn wǎng-wâa kun (kong) kâo-jai I hope you understand.  

2) อย่า(มา)ขัดจังหวะ yàa (maa) kàt-jang-wà 

Don't interrupt. / Don't interrupt me.  

3) มีอะไรให้(ผม)ช่วยไหมครับ mii à-rai hâi (pǒm) chûai mǎi kráp Is there something I can help you with?  

4) ขอลายเซ็นได้ไหม(ครับ) kɔ̌ɔ laai-sen dâi-mǎi (kráp) 

Can I have your autograph?  

5) ได้สิ(คะ/ครับ) dâi sì (ká/kráp) 

Sure. / Yes. / Certainly. (you can) 

6) (...)ยังว่างอยู่ (...)yang wâang-yùu 

(...) is still available. / (...) is still open.  

7) ทางเดียวที่จะ(รอด)คือ... taang-diao tîi jà (rɔ̂ɔt) kʉʉ... 

The only way to (survive) is...  

8) คุณจะรู้สึกยังไง(ถ้า...) kun jà rúu-sʉ̀k yang-ngai (tâa...) How would you feel (if...)?  

Note: ยังไง (yang-ngai) is a shortened, more casual form of อย่างไร (yàang rai). 

9) เอาไปเลย ao bpai ləəi 

(Just) take it. / (Just) keep it.  

10) สวัสดีตอนค่ SEMI-FORMAL sà-wàt-dii dtɔɔn-kâm 

Good evening.  

11) ยินดีรับใช้(ค่ะ/ครับ) FORMAL yin-dii ráp-chái (kà/kráp) At your service. / Welcome. 

12) มีรายได้เดือนละ (30,000) บาท mii raai-dâi dʉan lá (sǎam-mʉ̀ʉn) bàat (I) make (30,000) baht per month. / I have a monthly income of (30,000) baht. 

13) ฉันจะพยายามเต็มที่ chǎn jà pá-yaa-yaam dtem-tîi 

I'll do my best. / I'll try my hardest.  

14) คุณน่าจะโทรบอก kun nâa-jà too-bɔ̀ɔk 

You should have called. (and told me) 

15) ฉันจะโทรบอกเขา chǎn jà too bɔ̀ɔk kǎo 

I'll call him (and let him know).  

16) (คุณพ่อ)บอกเสมอว่า... (kun pɔ̂ɔ) bɔ̀ɔk sà-mə̌ə wâa... (My father) always said... / (My father) always used to say...  

17) เดินทางปลอดภัยนะ dəən-taang bplɔ̀ɔt-pai ná 

Have a safe trip.  

18) หน้าตาเหมือน(ดารา) nâa-dtaa mʉ̌an (daa-raa) 

He looks like a (movie star).  

19) ขอให้สนุกนะ kɔ̌ɔ-hâi sà-nùk ná 

Have a good time. / Enjoy (yourself). / Have fun.  

20) มาสายไปหน่อย maa sǎai bpai nɔ̀i 

I'm a bit late.  

Super Useful Expressions, Part 39 

(high-intermediate level) 

1) หน้าตาคล้ายกัน nâa-dtaa kláai-gan 

(They) look alike/similar. 

2) มีลักษณะเหมือน... SEMI-FORMAL mii lák-sà-nà mʉ̌an... (It) looks like/is similar to... (describing appearance) 

3) รูปร่างคล้าย(หัวใจ) rûup-râang kláai (hǔa-jai) 

(heart)-shaped / shaped like a (heart)  

4) (มัน)ใช้เพื่อ(การตกแต่ง) (man) chái pʉ̂a (gaan-dtòk-dtɛ̀ng) (It)'s used for (decoration).  

5) เมื่อเทียบกับ... FORMAL mʉ̂a tîap gàp... 

(When) compared to...  

6) หนึ่งในนั้นคือ... nʉ̀ng nai nán kʉʉ 

One of them is/was...  

7) แต่โชคร้าย... dtɛ̀ɛ chôok-ráai... 

(But) unfortunately...  

8) จนในที่สุด... SEMI-FORMAL jon nai-tîi-sùt... 

Eventually... / (Until) finally...  

9) เราไม่ประสบความสำเร็จ SEMI-FORMAL rao mâi bprà-sòp kwaam-sǎm-rèt We weren't successful. / We were unsuccessful.  

10) (ใน)สมัยนั้น (nai) sà-mǎi nán 

Back then / At that time / In the old days  

11) บนโลกใบนี้ bon lôok bai níi 

in this world / on Earth / on this planet  

12) ไม่ต้องพูดถึง... mâi-dtɔ̂ng pûut-tʉ̌ng... 

not to mention... / let alone...  

13) ไม่ได้จำกัดเฉพาะ/แค่... mâi-dâi jam-gàt chà-pɔ́/kɛ̂ɛ... 

(It's) not limited to only/just...  

14) ครั้งหนึ่ง(ฉัน)เคย... FORMAL/LITERARY kráng-nʉ̀ng (chǎn) kəəi...

Once (I)... 

15) ได้รับแรงบันดาลใจจากเรื่องจริง dâi-ráp rɛɛng-ban-daan-jai jàak rʉ̂ang-jing inspired by a true story  

16) นับเป็นเกียรติ(อย่างยิ่ง) FORMAL náp-bpen-gìat (yàang-yîng) It's an honor. / It's a great honor. 

17) ไปถึงไหนแล้ว bpai-tʉ̌ng nǎi lɛ́ɛo 

How far have you gotten? / Where are you (now)? / 

How's it going? (asked about one's progress) 

18) คงเป็นเพราะ... kong bpen prɔ́... 

It's probably because... / It could be because...  

19) ไหนๆก็มาแล้ว nǎi-nǎi gɔ̂ɔ maa lɛ́ɛo 

Now that you're here... / Since you're here...  

20) คนเราผิดพลาดกันได้ kon-rao pìt-plâat-gan dâi 

Everyone makes mistakes. / We all make mistakes.  

Super Useful Expressions, Part 40 

(low-intermediate level) 

1) คุณคิด(ว่า)ยังไง kun kít (wâa) yang-ngai 

What do you think?  

2) คิดว่าไง INFORMAL kít wâa ngai 

What do you think?  

3) เกิดขึ้นได้ตลอด(เวลา) gə̀ət-kʉ̂n dâi dtà-lɔ̀ɔt (wee-laa) 

(It) happens all the time. / (It) can happen at any time. 

4) ขอบคุณที่ใช้บริการ(...) FORMAL kɔ̀ɔp-kun tîi chái bɔɔ-rí-gaan (...) Thank you for using our service. / Thank you for shopping at... / Thank you for  staying at...  

5) ดีใจที่ได้รู้จัก(ครับ/ค่ะ) FORMAL dii-jai tîi dâi-rúu-jàk (kráp/kà) It's nice to meet you. / It was nice meeting you. 

Note: This is mostly used in formal situations.  

6) ยินดีที่ได้รู้จักคุณ(ครับ/ค่ะ) FORMAL yin-dii tîi dâi-rúu-jàk kun (kráp/kà) It's a pleasure to meet you. / It's nice to meet you. 

Note: This is mostly used in formal situations.  

7) ถามอะไรหน่อยสิ INFORMAL tǎam à-rai nɔ̀i sì 

Tell me something. / Can I ask you something?  

8) (มัน)ทำให้ฉันอึดอัด (man) tam-hâi chǎn ʉ̀t-àt 

(He) makes me feel uncomfortable. 

9) ไปนั่งรถเล่นกัน(ครับ) bpai nâng-rót-lên gan (kráp) 

Let's go for a ride.  

10) ถ้าต้องการอะไร... tâa dtɔ̂ng-gaan à-rai... 

If (you) need anything...  

11) อย่าลืมสิ(ครับ/คะ) yàa lʉʉm sì (kráp/ká) 

Don't forget (it/that). 

12) ต้องฉลอง(กัน) dtɔ̂ng chà-lɔ̌ɔng (gan) 

(We) should/need to celebrate.  

13) (รีโมท)เสีย (rii-mòot) sǐa 

The (remote)'s not working. / The (remote)'s broken.  

14) ผมไม่คุ้นกับ... pǒm mâi-kún-gàp... 

I'm not familiar with... / I'm not used to...

15) (น้)เย็นเฉียบ (náam) yen-chìap 

(The water)'s freezing/ice cold. 

16) เขาเรียกว่า... kǎo rîak-wâa... 

It's called... / They're called... / It's what you call...  

17) (มัน)อยู่ตรงไหน (man) yùu dtrong-nǎi 

Where is (it) exactly? / Where is (it)?  

18) อยากรู้ว่า(มัน)คืออะไร yàak rúu-wâa (man) kʉʉ à-rai I wanna know what it is. / I wanna know what that is.  

19) ผมรู้ว่า(มัน)คืออะไร pǒm rúu-wâa (man) kʉʉ à-rai I know what it is. / I know what that is.  

20) ไม่รู้ว่า(มัน)คืออะไร mâi-rúu-wâa (man) kʉʉ à-rai I don't know what it is. / I don't know what that is. 

Super Useful Expressions, Part 41  (low-intermediate level) 

1) ผมไว้ใจคุณ pǒm wái-jai kun 

I trust you.  

2) ไว้ใจไม่ได้ wái-jai mâi-dâi 

You can't trust (him). / (He) can't be trusted.  

3) ผมมีของให้คุณ pǒm mii kɔ̌ɔng hâi kun 

I have something for you. (a present, etc.) 

4) ไม่มาก mâi mâak 

Not much. / Not a lot.

5) ไม่อยากมีปัญหา(หรอก) mâi yàak mii bpan-hǎa (rɔ̀ɔk) I don't want any trouble. / I don't want to have any problem(s).  

6) เย็นไว้ INFORMAL yen-wái 

Easy! / Calm down! / Relax!  

7) ก๊อกๆ gɔ́k-gɔ́k 

Knock Knock  

8) (มัน)งี่เง่ามาก (man) ngîi-ngâo mâak 

It's so silly/stupid/ridiculous. 

9) ไม่ใช่ปัญหา(ของ)ฉัน mâi-châi bpan-hǎa (kɔ̌ɔng) chǎn It's not my problem. / That's not my problem.  

10) คุณมาช้าไป kun maa cháa bpai 

You're too late. / You're a little late.  

11) มันทำให้ผมบ้า man tam-hâi pǒm bâa 

It makes me crazy.  

12) จะบ้าตาย jà bâa dtaai 

It's driving me crazy! / It's making me nuts! / I'm going nuts!  

13) ผมเห็นด้วย(กับ...) pǒm hěn-dûai (gàp...) I agree (with...)  

14) หลายคนเชื่อว่า... lǎai kon chʉ̂a wâa... 

Many people believe... 

15) จำไว้ว่า... jam wái wâa... 

Remember (that)...  

16) (X)อยู่ถัดจาก(Y) (X) yùu tàt-jàak (Y) 

(X) is next to (Y). 

17) (X) อยู่ชั้นบน (X) yùu chán-bon 

(X) is upstairs.  

18) ฉันไม่มีอารมณ์(...) chǎn mâi-mii aa-rom(...) I'm not in the mood. / I don't feel like...  

19) ไม่มีอะไรต้องปิดบัง mâi-mii à-rai dtɔ̂ng bpìt-bang (I)'ve got nothing to hide.  

20) อย่าพลาด yàa plâat 

Don't miss it. / Don't miss out.  

Super Useful Expressions, Part 42  (low-intermediate level) 

1) ทำไปทำไม tam bpai tam-mai 

Why'd you do it/that? / What did you do that for?  

2) ฉันพูดจริงๆ chǎn pûut jing-jing 

I'm serious. / I'm not kidding. / I mean it.  

3) (ผม)ไม่รู้จริงๆ (pǒm) mâi-rúu jing-jing 

(I) really don't know. / (I) have no idea.  

4) ลองดูใหม่(สิ) INFORMAL lɔɔng duu mài (sì) Try again. / Try it again.  

5) จะลงไปข้างล่าง jà long bpai kâang-lâang I'm going downstairs.  

6) (ฉัน)อยากจะขอโทษ(...) (chǎn) yàak jà kɔ̌ɔ-tôot (...) I want to apologize(...) 

7) ไม่ต้องขอโทษ(หรอก) mâi-dtɔ̂ng kɔ̌ɔ-tôot (rɔ̀ɔk) 

You don't have to apologize.  

8) ฟังให้ดีนะ fang hâi-dii ná 

Listen carefully. / Listen to me.  

9) มันเป็นความจริง man bpen kwaam-jing 

It's true. / It's a fact.  

10) ฉันเคยอยู่ที่(ญี่ปุ่น) chǎn kəəi yùu-tîi (yîi-bpun) 

I used to live in (Japan).  

11) ผมไม่เก่ง pǒm mâi-gèng 

I'm not (very) good. / I'm no good. (at doing something) 

12) มีประชากรราว (70 ล้าน)คน mii bprà-chaa-gɔɔn raao (sìp-jèt-láan) kon It has a population of around (70 million) (people).  

13) ไม่มีใครอยู่บ้าน mâi-mii krai yùu bâan 

No one's home. / No one is at home.  

14) เห็นไหมล่ะ hěn mǎi lâ 

See?! / You see?! 

15) น่าประทับใจมาก nâa-bprà-táp-jai mâak 

Very impressive!  

16) เพราะอะไร prɔ́ à-rai 

Why? / Why's that? 

17) เป็นอะไรรึเปล่า bpen à-rai rʉ́-bplàao 

Is he okay? / Are you okay?  

18) ขอบคุณที่ช่วยฉัน kɔ̂ɔp-kun tîi chûai chǎn 

Thank you for helping me. 

19) อย่าทำแบบนั้นอีก yàa tam bɛ̀ɛp-nán ìik

Don't (ever) do that again!  

20) สมน้ำหน้า sǒm-nám-nâa 

(It) serves you right! / You deserve it!  

Super Useful Expressions, Part 43  (low-intermediate level) 

1) รู้สึกดีใช่ไหม rúu-sʉ̀k dii châi-mǎi 

It feels good, doesn't it?  

2) รู้สึกดีมาก rúu-sʉ̀k dii mâak 

It feels great!  

3) รู้สึกแปลกๆ rúu-sʉ̀k bplɛ̀ɛk-bplɛ̀ɛk 

It feels weird/strange.  

4) ไม่ไปเหรอ(คะ) mâi-bpai rə̌ə (ká) 

Aren't you going? / Aren't you going to go?  

5) แค่นี้นะ INFORMAL kɛ̂ɛ-níi ná 

Goodbye. (said on telephone) 

6) ทางนี้ค่ะ taang-níi kà 

This way. / This way please.  

7) ถ้าไม่ว่าอะไร tâa mâi-wâa à-rai 

If you don't mind. 

8) (ผู้ชายดีๆ)หายาก(มาก) (pûu-chaai dii-dii) hǎa-yâak (mâak) It's (really) hard to find (a good man).  

9) ต๊ายตาย dtáai-dtaai 

Oh dear! / Oh my (goodness)! (used by females mainly)!

10) (ฉันก็)แค่ถามเฉยๆ (chǎn gɔ̂ɔ) kɛ̂ɛ tǎam chə̌əi-chə̌əi I was just asking. / I just asked. / I'm just asking.  

11) เธอเป็นคนที่ใช่ təə bpen kon-tii-châi 

She's the one. (the right woman for me) 

12) ออกเสียงคล้ายกัน ɔ̀ɔk-sǐang kláai-gan 

(They) sound similar. / (They) are pronounced almost the same. 

13) ลืมยาก lʉʉm yâak 

It's hard to forget.  

14) เก็บไว้กินได้นาน gèp-wái gin dâi naan 

(It) keeps for a long time. / You can store (it) for a long time. (said of food) 

15) ถูกต้อง(ครับ/ค่ะ) tùuk-dtɔ̂ng (kráp/kà) 

That's right. / Correct. / Right. / Exactly.  

16) ถูกแล้ว tùuk lɛ́ɛo 

Right. / Yes. (that's right) 

17) ทำไมต้องเป็น(ฉัน) tam-mai dtɔ̂ng bpen (chǎn) 

Why does it have to be (me)?!  

18) บอกความจริงมา bɔ̀ɔk kwaam-jing maa 

Tell me the truth.  

19) ช่างโชคร้าย châng-chôok-ráai 

How unfortunate! / How unlucky!  

20) มาทำอะไรที่นี่ maa tam à-rai tîi-nîi 

What are you doing here?  

Super Useful Expressions, Part 44

(low-intermediate level) 

1) ไม่ต้องห่วงอะไร mâi-dtɔ̂ng hùang à-rai 

You don't need to worry about anything. / You've got nothing to worry about.  

2) ลืมเวลาไปเลย lʉʉm wee-laa bpai lɛ́ɛo 

(I) lost (all) track of time.  

3) ฉันมาหาคุณ chǎn maa-hǎa kun 

I came to see you.  

4) กำลังจะแต่งงาน gam-lang jà dtɛ̀ng-ngaan 

(I)'m getting married.  

5) (คุณ)เชื่อไหมว่า... (kun) chʉ̂a mǎi wâa... 

Do (you) believe...?  

6) ฉันอยากให้มันจบ chǎn yàak hâi man jòp 

I want to end this. / I want this to be over.  

7) ถ้า(มัน)เป็นไปได้ tâa (man) bpen-bpai-dâi 

If (it's) possible.  

8) ขอคุยกับ... kɔ̌ɔ kui gàp... 

I'd like to speak with... ? / Can I talk to... ?  

9) ถ้าผมเป็นเขา... tâa pǒm bpen kǎo... 

If I were him...  

10) ฉันคิดว่าถ้า... chǎn kít-wâa tâa... 

I thought if... / I think if...  

11) มีปัญหาเหรอ(ครับ) mii bpan-hǎa rə̌ə (kráp) 

Is there a problem?  

12) เธอโกหกฉันทำไม təə goo-hòk chǎn tam-mai

Why did you lie to me?  

13) คุณต้องไว้ใจ(ผม) kun dtɔ̂ng wái-jai (pǒm) 

You have to trust (me).  

14) คุณไว้ใจ(ฉัน)ไหม kun wái-jai (chǎn) mǎi 

Do you trust (me)? 

15) ลง(มา)จากรถ long (maa) jàak rót 

(He) got out of the car. / Get out of the car!  

Note: Depending on the context, this phrase can be used as past tense  or as a command.  

16) เลิกล้อเล่น(ได้แล้ว) lə̂ək lɔ́ɔ-lên (dâi-lɛ́ɛo) 

Stop joking around! / Stop kidding me!  

17) (6) เดือนต่อมา (hòk) dʉan dtɔ̀ɔ-maa 

(6) months later.  

18) รีบกลับมานะ rîip glàp-maa ná 

Hurry back. / Come back soon.  

19) เชิญเลย chəən ləəi 

Be my guest. / Go ahead.  

20) เลิกไม่ได้ lə̂ək mâi-dâi 

I can't stop. / I can't quit.  

Super Useful Expressions, Part 45 

(high-intermediate level) 

1) มันไม่ใช่ตัวฉัน man mâi-châi dtua chǎn 

It's not me. / It's not like me. (not my style, etc.)

2) ปัญหาก็คือ... / ปัญหาอยู่ที่... bpan-hǎa gɔ̂ɔ kʉʉ... / bpan-hǎa yùu-tîi... The problem is...  

3) ออกไปห่างๆ ผม CRUDE/ROUGH ɔɔ̀k-bpai hàang-hàang pǒm Stay (the hell) away from me! / Get away from me! 

4) ...และอื่นๆ อีกมากมาย ...lɛ́-ʉ̀ʉn-ʉ̀ʉn ìik-mâak-maai 

...and so on. / ...and many more. / ...and more others.  

5) (แต่)อย่างที่บอก... (dtɛ̀ɛ) yàang-tîi-bɔ̀ɔk... 

(But) as I said... / (But) like I said...  

6) จากวันนี้ไป... SEMI-FORMAL/LITERARY jàak wan-níi bpai... From this day on(wards)... / Starting from today...  

7) มันไม่ใช่เรื่อง(แพ้)หรือ(ชนะ)แต่เป็นเรื่องของ... man mâi-châi rʉ̂ang (pɛ́ɛ) rʉ̌ʉ (chá-ná) dtɛ̀ɛ bpen rʉ̂ang kɔ̌ɔng... 

It's not about (winning) or (losing). (But) it's about...  

8) มีอีกวิธีหนึ่ง(...) mii ìik wí-tii nʉ̀ng(...) 

There's another way (...)  

9) (ต้องระวัง)มากเป็นพิเศษ (dtɔ̂ng rá-wang) mâak-bpen-pí-sèet (You have to be) extra (careful) / especially (careful) / super (careful)  

10) เหตุผลเดียวที่... hèet-pǒn diao tîi... 

The only reason that... 

11) ยิ่งดีใหญ่ yîng-dii-yài 

Even better! / That's even better. / That would be even better.  

12) จะเป็น(อะ)ไรไหมถ้าฉัน... jà bpen (à)rai mǎi tâa chǎn... Would it be okay if I... ? / Do you mind if I... ?  

13) ใครเป็นคนสั่ง(ให้...) krai bpen kon sòng (hâi...) 

Who gave the order? / Who sent (him)? / Who told him to... 

14) จะไม่มีอะไรเปลี่ยนแปลง jà mâi mii à-rai bplìan-bplɛɛng Nothing will change. / Nothing's changing. 

15) ผมขอสารภาพว่า... FORMAL/LITERARY pǒm kɔ̌ɔ sǎa-rá-pâap wâa... I confess (that)...  

16) แค่อยากรู้ว่า... kɛ̂ɛ yàak rúu wâa... 

(I) just wanna know...  

17) กำลังจะหมดอายุ gam-lang jà mòt-aa-yú 

It's about to expire. / It's going to expire soon.  

18) คุณสัญญา(แล้ว)ว่าจะเลิก kun sǎn-yaa (lɛ́ɛo) wâa jà lə̂ək You promised you would (quit). / You promised you were going to (quit).  

19) ฉันอยากรู้ทุกอย่างเกี่ยวกับ(เขา) chǎn yàak rúu túk-yàang gìao-gàp (kǎo) I want to know everything about (him).  

20) มันหมายความว่า(ยัง)ไง INFORMAL man mǎai-kwaam wâa (yang) ngai What does it/that mean?  

Super Useful Expressions, Part 46 

(low-intermediate level) 

1) ขอคิด(ดู)ก่อน(นะ) kɔ̌ɔ kít (duu) gɔ̀ɔn (ná) 

Let me think about it. / Let's see... / I'll think about it. 

2) ไม่ต้องคิดมาก mâi-dtɔ̂ng kít-mâak 

Just relax, (don't worry about it.) / Don't worry. / Don't think about it too much. 

3) มันไม่ดีต่อสุขภาพ(ค่ะ) man mâi-dii dtɔ̀ɔ sùk-kà-pâap (kà) It's not good for you. / It's not healthy. 

4) ตอนนี้(หนู)ดีขึ้นแล้ว dtɔɔn-níi (nǔu) dii-kʉ̂n lɛ́ɛo (I)'m all better now. / (I)'m feeling better now.  

5) ปล่อยเขาไป bplɔ̀i kǎo bpai 

Let him go. / Let her go.  

6) ไม่มีหวัง mâi-mii wǎng 

It's hopeless. / There's no hope.  

7) สิ่งสำคัญที่สุด(คือ...) sìng sǎm-kan tîi-sùt (kʉʉ...) The most important thing (is...)  

8) ได้ข่าวมาว่า... dâi-kàao maa wâa... 

I heard (that)... / I got word (that)...  

9) ฉัน(ก็)เพิ่งรู้เรื่อง chǎn (gɔ̂ɔ) pə̂ng rúu-rʉ̂ang I just found out. / I just heard.  

10) ฉันจะไม่บอกใคร chǎn jà mâi bɔ̀ɔk krai 

I won't tell anyone.  

11) ยังไม่ได้ดู(เลย) yang mâi-dâi duu (ləəi) 

You haven't even looked (at it) yet. / I haven't (even) looked yet. 

12) ฉันจะพาคุณไป(ห้องพัก) chǎn jà paa kun bpai (hɔ̂ng-pák) I'll show/take you to (your room).  

13) คาดเข็มขัดด้วยค่ะ kâat kěm-kàt dûai kà 

Please fasten your seatbelt.  

14) คุณถูกจับแล้ว kun tùuk jàp lɛ́ɛo 

You're under arrest. 

15) อยากอ้วก yàak ûak 

(I) want to throw up.  

16) ไม่ได้โกง mâi-dâi goong

I didn't cheat.  

17) ฉลาดมาก chà-làat mâak 

(You)'re very clever/smart.  

18) เคยมาแล้ว kəəi maa lɛ́ɛo 

I've been here before.  

19) เดี๋ยว(ผม)โทรกลับ(นะครับ) dǐao (pǒm) too-glàp (ná-kráp) I'll call you right back.  

20) เราต้องไปเดี๋ยวนี้ rao dtɔ̂ng bpai dǐao-níi 

We have to go now! / We gotta go right now!  

Super Useful Expressions, Part 47  (High-beginner level) 

1) ฉันชื่อ... chǎn chʉ̂ʉ... 

My name's...  

2) ฉันเป็นลูกคนเดียว chǎn bpen lûuk-kon-diao I'm an only child.  

3) คุณมีลูกไหม(ครับ) kun mii lûuk mǎi (kráp) 

Do you have any children/kids?  

4) (ลูกชาย) อายุ (4) ขวบ (lûuk-chaai) aa-yú (sìi) kùap (My son)'s (4) years old. 

Note: For ages over 10, use ปี (bpii) instead of ขวบ (kùap).  

5) ผมอายุมากกว่าคุณ pǒm aa-yú mâak-gwàa kun I'm older than you. 

6) ฉันอายุน้อยกว่าคุณ pǒm aa-yú nɔ́ɔi-gwàa kun 

I'm younger than you.  

7) เกิดปีไหน gə̀ət bpii nǎi 

What year were (you) born? 

8) วันนี้(เป็น)วันเกิด(ของ)ฉัน wan-níi (bpen) wan-gə̀ət (kɔ̌ɔng) chǎn Today's my birthday.  

9) ใคร(ครับ) krai (kráp) 

Who? Who is/was it?  

10) ยังเลย yang ləəi 

Not yet. 

11) ไปไหนมา bpai nǎi maa 

Where did you go? / Where have you been?  

12) ผมรู้ว่า... pǒm rúu wâa... 

I know...  

13) ยังไม่ว่าง yang mâi wâang 

(I)'m busy. / (I) don't have time. / (I)'m not free.  

14) ขอยืม(ปากกา)ได้ไหม(คะ/ครับ) kɔ̌ɔ yʉʉm (bpàak-gaa) dâi-mǎi (ká/kráp) Can I borrow (a/your pen)? 

15) แล้วเจอกัน(นะ) lɛ́ɛo jəə gan (ná) 

See you (later). / See you (in a bit).  

16) ในนี้มีอะไร nai níi mii à-rai 

What's in it? / What's in this? / What's in here?  

17) ดีมาก dii mâak 

Great! / Excellent! / Very good.  

18) กี่โมงแล้ว(ครับ) gìi-moong-lɛ́ɛo (kráp)

What time is it? / Do you have the time?  

19) หิวมาก hǐu mâak 

I'm so hungry!  

20) คุณจะทำอะไร kun jà tam à-rai 

What are you gonna do?  

Super Useful Expressions, Part 48  (low-intermediate level) 

1) เป็นความคิดที่ดี bpen kwaam-kít tîi dii That's a good idea.  

2) เป็นความคิดที่ดีมาก bpen kwaam-kít tîi dii mâak That's a great idea.  

3) รหัสไม่ถูกต้อง rá-hàt mâi-tùuk-dtɔ̂ng Wrong password. / Incorrect code. / Invalid code.  

4) ไม่รู้สิ INFORMAL mâi-rúu sì 

I dunno.  

5) ขอดูได้ไหม kɔ̌ɔ duu dâi-mǎi 

Can I have a look? / May I see (it)?  

6) ว่าไงนะ INFORMAL wâa-ngai ná 

What? / Say again.  

7) โทรหาด้วยนะ too-hǎa dûai ná 

Give me a call. / Call me. / Give me a ring.  

8) รอตั้งนานแล้ว rɔɔ dtâng naan lɛ́ɛo 

(I)'ve been waiting for a long time/for ages. 

9) ไม่อยากได้อะไร mâi yàak dâi à-rai 

(I) don't want/need anything.  

10) อย่าบังคับฉัน yàa bang-káp chǎn 

Don't force me.  

11) นึกไม่ออกเลยว่า... nʉ́k-mâi-ɔ̀ɔk ləəi wâa... 

(I) can't imagine... / (I) can't picture...  

12) จำอะไรไม่ได้ jam à-rai mâi-dâi 

(I) can't remember a thing. / (I) don't remember anything.  

13) แบตหมด bɛ̀t mòt 

My battery's dead. / The battery died.  

14) ไม่เคยบอกใคร mâi-kəəi bɔ̀ɔk krai 

(I) haven't told anyone. 

15) อย่าบอกใคร yàa bɔ̀ɔk krai 

Don't tell anyone.  

16) อย่าบอกเรื่องนี้กับใคร(ครับ/ค่ะ) yàa bɔ̀ɔk krai rʉ̂ang níi gàp krai (kráp/kà) Don't tell anyone about this.  

17) แน่ใจ(สิ)ครับ/ค่ะ nɛ̂ɛ-jai (sì) kráp/kà 

I'm sure. / Sure.  

18) อย่าเสียเวลา(ไปเลย) yàa sǐa-wee-laa (bpai ləəi) 

Don't waste your time. / Let's not waste time.  

19) เดี๋ยวรู้เอง dǐao rúu eeng 

You'll (soon) find out. / You'll know soon enough.  

20) ดูแลสุขภาพด้วย(นะ) duu-lɛɛ sùk-kà-pâap dûai (ná) 

Take care of yourself. / Stay healthy.

Super Useful Expressions, Part 49  (low-intermediate level) 

1) ตามใจนะ dtaam-jai ná 

Up to you. / As you wish. / All right (as you wish). / Suit yourself.  

2) ผมจะwa pǒm jà kâo-nɔɔn 

I'm going to bed. 

3) ฉันเพิ่งตื่น(นอน) chǎn pə̂ng dtʉ̀ʉn (nɔɔn) 

I just woke up.  

4) ตายแล้วหรอ dtaai lɛ́ɛo rɔ̌ɔ 

Did (she) die? / Is (she) dead?  

5) ทำไมถึงตาย tam-mai tʉ̌ng dtaai 

How/why did (she) die?  

6) อย่าไปยุ่ง yàa bpai yûng 

Leave (him) alone! / Stay away from (him)! / Don't touch (that)!  

7) มันเป็นของปลอม man bpen kɔ̌ɔng-bplɔɔm It's fake. / This is a fake.  

8) ผมจะฟ้องคุณ pǒm jà fɔ́ɔng kun 

I'll sue you! / I'm gonna sue you!  

9) ผม(จะ)ยกให้ pǒm (jà) yók-hâi 

I'll give it to you. / I'll give you it. (implication: and you keep it)  

10) เหลวไหล lěeo-lǎi 

Ridiculous! / Nonsense!  

11) ไม่สะดวก mâi sà-dùak

It's inconvenient. / It's not a good time. / It's difficult (because one isn't free, etc.)  

12) (ฉัน)ไม่ได้ทำอะไร (chǎn) mâi-dâi tam à-rai 

(I) didn't do anything. 

13) (คุณ)ไม่ได้ทำอะไรผิด (kun) mâi-dâi tam à-rai pìt 

(You) didn't do anything wrong.  

14) ตาม(ฉัน)มาทำไม dtaam (chǎn) maa tam-mai 

Why are you following me?! 

15) ว่าแล้วเชียว wâa lɛ́ɛo chiao 

I knew it! / I thought so! / Just as I thought!  

Note: เชียว (chiao) is an intensifier used for emphasis here.  

16) ต้องระวังตัว dtɔ̂ng rá-wang dtua 

(You) have to be careful.  

17) ยังบอกอะไรไม่ได้ yang bɔ̀ɔk à-rai mâi-dâi 

(I) can't tell yet. / (I) don't know yet.  

18) มันไปอยู่ที่ไหน man bpai yùu-tîi-nǎi 

Where did it go?!  

19) ไม่ได้เอาไป mâi-dâi ao-bpai 

(I) didn't take it/them!  

20) สนใจหรือเปล่า sǒn-jai rʉ̌ʉ-bplàao 

Are you interested (or not)?  

Super Useful Expressions, Part 50 

(low-intermediate level)

1) (ตอนนี้คุณ)คบกับใครอยู่หรือเปล่า(คะ) (dtɔɔn-níi kun) kóp-gàp krai yùu rʉ̌ʉ-bplàao (ká) 

Are you dating anyone? / Are you seeing anyone (now)? 

2) คุณพอจะมี... คะ/ครับ POLITE kun pɔɔ jà mii... ká/kráp Do you (happen to) have... ?  

3) คุณดูผอมลง kun duu pɔ̌ɔm long 

You look like you've lost weight. / You look thinner.  

4) แต่งงานกันนะ dtɛ̀ng-ngaan gan ná 

Let's get married.  

5) เรารู้จักกันมานาน rao rúu-jàk gan maa naan 

We've know each other for a long time.  

6) ผมพูดไม่ได้ pǒm pûut mâi-dâi 

I can't talk about (it). / I can't tell (you).  

7) ตั้งใจทำงาน(นะ) dtâng-jai tam-ngaan (ná) 

Work hard! / Do your best at work.  

8) นั่นนะสิ(ค่ะ) nân ná sì (kà) 

Yeah, (that's right). / That's right.  

9) (ขอ)ถามหน่อย (kɔ̌ɔ) tǎam nɔ̀i 

Let me ask you... / May I ask... ?  

10) อยากลองชิม yàak lɔɔng chim 

I wanna try it. / I want to have a taste.  

11) จะโอนเงินให้ jà oon-ngən hâi 

(I)'ll wire you the money.  

12) ใครส่งมา krai sòng maa 

Who sent it? 

13) (เขา)ทำอะไรผิด (kǎo) tam à-rai pìt 

What did (he) do wrong?  

14) ตั้งหน้าตั้งตา... dtâng-nâa-dtâng-dtaa 

(I)'m looking forward to... 

15) ทุกอย่างมันเป็นไปได้ túk-yàang man bpen-bpai-dâi Anything's possible.  

16) โลกกลม lôok glom 

It's small world! / Small world!  

17) น่ากลัวจริงๆ nâa-glua jing-jing 

It's really scary. / That's really scary.  

18) ขนลุกเลย kǒn-lúk ləəi 

Really creepy! / Makes my hair stand on end! / Gives me the goosebumps!  

19) เขาเพิ่งเรียนจบ kǎo pə̂ng rian jòp 

He just graduated. 

20) ต้องดูแลตัวเองดีๆ dtɔ̂ng duu-lɛɛ dtua-eeng dii-dii You have to take good care of yourself.  

Super Useful Expressions, Part 51 

(high-intermediate level) 

1) ปัดโธ่(เว้ย) bpàt-tôo(wóoi) 

Damn it! / Goddamn it! 

2) (ฉัน)ไม่รู้ว่าคุณพูด(ถึง)เรื่องอะไร (chǎn)mâi-rúu-wâa kun pûut(-tʉ̌ng) rʉ̂ang à-rai 

(I) don't know what you're talking about. 

3) ไม่เคยมีใครถามฉันมาก่อน mâi kəəi mii krai tǎam chǎn maa-gɔ̀ɔn No one's ever asked me that before.  

4) อย่างที่เคยพูด/บอก... yàang-tîi kəəi pûut/bɔ̀ɔk... 

As I said... / Like I told you...  

5) หลายต่อหลายครั้ง lǎai-dtɔ̀ɔ-lǎai-kráng 

time and time again / on numerous occasions / many many times  

6) ครั้งแล้วครั้งเล่า kráng-lɛ́ɛo-kráng-lâo 

over and over again / time after time 

7) นี่คือสาเหตุที่... SEMI-FORMAL/LITERARY nîi kʉʉ sǎa-hèet tîi... That's the reason... / That's why...  

8) เอาไว้ทำอะไร ao wái tam à-rai 

What is that for? / What's it for? (asking about purpose or function)  

9) อยากรู้ใช่ไหมว่า(เอาไว้ทำอะไร) yàak rúu châi-mǎi wâa (ao wái tam à-rai) You wanna know (what it's for)? / Do you want to know (what it's for)? /  You wanna know (what it's for), don't you?  

10) (คุณ)ช่วยอธิบาย(หน่อย)ได้ไหมว่า... (kun) chûai à-tí-baai (nɔ̀i) dâi-mǎi wâa... 

Do you mind explaining/telling me...?  

11) เธอขึ้นปก (VOGUE Thailand) təə kʉ̂n-bpòk (VOGUE Thailand) She was on the cover of (VOGUE Thailand).  

12) อย่าปล่อยให้ตัวเอง... yàa bplɔ̀i hâi dtua-eeng... 

Don't let yourself...  

13) ไม่มีอะไรแน่นอน mâi mii à-rai nɛ̂ɛ-nɔɔn 

There are no certainties. / Nothing is certain.  

14) ในทางกลับกัน SEMI-FORMAL nai-taang-glàp-gan 

On the other hand...

15) เป็นเทรนด์มาแรง bpen treen maa-rɛɛng It's a hot trend. / It's very trendy.  

16) มีอะไรให้ทำเยอะ(เลย) mii à-rai hâi tam yə́ (ləəi) There's plenty to do. / There's a lot to do.  

17) ซึ่งหมายถึง... SEMI-FORMAL sʉ̂ng mǎai-tʉ̌ng... Which means...  

18) มันเป็นข้อเท็จจริง FORMAL man bpen kɔ̂ɔ-tét-jing It's a fact. / It's true.  

19) ความจริง(ก็)คือ... kwaam-jing (gɔ̂ɔ) kʉʉ... The truth is...  

20) ห้ามพลาดเด็ดขาด hâam plâat dèt-kàat By all means don't miss it! / It's not to be missed!  

Super Useful Expressions, Part 52  (high-beginner level) 

1) ทำไม tam-mai 

Why? / How come?  

2) มันคืออะไร man kʉʉ à-rai 

What is it?  

3) ฉันรู้ chǎn rúu 

I know.  

4) ดูนี่สิ duu nîi sì 

Look at this. / Look here. 

5) พร้อมแล้ว prɔ́ɔm lɛ́ɛo 

(I)'m (all) ready. / (I)'m prepared.  

6) ยังไม่พร้อม yang mâi prɔ́ɔm  

(I)'m not ready (yet). 

7) อย่าหยุด yàa yùt 

Don't stop.  

8) ยังไม่อิ่ม yang mâi ìm 

(I)'m not full (yet).  

9) ปล่อย(นะ) bplɔ̀i (ná) 

Let go! 

10) เอาอันนี้(นะครับ) ao an-níi (ná kráp) 

I'll take this/that one. (shopping, etc.)  

11) เหมือนฉันเลย mʉ̌an chǎn ləəi 

Just like me! / Same as me!  

12) โทรมาทำไม too maa tam-mai 

Why did you call me? / Why is he calling me? / Why are you calling?  

13) ยังไม่มา yang mâi maa 

He's not here yet. / They haven't arrived yet. / It hasn't come yet.  

14) ฉันอยู่ในห้อง (751) chǎn yùu nai hɔ̂ng (jèt-hâa-nʉ̀ng) I'm in room (751). 

15) (แล้ว)เจอกัน(นะ) (lɛ́ɛo) jəə-gan (ná) 

See ya.  

16) ผมจะคิดถึงคุณ pǒm jà kít-tʉ̌ng kun 

I'm gonna miss you.  

17) ฉันลืมไป chǎn lʉʉm bpai

I forgot.  

18) ฉันลืมไปเลย chǎn lʉʉm bpai ləəi 

I completely forgot! / I totally forgot about it!  

19) จ... ได้ไหม jam... dâi mǎi 

Do you remember...?  

20) จำได้ jam dâi 

Yes, I do. (remember) / Yeah, I remember.  

Super Useful Expressions, Part 53  (low-intermediate level) 

1) กลับมาตั้งแต่เมื่อไหร่ glàp maa dtâng-dtɛ̀ɛ mʉ̂a-rài When did you get back?  

2) (คุณ)บ้าหรือเปล่า (kun) bâa rʉ̌ʉ-bplàao Are you crazy?!  

3) ใครส่งข้อความมา krai sòng kɔ̂ɔ-kwaam maa Who texted you? / Who sent you a message?  

4) ผู้ชายคนนั้น... pûu-chaai kon-nán 

That guy... / That man...  

5) ผู้หญิงคนนั้น... pûu-yǐng kon-nán 

That lady... / That woman...  

6) อยู่ตรงนี้ไง yùu dtrong-níi ngai 

(It)'s right here. 

7) ท้อง(ฉัน)ร้อง tɔ́ɔng (chǎn) rɔ́ɔng 

My stomach is growling. (I'm hungry.) 

8) (นั่ง)กินเป็นเพื่อนหน่อย (nâng) gin bpen-pʉ̂an nɔ̀i (Sit down and) have something to eat with me. 

9) คุณทำผมตกใจ kun tam pǒm dtòk-jai You scared me. / You startled me.  

10) มันผิดกฎหมาย man pìt-gòt-mǎai 

It's illegal. / It's against the law.  

11) เป็นเรื่องธรรมชาติ bpen rʉ̂ang tam-má-châat It's (perfectly/completely) natural.  

12) ผมไม่ได้ตั้งใจ pǒm mâi-dâi dtâng-jai I didn't mean to. / That wasn't my intention. 

13) (...)คงไม่เป็นไร (...) kong mâi-bpen-rai It will be okay. / He will be all right.  

14) เขาไม่เคยบอกผม kǎo mâi-kəəi bɔ̀ɔk pǒm He never told me. / She's never told me. 

15) ยอดไปเลย yɔ̂ɔt bpai ləəi 

That's great/fantastic/awesome/wonderful!  

16) ไม่ใช่อย่างนั่น mâi-châi yàang-nân 

It's not like that! / That's not it! / That's not why! 

17) ไม่ใช่เรื่องใหญ่ mâi-châi rʉ̂ang-yài It's no big deal. / No biggie.  

18) อดทนไว้ òt-ton wái 

Hang on! / Be patient. / Hang in there.  

19) ไม่ใช่สเปคผม mâi-châi sà-bpék pǒm (She)'s not my type. 

20) ฉันลืมถามว่า... chǎn lʉʉm tǎam wâa...

I forgot to ask...  

Super Useful Expressions, Part 54 

(high-intermediate level) 

1) เป็นบ้าอะไรของคุณ bpen bâa à-rai kɔ̌ɔng kun 

What the hell's wrong with you?! / What on earth is wrong with you?!  

2) อยากทำอะไรก็เชิญ yàak tam à-rai gɔ̂ɔ chəən (Go ahead) Do whatever you want.  

3) อย่าทำให้เป็นเรื่อง(ใหญ่) yàa tam-hâi bpen rʉ̂ang (yài) Don't make a big deal of it. / Don't make a scene. 

4) ในพริบตา nai-príp-dtaa 

in a blink of an eye / in an instant  

5) เท่าที่ทราบ/รู้... tâo-tîi sâap/rúu... 

As far as I know...  

6) แต่พอคิดอีกที... dtɛ̀ɛ pɔɔ kít ìik tii... 

But on second thought...  

7) ฉันต้องสารภาพ(ว่า...) chǎn dtɔ̂ng sǎa-rá-pâap (wâa...) I must confess (that...)  

8) ...ตั้งแต่จำความได้... ...dtâng-dtɛ̀ɛ jam-kwaam dâi... Ever since I can remember... / ...for as long as I can remember.  

9) ...ไม่ช้าก็เร็ว... ...mâi-cháa-gɔ̂ɔ-reo... 

...sooner or later...  

10) ผมมีสิทธิ์ที่จะ(รู้) pǒm mii-sìt tîi jà (rúu) 

I have a right to (know). 

11) กรุณากรอกแบบฟอร์มนี้ FORMAL gà-rú-naa grɔ̀ɔk bɛ̀ɛp-fɔɔm níi Please fill out this form.  

12) วันเกิดอายุ(ครบ) (60) ปี wan-gə̀ət aa-yú (króp) (hòk-sìp) bpii (60th) birthday  

13) (ยัง)มีอีกเพียบ (yang) mii ìik pîap 

There's plenty more. / We have plenty more.  

14) ไม่มีใครได้รับบาดเจ็บ mâi-mii krai dâi-ráp bàat-jèp No one was injured/hurt. 

15) ไม่มีอำนาจ... SEMI-FORMAL mâi-mii am-nâat 

(He) doesn't have the authority/power (to)... 

16) ขอโทษที่ทำให้เดือดร้อน kɔ̌ɔ-tôot tîi tam-hâi dʉ̀at-rɔ́ɔn I'm sorry for the trouble I've caused you.  

17) มาถึงจนได้ maa-tʉ̌ng jon-dâi 

We made it! / We finally arrived! / It finally arrived.  

18) ไม่ว่าอะไรจะเกิดขึ้น... mâi-wâa à-rai jà gə̀ət-kʉ̂n... No matter what happens... / Whatever happens...  

19) ไม่มีที่ติ mâi-mii tîi-dtì  

It's flawless. / It's perfect.  

20) ยิ่ง(แย่)เข้าไปใหญ่ yîng (yɛ̂ɛ) kâo-bpai yài 

It's even (worse). / That makes it all the (worse).  

Super Useful Expressions, Part 55 

(low-intermediate level) 

1) เอาเลย ao ləəi

Come on. / Go on. (do it)  

2) อย่าดื้อ(สิ) yàa dtʉ̂ʉ (sì) 

Don't be (so) stubborn!  

3) มองไม่เห็น mɔɔng-mâi-hěn 

I can't see (it).  

4) มันไม่ใช่ความรัก man mâi-châi kwaam-rák It's not love. / That's not love.  

5) อย่าบอกนะว่า... yàa bɔ̀ɔk ná wâa... 

Don't tell me...  

6) ห้ามแตะต้อง(...) hâam dtɛ̀-dtɔ̂ng(...) 

Don't touch (...) (contact or touch)  

7) ไม่รอแล้ว mâi-rɔɔ lɛ́ɛo 

(I)'m not waiting. / (I) can't wait anymore.  

8) มันก็พูดยากนะ man gɔ̂ɔ pûut yâak ná 

It's hard/difficult to say.  

9) ก็ใช่นะสิ gɔ̂ɔ châi ná sì 

Yeah (it is!/you are!). / Of course. / Exactly.  

10) กลับไปพักผ่อน glàp bpai pák-pɔ̀n 

Go back (home) and get some rest.  

11) เขาเป็นโรคจิต kǎo bpen rôok-jìt 

He's mentally ill. / He's psycho. / She has mental problems.  

12) ไปทางไหนดี bpai taang-nǎi dii 

Which way should I go? / Which way?  

13) ไม่มีสมาธิ mâi-mii sà-maa-tí 

(I) can't concentrate. / (I) can't focus/ (I) get distracted. 

14) ไปหาอะไรกินกัน bpai hǎa à-rai gin gan 

Let's grab a bite to eat. / Let's go find something to eat. / Let's go get something  to eat. 

15) พรุ่งนี้มีสอบ prûng-níi mii sɔ̀ɔp 

We have a test tomorrow. / There's a test tomorrow.  

16) คุณอยากรู้ไปทำไม SOMEWHAT STRONG kun yàak rúu bpai tam-mai Why do you wanna know?  

17) ดูธรรมชาติ duu tam-má-châat 

It looks natural.  

18) อย่าเถียง yàa tǐang 

Don't argue (with me)!  

19) ทำไมไม่เชื่อฉัน tam-mai mâi-chʉ̂a chǎn 

Why don't you believe me?  

20) ใจร้ายจัง jai-ráai jang 

You're so mean/cruel/wicked!  

Super Useful Expressions, Part 56 

(low-intermediate level) 

1) ทำใจให้สบาย tam-jai hâi sà-baai 

Relax. / Chill out. / Don't worry about it. / Take it easy.  

2) (แก)พูดเกินไป (gɛɛ) pûut gəən-bpai 

(You)'re exaggerating! / That's a bit too much!  

3) มีอะไรกินบ้าง mii à-rai gin bâang 

What's there eat? / What do we have to eat? 

4) จะพาฉันไปไหน jaà paa chǎn bpai-nǎi 

Where are you taking me?  

5) ได้เลย dâi ləəi 

Sure! / Okay! / All right! (agreeing to something)  

6) เพราะอะไรรู้มั้ย INFORMAL prɔ́-à-rai rúu-mái You know why? / Do you know why?  

7) อาจ(จะ)เป็นอุบัติเหตุ àat (jà) bpen ù-bàt-dtì-hèet Maybe it was an accident.  

8) ได้ครบแล้ว dâi króp lɛ́ɛo 

(I) have everything. / (They) got it all.  

9) ขอโทษที่ทำให้เสียเวลา kɔ̌ɔ-tôot tîi tam-hâi sǐa-wee-laa Sorry for wasting your time.  

10) เปิดประตู(หน่อย)(สิ) bpə̀ət bprà-dtuu (nɔ̀i) (sì) Open the door.  

11) เราเป็นเพื่อนสนิทกัน rao bpen pʉ̂an sà-nìt gan We're close friends. / We're best friends.  

12) จะพิสูจน์ให้ดู jà pí-sùut hâi duu 

(I)'ll prove it to you.  

13) อย่าไปไหน(นะ) yàa bpai-nǎi (ná) 

Don't go anywhere.  

14) มันไม่ยุติธรรม man mâi-yùt-dtì-tam 

It's not fair. 

15) มันเป็นทางตัน man bpen taang-dtan 

It's a dead end.  

16) ผมไม่อยากอยู่คนเดียว pǒm mâi-yàak yùu kon-diao

I wanna be alone.  

17) มากไปแล้ว mâak bpai lɛ́ɛo 

It's/That's too much!  

18) มันไม่โอเค man mâi oo-kee 

It's not okay.  

19) ถามทำไม tǎam tam-mai 

Why do you ask?  

20) ยิ้มอะไร(ครับ/คะ) yím à-rai (kráp/ká) 

What are you smiling at?  

Super Useful Expressions, Part 57 

(high-beginner level) 

1) ไม่ต้องอาย mâi-dtɔ̂ng aai 

Don't be shy. / You don't need to be shy.  

2) แปลกจริงๆ bplɛ̀ɛk jing-jing 

It's so strange. / That's really weird.  

3) ฉันกลัว chǎn glua 

I'm scared/afraid.  

4) ฉันไม่ได้กลัว chǎn mâi-dâi glua 

I'm not scared/afraid.  

5) เขาเป็นใคร kǎo bpen krai 

Who is she? / Who is he? / Who are they?  

Note: พวกเขา (pûak-kǎo) is the 'proper' way to say 'they' but sometimes it's  shortened to just เขา (kǎo).

6) มองผมสิ mɔɔng pǒm sì 

Look at me.  

7) มีคนให้มา mii kon hâi maa 

Someone gave it to me. / Somebody gave me this.  

8) เสร็จแล้ว sèt lɛ́ɛo 

All finished. / (We)'re all finished. / (We)'re done.  

9) มันน่าเบื่อ man nâa-bʉ̀a 

It's boring.  

10) ลดหน่อยได้ไหมคะ lót nɔ̀i dâi mǎi ká Can you give me a discount? / Can you lower the price?  

11) ฉันไม่ได้ทำนะ chǎn mâi-dâi tam ná 

I didn't do it!  

12) ไม่แน่ใจ mâi-nɛ̂ɛ-jai 

(I)'m not sure.  

13) (เขา)จมน้ำตาย (kǎo) jom-náam-dtaai (He) drowned.  

14) คุณสวยมากเลย kun sǔai mâak ləəi You're so beautiful! 

15) คอแห้ง kɔɔ hɛ̂ng 

My throat's dry. / I'm thirsty.  

16) ขอฉันดูหน่อย kɔ̌ɔ chǎn duu nɔ̀i 

Let me see (it). / Let me have a look.  

17) เราจะรวย rao jà ruai 

We'll be rich! / We'll make loads of money! 

18) คุณทำได้ kun tam dâi 

You can do it.  

19) ผมเลี้ยงเอง pǒm líang eeng 

It's my treat. / My treat. 

20) ไว้เจอกันนะ wái jəə-gan ná 

See you later.  

Super Useful Expressions, Part 58 

(high-intermediate level) 

1) เข้มแข็งไว้(นะ) kêm-kɛ̌ng wái (ná) 

Be strong. (emotionally) 

2) ไม่เคยทำร้ายใคร mâi-kəəi tam-ráai krai 

(He)'s never hurt anybody. / (It) never hurt anyone.  

3) ฉันอยากจะย้อนเวลา(กลับไป)... chǎn yàak jà yɔ́ɔn wee-laa (glàp-bpai)... I wanna turn back the clock... / I want to go back in time... / I wish I could go  back in time...  

4) หายเข้าไปใน(ป่า) hǎai kâo bpai nai (bpàa) 

(He) disappeared into the (forest).  

5) อยากให้ของขวัญ แต่ไม่รู้จะให้อะไร yàak hâi kɔ̌ɔng-kwǎn dtɛ̀ɛ mâi-rúu jà hâi à-rai 

I wanna get (her) something, but don't know what to get.  

6) ฉันไม่สมควรได้รับ(มัน) chǎn mâi sǒm-kuan dâi-ráp (man) I don't deserve (it). / I didn't deserve (it).  

7) ไม่มีวี่แววของ(ฝน) mâi-mii wîi-wɛɛo kɔ̌ɔng (fǒn) 

There's no sign of (rain). / It doesn't look like it's going to (rain). 

8) มันเป็นแค่ภาพลวงตา man bpen kɛ̂ɛ pâap-luang-dtaa 

It's just an illusion. / It's just a mirage. / It's just a hallucination. 

9) เขาบริจาคเงินให้กับ(การกุศล) kǎo bɔɔ-rí-jàak ngən hâi-gàp (gaan-gù-sǒn) He donated/gave money to (charity).  

10) มีเวลาไม่มาก mii wee-laa mâi mâak 

You don't have much time. / There isn't much time.  

11) ยังหนุ่มยังแน่น yang nùm yang nɛ̂n 

(He)'s still young and fit/strong.  

12) ไม่งั้น... mâi-ngán... 

Otherwise... / If not...  

13) ไม่มีอะไรมากกว่านั้น mâi-mii à-rai mâak-gwàa nán 

There's nothing more. / There's nothing else.  

14) ปกปิดความจริง bpòk-bpìt kwaam-jing 

hide the truth / cover up the truth 

15) เขาทำได้ทุกอย่าง kǎo tam-dâi túk-yàang 

He can do it all. / He can do anything. / He can do everything.  

16) คุยอะไรกันอยู่(เหรอ) kui à-rai gan yùu (rə̌ə) 

What are you guys/two talking about?  

17) ลืมของไว้(ในรถ) lʉʉm kɔ̌ɔng wái (nai rót) 

(You) left something (in the car).  

18) (มันก็)แปลกดีนะ (man gɔ̂ɔ) bplɛ̀ɛk dii ná 

It's weird. / It's strange. / It's funny.  

19) หลีกเลี่ยงไม่ได้ lìik-lîang-mâi-dâi 

It's inevitable/unavoidable.  

20) ลุยให้เต็มที่ SLANG lui hâi dtem-tîi

Go all out! / Give it your best shot! / Break a leg! 

Super Useful Expressions, Part 59  (low-intermediate level) 

1) เธออาจจะติดคุก təə àat-jà dtìt-kúk 

You could go to jail. 

2) ไม่อยากติดคุก mâi-yàak dtìt-kúk 

I don't wanna go to jail. 

3) (คุณ)ดูเปลี่ยนไป (kun) duu bplìan-bpai 

You look different. / You seem different.  

4) เขาช่วยชีวิตผม kǎo chûai-chii-wít pǒm 

He saved my life. / He rescued me. 

5) เคยทำมาแล้ว kəəi tam maa lɛ́ɛo 

I've done that (before).  

6) ไม่ค่อยได้นอน mâi-kɔ̂i dâi-nɔɔn 

I don't get much sleep. / I didn't sleep much. / I haven't slept much. 

7) บอกมาเลยว่า... BIT STRONG bɔ̀ɔk maa ləəi wâa... (Just) tell me...  

8) ถ้าเลือกได้... tâa lʉ̂ak dâi... 

If I could choose... / If I had a choice... / If given a choice...  

9) หยุดคิดเรื่อง... ไม่ได้ yùt kít rʉ̂ang... mâi-dâii (I) can't stop thinking about...  

10) เข้ามาได้ยังไง kâo-maa dâi yang-ngai 

How did you get in here?! 

11) ตั้งใจฟังให้ดี dtâng-jai fang hâi-dii 

Listen carefully! / Listen to me carefully!  

12) ถามไปแล้ว tǎam bpai lɛ́ɛo 

(I) asked already.  

13) เก็บเป็นความลับ gèp bpen kwaam-láp 

Keep it (a) secret.  

14) มีเรื่องสำคัญจะบอก mii rʉ̂ang sǎm-kan jà bɔ̀ɔk 

(I) have something important to tell you. 

15) ...ดีพอสำหรับฉัน ...dii pɔɔ sǎm-ràp chǎn 

...is good enough for me.  

16) มีแฟนหรือยัง mii fɛɛn rʉ̌ʉ yang 

Do you have a boy/girlfriend?  

17) ยังไม่นอนอีกเหรอ yang mâi nɔɔn ìik rə̌ə 

Are you still awake? / You're still up? / You're not sleeping yet?  

18) ไม่มีใครรักฉัน mâi-mii-krai rák chǎn 

No one loves me! / Nobody loves me!  

19) ดูไม่ดีเลย duu mâi-dii ləəi 

(You) don't look so good. / (You) don't look well. / (It) doesn't look (so) good. 

20) ไม่มีอะไรพิเศษ mâi-mii-à-rai pí-sèet 

Nothing special. / I didn't do anything special. 

Super Useful Expressions, Part 60 

(high-beginner level) 

1) ใช่ใช่ใช่ châi châi châi 

Yes! Yes! Yes! / Yeah, yeah, yeah.  

2) ไม่ไม่ไม่ mâi mâi mâi  

No! No! No! / No, no, no.  

3) เอาไหม(ครับ/คะ) ao mǎi (kráp/ká) 

Do you want one? / Do you want some? / Want some?  

4) ยังไม่แน่ใจ yang mâi-nɛ̂ɛ-jai 

(I)'m not sure yet. 

5) ได้โปรด FORMAL dâi-bpròot 

Please! / Please...  

6) ผมมีนัดกับ(คุณ...) pǒm mii nát gàp (kun...) I have an appointment with...  

7) เป็นผมเอง bpen pǒm eeng 

That's me. / That was me. 

8) ไม่อยากรู้ mâi-yàak rúu 

(I) don't wanna know.  

9) ผมชอบกิน(มะม่วง) pǒm chɔ̂ɔp gin (má-mûang) I like (mangoes).  

10) ชอบกิน(มะม่วง)เรือ chɔ̂ɔp gin (má-mûang) rə̌ə Do you like (mangoes)?  

11) ชอบกินมาก chɔ̂ɔp gin maâk 

I love them. / I love it.  

12) ผมไม่ชอบกิน(มะม่วง) pǒm mâi chɔ̂ɔp gin (má-mûang) I don't like (mangoes).  

13) น่ากิน(จังเลย) nâa-gin (jang ləəi) 

Looks good! / Looks delicious! / Yum! 

14) (ฉัน)ไม่อยากไป (chǎn) mâi-yàak bpai 

(I) don't wanna go. 

15) ปวดหัว bpùat-hǔa 

I have a headache.  

16) ยังไม่ตาย yang mâi dtaai 

You're still alive. / You're not dead yet!  

17) นี่(ครับ/ค่ะ) nîi (kráp/kà) 

Here you are. / Here you go. (handing something to someone)  

18) เยี่ยม yîam 

Great! / Excellent!  

19) ตลกดี(นะ) dtà-lòk dii (ná) 

That's funny! / He's funny.  

20) เดี๋ยวๆ(ๆ) dǐao-dǐao 

Wait, wait. / Whoa---wait a second.  

Super Useful Expressions, Part 61  (low-intermediate level) 

1) ตกใจหมดเลย dtòk-jai mòt ləəi 

You scared me! / You scared the hell out of me! / I was shocked!  

2) มันเป็นเรื่องบังเอิญ man bpen rʉ̂ang-bang-əən It's a coincidence.  

3) หน้าเด็กจัง nâa dèk jang 

(You) look so young! / (You) have such a baby face! 

4) มีเคล็ดลับอะไร mii klét-láp à-rai 

What's your secret? (to looking young, etc.)  

5) ขยันจัง(เลย) kà-yǎn jang (ləəi) 

(You)'re so hardworking! / So diligent! / (You)'re such a hard worker!  

6) ตอบตกลง dtɔ̀ɔp dtòk-long 

Say yes. / Said yes. (answered yes and agreed to something)  

7) (พี่)มาหาเพื่อน (pîi) maa-hǎa pʉ̂an 

(I) came to visit a friend. / (I)'m visiting a friend.  

Note: The speaker uses พี่ (pîi) here because s/he is older than the listener.  

8) คุณต้องเข้าใจ(นะครับ)(...) kun dtɔ̂ng kâo-jai (ná-kráp) (...) You have to understand(...)  

9) เป็นเพื่อนกันตลอดไป bpen pʉ̂ang gan dtà-lɔ̀ɔt-bpai (We'll be) friends forever.  

10) เกิดอุบัติเหตุขึ้น gə̀ət ù-bàt-dtì-hèet kʉ̂n 

There was an accident. / There's been an accident.  

11) ดูท่าว่า... duu tâa-wâa... 

It seems like... / It looks like...  

12) ขายไม่ออก kǎai-mâi-ɔ̀ɔk 

(We) can't sell it. / (We) were unable to sell them.  

13) เหมือนเทพนิยาย mʉ̌an têep-ní-yaai 

(It's) like a fairy tale.  

14) ใครเล่นเป็น(นางเอก) krai lên bpen (naang-èek) 

Who (plays) the (lead actress)? 

15) เขาเสียเลือดมาก kǎo sǐa lʉ̌at mâak 

He lost a lot of blood. (in an accident, an operation, etc.) 

16) ทุกอย่างเรียบร้อยดี túk-yàang rîap-rɔ́ɔi-dii 

Everything's fine. / Everything's under control. / Everything's all right.  

17) คุณต้องสัญญากับผม kun dtɔ̂ng sǎn-yaa gàp pǒm You have to promise me. / You must promise me.  

18) ทะเลาะเรื่องอะไร tá-lɔ́rʉ̂ang à-rai 

What were (you) arguing about? / What did (you) have a fight about?  

19) ขอตัวไปนอนก่อน kɔ̌ɔ-dtua bpai nɔɔn gɔ̀ɔn 

I'm going to bed. / I'm gonna turn in now.  

20) มันจั๊กจี้ man ják-gà-jîi 

It tickles!  

Super Useful Expressions, Part 62 

(high-intermediate level) 

1) ฝันไปเถอะ fǎn bpai tə̀ 

In your dreams! / You're dreaming! / Dream on!  

2) จะเริ่มตรงไหนดี(ครับ) jà rə̂əm dtrong-nǎi dii (kráp) Where do I start? / Where do I begin? 

3) (ฉัน)ไม่ได้ข่าวจาก(เขา) (chǎn) mâi-dâi kàao jàak (kǎo) (I) haven't heard from (him).  

4) มีอะไรมากกว่านั้น mii à-rai mâak-gwàa nán 

There's something more. / There's something else.  

5) ข่าวล่าสุด kàao lâa-sùt 

The latest / The latest news 

6) แต่ละคนมีนิยาม(ความสุข)ที่ต่างกัน dtɛ̀ɛ-lá kon mii ní-yaam (kwaam-sùk) tîi dtàang-gan 

Everyone/Each of us has a different definition of (happiness). 

7) ...วันรุ่งขึ้น... SEMI-LITERARY ...wan-rûng-kʉ̂n... 

The next day... / ...the next day.  

8) (Tesla Model 3) ถูกยกให้เป็น(รถที่ปลอดภัยที่สุด) (Tesla Model 3) tùuk-yók hâi-bpen (rót tîi bplɔ̀ɔt-pai tîi-sùt) 

(The Tesla Model 3) is considered to be (the safest car). / (The Tesla Model 3)  was called (the safest car). / (The Tesla Model 3) was described as (the safest  car).  

9) นั่นแหละชีวิต nân-lɛ̀ chii-wít 

That's life!  

10) ไม่เคยมีประสบการณ์มาก่อน mâi-kəəi mii bprà-sop-gaan maa-gɔ̀ɔn (I) have no experience. / (I) have no previous experience.  

11) ไม่จำเป็นต้องมีประสบการณ์มาก่อน mâi-jam-bpen dtɔ̂ng mii bprà-sop gaan maa-gɔ̀ɔn 

No experience necessary. / You don't need to have any previous experience.  

12) ไม่ได้มีแต่(อาหาร แต่ยังมี กาแฟ และ...) mâi-dâi mii dtɛ̀ɛ (aa-hǎan dtɛ̀ɛ yang mii gaa-fɛɛ lɛ́...) 

(Our restaurant) doesn't only/just have (food, but we have coffee and...)  

13) (...)อีกไม่กี่วัน(...) (...)ìik-mâi-gìi wan(...) 

In (just) a few days(...) / ...any day (now).  

14) (...)อีกไม่กี่นาที(...) (...)ìik-mâi-gìi naa-tii(...) 

In (just) a few minutes(...) / ...any minute (now). 

15) หล่นมาจาก(ฟ้า) lòn maa-jàak (fáa) 

(It) fell out of/dropped from (the sky). 

16) (เรื่องนี้)สอนให้รู้ว่า... (rʉ̂ang níi) sɔ̌ɔn hâi rúu wâa... The moral of the story is... / It teaches (us)...  

17) ไม่มีร่องรอย(การต่อสู้) mâi-mii rɔ̂ng-rɔɔi (gaan-dtɔ̂ɔ-sûu) There was no trace/sign (of a struggle).  

18) ท(ความ)ฝันให้เป็นจริง tam-(kwaam)fǎn -hai-bpen-jing Make a dream come true. / Make a dream become reality.  

19) แน่อยู่แล้ว nɛ̂ɛ yùu-lɛ́ɛo 

Sure. / I'm sure it was. / Absolutely. / Of course (it is).  

20) สับสนไปหมด sàp-sǒn bpai mot 

I'm so confused! / It's so confusing!  

Super Useful Expressions, Part 63  (high-beginner level) 

1) ฉันท้อง chǎn tɔ́ɔng 

I'm pregnant!  

2) ขับช้าๆ kàp chá-chá 

Drive slowly.  

3) ระวังหน่อย rá-wang nɔ̀i 

Be careful. / Watch it!  

4) เห็น... ไหม hěn... mǎi 

Did you see...? / Have you seen...?  

5) ไม่เห็น(นะ) mâi-hěn (ná) 

No, (I) didn't/haven't. / No.  

6) อีกนิดหนึ่ง ìik nít-nʉ̀ng

A bit more. / A little more. 

7) คุณพูดถูก kun pûut tùuk 

You're right. / You were right. (what you said is true)  

8) คุณพูดผิด kun pûut pìt 

You're wrong. / You were wrong. (what you said isn't true)  

9) ไม่เท่าไร mâi tâo-rai 

Not so much. / Not really. / Not very. 

10) สาธุ sǎa-tú 

Amen!  

11) ขอบคุณ(มาก)นะคะคุณหมอ kɔ̀ɔp-kun (mâak) ná-ká kun mɔ̌ɔ Thank you (very much) Doctor.  

12) สัญญากับผม(ว่า...) sǎn-yaa gàp pǒm (wâa...) Promise me (that...)  

13) กุญแจอยู่ในรถ gun-jɛɛ yùu nai rót 

The key's in the car. / The keys are in the car.  

14) ดีกว่ามาก dii-gwàa mâak 

Much better. 

15) ฝนกำลังจะตก fǒn gam-lang jà dtòk 

It's going to rain.  

16) มานั่ง(สิ) INFORMAL maa nâng (sì) 

Come sit. / Come and sit down. / Come. Have a seat.  

17) ฉัน(ก็)ไม่รู้ chǎn (gɔ̂ɔ) mâi-rúu 

I don't know. 

18) ไม่จริง(หรอก) mâi-jing (rɔ̀ɔk) 

That's not true! / Not true! / That's a lie! 

19) ดีจังเลย dii jang ləəi 

That's great. / That's really good. 

20) พยายามอยู่ pá-yaa-yaam yùu 

(I)'m trying.  

Super Useful Expressions, Part 64  (low-intermediate level) 

1) หายไวๆนะ hǎai wai-wai ná 

Get better soon. / Get well soon.  

2) ผมอาจจะกลับดึก pǒm àat-jà glàp dʉ̀k I might be late. / I might come home late.  

3) ให้ผมไปรับไหม hâi pǒm bpai ráp mǎi Do you want me to pick you up?  

4) อยากให้คุณไปด้วย yàak hâi kun bpai dûai I want you to go with me. / I'd like you to join me.  

5) ไม่อยากเป็นภาระ mâi-yàak bpen paa-rá I don't want to be a burden. / I don't want to burden you.  

6) อ่านต่อไป(เถอะ) àan dtɔ̀ɔ-bpai (tə̀) 

Keep (on) reading.  

7) ผมไม่สน(ใจ)ว่า... pǒm mâi-sǒn (jai) wâa... I don't care (if/what/that...)  

Note: ไม่สน (mâi-sǒn) is more casual.  

8) (...)ทั้งคืนเลย (...) táng-kʉʉn ləəi

...all night. / All night long.  

9) อย่าลืมบอกผม yàa lʉʉm bɔ̀ɔk pǒm 

Don't forget to tell me.  

10) ไม่ใช่แบบนั้น(ค่ะ) mâi-châi bɛ̀ɛp-nán (kà) 

Not like that. / It's not like that.  

11) เร็วกว่าที่คิด reo gwàa tîi kít 

Sooner than you think. / Faster than I thought. / Earlier than expected.  

12) รัก(เธอ)หมดใจ rák (təə) mòt-jai 

I love (her) with all my heart!  

13) คืนดีแล้ว kʉʉn-dii lɛ́ɛo 

They're back together again. / They've reconciled.  

14) ฉันกำลังจะบอกคุณว่า... chǎn gam-lang jà bɔ̀ɔk kun wâa... I'm telling you... / I'm going to tell you... 

15) จะไปไหนต่อ jà bpai-nǎi dtɔ̀ɔ 

Where are you going after this? / What are you doing  later? / Where're we going now?  

16) ต้องทำความสะอาดบ้าน dtɔ̂ng tam-kwaam-sà-àat bâan (I) have to clean the house.  

17) ไม่ได้คิดอะไร mâi-dâi kít à-rai 

(I) wasn't thinking anything. / (I)'m not thinking about anything.  

18) จำได้แล้ว jam-dâi lɛ́ɛo 

I remember now. / Now I remember.  

19) ทำไมไม่รับโทรศัพท์ tam-mai mâi ráp too-rá-sàp Why didn't (you) answer (your) phone?  

20) (ผมก็)แค่อยากรู้เฉยๆ (pǒm gɔ̂ɔ) kɛ̂ɛ yàak rúu chə̌əi-chə̌əi

(I) was just wondering. / (I) just wanna know. / (I) was just curious.  

Super Useful Expressions, Part 65 

(low-advanced level) 

1) ไม่ทันตั้งตัว mâi-tan dtâng-dtua 

(I) was caught off guard.  

2) คงต้องคุยกัน(เรื่อง...) kong dtɔ̂ng kui gan (rʉ̂ang...) 

(We) need to talk about it. / (We) need to talk about...  

3) มันถูกนำไปใช้กับ(โควิด-19) FORMAL man tùuk nam-bpai chái gàp (koo wít sìp-gâao) 

It can be used with (Covid-19). / It can be applied for use with (Covid-19). 

4) (เขาเป็นแบบนี้)ประจำเลย (kǎo bpen bɛ̀ɛp-níi) bprà-jam ləəi (He's) always (like this).  

Note: This means habitually or regularly. 

5) อยู่กับปัจจุบัน LITERARY yùu-gàp bpàt-jù-ban 

live in the present / live in the moment / stay in the present  

6) ต้องจากโลกนี้ไป LITERARY dtɔ̂ng jàak lôok níi bpai 

(We all) have to leave this world. / (We all) have to die (someday).  

7) ...เป็นที่รู้จักกันดี(...) SEMI-FORMAL ...bpen tîi-rúu-jàk gan dii(...) ... is well known (for/as)(...) / ... is famous (for)(...)  

8) ...ในเวลาต่อมา FORMAL ...nai wee-laa dtɔ̀ɔ-maa 

...subsequently / ...later (on)  

9) (เขา)มีส่วนเกี่ยวข้อง(...) SEMI-FORMAL (kǎo) mii sùan-gìao-kɔ̂ng(...) (He) is involved(...) / (He) was involved in (...) 

10) ไม่มีส่วนเกี่ยวข้อง SEMI-FORMAL mâi-mii sùan-gìao-kɔ̂ng (I) had nothing to do with it. / (I) wasn't involved.  

11) หลบไป RUDE lòp bpai 

Get out of my way! / Move it! / Clear out!  

12) ตอนที่เกิดเหตุ... dtɔɔn-tiî gə̀ət-hèet... 

When it happened... / When the incident occurred... / When the accident  happened...  

13) อย่ามาทำเป็นเล่นตัว yàa maa tam-bpen lên-dtua 

Don't play hard to get!  

14) ถ้าทุกอย่างเป็นไปตามแผน tâa túk-yàang bpen-bpai dtaam-pɛ̌ɛn If everything goes as planned...  

15) ยิ่งคุณทำแบบนี้(ฉันก็)ยิ่ง(เกลียดคุณ) yîng kun tam bɛ̀ɛp-níi (chǎn gɔ̂ɔ) yîng (glìat kun) 

The more you do that, the more (I hate you.).  

16) ...ขึ้นอยู่กับโชคชะตา ...kʉ̂n-yùu-gàp chôok-chá-dtaa (It)'s up to fate. / (It)'s predestined.  

17) ในบางกรณี... SEMI-FORMAL/LITERARY nai baang gɔɔ-rá-nii... In some cases...  

18) นังแพศยา VERY VULGAR nang pɛ̂ɛt-sà-yǎa 

You whore! / You (fuxxin') bitch! 

19) อย่าหักโหม yàa hàk-hǒom 

Don't overwork (yourself). / Don't overdo it. / Don't work too hard.  

20) รู้อย่างนี้แล้ว... rúu yàang-níi lɛ́ɛo 

Now that you know (this)... / Knowing this... 

Super Useful Expressions, Part 66 

(low-intermediate level) 

1) คุณทำอาหารเก่ง kun tam-aa-hǎan gèng 

You're a good cook!  

2) ฉันรู้สึกแย่มาก(เลย)(...) chǎn rúu-sʉ̀k yɛ̂ɛ mâak (ləəi)(...) I feel really bad(...) / I feel terrible about...  

3) ฉันรู้สึกอึดอัด(มาก) chǎn rúu-sʉ̀k ʉ̀t-àt (mâak) 

I feel (very) awkward/uncomfortable.  

4) เหมาะกับคุณมาก mɔ̀-gàp kun mâak 

It suits you well. / It looks great on you.  

5) ทิ้งไปแล้ว tíng bpai lɛ́ɛo 

(I) threw it away.  

6) ไม่เก็ท INFORMAL/SLANG mâi gèt 

(I) don't get it. / (I) don't understand.  

7) หายตัวไปแล้ว hǎai dtua bpai lɛ́ɛo 

She's disappeared! / He just vanished! / She went missing. 

8) ต้องใจเย็น ๆ dtɔ̂ng jai yen-yen 

You have to calm down. / You have to relax. (Don't get so worked up.)  

9) คุณไม่ยอมฟังผม kun mâi-yɔɔm fang pǒm 

You wouldn't listen to me. / You're not listening to me. 

10) จะติดต่อได้ยังไง(คะ) jà dtìt-dtɔ̀ɔ dâi yang ngai (ká) How can I contact you? / How will I be able to contact you?  

11) ไปทำงานได้แล้ว bpai tam-ngaan dâi lɛ́ɛo 

(You'd better) get to work. / Time to get to work. 

12) เดี๋ยว(จะ)สาย dǐao (jà) sǎai 

You'll be late. / Careful, or you'll be late.  

13) ไม่กี่วันเอง mâi-gìi wan eeng 

Only a couple of days / Only a few days  

14) เก็บ(เอา)ไว้ทำไม gèp (ao) wái tam-mai 

Why are you saving/keeping (it/them)? 

15) ผมทำให้ pǒm tam-hâi 

I'll do that (for you). / I'll get that (for you).  

16) มันเป็นโอกาสที่ดี man bpen oo-gàat tîi dii It's a good opportunity. / It was a good chance.  

17) แค่นั้นก็พอแล้ว kɛ̂ɛ nán gɔ̂ɔ pɔɔ lɛ́ɛo 

That's enough. / That would be enough. / That's plenty.  

18) ฉันไม่แพ้ใคร chǎn mâi-pɛ́ɛ krai 

I don't lose to anyone! / No one beats me! / I'm number one!  

19) เดินไม่ไหว dəən mâi-wǎi 

I can't walk (anymore)! / I can't even walk!  

20) ค่อยยังชั่ว(หน่อย) kɔ̂i yang chûa (nɔ̀i) 

I feel a bit better. / That's a relief.  

Note: This expression is very idiomatic, so don't spend too much  time trying to break down the meaning.  

Super Useful Expressions, Part 67  (low-intermediate level)

1) ทำให้เต็มที่(นะคะ) tam-hâi dtem-tîi (ná-ká) 

Do your best! / Give it all you got!  

2) มาเริ่มเลยดีกว่า maa rə̂əm ləəi dii-gwàa 

Let's get started.  

3) เลิกพูดเรื่อง... lə̂ək pûut rʉ̂ang... 

Stop talking about... !  

4) ฟังฉันอยู่รึเปล่า fang chǎn yùu rʉ́-bplàao 

Are (you) listening to me?  

5) ไม่เกรงใจเลย mâi greeng-jai ləəi 

(He)'s so inconsiderate! / (He) doesn't think about others!  

6) ใกล้เวลาแล้ว glâi wee-laa lɛ́ɛo 

It's almost time. / The time is near.  

7) ใกล้เวลาที่... glâi wee-laa tîi... 

It's nearly time to... / It's almost time to...  

8) ฉันปฏิเสธไม่ได้ chǎn bpà-dtì-sèet mâi-dâi 

I couldn't say no. / I couldn't refuse.  

9) เขาไม่ได้ว่าอะไร kǎo mâi-dâi wâa à-rai 

He didn't say anything.  

10) จำไม่ค่อยได้ jam mâi-kɔ̂i dâi 

(I) don't remember it so well. / (I) can't recall. / (I) don't remember much (about  it).  

11) หน้ายังเหมือนเดิม nâa yang mʉ̌an-dəəm 

(You) still look the same! / (Your) face hasn't changed at all. 

12) หนูเพิ่งจะเสร็จ(งานเอกสาร) nǔu pə̂ng-jà sèt (ngaan-èek-gà-sǎan) I just finished (my paperwork). 

13) เห็นแก่เงิน hěn-gɛ̀ɛ ngən 

(You) only care about money! / (You) are obsessed with money!  

14) อบอุ่นดี òp-ùn dii 

(It)'s nice and warm. / (It's) warm and cozy. 

15) ผมจะเข้าไปข้างใน(แล้ว) pǒm jà kâo-bpai kâang-nai (lɛ́ɛo) I'm gonna go inside. / I'm going inside now.  

16) ตอบเขาว่ายังไง dtɔ̀ɔp kǎo wâa yang-ngai 

How did (you) answer her? / What did you tell her?  

17) ผม(ก็)บอกเขาว่า... pǒm (gɔ̂ɔ) bɔ̀ɔk kǎo wâa... I told her...  

18) เป็นแผนที่ดีมาก bpen pɛ̌ɛn tîi dii mâak 

That's a great plan!  

19) ฉลองกันเถอะ chà-lɔ̌ɔng gan-tə̀ 

Let's celebrate!  

20) ไม่รู้จะทำอะไร mâi-rúu jà tam à-rai 

(I) don't know what to do.  

Super Useful Expressions, Part 68  (low-intermediate level) 

1) อยากมีลูกกี่คน yàak mii lûuk gìi kon 

How many kids do you want to have?  

2) รู้ว่าคุณคิดอะไร rúu wâa kun kít à-rai  

(I) know what you're thinking.  

3) จำที่นี่ได้ไหม jam tîi-nîi dâi-mǎi 

Do you remember this place? 

4) แล้วนี่อะไร lɛ́ɛo nîi à-rai 

So what is this?! / What is this?! / And what is this?!  

5) ไม่ต้องเป็นห่วง mâi-dtɔ̂ng bpen-hùang 

Don't worry about (me).  

6) เป็นเพราะ... bpen-prɔ́... 

That's because... / It's because...  

7) เคยคิดบ้างไหม(...) kəəi kít bâang mǎi (...) Have you ever thought...? / Has it ever occurred to you(...)?  

8) ถ้า(คุณ)อยากจะบอกว่า... tâa (kun) yàak jà bɔ̀ɔk wâa... If you want to say...  

9) เมื่อเช้านี้เอง mʉ̂a-cháao níi eeng 

Just this morning.  

10) นี่ฉันเอง nîi chǎn eeng 

Hey, it's me! / It's me!  

11) บางทีฉันก็คิด(ว่า)... baang-tii chǎn gɔ̂ɔ kít (wâa)... Sometimes I think...  

12) ผมมีเพื่อนไม่มาก pǒm mii pʉ̂an mâi-mâak I don't have many friends.  

13) เข้าไปได้ยังไง kâo-bpai dâi yang-ngai 

How did (he) get in?  

14) (จะ)ทำยังไงดี INFORMAL (jà) tam yang-ngai dii What can I do? / What should I do? / What do I do now? 

15) รับไปเถอะ(ค่ะ) ráp-bpai tə̀ (kà) 

Take it. / Please accept it. 

16) ผมจะไปรู้ได้ยังไงอ่ะ BIT STRONG pǒm jà bpai rúu dâi yang-ngai à How would I know?! / How the hell would I know?!  

17) แกพูดเรื่องอะไร gɛɛ pûut rʉ̂ang à-rai 

What are you talking about?!  

18) ร้องไห้อีกแล้วเหรอ rɔ́ɔng-hâi ìik-lɛ́ɛo rə̌ə 

Are you crying again?  

19) ใช้เวลานานแค่ไหน chái-wee-laa naan-kɛ̂ɛ-nǎi 

How long does it take?  

20) ตัดทรงอะไรดี dtàt song à-rai dii 

How would you like your hair cut? / What style do you want?  

Super Useful Expressions, Part 69 

(low-advanced level) 

1) กลับบ้านไม่ถูก glàp-bâan mâi-tùuk 

(I) couldn't find my way back home.  

2) ห่วงแทบแย่ hùang tɛ̂ɛp yɛ̂ɛ 

(I)'ve been worried sick. / (I)'ve been really worried about (you).  

3) ต้องค่อยเป็นค่อยไป dtɔ̂ng kɔ̂i bpen kɔ̂i bpai 

(We) have to take it slow/proceed gradually.  

4) ยังไม่ถึงขั้นนั้น yang mâi-tʉ̌ng kân nán 

(We)'re not to that point yet. / (We)'re not there yet. / (We)'re not to that stage yet.  

5) เอามาทำอะไร(ได้บ้าง) ao maa tam à-rai (dâi bâang) 

What can you do with it? / What can you use it for? (an old cell phone, etc.)  

6) (ไป)โดนตัวไหนมา SLANG (bpai) doon dtua nǎi maa 

What are you on?! 

7) ทำอย่างไรถึงจะ(มั่นใจในตนเอง) tam yàang-rai tʉ̌ng jà (mân-jai nai dton eeng) 

What can I do to (be self-confident)? / What can I do so that I will (have self confidence)?  

8) ทุกอย่างเข้าที่เข้าทาง túk-yàang kâo-tîi kâo-taang 

Everything's in order. / Everything's in place.  

9) (ผม)ไม่มีส่วนเกี่ยวข้องกับ... SEMI-FORMAL (pǒm) mâi-mii sùan-gìao kɔ̂ng gàp... 

(I) have nothing to do with... / (I)'m not involved with...  

10) นับจากนี้ไป... FORMAL/LITERARY náp jàak níi bpai... From here on out... / From now on... / From this day on...  

11) ไม่เอาดีกว่า mâi-ao dii-gwàa 

I'd better not. / I'll pass.  

12) ยิ่งใหญ่กว่าเดิม yîng-yài gwàa-dəəm 

Bigger than before! / Better than ever! / More powerful than ever!  

13) มาไกลขนาดนี้แล้ว(...) maa glai kà-nàat-níi lɛ́ɛo (...) 

(I)'ve come this far(...)  

14) ยังมีหน้า(มาขอเงินเธอ) yang mii-nâa (maa kɔ̌ɔ ngən təə) You have the nerve to (ask her for money)! / You have the gall to (ask her for  money).  

15) จะอธิบายเรื่องนี้(ได้)ยังไง jà à-tí-baai rʉ̂ang níi (dâi) yang-ngai How do you explain this?  

16) อย่ามาอ้าง(...) yàa maa âang (...) 

Don't make excuses! / Don't use ... as an excuse!  

17) เรื่องบ้าอะไรกัน rʉ̂ang bâa à-rai gan 

What the hell is this? / What is this bullshit? 

18) ไม่ต้องมาทำเป็นไม่รู้เรื่อง STRONG mâi-dtɔ̂ng maa tam-bpen mâi-rúu-rʉ̂ang Don't pretend you don't know what's going on! / Don't act like you know nothing!  

19) มันไม่ใช่ภาพหลอน man mâi-châi pâap-lɔ̌ɔn 

It wasn't an illusion. / You weren't hallucinating.  

20) มันเสียแรง(เปล่าๆ) man sǐa-rɛɛng (bplàao-bplàao) 

It's a waste of effort. / It was a waste of energy. / We did it for nothing.  

Super Useful Expressions, Part 70 

(low-intermediate level) 

1) ฉันอิจฉาคุณ chǎn ìt-chǎa kun 

I envy you. / I'm jealous!  

2) คิดว่าดีไหม kít-wâa dii mǎi 

Do you think that's a good idea? / Does that sound good?  

3) ยังไม่(ได้)จ่ายเงิน yang mâi(dâi) jàai ngən 

(I) haven't paid yet. / (You) didn't pay yet.  

4) จะอธิบายยังไงดี jà à-tí-baai yang-ngai dii 

How can (I) explain?  

5) ...จริงไหม ...jing mǎi 

..., right? / ..., isn't it? / ..., aren't they? (etc.)  

6) ใช่คนนี้หรือเปล่า châi kon-níi rʉ̌ʉ-bplàao 

Is this the person?  

7) ฉันจะบอกให้ทุกคนรู้(ว่า...) chǎn jà bɔ̀ɔk hâi túk kon rúu (wâa...) I'm gonna tell everyone! (...) 

8) ต้องการอะไรจากผม STRONG dtɔ̂ng-gaan à-rai jàak pǒm What do (you) want from me?!  

9) ผมทำไม่สำเร็จ pǒm tam mâi-sǎm-rèt 

I didn't make it. / I wasn't successful. / I couldn't do it.  

10) มาหาฉันหน่อยได้ไหม maa-hǎa chǎn nɔ̀i dâi-mǎi 

Can you come and see me? / Can you come and visit me?  

11) น่าปวดหัว nâa-bpùat-hǔa 

It gives me a headache! / It's really hard!  

12) ตอบไม่ได้จริงๆ dtɔ̀ɔp mâi-dâi jing-jing 

(I) really can't answer. / (I) really can't say.  

13) (มัน)เป็นเรื่องของฉัน (man) bpen rʉ̂ang kɔ̌ɔng chǎn 

It's my business. / That's my business. (used when someone asks you something  too personal, etc.)  

14) มีผู้ช่วยด้วย mii pûu-chûai dûai 

I have a helper. / I have someone to help me. / I have an assistant.  

Note: Adding ด้วย (dûai) here gives the meaning that the person normally  doesn't have a helper. 

15) เคยเห็นในหนัง... kəəi hěn nai nǎng... 

(I) (once) saw it in a movie(...)  

16) ต้องขับรถกลับ(บ้าน) dtɔ̂ng kàp-rót glàp(bâan) 

(I) have to drive home/back.  

17) รักที่สุดในโลก rák tîi-sùt nai lôok 

I love you more than anything in this world! / I love you so much!  

18) เอาใหม่ INFORMAL ao mài 

Try it again. / Once more. 

Note: Be careful to say ใหม่ (mài) with a falling tone. Otherwise, it might  sound like you are asking if someone wants something. 

19) ไม่ค่อยมีเงิน mâi-kɔ̂i mii ngən 

I don't have much money. / I'm short on cash.  

20) ฉันเขินนะ chan kə̌ən ná 

I'm shy. / I'm embarrassed. (blushing)  

Super Useful Expressions, Part 71 

(high-intermediate level) 

1) ตั้งสติไว้ dtâng sà-dtì wái 

Get a grip! / Come to your senses! / Pull yourself together!  

Note: The literal meaning is 'set up your consciousness/mind'.  

2) ฉันเป็นหนี้บุญคุณเขา(มาก) SEMI-LITERARY chǎn bpen-nîi-bun-kun kǎo (mâak) 

I'm (very) indebted to him. / I'm feel (very) grateful to him. / I owe him a lot.  

3) จุดแข็งของคุณคืออะไร(ครับ) jùt-kɛ̌ng kɔ̌ɔng kun kʉʉ à-rai (kráp) What's your strong point/strength?  

4) จุดอ่อนของคุณคืออะไร(ครับ) jùt-ɔ̀ɔn kɔ̌ɔng kun kʉʉ à-rai (kráp) What's your weak point/weakness?  

5) ไอ้สารเลว VULGAR âi sǎa-rá-leeo 

(You) Bastard! / Son of a bitch!  

Note: The literal meaning of this expression doesn't sound  

very strong at all. It means 'bad content/substance'.  

6) ฉันเกลียดแก chǎn glìat gɛɛ

I hate you! / I can't stand you!  

7) สิ่งเดียวที่ผมทำได้(คือ)... sìng diao tîi pom tam dâi (kʉʉ)... The only thing I could do is... / The only thing I can do is...  

8) ...ซึ่งก็คือ... SEMI-LITERARY ...sʉ̂ng gɔ̂ɔ kʉʉ... 

...which is...  

9) (มันดีมากๆ)อยู่พักนึง (man dii mâak-mâak) yùu pák-nʉ̀ng (It was really great) for a while.  

10) นิยามของคำว่า (X) คืออะไร FORMAL/LITERARY ní-yaam kɔ̌ɔng kam wâa (X) kʉʉ à-rai 

What's the definition of (X)? / What does (X) mean?  

11) ...จะช่วยให้คุณ(หลับสบายได้มากขึ้น) ...jà chûai hâi kun (làp sà-baai dâi mâak-kʉ̂n 

...will help you (to sleep better).  

12) ต้องหัด(พูดไทย) dtɔ̂ng hàt (pûut tai) 

(You)'ve got to practice/start (speaking Thai).  

13) ถ้าไม่ใช่เพราะ(ฉัน) tâa mâi-châi prɔ́(chǎn) 

If it wasn't/weren't for (me)...  

14) ชัดเจนเลยว่า... chát-jeen ləəi wâa... 

Clearly... / Obviously...  

15) ร้อนจะตาย(อยู่แล้ว) rɔ́ɔn jà dtaai (yùu-lɛ́ɛo) 

I'm so hot! / It's so hot. 

16) อยู่ได้ยังไง yùu dâi yang-ngai 

How can you stand it?! / How can you live (like that)?! / How can you stand being  here?! 

17) ไม่รู้หายไปไหน mâi-rúu hǎai bpai nǎi 

(I) don't know where it went. / (I) don't know where it disappeared to.

18) อย่าหลอกตัวเอง yàa lɔ̀ɔk dtua-eeng 

Don't fool yourself. / Don't kid yourself. / Don't deceive yourself.  

19) ต้องฟิตร่างกาย dtɔ̂ng fít-râang-gaai 

(I) gotta get in shape. / (I) gotta get fit.  

20) (มัน)ต้องอย่างนี้(สิ) (man) dtɔ̂ng yàang-níi (sì) That's it! / Way to go! (Good job---that's the way to do it.)  

Super Useful Expressions, Part 72  (high-beginner level) 

1) ไม่ดีเลย(อ่ะ) mâi-dii ləəi (à) 

Not good. / That's not good. 

Note: อ่ะ (à) is a shortened, informal form of ล่ะ (là). 

2) มันอยู่ไหน man yùu-nǎi 

Where is it?  

3) ฉันเลี้ยง(แมว)(สอง)ตัว chǎn líang (mɛɛo) (sɔ̌ɔng) dtua I have (two) (cats).  

4) ขับรถไม่เป็น kàp-rót mâi-bpen 

I can't drive.  

5) เราอายุเท่ากัน rao aa-yú tâo-gan 

We're the same age.  

6) อายุแค่ (7) ขวบ aa-yú kɛ̂ɛ (jèt) kùap 

(She)'s only (7) years old. 

7) มันเสียเวลา man sǐa-wee-laa 

It's a waste of time. 

8) ขอยืมหน่อย INFORMAL kɔ̌ɔ yʉʉm nɔ̀i 

Can I borrow it? / Let me borrow it.  

9) วันนี้เป็น(วันแห่งความรัก) wan-níi bpen (wan hɛ̀ng kwaam-rák) Today is (Valentine's Day).  

10) มีสิ(ครับ) mii sì (kráp) 

Yes, (I have one/some). / Sure.  

11) ผมเข้าใจแล้ว pǒm kâo-jai lɛ́ɛo 

I understand (now).  

12) ฉันทำงานทุกวัน(เลย) chǎn tam-ngaan túk-wan (ləəi) I work every day.  

13) เขาไม่อยู่แล้ว kǎo mâi-yùu lɛ́ɛo 

He's not here's anymore.  

14) ไม่รู้เธออยู่ไหน mâi-rúu təə yùu-nǎi 

I don't know where she is. 

15) จำง่าย(ดี) jam ngâai (dii) 

Easy to remember. / That's easy to remember.  

16) ไม่เห็นหรือ mâi hěn rʉ̌ʉ 

Don't you see it? / Can't you see it?  

17) มาหาฉันหน่อยสิ maa-hǎa chǎn nɔ̀i sì 

Come and see me. / Come and visit me.  

18) ฉันรู้สึกเหนื่อย chǎn rúu-sʉ̀k nʉ̀ai 

I feel tired.  

19) เพลงเพราะ pleeng prɔ́

(That) song's nice. (sounds good) / Nice song.  

20) มีความหมายดี mii kwaam-mǎai dii 

It has good meaning.  

Super Useful Expressions, Part 73 

(low-advanced level) 

1) พูดยังไม่ทันจบ pûut yang mâi-tan jòp 

(I) haven't finished talking yet! / (I) haven't finished what I was going to say!  

2) ไม่คิดหน้าคิดหลัง mâi kít nâa kít lǎng 

(She) just does it without thinking about the consequences! / (She)'s so rash! /  (She) just goes ahead and does it without a second thought.  

3) ดูภายนอก... duu paai-nɔ̂ɔk... 

Superficially... / Outwardly... / On the surface...  

4) แต่ติดปัญหาตรง(ที่ว่า)... dtɛ̀ɛ dtìt bpan-hǎa dtrong (tîi-wâa)... But the problem is (that)...  

5) มีกฎหมายข้อไหนไม่ให้(ถ่ายรูป) mii gòt-mǎai kɔ̂ɔ nǎi mâi-hâi (tàai-rûup) Is there a law against (taking pictures)?  

6) ไม่ต้องน้อยใจ mâi-dtɔ̂ng nɔ́ɔi-jai 

Don't feel slighted. 

7) กินข้าวพร้อมหน้าพร้อมตา gin-kâao prɔ́ɔm-naa-prɔ́ɔm-dtaa (Having dinner) all together.  

8) ไม่มีผู้ชายคนไหน(ชอบผู้หญิงขี้บ่น) mâi-mii pûu-chaai kon-nǎi (chɔ̂ɔp pûu-yǐng kîi-bon) 

No guy... (likes a woman who complains all the time). 

9) ทำไมเรื่องแบบนี้ต้องเกิดขึ้นกับฉัน tam-mai rʉ̂ang bɛ̀ɛp-níi dtɔ̂ng gə̀ət-kʉ̂n gàp chǎn 

Why does this have to happen to me?!  

10) ใช้เวลาให้คุ้มค่าที่สุด chái-wee-laa hâi kúm-kâa tîi-sùt Make the most of your time. / Make your time really matter.  

11) ค่อยว่ากันอีกที kɔ̂i wâa gan ìik tii 

Let's talk about it another time. / We can discuss it later. 

12) มันไม่จบง่ายๆ man mâi-jòp ngâai-ngâai 

It ain't over 'til it's over! / It ain't over 'til the Fat Lady sings! / It won't be over so  easily!  

13) เวลาจะเยียวยาทุกอย่าง LITERARY wee-laa jà yiao-yaa túk-yàang Time will heal everything.  

14) ใช้เวลานานแค่ไหนกว่าจะ(เห็นผล) chái-wee-laa naan kɛ̂ɛ-nǎi gwàa jà (hěn pǒn) 

How long does it take before (you see results)? 

15) (จะ)ไม่มีอะไรเหมือนเดิมอีกแล้ว (jà) mâi-mii à-rai mʉ̌an-dəəm ìik lɛ́ɛo Nothing will ever be the same again. / Nothing will be like it was before.  

16) ใช้ได้บ้างไม่ได้บ้าง chái-dâi bâang mâi-dâi bâang 

Sometimes (it) works, sometimes it doesn't.  

17) มาถึงขนาดนี้แล้ว... maa-tʉ̌ng kà-nàat-níi lɛ́ɛo... 

(We)'ve come this far... / (You)'ve made it this far...  

18) ผมต้องแบกรับภาระ... pǒm dtɔ̂ng bɛ̀ɛk-ráp paa-rá... 

I have to bear/carry the burden (of)... 

19) หล่อนไม่สนใจอะไรเลยนอกจาก(เงิน) lɔ̀n mâi sǒn-jai à-rai ləəi nɔ̂ɔk-jàak (ngən) 

She's not interested in anything except (money). 

20) ผมจะทำอะไรได้ pǒm jà tam à-rai dâi 

What can I do? (This is sarcastic and means you can't do anything.)  

Super Useful Expressions, Part 74 

(low-intermediate level) 

1) เขาหุ่นดี(นะ) kǎo hùn dii (ná) 

He's got a nice body. / She's got a nice figure.  

2) ฉันว่าอ้วนไปหน่อย chǎn wâa ûan bpai-nɔ̀i 

I think I'm a bit fat/overweight.  

3) ไม่หรอก(ครับ) mâi rɔ̀ɔk (kráp) 

You are not. / No, (not at all).   (disagreeing or clarifying)  

4) มีรอยสักที่แขน mii rɔɔi-sàk tîi kɛ̌ɛn 

S/he has a tattoo on her arm.  

5) ตอบ(ฉัน)มา(สิ) dtɔ̀ɔp (chǎn) maa (sì) 

Answer me!  

6) ผมทำงานส่วนตัว pǒm tam-ngaan sùan-dtua 

I'm self-employed. 

7) ฉัน(ก็)ใช้บ่อย chǎn (gɔ̂ɔ) chái bɔ̀i 

I use (it) a lot. / I often use (it).  

8) เขาเป็นคนยังไง kǎo bpen kon yang-ngai 

What's he like?  

9) กินอะไรสักหน่อย gin à-rai sàk-nɔ̀i 

Eat something. / Have something to eat. 

10) ให้เวลาอีก(ห้านาที) hâi wee-laa ìik (hâa naa-tii) 

Give it (5 more minutes).  

11) คนไม่แน่น kon mâi-nɛ̂n 

It's not crowded.  

12) มีตั้งเยอะ mii dtâng yə́ 

There are a lot of them! / There's plenty.  

13) ไม่มีมารยาท mâi-mii maa-rá-yâat 

You have no manners. / You're being rude.  

Note: มารยาท (maa-rá-yâat) is also pronounced as man-rá-yâat or man-yâat. 

14) ค่อยๆ(ค่ะ) kɔ̂i-kɔ̂i (kà) 

Easy! / (Go) slowly! / Gently! 

15) เขาถูกรถชน kǎo tùuk rót-chon 

He was hit by a car.  

16) เราเป็นเพื่อนสนิทตั้งแต่(เรียนมัธยม) rao bpen pʉ̂an sà-nìt dtâng-dtɛ̀ɛ (rian mát-tá-yom) 

We've been close friends since (high school).  

17) ผมไม่ได้โมโห pǒm mâi-dâi moo-hǒo 

I'm not angry!  

18) มีอะไรแปลกๆ mii à-rai bplɛ̀ɛk-bplɛ̀ɛk 

There's something strange (going on). / Something is off.  

19) ไม่เห็นมีอะไร(เลย) mâi-mii à-rai (ləəi) 

(I) don't see anything! / There's nothing there!  

20) หนูว่าเขาน่ารักดี nǔu wâa kǎo nâa-rák dii 

I think s/he's cute.

Super Useful Expressions, Part 75 

(low-intermediate level) 

1) โทษที่ไม่ได้บอก INFORMAL tôot tîi mâi-dâi bɔ̀ɔk 

Sorry I didn't tell you.  

2) ยกเลิก(ไป)แล้ว yók-lə̂ək (bpai) lɛ́ɛo 

(It) was cancelled. / (It) was called off.  

3) เขาขยันทำงาน kǎo kà-yǎn tam-ngaan 

He's very hard-working. / He works very hard.  

4) ต้องทำโอที INFORMAL dtɔ̂ng tam-oo-tii 

(I) have to work overtime. / (I) had to work overtime. 

Note: More formally overtime is ทำงานนอกเวลา (tam-ngaan nɔ̂ɔk-wee-laa). 

5) พูดแบบนี้ได้ยังไง pûut bɛ̀ɛp-níi dâi yang-ngai 

How can you say that? / How can you talk that way?  

6) (10) กว่าปี (sìp) gwàa bpii 

For over (10) years. / More than (10) years.  

7) เจอ(ได้)บ่อย jəə (dai) bɔ̀i 

(It)'s common. / You come across (it) a lot. 

8) ฉันไม่ได้คิดไปเอง chǎn mâi-dâi kít bpai eeng 

I wasn't just imagining. / I wasn't just my imagination. / It wasn't all in my head.  

9) หนูคิดถึงคุณพ่อคุณแม่(มาก) nǔu kít-tʉ̌ng kun-pɔ̂ɔ kun-mɛ̂ɛ (mâak) I (really) miss my parents! / I (really) miss my mother and father!  

10) หูจะแตก hǔu jà dtɛ̀ɛk 

My eardrums are gonna burst! / I'm gonna go deaf! 

11) คุณซื้ออะไรให้เขา kun sʉ́ʉ à-rai hâi kǎo 

What did you get him?  

12) ผมเป็นคนต่างจังหวัด pǒm bpen kon dtàang-jang-wàt I'm from another province. / I'm from a rural province. / I'm a country boy. 

13) พอกินได้ pɔɔ-gin-dâi 

It's edible. / I can eat it. (I don't like it so much, but that's okay.)  

14) ...ไม่เท่ากัน ...mâi tâo-gan 

They're not equal. / It's unequal. 

15) งานแรกที่ทำคือ(ล้างจาน) ngaan rɛ̂ɛk tîi tam kʉʉ (láang-jaan) My first job was (washing dishes). / I first worked as (a dishwasher).  

16) เขาไม่มีการศึกษา kǎo mâi mii gaan-sʉ̀k-sǎa 

He's uneducated. / He didn't have an education.  

17) โง่สุดๆ ngôo sùt-sùt 

So stupid! / You're an idiot! / You're really dumb!  

18) ฉันไม่ได้แต่งหน้า(เลย) chǎn mâi-dâi dtɛ̀ng-nâa (ləəi) I'm not wearing any makeup. / I didn't put on any makeup.  

19) พูดใหม่สิ INFORMAL pûut mài sì 

Say again. / Come again.  

20) เชื่อ(ผม)หรือยัง chʉ̂a (pǒm) rʉ̌ʉ-yang 

Now do you believe me? / Do you believe me now?  

Super Useful Expressions, Part 76 

(low-intermediate level) 

1) จะลืมได้ยังไง jà lʉʉm dâi yang-ngai

How can I forget? / How can I forget (you)?  

2) ทำต่อไป tam dtɔ̀ɔ-bpai 

Keep going (keep doing it). / Carry on. 

3) มีเท่านี้ mii tâo-níi 

That's all (I have). / That's it.  

4) เมื่อ(วันเสาร์)ที่ผ่านมา mʉ̂a (wan-sǎao) tîi-pàan-maa Last (Saturday)...  

5) เริ่มต้นใหม่อีกครั้ง rə̂əm-dtôn mài ìik kráng Start (all) over again. / Start afresh.  

6) มีหลายสาขา mii lǎai sǎa-kǎa 

There are many branches. (stores, etc.)  

7) พ่อต้องเชื่อผม pɔ̂ɔ dtɔ̂ng chʉ̂a pǒm 

You have to believe me, Dad! / You gotta believe me!  

8) เขาทำอะไรคุณหรือเปล่า kǎo tam à-rai kun rʉ̌ʉ-bplàao Did they do anything to you? 

9) เลิกงานดึก lə̂ək-ngaan dʉ̀k 

(I) was working late. / I finished work late. 

10) ไปกินข้าวกัน bpai gin-kâao gan 

Let's go eat. / Let's go get something to eat.  

11) กินอะไรดีนะ gin à-rai dii ná 

What do you wanna eat? / What should we eat?  

12) ฉัน(ได้)มีโอกาสคุยกับ... chǎn (dâi) mii oo-gàat kui gàp... I had a chance to talk with...  

13) จำรายละเอียดไม่ได้ jam raai-lá-ìat mâi-dâi (I) can't remember the details.

14) เข้าใจที่ฉันพูดไหม kâo-jai tîi chǎn pûut mǎi 

Do you understand me? / Do you understand what I'm saying? / Do you  understand what I said? 

15) ฉันเคยมาที่นี่หลายครั้ง chǎn kəəi maa tîi-nîi lǎai kráng I'm been here many times (before).  

16) (จะ)ทำให้ดูสวยขึ้น (jà) tam-hâi duu sǔai kʉ̂n 

(It) (will) make you look more beautiful. / (It) makes it look more attractive. 

17) (ผม)ยินดีช่วย (pǒm) yin-dii chûai 

(I)'m happy to help. / Happy to help.  

18) หวังว่าคุณจะเข้าใจ wǎng-wâa kun jà kâo-jai 

(I) hope you understand.  

19) ขอถ่ายรูปได้ไหมคะ kɔ̌ɔ tàai rûup dâi-mǎi ká 

Can I take your/a picture?  

20) อย่าทำให้(ผม)โมโห yàa tam-hâi (pǒm) moo-hǒo Don't make me angry/mad.  

Super Useful Expressions, Part 77 

(low-intermediate level) 

1) แกต้องช่วยฉัน gɛɛ dtɔ̂ng chûai chǎn 

You have to help me!  

2) ฟังอยู่รึเปล่า fang yùu rʉ́-bplàao 

Are you there? / Are you listening?  

3) ไม่น่าทำแบบนี้(ครับ) mâi-nâa tam bɛ̀ɛp-níi (kráp) 

You shouldn't have done that. 

4) ผมไม่เคยพูดแบบนั้น pǒm mâi kəəi pûut bɛ̀ɛp-nán 

I never said that.  

5) สำหรับผม(แล้ว)... sǎm-ràp pǒm (lɛ́ɛo) 

For me... / To me...  

6) ไม่ต้องบอก mâi-dtɔ̂ng bɔ̀ɔk 

You don't need to tell (me). / It's not necessary to tell (me).  

7) (มัน)ทำให้ผมงง (man) tam-hâi pǒm ngong 

(It) confused me. / (It) confuses me. 

8) แปลกดีนะ bplɛ̀ɛk-dii ná 

That's odd/strange/weird.  

Note: This doesn't have a negative connotation. It means weird in an interesting  way.  

9) ใส่ตัวไหนดี sài dtua nǎi dii 

Which one should I wear?  

10) หาอะไรทำดีกว่า hǎa à-rai tam dii-gwàa 

Let's find something to do.  

11) เขาฆ่าตัวตาย kǎo kâa-dtua-dtaai 

He killed himself. / He committed suicide.  

12) ไม่มีความหมายอะไร mâi-mii kwaam-mǎai à-rai 

It doesn't mean anything. / It has no meaning. / It's meaningless.  

13) วันเว้นวัน wan-wén-wan 

Every other day.  

14) แกรักเขามากใช่ไหม gɛɛ rák káo mâak châi-mǎi 

You love him a lot, don't you?

15) ทำไมไม่รับเลย tam-mai mâi-ráp ləəi 

Why doesn't (he) answer! / Why doesn't (he) pick up the phone!  

16) รู้ว่าผมเป็นใคร BIT ROUGH rúu-wâa pǒm bpen krai  

Do (you) know who I am?  

17) ทำอะไรพิเศษ tam à-rai pí-sèet 

Do something special.  

18) ไม่มากเท่าไหร่ mâi-mâak tâo-rài 

Not much. / Not so much.  

19) โกหกหรือเปล่า goo-hòk rʉ̌ʉ-bplàao 

Are you lying?  

20) หมดเวลาแล้ว mòt-wee-laa lɛ́ɛo 

Time's up.  

Super Useful Expressions, Part 78 

(low-advanced level) 

1) เวลาผ่านไปไวเหมือนโกหก IDIOM wee-laa pàan bpai wai mʉ̌an goo-hòk Time flies! / Time goes by quickly. 

2) และแล้ววันนั้นก็มาถึง lɛ́-lɛ́ɛo wan-nán gɔ̂ɔ maa-tʉ̌ng 

The day finally arrived. / That day finally came.  

3) กรรมตามสนอง IDIOM gam dtaam sà-nɔ̌ɔng 

You reap what you sow. / Karma will get you in the end. / He will get what he  deserves. / It's karmic retribution.  

4) กาลครั้งหนึ่งนานมาแล้ว... LITERARY gaa-lá-kráng-nʉ̀ng-naan-maa-lɛ́ɛo Once upon a time...  

5) อีก(ค)หนึ่งที่น่าสนใจก็คือ... ìik (kam) nʉ̀ng tîi nâa-sǒn-jai gɔ̂ɔ kʉʉ...

Another interesting (word) is...  

6) ทุกอย่างก็ผ่านมาได้ด้วยดี túk-yàang gɔ̂ɔ pàan maa dâi dûai dii Everything turned out well. / (We) made it through everything just fine.  

7) ไม่ว่าจะเป็น... mâi-wâa jà bpen... 

Whether (it be)...  

8) เตรียมรับมือไว้ dtriam ráp-mʉʉ wái 

Be prepared (for...) / Get ready (for...)  

9) ...หลักๆก็คือ... FORMAL/LITERARY ...làk-làk gɔ̂ɔ kʉʉ... Essentially... / Basically... / Mainly...  

10) ต้องมีอะไรปิดบังแน่ๆ dtɔ̂ng mii à-rai bpìt-bang nɛ̂ɛ-nɛ̂ɛ They must be hiding something! / He must be covering something up!  

11) (รู้จักเขา)มากน้อยแค่ไหน (rúu-jàk kǎo) mâak-nɔ́ɔi-kɛ̂ɛ-nǎi How well (do you know him)? / How much (...)?  

12) ไม่มีหลักฐานแน่ชัด(...) mâi-mii làk-tǎan nɛ̂ɛ-chát (...) 

There's no clear evidence (...)  

13) (Its) กับ (It's) ใช้ไม่เหมือนกัน (Its) gàp (It's) chái mâi-mʉ̌an-gan (Its) and (It's) are used differently. 

14) ...เป็นผลเสียต่อร่างกาย SEMI-FORMAL ...bpen pǒn-sǐa dtɔ̀ɔ râang-gaai (It)'s harmful to your body. 

15) เธอชอบแต่งตัวโป๊ təə chɔ̂ɔp dtɛ̀ng-dtua bpóo 

She dresses a bit too sexily. / She dresses like a slut. 

16) ผมท้องไส้ปั่นป่วน pǒm tɔ́ɔng-sâi-bpàn-bpùan 

I have an upset stomach. / My stomach is very upset. / I have butterflies in my  stomach. 

17) (ฟุตบอล)เป็นชีวิตจิตใจ LITERARY/FANCY/IDIOM (fút-bɔn) bpen chii-wít  jìt-jai 

(Football) is my passion! / I'm crazy about (football)! / (Football) is the joy of my  life! / (Football) is who I am!  

18) ทายสิใครเอ่ย taai sì krai ə̀əi 

Guess who?! / Who is it?! (used as guessing game)  

19) เป็นสิ่งที่เราขาดไม่ได้ bpen sìng tîi rao kàat-mâi-dâi 

(It)'s something that we can't do without. / (It)'s something that's indispensible for  us.  

20) ไม่ต้องอ้อมค้อม mâi-dtɔ̂ng ɔ̂ɔm-kɔ́ɔm 

Don't beat around the bush!  

Super Useful Expressions, Part 79 

(high-intermediate level) 

1) คืองี้... kʉʉ-ngíi... 

It's like this... / Look... / This is how it is... / Here's the thing... (said before  explaining something) 

Note: งี้ (ngíi) is a very abbreviated form of อย่างนี้ (yàang-níi). 

2) มีอะไรให้(คุณ)ดู mii à-rai hâi (kun) duu 

I have something to show you. / I wanna show you something.  

3) ผมก็ว่าอย่างนั้น(แหละ) pǒm gɔ̂ɔ wâa yàang-nán (lɛ̀) 

That's what I thought. / I thought so.  

4) กลับกลายเป็นว่า... glàp glaai-bpen wâa... 

It turns/turned out that...  

5) ผมสังหรณ์ใจว่า... pǒm sǎng-hɔ̌ɔn-jai wâa...

I have a (bad) feeling (that)... / I have a hunch that...  

6) (แล้ว)ผมก็คิดได้ว่า... (lɛ́ɛo) pǒm gɔ̂ɔ kít-dâi wâa... (And then) I realized...  

7) นานแสนนาน LITERARY naan-sɛ̌ɛn-naan 

For such a long time / For so long  

8) พูดดีๆ(ก็ได้) pûut dii-dii (gɔ̂ɔ-dâi) 

Speak nicely, please. / Can't you be polite (and speak nicely)?  

9) ทำได้ดีมาก tam-dâi-dii mâak 

Good work! / Nice job! / Well done!  

10) มันดูไม่ค่อย(ดี) man duu mâi-kɔ̂i (dii) 

It doesn't look so (good).  

11) อย่าส่งเสียงดัง yàa sòng-sǐang-dang 

Don't be noisy. / Keep quiet. / Quit making (all that) noise.  

12) ทำไมทำหน้าแบบนั้น tam-mai tam-nâa bɛ̀ɛp-nán Why are you making that face? / Why are you making a face like that?  

13) ทำไมเพิ่งบอก tam-mai pə̂ng bɔ̀ɔk 

Why are you just telling me now?! / Why did you wait until now to tell me?!  

14) มาหาฉันหน่อย maa-hǎa chǎn nɔ̀i 

Come see me. 

15) ทำไมยังไม่มาอีก tam-mai yang mâi maa ìik 

Why hasn't he come yet?! / Why isn't he here yet?!  

16) จัดการเรียบร้อย SEMI-FORMAL jàt-gaan rîap-rɔ́ɔi It's all taken care of. / I took care of it already. / It's all done.  

17) ไว้ค่อยคุยกันนะ wái kɔ̂i kui gan ná 

Let's talk (about it) later. / Talk to you later. 

18) ไว้คุยกัน(พรุ่งนี้) wái kui gan (prûng-níi) 

Let's talk about it (tomorrow). / We can talk about it (tomorrow).  

19) (ไอ้)เด็กเวร VULGAR (âi) dèk-ween 

You little shit! / You punk! / You little bastard!  

20) (คุณ)เจอดีแน่ STRONG (kun) jəə-dii nɛ̂ɛ You're gonna get it! / Your ass is grass! / You'll be in deep shit!  

Super Useful Expressions, Part 80 (high-beginner level) 

1) วันนี้วันหยุด wan-níi wan-yùt 

Today's (my) day off. / Today is a holiday.  

Note: หยุด (yùt) means 'stop', so the meaning is 'stop working'.  

2) คุณเป็นฮีโร่ของผม kun bpen hii-rôo kɔ̌ɔng pǒm You're my hero!  

3) ฉันทำได้ chǎn tam-dâi 

I can do it.  

4) คุยกับใคร kui gàp krai 

Who were you talking with? / Who are you talking to?  

5) ซื้อมาทำไม sʉ́ʉ maa tam-mai 

Why did you buy it/this/that?  

6) เหนื่อยไหมคะ nʉ̀ai mǎi ká 

Are you tired?  

7) โอ๊ย óoi

Ouch! / Ow!  

8) ปวดท้อง bpùat-tɔ́ɔng 

I have a stomachache.  

9) ผมปวดฟัน pǒm bpùat-fan 

I have a toothache.  

10) ฉันปวดหลัง chǎn bpùat-lǎng 

I have a backache.  

11) คุณได้เจอ... ไหม kun dâi jəə... mǎi 

Have you met/seen (name)?  

12) จะไม่กลับไป jà mai glàp bpai 

(I)'m not going back.  

13) เขาแข็งแรงมาก kǎo kɛ̌ng-rɛɛng mâak He's very strong.  

14) ห้ามเปิดดู hâam bpə̀ət duu 

Don't open it. / You mustn't open it (and look inside). 

15) ฉันทำงานทุกวัน(เลย) chǎn tam-ngaan túk-wan (ləəi) I work every day. / I've been working every day.  

16) ฉันจะรอนะ chǎn jà rɔɔ ná 

I'll be waiting. / I'll wait.  

17) เช็คบิลด้วย(ครับ) chék-bin dûai (kráp) Check please. / Can I have the check, please?  

18) ฉันไม่ได้ถาม chǎn mâi-dâi tǎam 

I didn't ask. / I didn't ask him.  

19) หยุด yùt 

Stop! / Stop it! 

20) หยุดพูด yùt pûut 

Stop saying that! / Stop it! (saying that)  

Super Useful Expressions, Part 81 

(low-intermediate level) 

1) เมื่อไหร่ก็ได้ mʉ̂a-rài gɔ̂ɔ dâi 

Anytime. / Whenever you want. 

2) มีอะไรอีก mii à-rai ìik 

What else? / Anything else? 

3) พักผ่อนดีกว่านะ pák-pɔ̀n dii-gwàa ná 

(You) should get some rest.  

4) เพลงเพราะและมีความหมายดีด้วย pleeng prɔ́ lɛ́ mii kwaam-mǎai dii dûai It's a nice song and has good meaning too. 

5) ไม่ได้กินมานาน mâi-dâi gin maa naan 

I haven't had (that) for a long time. / It's been awhile since I ate (that).  

6) ไม่ได้เป็นความจริง mâi-dâi bpen-kwaam-jing 

It's not true.  

7) เขามีความรู้เยอะ kǎo mii kwaam-rúu yə́ 

He's very knowledgable.  

8) เห็นกันบ่อย hěn gan bɔ̀i 

You see (it) often. 

9) โดยเฉพาะ(ตอนกลางคืน) dooi-chà-pɔ́(dtɔɔn-glaang-kʉʉn)

Especially (at night).  

10) เราจะทำอะไรก็ได้ rao jà tam à-rai gɔ̂ɔ-dâi 

We could/can do anything (we want).  

11) ยังไม่อยากกลับ yang mâi-yàak glàp 

(I) don't wanna go back (yet).  

12) ยังไม่อยากตาย yang mâi-yàak dtaai 

(I) don't wanna die (yet)! / (I)'m not ready to die!  

13) ยังไม่อยากตื่น yang mâi-yàak dtʉ̀ʉn 

(I) don't wanna get up (yet).  

14) มันน่าสงสัย man nâa-sǒng-sǎi 

It seems suspicious. / It's (kind of) suspicious. 

15) ไปนานแค่ไหน bpai naan-kɛ̂ɛ-nǎi 

How long are (you) going for? / How long for? 

16) จะกลับมาเมื่อไร jà glàp maa mʉ̂a-rai 

When are you coming back? / When does she get back?  

17) อดขำไม่ได้ òt kǎm mâi-dâi 

(I) can't help but laugh. / (I) can't keep from laughing.  

18) หยุดได้แล้ว yùt dâi lɛ́ɛo 

Stop it! / Knock it off!  

19) (มัน)ไม่ใช่เรื่องของ(คุณ) (man) mâi-châi rʉ̂ang kɔ̌ɔng (kun) It's none of (your) business.  

20) ไม่ได้เรื่อง(เลย) mâi-dâi-rʉ̂ang(ləəi) 

(You)'re hopeless/useless/good-for-nothing.  

Super Useful Expressions, Part 82

(low-intermediate level) 

1) มันอธิบายยาก man à-tí-baai yâak 

It's difficult to explain. / It's hard to explain.  

2) พระเจ้า prá-jâo 

God! / Oh my God!  

3) คิดว่านะ kít-wâa ná 

I think (so). / I guess (so). (I think I understand, etc.) 

4) เมืองที่ใหญ่ที่สุดคือ(กรุงมาดริด) mʉ̂ang tîi yài-tîi-sùt kʉʉ (grung-maa-drìt) The largest city is (Madrid).  

5) เล็กกว่าประเทศไทย lék-gwàa bprà-têet-tai 

(It)'s smaller than Thailand.  

6) ภาษาราชการคือภาษา(สเปน) paa-sǎa râat-chá-gaan kʉʉ paa-sǎa (sà-bpeen) The official language is (Spanish).  

7) ฉันจะนั่งรออยู่ตรงนี้ chǎn jà nâng rɔɔ yùu-dtrong-nìi 

I'll sit (right) here and wait for you. / I'll be sitting here waiting for you.  

8) ช่วยบอกผมหน่อย(ว่า)... chûai bɔ̀ɔk pǒm nɔ̀i (wâa)... 

Please tell me...  

9) หลับไม่สนิท làp mâi-sà-nìt 

(I) didn't sleep well. / (I) don't sleep soundly. 

10) ผมอยากรู้เกี่ยวกับ (MacBook Air) pǒm yàak rúu gìao-gàp (MacBook Air) I'd like to know about (the MacBook Air). / I wanna know about (the MacBook Air).  

11) ผมให้โอกาสคุณ(...) pǒm hâi-oo-gàat kun (...) 

I gave you a/the chance (...)  

12) คิดว่าอะไรคือ... kít-wâa à-rai kʉʉ...

What do you think is... ?  

13) จริงอ่ะ INFORMAL jing à 

Really?  

14) มีหลายความหมาย mii lǎai kwaam-mǎai 

(It) has a lot of (different) meanings. / (It) has various meanings. 

15) อะไรว่ะ STRONG à-rai wâ 

What!? / What the hell?! / Damn it!  

16) กลัว(จะ)อ้วน glua (jà) ûan 

(I)'m afraid I'll gain weight. / (I)'m worried about getting fat.  

17) คุณไม่เชื่อผมใช่มั้ย kun mâi-chʉ̂a pǒm châi-mái You don't believe me, do you?  

18) ไม่ต้องรีบ(ร้อน) mâi-dtɔ̂ng rîip (rɔ́ɔn) 

No need to rush. / There's no hurry. / No rush.  

19) ผมไม่กลัวตาย pǒm mâi glua dtaai 

I'm not afraid to die. 

20) ทำไมไม่บอกฉัน tam-mai mâi bɔ̀ɔk chǎn 

Why didn't you tell me?  

Super Useful Expressions, Part 83 (low-intermediate level) 

1) ยังมี(ความ)หวัง yang mii (kwaam)wǎng 

There's still hope. / He still has hope. 

2) ทำอะไรดี tam à-rai dii 

What should I do? / What should we do? 

3) เข้าใจป่ะ VERY INFORMAL kâo-jai bpà 

Got it? / Understand?  

Note: ป่ะ (bpà) is a shortened form of เปล่า (bplàao) [from หรือเปล่า (rʉ̌ʉ-bplàao)].  

4) ใส่ไว้ใน(กระเป๋า) sài-wái-nai (grà-bpǎo) 

Put it in (your pocket). 

5) ผลิตที่ไหน pà-lìt tîi-nǎi 

Where was (it) made/manufactured?  

6) ได้เลย dâi ləəi 

Sure! / Okay! (I'll do it, etc.)  

7) ยินดีครับ/ค่ะ yin-dii kráp/kà 

You're welcome. / My pleasure. / Any time. / Sure.  

8) ...ตกบนพื้น dtòk bon pʉ́ʉn 

(It) fell on the floor. / (It) fell on the ground.  

9) จะต้องชอบแน่ๆ jà dtɔ̂ng chɔ̂ɔp nɛ̂ɛ-nɛ̂ɛ 

You would love it! / You will love it! / You're gonna love it! / You would have loved it!  

10) มันเป็นช่วงฤดู(หนาว) SEMI-FORMAL man bpen chûang rʉ́-duu (nǎao) It was (winter). / It was in the (winter) time.  

11) อากาศ(หนาว)มาก aa-gàat (nǎao) mâak 

It was very (cold). / The weather was very (cold).  

12) มันอร่อยจริงๆ man à-rɔ̀i jing-jing 

This is really good! / This is really delicious!  

13) ผมไม่มีอะไรจะพูด pǒm mâi-mii-à-rai jà pûut 

I have nothing to say.  

14) น่าสนนะ INFORMAL nâa-sǒn ná

Interesting! / Sounds interesting! 

15) เดี๋ยวมานะ dtǐao maa ná 

I'll be right back. / Be right back.  

16) มีปัญหาใหญ่ mii bpan-hǎa yài 

(We) have a big problem.  

17) ทำการบ้านเสร็จหรือยัง tam gaan-bâan sèt-rʉ̌ʉ-yang Have you finished your homework?  

18) (มัน)ง่ายจะตาย (man) ngâai jà dtaai 

It's so easy! / It's a piece of cake!  

19) ขอบคุณเรื่องอะไร kɔ̂ɔp-kun rʉ̂ang à-rai 

Thank you for what? / Thank me for what?  

20) จะรับอะไรเพิ่มหรือเปล่าครับ jà ráp à-rai pə̂əm rʉ̌ʉ-bplàao kráp Would you like something else? 

Super Useful Expressions, Part 84 

(low-intermediate level) 

1) เจ๋งสุดๆ jěng sùt-sùt 

Awesome! / Cool! 

2) หนาวจังเลย nǎao jang ləəi 

It's freezing! / It's so cold! / It was freezing!  

3) ผมรู้สึกโล่งใจ(มาก) pǒm rúu-sʉ̀k lôong-jai (mâak) I feel (so) relieved.  

4) บอกตอนนี้ไม่ได้เหรอ bɔ̀ɔk dtɔɔn-níi mâi-dâi rə̌əi Can't you tell me now? 

5) เขาเปลี่ยนไปเยอะเลย / เขาเปลี่ยนไปมากเลย kǎo bplìan bpai yə́ ləəi / kǎo bplìan bpai mâak ləəi 

He's changed a lot. / He's really changed. 

6) (กิน)เร็วเกินไป (gin) reo gəən-bpai 

(Eat) too fast. / (Eat) too quickly.  

7) ผมไม่ใช่คนฉลาด pǒm mâi-châi kon chà-làat 

(I)'m not smart. / (I)'m not a smart person.  

8) ฉันเพิ่งรู้ว่า... chǎn pə̂ng rúu-wâa... 

I just found out... / I just realized... 

9) ผมมีปัญหาเรื่องเงิน pǒm mii bpan-hǎa rʉ̂ang ngən 

I'm having money troubles. / I have a money problem.  

10) ต้นเดือน(กันยา) dtôn dʉan (gan-yaa) 

At the beginning of (September) / In early (September) 

11) ฉันได้รับโทรศัพท์จาก(ร.พ.สัตว์) chǎn dâi-ráp too-rá-sàp jàak (roong-pá yaa-baan-sàt) 

I got a call from (the animal hospital).  

Note: ร.พ. is an abbreviation of โรงพยาบาล (roong-pá-yaa-baan). 

12) ฉันอ่านเจอใน (Facebook) chǎn àan-jəə nai (Facebook) I read it in/on (Facebook).  

13) ทำแบบนี่ทำไม tam bɛ̀ɛp-níi tam-mai 

Why are you doing this? / Why did you do that?  

14) บอกให้หยุด bɔ̀ɔk hâi yùt 

I said stop! / I told you to stop (it)! 

15) เขาได้รับบาดเจ็บ kǎo dâi-ráp bàat-jèp 

He was injured/wounded. 

16) (มัน)เป็นเรื่องจริงหรือเปล่า (man) bpen rʉ̂ang-jing rʉ̌ʉ-bplàao Is it true? / Is it a true story?  

17) ไม่รู้เหตุผล mâi-rúu hèet-pǒn 

(I) don't know the reason. / (I) don't know why.  

18) อย่าทิ้งฉันไว้คนเดียว yàa tíng chǎn wái kon-diao Don't leave me all alone!  

19) วิวสวยดีนะ / วิวสวยจังเลย wiu sǔai dii ná / wiu sǔai jang ləəi What a nice view! / Nice view!  

20) รู้สึกเป็นไง(บ้าง) rúu-sʉ̀k bpen-ngai (bâang) 

How do you feel? / How are you feeling? / How does it feel? 

Super Useful Expressions, Part 85 

(low-intermediate level) 

1) ฉันยินดีจะช่วยคุณ chǎn yin-dii jà chûai kun 

I'm happy to help (you). / I'd be glad to help. 

2) ผมช่วยดีกว่า pǒm chûai dii-gwàa 

I'd better help (you). / Let me help (you).  

3) รู้อะไร(ไหม) rúu à-rai (mǎi) 

Know what? / Do you know what?  

4) อย่าลืมเล่าให้ฟัง yàa lʉʉm lâo-hâi-fang 

Don't forget to tell (him) (about it).  

5) เพิ่งมาถึง pə̂ng maa-tʉ̌ng 

(I) just got here. / (I) just got back. / (I) just arrived. 

6) กลัวไม่หล่อ glua mâi lɔ̀ɔ 

(He)'s worried he won't look good. / (He)'s concerned about how he looks. /  (He)'s afraid he won't look handsome. (hair messed up, etc.) 

7) ไม่เคยเห็นเขามาก่อน mâi-kəəi hěn kǎo maa-gɔ̀ɔn 

(I)'ve never seen him/her before.  

8) ทำไมไม่เคยบอกฉัน... tam-mai mâi-kəəi bɔ̀ɔk chǎn... Why didn't you tell me... ? / Why haven't you told me... ?  

9) จำได้ว่าตอนนั้น... jam-dâi wâa dtɔɔn-nán... 

(I) remember at that time...  

10) ฉันรู้สึกแบบนั้น chǎn rúu-sʉ̀k bɛ̀ɛp-nán 

I feel that way. / That's how I feel. / I feel like that.  

11) ต้องทำอะไรสักอย่าง dtɔ̂ng tam à-rai sàk-yàang 

We have to do something. / You gotta do something.  

12) กำลังกลับบ้าน gam-lang glàp-bâan 

I was on my way home. / She's heading home. / He's coming home.  

13) พอจะรู้(ว่า)... pɔɔ jà rúu (wâa)... 

Do you happen to know... ? / Do you have any idea... ?  

14) ฉันไม่น่าบอก(เธอ) chǎn mâi-nâa bɔ̀ɔk (təə) 

I shouldn't have told (you). 

15) (มัน)ไม่มีความหมายกับผม (man) mâi-mii-kwaam-mǎai gàp pǒm (It) means nothing to me. / (It) didn't mean anything to me.  

16) ตื่นตั้งแต่เมื่อไร dtʉ̀ʉn dtâng-dtɛ̀ɛ mʉ̂a-rai 

How long have you been up? / When did you wake up?  

17) ฉันมีความสุขที่สุด chǎn mii-kwaam-sùk tîi-sùt 

I'm really really happy! / I'm so very happy! / I'm feeling great! 

18) อย่าล้อเล่น(แบบนี้) yàa lɔ́ɔ-lên (bɛ̀ɛp-níi) 

Don't joke around (like that)! / Don't tease me (like that)!  

19) (หิว)สุดๆเลย (hǐu) sùt-sùt ləəi 

(I'm) super (hungry).  

20) ดูอะไรอยู่(คะ) duu à-rai yùu (ká) 

What are you looking at?  

Super Useful Expressions, Part 86 

(high-intermediate level) 

1) ขอบคุณที่เตือน kɔ̂ɔp-kun tîi dtʉan 

Thanks for warning me. / Thanks for the heads up. / Thanks for the warning. 

2) ว่าแต่(ว่า)... wâa-dtɛ̀ɛ (wâa)... 

By the way... / So... (speaking of...)  

3) (คง)ไม่มีอะไรดีขึ้น (kong) mâi-mii à-rai dii-kʉ̂n 

Nothing would come of it. / No good would come of it. / It wouldn't make any  difference. / It wouldn't get any better.  

4) ผมขอยืนยันว่า... SEMI-FORMAL pǒm kɔ̌ɔ yʉʉn-yan wâa... I assure you... / I insist... / I am positive... 

5) โทรไป(ก็)ไม่รับ INFORMAL too-bpai (gɔ̂ɔ) mâi-ráp 

(I) called you but (you) didn't answer.  

6) ต้องจากโลกนี้ไป LITERARY dtɔ̂ng jàak lôok níi bpai (We all) have to leave this world. / (We all) have to die (someday).  

7) ...ต่อไปไม่ได้อีก(แล้ว) ...dtɔ̀ɔ-bpai mâi-dâi ìik (lɛ́ɛo) 

I can't ... anymore. / I can't ... any longer.

8) (...)เดี๋ยวใคร(จะ)มาเห็น (...) dtǐao krai (jà) maa hěn 

...or someone will see you! / Someone could see you! (warning given when about  to do something embarrassing/wrong/etc.) 

9) ขอแนะนำให้รู้จัก(กับ)... FORMAL kɔ̌ɔ nɛ́-nam-hâi-rúu-jàk (gàp)... (I)'d like to introduce you to... / (I)'d like you to meet...  

10) (เขา)ยังไม่ได้ติดต่อเรา(มาเลย) (kǎo) yang mâi-dâi dtìt-dtɔ̀ɔ rao (maa ləəi) (He/They) haven't contacted us yet. / (He/They) haven't called us yet. 

11) (เขา)(มา)เกี่ยวอะไรด้วย (kǎo) (maa) gìao à-rai dûai 

What's (he) got to do with it? / What does (he) have to do with anything? 

12) กลับหัวอยู่ glàp-hǔa yùu 

(It's) upside down.  

13) เหมือนกันเปี๊ยบ(เลย) mʉ̌an-gan bpíap (ləəi) 

(They)'re exactly the same. / (They) have the exact same... / (They) look exactly  the same. / (They)'re identical. 

14) ไม่ต้องกลัวอะไร(ทั้งนั้น) mâi-dtɔ̂ng glua à-rai (táng-nán) No need to worry about anything. / You've got nothing to worry about. / You have  nothing to fear. 

15) คิดจริงๆหรือว่า... kít jing-jing rʉ̌ʉ waa... 

Do you really think...?  

16) อยู่กับปัจจุบัน LITERARY yùu gàp bpàt-jù-ban 

live in the present / live in the moment / stay in the present  

17) อีบ้า VULGAR ii-bâa 

Crazy (bastard/guy)! / Crazy (bitch/woman)! 

18) ไม่สมเหตุสมผล mâi-sǒm-hèet-sǒm-pǒn 

It/That doesn't make any sense. 

19) ไม่เห็นมี(อะไร)เลย mâi hěn mii (à-rai) ləəi

(I) don't see anything. / There's nothing here. / (I) don't see any...  

20) นึกว่าใคร nʉ́k-wâa krai 

(Well, well, well...) if it isn't... / Look who's here! 

Super Useful Expressions, Part 87 

(low-advanced level) 

1) ทะเลาะกันยกใหญ่ tá-lɔ́-gan yók-yài 

(They) had a huge fight.  

Note: ยกใหญ่ (yók-yài) means 'so much', 'big time', 'a lot', etc. 

2) ถูกยิง(ใน)ระยะประชิด tùuk ying (nai) rá-yá bprà-chít 

(He) was shot at close range. / (He) was shot point blank.  

3) ในช่วงปลายยุค (60) nai chûang bplaai yúk (hòk-sìp) 

in the late (60s)  

4) ในช่วงต้นยุค (60) nai chûang dtôn yúk (hòk-sìp) 

in the early (60s)  

5) เมื่อได้ยินดังนั้น... LITERARY mʉ̂a dâi-yin dang-nán... 

When (he) heard that/it... ; Upon hearing that/it...  

6) แต่ละคนต่าง... dtɛ̀ɛ-lá-kon dtàang... 

Each person... / Every person... / Everyone... / Each of them...  

7) (...)เป็นความทรมานอย่างหนึ่ง (...)bpen kwaam-tɔɔ-rá-maan yàang-nʉ̀ng (...) it's a kind of torture. (=really hard to go through) 

8) มีปัญหาสุขภาพจิต mii bpan-hǎa sùk-kà-pâap-jìt 

(Those) with/having mental health problems. / (She) has mental health problems. 

9) กวาดเรียบ(เลย) gwàat-rîap (ləəi) 

(It) was completely cleaned out. / Nothing was left at all. (money stolen, damage  done, etc.) 

10) ฉันได้รับการติดต่อจาก... SEMI-FORMAL chǎn dâi-ráp gaan-dtìt-dtɔ̀ɔ jàak... 

I was contacted by...  

11) มีเหตุผลหลายอย่างที่... mii hèet-pǒn lǎai-yàang tîi... 

I have many reasons (for.../to.../why...) / There are many reasons (for.../to.../ why...)  

12) ไสหัวไปให้พ้น STRONG/ANGRY sǎi-hǔa-bpai-hâi-pón Get the hell out of here! / Get the fuck away! / Fuck off!  

13) ...ไม่งั้นคงตาย ...mâi-ngán kong dtaai 

...otherwise (he) probably would have died. 

14) (...)เขาคงดีใจมาก (...)kǎo kong dii-jai mâak 

(...)he'll be really glad. / (...)she'd be very happy. 

15) (ทําไมไม่พูด)ตั้งแต่แรก (tam-mai mâi pûut) dtâng-dtɛ̀ɛ-rɛ̂ɛk) (Why didn't you tell me) in the first place?  

16) ใช้เฉพาะใน... (เท่านั้น) chái chà-pɔ́ nai... (tâo-nán) 

(It)'s used only in... / (It)'s used exclusively in...  

17) เหนือสิ่งอื่นใด LITERARY nʉ̌a-sìng-ʉ̀ʉn-dai 

Above all / Most of all / More than anything  

18) มันผิดศีล(ธรรม) man pìt-sǐin (lá-tam) 

It's immoral. / You're breaking a moral precept. / It's sinful. 

19) ค่อยๆคิดค่อยๆท kɔ̂i-kɔ̂i kít kɔ̂i-kɔ̂i tam 

Take it slowly. / Don't rush into things. (Think about it carefully before doing it.) 20) มีอะไรไม่ชอบมาพากล mii à-rai mâi-chɔ̂ɔp maa paa gon

Something is fishy. / There's something suspicious. / Something's not right.  

Super Useful Expressions, Part 88 

(low-intermediate level) 

1) ล้อเล่นนะ lɔ́ɔ-lên ná 

(I)'m kidding. / (I)'m joking.  

2) ลืมทุกที lʉʉm túk-tii 

(I) always forget. / (I) keep forgetting. 

3) ไม่เคยกิน (อะโวคาโด) เลย mâi-kəəi gin (à-woo-kaa-dôo) ləəi I've never eaten (avocado). / I've never had (an avocado).  

4) รสชาติ(มัน)เป็นอย่างไร rót-châat (man) bpen-yàang-rai What does it taste like?  

5) วันนี้ขายดี wan-níi kǎai-dii 

(They're) selling really well today!  

6) โอ้โห ôo-hoo 

Oh, no! / Oh! / Wow! / Oh, my God!  

7) พอที INFORMAL pɔɔ-tii 

That's enough!  

8) เขาเป็นคนติดดิน(มาก) kǎo bpen kon dtìt-din (mâak) He's (very) down-to-earth.  

9) (แม่)โทรมาบอกว่า... (mɛ̂ɛ) too-maa bɔ̀ɔk wâa... 

(Mom) called and said...  

10) ไม่ค่อยสบาย mâi-kɔ̂i sà-baai 

(I)'m not feeling (so) well. / I'm feeling ill. 

11) น่ารำคาญ nâa-ram-kaan 

So annoying/irritating!  

12) ขอร้อง kɔ̌ɔ-rɔ́ɔng 

Please! / I'm begging you!  

13) ดีใจที่ชอบ(นะคะ) dii-jai tîi chɔ̂ɔp (ná-ká) 

I'm glad you like it/them.  

14) ยังไม่กลับอีกหรอ yang mâi-glàp ìik rə̌ə 

Aren't you going home (yet)? / Not going back yet? 

15) ฉันดูเป็นอย่างไรบ้าง chǎn duu bpen yàang-rai bâang How do I look?  

16) เป็นคนเงียบๆ bpen kon ngîap-ngîap 

He's quiet.  

17) เป็นคนพูดน้อย bpen kon pûut nɔ́ɔi 

He doesn't say too much. / He's not talkative.  

18) เขามีน้ำใจ kǎo mii-nám-jai 

He's kind/generous.  

19) เธอมีเสน่ห์ təə mii-sà-nèe 

She's charming.  

20) ทะลึ่ง tá-lʉ̂ng 

You're naughty. / You're rude (get away from me). / How cheeky!  

Super Useful Expressions, Part 89 (high-intermediate level)

1) ไม่เสียอะไร mâi sǐa à-rai 

(You)'ve got nothing to lose. / (It) won't cost you anything.  

2) รออะไรอีกล่ะ STRONG rɔɔ à-rai ìik lâ 

What are you waiting for?!  

3) ฉันได้คิดทบทวน... chǎn dâi kít tóp-tuan... 

I've been thinking (about)... / I've spent some time thinking carefully (about)... /  I've been doing some thinking...  

4) อย่าถือสา... yàa tʉ̌ʉ-sǎa... 

Don't mind (me)... / Don't take offense... / Don't take it personally... / Don't hold  it against (me)...  

5) อย่ามั่นหน้า yàa mân-nâa 

Don't be so sure of yourself!  

6) เป็นช่วงเวลาสั้นๆ SEMI-LITERARY bpen chûang-wee-laa sân-sân It was just a short period of time. / It's only a short time.  

7) ยังไม่รู้เรื่อง yang mâi-rúu-rʉ̂ang 

(We) don't know (anything) yet. / (We) don't know what happened yet.  

8) อะไรอีก(ล่ะ) à-rai ìik (lâ) 

Now what?! / What is it now?! (showing frustration) 

9) กินได้หมด gin dâi mòt 

Anything is okay. / I can eat anything.  

10) กินคนเดียวไม่หมด gin kon-diao mâi mòt 

(I) can't finish it all by myself. / (I) can't eat it all alone.  

11) เห็นเต็มสองตา hěn dtem sɔ̌ɔng dtaa 

(I) saw it with my own two eyes! / I saw it clearly!  

12) หัวเราะอะไรกัน hǔa-rɔ́ à-rai gan

What are you laughing about?  

13) ก็ดีสิ gɔ̂ɔ dii sì 

(Well/Oh) that's good.  

14) ทำไมต้องบอก(คุณด้วย) tam-mai dtɔ̂ng bɔ̀ɔk (kun dûai) Why do I have to tell you?! / Why should I tell you?!  

15) เป็นมิตรต่อสิ่งแวดล้อม bpen-mít dtɔ̀ɔ sìng-wɛ̂ɛt-lɔ́ɔm It's environmentally friendly. 

16) ได้เงินเป็นกอบเป็นก dâi-ngən bpen-gɔ̀ɔp-bpen-gam (He)'s raking in the bucks. 

17) ไม่ต้องทำขนาดนั้น(หรอก) mâi-dtɔ̂ng tam kà-nàat-nán (rɔ̀ɔk) You don't have to go that far. / You don't have to do that.  

18) ยังพูดไม่จบ yang pûut mâi-jòp 

I wasn't finished yet! / I haven't finished! / I'm not done (talking)!  

19) อยากจะพูดอะไรก็พูดไปเถอะ yàak jà pûut à-rai gɔ̂ɔ pûut bpai tə̀ If you have something to say, just say it. / Go ahead---say what you wanna say.  

20) บอกตามตรง... bɔ̀ɔk-dtaam-dtrong... 

To be honest... / Frankly... / Honestly... / To tell you the truth...  

Super Useful Expressions, Part 90 

(low-advanced level) 

1) มึงอยากมีเรื่องใช่มั้ย STRONG/ANGRY mʉng yàak mii-rʉ̂ang châi-mái You wanna fight, huh?!  

2) (ดูแปลกๆ)ยังไงก็ไม่รู้ (duu bplɛ̀ɛk-bplɛ̀ɛk) yang-ngai gɔ̂ɔ mâi-rúu Somehow (it looks strange). / (It looks strange) somehow. 

3) ถูกบีบคอตาย tùuk bìip-kɔɔ dtaai 

(She) was strangled to death.  

4) กรรมตามทัน gam-dtaam-tan 

Karmic justice. / Poetic Justice. / You get what you deserve. 

5) เคยเป็นอาณานิคมของ(อังกฤษ)มาก่อน kəəi bpen aa-naa-ní-kom kɔ̌ɔng (ang-grìt) maa-gɔ̀ɔn 

It was formerly a colony of (England). / It used to be a colony of (England).  

6) พ้นผิด(แล้ว) pón-pìt (lɛ́ɛo) 

(He) was acquitted.  

7) (มัน)บังเอิญเกินไป(...) (man) bang-əən gəən bpai (...) 

It's too much of a coincidence(...) / It's too coincidental.  

8) ขอติดไปด้วย(คน)ได้มั้ย INFORMAL kɔ̌ɔ dtìt bpai dûai (kon) dâi-mái Can I tag along? / Can I go with you?  

9) ลอง(ดู)สักตั้ง lɔɔng (duu) sàk-dtâng 

Give it a shot.  

10) ...แต่ความจริงไม่ใช่ ...dtɛ̀ɛ kwaam-jing mâi-châi 

...but it wasn't true. / ...but that wasn't really what happened. / ...but that wasn't  how it was. 

11) (คุณ)ยังสนใจ.... อยู่(อีก)หรือ (kun) yang sǒn-jai... yùu (ìik) rʉ̌ʉ Are you still interested in... ? 

12) เหมือนงมเข็มในมหาสมุทร mʉ̌an ngom kěm nai má-hǎa-sà-mùt It's like trying to find a needle in a haystack. (=groping for a needle in an ocean) 

13) ขอให้พระคุ้มครองคุณ kɔ̌ɔ-hâi prá kúm-krɔɔng kun 

May Buddha bless you. / May Buddha be with you.  

14) ขอให้พระเจ้าคุ้มครองคุณ kɔ̌ɔ-hâi prá-jâo kúm-krɔɔng kun

May God bless you. / May God be with you. 

15) ต้องระวังตัวให้มากกว่านี้ dtɔ̂ng rá-wang-dtua hâi mâak-gwàa níi (You) have to be more careful.  

16) มีอะไรคืบหน้าหรือ mii à-rai kʉ̂ʉp-nâa rʉ̌ʉ 

Did anything come up?  

17) ถ้ามีอะไรคืบหน้า(จะบอก) tâa mii à-rai kʉ̂ʉp-nâa (jà bɔ̀ɔk) If anything comes up, (I'll let you know). / If something develops, (l'll let you  know).  

18) ช่างเอาอกเอาใจ châng ao-òk-ao-jai 

(He)'s very thoughtful. / (He) treats me very well.  

19) เราไม่ได้เป็นอะไรกัน ao mâi-dâi bpen-à-rai gan 

There's nothing between us. / We aren't a couple. / We're not (going) together. /  We're just friends, nothing more.  

20) ฉันทำอะไรให้เขาโกรธ chǎn tam à-rai hâi kǎo gròot 

What did I do to make him angry?  

Super Useful Expressions, Part 91 

(low-intermediate level) 

1) เห็นข่าวรึยัง hěn kàao rʉ́-yang 

Did you see the news? / Have you heard the news?  

2) ข่าวอะไร kàao à-rai 

What news?  

3) ต้องอดทน dtɔ̂ng òt-ton 

(You) have to be patient/endure it/bear with it/hang on. 

4) คุณจะชินไปเอง kun jà chin bpai-eeng 

You'll get used to it.  

5) เอามานี่ INFORMAL ao maa nîi 

Gimme that! / Give it to me.  

6) มองอะไร STRONG mɔɔng à-rai 

What are looking at?! (=Why are you looking at me?) 

7) ฉันอยากได้จริงๆ chǎn yàak dâi jing-jing 

I really want (it)! 

8) คุณไม่รู้หรอกหรือ kun mâi-rúu rɔ̀ɔk rʉ̌ʉ 

You didn't know (that)? / Don't you know?!  

9) ก็ดี(นะ)(ครับ) gɔ̂ɔ dii (ná) (kráp) 

Good. / Fine. (answering)  

10) (X)คบอยู่กับ(Y) (X) kóp yùu gàp (Y) 

X is dating Y. / X is going out with Y. 

11) (คน)จากทั่วประเทศ (kon) jàak tûa bprà-têet (People) from all over the country. / (People) nationwide.  

12) ไปเยี่ยมเพื่อนเก่า bpai yîam pʉ̂an gào 

(I) went to visit an old friend.  

13) เป็นเรื่องเกี่ยวกับ... bpen rʉ̂ang gìao-gàp... 

It's about... / It's a story about...  

14) ฉันไม่ไหว(หรอก) chǎn mâi-wǎi (rɔ̀ɔk) 

I can't do it. / I can't handle it. (=it's too much for me) 

15) (ผม)กำลังขับรถกลับบ้าน (pǒm) gam-lang kàp-rót glàp-bâan (I)'m driving home. / (I) was driving home. / (I)'m on my way home.  

16) (ผม)ไม่ใช่คนเลว (pǒm) mâi-châi kon leeo

(I)'m not a bad person.  

17) ผมมั่นใจว่า... pǒm mân-jai wâa... 

I'm sure (that)... / I'm confident (that)...  

18) ไม่อยากให้ใครรู้ mâi yàak hâi krai rúu 

(I) don't want anyone to know.  

19) ลองอ่านดู(สิ) lɔɔng àan duu (sì) 

Read it. / Check it out. / Have a look.  

20) ขอฉันไปด้วย kɔ̌ɔ chǎn bpai dûai 

Can I go with you (please)? / Can I go along?  

Super Useful Expressions, Part 92 

(low-intermediate level) 

1) มีอะไรให้ช่วยมั้ย INFORMAL mii à-rai hâi chûai mái 

Can I help you?  

2) ขอบคุณ(มาก)ที่เลี้ยง(...) kɔ̂ɔp-kun (mâak) tîi líang(...) Thanks for treating me. / Thank you (very much) for...  

3) ไปทำอะไรมา bpai tam à-rai maa 

What have you been doing? / What have you been up to? / What did you do? /  What have you done?  

4) มันไม่ใช่แค่นั้น man mâi-châi kɛ̂ɛ nán 

That's not all. / It's more than that. / Not only that.  

5) (เขาก็)เหมือนคนในครอบครัว (kǎo gɔ̂ɔ) mʉ̌an kon nai krɔ̂ɔp-kruai (He)'s like a member of the family. / (He)'s (just) like family.  

6) (คุณ)พูดถึงใคร (kun) pûut-tʉ̌ng krai

Who are you talking about?  

7) (อยู่)ไม่ไกลจากที่นี่ (yùu) mâi-glai jàak tîi-nîi 

(It)'s not far from from here.  

8) เปิดปากสิ STRONG bpə̀ət-bpàak sì 

Open up! / Open your mouth!  

9) มีนิสัยใจคออย่างไร SEMI-LITERARY/FORMAL mii ní-sǎi-jai-kɔɔ yàang-rai What's his personality like? / What kind of temperament does he have?  

10) เดินไม่ตรง dəən mâi-dtrong 

I can't walk straight. / He doesn't walk straight.  

11) เข้าไปไม่ได้ kâo-bpai mâi-dâi 

You can't go in there. / I can't get in.  

12) ฉันได้(หนังสือเล่มนี้)มาจาก... chǎn dâi (nǎng-sʉ̌ʉ lêm-níi) maa jàak... You I got (this book) from...  

13) กินไม่ได้นอนไม่หลับ gin mâi-dâi nɔɔn-mâi-làp 

I can't eat; I can't sleep. / She couldn't eat or sleep. 

14) คุณตัดสินใจหรือยัง kun dtàt-sǐn-jai rʉ̌ʉ-yang 

Have you decided yet? / Have you made up your mind? 

15) ไม่ต้องหรอก mâi-dtɔ̂ng rɔ̀ɔk 

No, I'm fine. / No, it's fine. / No need. (No need to do anything.) 

16) อ่านเรียบร้อย àan rîap-rɔ́ɔi 

I finished reading it. / I'm done reading it.  

17) เปิดมากกว่า(50 ปี)(แล้ว) bpə̀ət mâak-gwàa (hâa-sìp bpii) (lɛ́ɛo) (They've) been in business for over (50 years).  

18) เจอ(กัน)อีกแล้ว jəə (gan) ìik-lɛ́ɛo 

We meet again! 

19) อาจจะกลับดึก(หน่อย) àat-jà glàp dʉ̀k (nɔ̀i) 

(I) might be home (a bit) late.  

20) เมื่อคืนฝันถึงพี่ mʉ̂a-kʉʉn fǎn-tʉ̌ng (pîi) 

I dreamt about you last night.  

Super Useful Expressions, Part 93 

(low-intermediate level) 

1) อยากมีเงินเยอะๆ yàak mii ngən yə́-yə́ 

I want to have a lot of money! / I wanna have lots and lots of money!  

2) อย่าทำหก / อย่าให้หก yàa tam hòk / yàa hâi hòk Don't spill it.  

3) มีหลายรูปแบบ mii lǎai rûup-bɛ̀ɛp 

There are a lot of styles. / There's many types.  

4) อย่าไปไหนนะ yàa bpai-nǎi 

Don't go anywhere. / Stick around. / You stay here. 

5) ผมไม่ไปไหน pǒm mâi bpai-nǎi 

I'm not going anywhere.  

6) (สุขภาพ)ไม่ค่อยแข็งแรง (sùk-kà-pâap) mâi-kɔ̂i kɛ̌ng-rɛɛng (Her health) isn't so strong. / She's not very strong.  

7) อยู่มากี่ปี(แล้ว) yùu maa gìi bpii (lɛ́ɛo) 

How many years have you been here?  

8) ไม่นานมาก mâi naan mâak 

Not so long. / Not that long.

9) อยู่มานานแล้ว yùu maa naan lɛ́ɛo 

(It)'s been here for a long time. / (It)'s been around a long time. 

10) ซ่อมเองได้ sɔ̂m eeng dâi 

(You) can fix/repair it yourself.  

11) ต้องเข้าคิวรอ dtɔ̂ng kâo-kiu rɔɔ 

You have to wait in line. / You have to get in line and wait.  

12) ต้องรอนานแค่ไหน dtɔ̂ng rɔɔ naan kɛ̂ɛ-nǎi 

How long do I have to wait? / How long does it take?  

13) นานแค่ไหนก็รอได้ naan kɛ̂ɛ-nǎi gɔ̂ɔ rɔɔ dâi 

No matter how long, (I) can wait. / (I) can wait no matter how long it takes.  

14) ต้องมีใบอนุญาต dtɔ̂ng mii bai-à-nú-yâat 

You need a license/permit. 

15) ฉันคิดว่าไม่ใช่(...) chǎn kít-wâa mâi-châi(...) 

I don't think so. / I don't think...  

16) ไม่เป็นห่วง... หรือ mâi bpen-hùang... rʉ̌ʉ 

Aren't you worried/concerned about... ?  

17) ลองเปิดดู lɔɔng bpə̀ət duu 

Have a look. / Open it (and have a look).  

18) เขียนว่าอะไร kǐan wâa à-rai 

What does this say? / What did it say?  

19) มัน(เป็น)เรื่องเล็ก man (bpen) rʉ̂ang-lék 

It's a small matter. / It's nothing. / It's no big deal.  

20) เลิกฝันได้แล้ว lə̂ək fǎn dâi lɛ́ɛo 

Stop dreaming!

Super Useful Expressions, Part 94 

(low-intermediate level) 

1) รักคุณคนเดียว rák kun kon-diao 

I only love you. / I love only you.  

2) (พรุ่งนี้)จะให้คำตอบ (prûng-níi) jà hâi kam-dtɔ̀ɔp I'll give you an answer (tomorrow). / I'll answer you (tomorrow).  

3) มันไม่ใช่ของจริง man mâi-châi kɔ̌ɔng jing 

It's not real. / It wasn't real.  

4) อย่าไปโกรธเขา yàa bpai gròot kǎo 

Don't be angry with him. / Don't get mad at her.  

5) ไม่เคยทำมาก่อน mâi-kəəi tam maa-gɔ̀ɔn 

(I)'ve never done it (before). / (You)'ve never done that before.  

6) เคยได้ยินไหม kəəi dâi-yin mǎi 

Have you ever heard (...)?  

7) ยังไม่ได้คิด(เลย) yang mâi-dâi kít (ləəi) 

(I) haven't thought about it yet.  

8) เขาไม่ค่อยฉลาด kǎo mâi-kɔ̂i chà-làat 

He's not so smart.  

9) ฉันอยากจะอยู่ต่อ chǎn yàak jà yùu dtɔ̀ɔ 

I want to stay (longer). / I want to live here longer.  

10) อยู่นี่ INFORMAL yùu nîi 

It's right here. / I'm right here.  

11) เขาไม่ใช่คนธรรมดา kǎo mâi-châi kon tam-má-daa He's not an ordinary person. / He's no ordinary guy. / He's not normal. 

12) 2-3 วันก่อน(...) sɔ̌ɔng sǎam wan gɔ̀ɔn (...) 

A few day ago (...)  

13) เข้าใจทุกอย่าง kâo-jai túk-yàang 

(I) understand everything. / (I) understood everything.  

14) หนูอยากถาม(เขา)ว่า... nǔu yàak tǎam (kǎo) wâa... I want to ask (him)... 

15) ผมตื่นขึ้นมา(กลางดึก) pǒm dtʉ̀ʉn-kʉ̂n maa (glaang-dʉ̀k) I woke up (in the middle of the night).  

16) ดูแลตัวเองด้วย duu-lɛɛ dtua-eeng dûai 

Take care. / Take care of yourself.  

17) มีธุระด่วน mii tú-rá dùan 

Something urgent came up. / I had some urgent business.  

18) ชอบที่นี่มาก(เลย) chɔ̂ɔp tîi-nîi mâak (ləəi) 

(I) love this place. / I really like it here.  

19) ฉันไม่ได้โกหก(คุณ) chǎn mâi-dâi goo-hòk (kun) 

I didn't lie (to you). / I'm not lying (to you).  

20) อ่านใจคุณได้ àan-jai kun dâi 

(I) can read your mind. 

Super Useful Expressions, Part 95 

(low-intermediate level) 

1) เอานี้(ไป) INFORMAL ao níi (bpai) 

Here. / Here you go. / Here---take this. (handing something to someone) 2) อย่าวางสาย yàa waang-sǎai

Don't hang up.  

3) (มัน)เป็นความผิดของฉัน(เอง) (man) bpen kwaam-pìt kɔ̌ɔng chǎn (eeng) It was my fault.  

4) หนูชอบเวลาที่(เธอยิ้ม) nǔu chɔ̂ɔp wee-laa tîi (təə yím) I like it when (she smiles).  

5) หนูไม่ชอบเวลาที่(ฝนตก) nǔu mâi chɔ̂ɔp wee-laa tîi (fǒn-dtòk) I don't like it when (it rains).  

6) คำถามสุดท้าย(นะคะ) kam-tǎam sùt-táai (ná-ká) 

(One) last question...  

7) ว่ามา INFORMAL wâa-maa 

Go on (tell me). / Shoot. 

8) ผมทำมันหาย pǒm tam man hǎai 

I lost it.  

9) (อาหาร)อร่อยทุกอย่าง(เลย)(ครับ/ค่ะ) (aa-hǎan) à-rɔ̀i túk-yàang (ləəi)  (kráp/kà) 

Everything's so good. / It's all so delicious!  

10) เขาดูแลฉันดีมาก kǎo duu-lɛɛ chǎn dii-mâak 

He took very good care of me. 

11) ผมยังไม่ได้บอกว่า... pǒm yang mâi-dâi bɔ̀ɔk wâa... 

I didn't tell you yet... / I haven't told you yet... / I haven't told her... yet.  

12) ทำเป็นทุกอย่าง tam-bpen túk-yàang 

She can make anything! / I can cook everything.  

13) แล้วแต่อารมณ์ lɛ́ɛo-dtɛ̀ɛ aa-rom 

It depends on (my) mood. / It depends on how (I) feel.  

14) ไม่มีคนช่วย mâi-mii kon chûai

I have no one to help (me). / She has no one to help her. 

15) ไม่เคยเล่าให้(ผม)ฟัง mâi-kəəi lâo-hâi-(pǒm)fang You've never told me before. / I never told you before.  

16) เอาอะไรดี INFORMAL ao à-rai dii 

What do you want? / What would you like? / What can I get you?  

17) ซื้อ 1 แถม 1 sʉ́ʉ nʉ̀ng tɛ̌ɛm nʉ̀ng 

Buy one get one free.  

18) ฉันสงสัยว่าทำไม... chǎn sǒng-sǎi wâa tam-mai... I wondered why... / I wonder why... / I was curious why...  

19) ผมเอง(นะ) pǒm eeng (ná) 

It's me. (on the phone) 

20) ความจำดี kwaam-jam dii 

(You) have a good memory!  

Super Useful Expressions, Part 96 

(low-intermediate level) 

1) ดื่มหนักไป(หน่อย) dʉ̀ʉm nàk bpai (nɔ̀i) 

I had (a bit) too much to drink.  

2) ไหวไหม wǎi mǎi 

Can you make it? / Can you handle it? / Can you walk, stand up, etc.? (situation--- feeling weak, sick, drunk, etc.) 

3) เอามาซิ STRONG ao maa sì 

Give me it/that.  

4) คิดว่า(คุณ)เป็นคนอื่น kít wâa (kun) bpen kon ʉ̀ʉn

I thought (you) were someone else. 

5) ไร้สาระ rái-sǎa-rá 

(That's/It's) ridiculous!  

6) ไม่ได้ยินอะไรเลย mâi dâi-yin à-rai ləəi 

(I) didn't hear anything. / (I) didn't hear a thing. / (I) can't hear anything. 

7) ผมเชื่อมาตลอดว่า... pǒm chʉ̂a maa dtà-lɔ̀ɔt wâa... 

I've always believed (that)...  

8) ผมเริ่มไม่แน่ใจแล้ว(...) pǒm rə̂əm mâi nɛ̂ɛ-jai lɛ́ɛo (...) 

I'm starting to wonder(...) / I'm not so sure anymore.  

9) ฉันมีความฝันอยากเป็น(แอร์โฮสเตส) chǎn mii kwaam-fǎn yàak bpen (ɛɛ-hôot-dtèt) 

I dreamed of being (a flight attendant). / I want to be (a flight attendant).  

10) เสียเงินเปล่า(ๆ) sǐa-ngən bplàao(bplàao) 

(It)'s a waste of money. / (That) was a waste of money.  

11) ไม่คิดเงิน mâi-kít-ngən 

Free of charge / For free.  

12) ฉันเกรงใจ chǎn greeng-jai 

I don't want to impose. / I don't wanna bother you. / I don't want to inconvenience  you.  

13) ไม่ต้องเกรงใจ mâi-dtɔ̂ng greeng-jai 

Feel free to... (don't worry about me---do what you want.) 

14) ไม่เกี่ยวกันหรอก/เลย mâi-gìao-gàn rɔ̀ɔk/ləəi 

That has nothing to do with it. / It's not related at all. 

15) ทำได้ยังไง tam dâi yang-ngai 

How did (you) do that? / How do (you) do it? / How can (you) do that? 

16) คุณพอจะมีเวลาไหม POLITE kun pɔɔ jà mii wee-laa mǎi Do you have a minute?  

17) ต้องทำยังไง dtɔ̂ng tam yang-ngai 

What do I need to do? / What should I do? / What do I have to do?  

18) น่ารักที่สุดเลย nâa-rák tîi-sùt ləəi 

You're the sweetest! / You're the cutest! / How sweet!  

19) เรื่องเล็ก rʉ̂ang-lék 

No problem. / It's no big deal. / It was nothing. (what I did was a small matter) 

20) คุณมีพรสวรรค์ kun mii pɔɔn-sà-wǎn 

You are gifted. / You're very talented.  

Super Useful Expressions, Part 97 

(low-intermediate level) 

1) ทำดีมาก tam dii-mâak 

Nice work! / Good job!  

2) คุณต้องทำได้ kun dtɔ̂ng tam dâi 

You can do it!  

3) ฉันถามว่าทำไม chǎn tǎam wâa tam-mai 

I asked (you) why. / I'm asking (you) why.  

4) สิ่งที่ฉันไม่ชอบที่สุด(...) sìng tîi chǎn mâi-chɔ̂ɔp tîi-sùt (...) The thing I dislike/hate the most(...)  

5) รู้สึกเหมือนว่า... rúu-sʉ̀k mʉ̌an wâa... 

I feel like... / I have a feeling that...  

6) ฉันตื่นแต่เช้า chǎn dtʉ̀ʉn dtɛ̀ɛ-cháao 

I woke up early. / I got up early. 

7) หายใจไม่ออก hǎai-jai-mâi-ɔ̀ɔk 

I can't breathe! / I'm suffocating!  

8) มันจะรู้สึกยังไง man jà rúu-sʉ̀k yang-ngai 

What does it feel like?  

9) คุณอยากจะบอกอะไร(กับ)ผม kun yàak jà bɔ̀ɔk à-rai (gàp) pǒm What do you want to tell me?  

10) บอกผมได้นะ bɔ̀ɔk pǒm dâi ná 

You can tell me.  

11) ดูดีออก duu-dii ɔ̀ɔk 

(It) looks good.  

12) ทอดจนเหลืองกรอบ tɔ̂ɔt jon lʉ̌ang grɔ̀ɔp 

Deep fry until golden and crispy.  

13) ผัดให้เข้ากัน pàt hâi-kâo-gan 

(Stir-)fry them together. 

14) รับมือไม่ไหว ráp-mʉʉ mâi-wǎi 

(I) can't handle it. / (I) can't deal with this. 

15) เรากลับกันดีกว่า rao glàp gan dii-gwàa 

We should get going. / We'd better get back.  

16) จะให้เท่าไหร่ jà hâi tâo-rài 

How much will you pay? / How much will you pay me?  

17) ไม่คิดว่าคุณจะมา(นะ) mâi kít wâa kun jà maa (ná) I didn't think you'd come.  

18) ทำทำไม INFORMAL tam tam-mai 

Why did you do it? 

19) ตอนนี้ผมเข้าใจแล้ว dtɔɔn-níi pǒm kâo-jai lɛ́ɛo Now I understand. / Now I get it.  

20) คุณรู้จักฉันดี kun rúu-jàk chǎn dii 

You know me well.  

Super Useful Expressions, Part 98 (high-beginner level) 

1) สบายดีไหม sà-baai dii mǎi 

How are you doing? / Are you well? / How are you?  

2) ใครโทรมา krai too maa 

Who called? 

3) รู้จักเขาไหม rúu-jàk kǎo mǎi 

Do you know him?  

4) เจอไหม jəə mǎi 

Did you find it? / Did you find him?  

5) ไม่เจอ mâi jəə 

No, I didn't.  

6) ยังไม่เจอ yang mâi jəə 

I haven't found (it) yet. 

7) ฉันรักคุณมาก chǎn rák kun mâak 

I love you so much. / I love you very much.  

8) ไม่ได้สั่งค่ะ mâi-dâi sàng kà 

I didn't order (that).  

9) ไม่ชอบเขา mâi chɔ̂ɔp kǎo 

I don't like him!

10) ไม่มีความสุข(เลย) mâi mii kwaam-sùk (ləəi) I'm not happy (at all). / He wasn't happy (at all).  

11) เสร็จรึยัง sèt rʉ́-yang 

Are you done yet? / Are you finished yet?  

12) ปวดหลัง bpùat lǎng 

My back hurts. / I have a backache.  

13) ไปกินกาแฟ(กัน)(นะ) bpai gin gaa-fɛɛ (gan) (ná) Let's go have some coffee. / Let's go get a cup of coffee.  

14) ฉันสอบผ่าน chǎn sɔ̀ɔp-pàan 

I passed the test. / I passed. 

15) ตื่นเช้ามาก(เลย) dtʉ̀ʉn cháao mâak (ləəi) You woke up so early today! / You're up very early!  

16) อยากจะทำอะไร yàak jà tam à-rai 

What do (you) wanna do?  

17) เข้ามาสิ INFORMAL kâo maa sì 

Come in. / Come on in.  

18) ผมไม่ถนัด pǒm mâi tà-nàt 

I'm not good at (it). / I'm no good.  

19) คุณทำอะไรอยู่ kun tam à-rai yùu 

What are you doing?  

20) ไปเที่ยวกัน(เถอะ) bpai tîao gan (tə̀) 

Let's go out. / Let's go hang out. / Wanna go out? Super Useful Expressions, Part 99

(low-advanced level) 

1) เพ้อเจ้อไปเรื่อย pə́ə-jə̂ə bpai rʉ̂ai 

(You)'re always talking nonsense/making stuff up/imagining things!  

2) ชีวิตบัดซบ IDIOM chii-wít bàt-sóp 

Life is shit! / Life is all fucked up!  

3) (ถ้าไม่มา)คงเสียใจแย่ (tâa mâi maa) kong sǐa-jai yɛ̂ɛ (If you don't come,) you'll really regret it/be sorry.  

4) การติดคุกตลอดชีวิต gaan-dtìt-kúk dtà-lɔ̀ɔt chii-wít imprisoned for life / life imprisonment / life sentence  

5) เขาถูกตัดสินประหารชีวิต kǎo tùuk dtàt-sǐn bprà-hǎan-chii-wít He was sentenced to death. / He was given the death penalty.  

6) พยานให้การเท็จ pá-yaan hâi-gaan-tét 

The witness gave false testimony/committed perjury.  

7) (ผู้ต้องหา)หลบหนีไป(ได้) (pûu-dtɔ̂ng-hǎa) lòp-nǐi bpai (dâi) (The suspect) escaped. 

8) (เขาถูก)ฆ่าหั่นศพ (kǎo tùuk) kâa hàn sòp 

(He was) killed and dismembered.  

9) (ไม่ว่า)เป็นหรือว่าตาย (mâi-wâa) bpen rʉ̌ʉ wâa dtaai (Whether) dead or alive.  

10) ถูกตั้งข้อหา(ก่อการร้าย) tùuk dtâng-kɔ̂ɔ-hǎa (gɔ̀ɔ-gaan-ráai) (He) was charged for (terrorism). / (He) was accused of (terrorism).  

11) ผมหาทางออกไม่เจอ pǒm hǎa-taang-ɔ̀ɔk mâi-jəə I can't find a way out. / I can't find a solution. 

12) ความดีย่อมชนะความชั่ว(เสมอ) kwaam-dii yɔ̂m chá-ná kwaam-chûa (sà mə̌ə) 

Good will (always) overcome evil. / The good will (always) win over evil.  

13) เป็นอัมพาตตั้งแต่คอลงไป bpen am-má-pâat dtâng-dtɛ̀ɛ kɔɔ long-bpai (He)'s paralyzed from the neck down.  

14) หาไม่ได้อีกแล้ว hǎa-mâi-dâi ìik-lɛ́ɛo 

(You) can't find (this kind) any more! / (This) is rare. / (This) is hard to find  nowadays. 

15) ชีวิตไม่ได้โรยด้วยกลีบกุหลาบ chii-wít mâi-dâi rooi dûai glìip gù-làap Life is not a bed of roses. / Life is not all roses.  

16) ...ในหลายแง่มุม ...nai lǎai ngɛ̂ɛ-mum 

...in many ways. / ...in many aspects. / ...from various angles. 

17) เขาเป็นตัวเก็งที่จะได้รับ(รางวัลออสการ์) kǎo bpen dtua-gèng tîi-jà dâi-ráp  (raang-wan ɔ́ɔt-sà-gâa) 

He's the favorite to win (the Oscar). / He's favored to win (the Oscar).  

18) คิดเท่าไหร่ก็คิดไม่ออก kít tâo-rài gɔ̂ɔ kít-mâi-ɔ̀ɔk 

No matter how much/hard I think, I can't figure it out.  

19) ฉันจำได้ขึ้นใจ chǎn jam-dâi-kʉ̂n-jai 

I know it by heart.  

20) กินเพื่ออยู่ไม่ใช่อยู่เพื่อกิน gin pʉ̂a yùu mâi-châi yùu pʉ̂a gin Eat to live, not live to eat.  

Super Useful Expressions, Part 100 

(high-intermediate level) 

1) เตรียมตัวเอาไว้ dtriam-dtua ao-wái

Get ready! / Prepare yourself!  

2) อยากจะถามว่า... yàak jà tǎam-wâa... 

I wanted to ask you... / I was going to ask you... 

3) มีอะไรผิดปกติ mii à-rai pìt-bpòk-gà-dtì 

Something's off/wrong/out of place. 

Note: ปกติ (bpàk-gà-dtì) is an alternative pronunciation for this word.  

4) ต้องทำใจยอมรับ dtɔ̂ng tam-jai yɔɔm-ráp 

(You) have to accept it. / (You) have to come to terms with it.  

5) รู้สึกเป็นเกียรติมาก(...) FORMAL rúu-sʉ̀k bpen-gìat mâak (...) I'm so/very honored (...)  

6) ฉัน(จะ)ยอมทำทุกอย่างเพื่อ... chǎn (jà) yɔɔm tam túk-yàang pʉ̂a... I would do anything for...  

7) ผมจะไม่ยอมแพ้ง่ายๆ pǒm jà mâi yɔɔm-pɛ́ɛ ngâai-ngâai I'm not gonna give up easily. / I'm no quitter.  

8) วันหลังนะ INFORMAL wan-lǎng ná 

Later, (okay)? / Some other time, (all right)?  

9) เอาไว้วันหลัง(แล้วกัน) ao-wái wan-lǎng (lɛ́ɛo-gan) 

Some other time. / Maybe another time. / Perhaps another time.  

10) ดูเหมือนจริงมาก duu mʉ̌an jing mâak 

(It) looks so real! 

11) (มัน)เป็นความฝันหรือความจริง (man) bpen kwaam-fǎn rʉ̌ʉ kwaam-jing Was it real or was it a dream?  

12) อยู่ให้ห่าง(จาก)... yùu-hâi-hàang (jàak)... 

Stay away from... / Keep your distance. 

13) ทั้ง(3)อย่าง táng (sǎam) yàang 

All (three). (types, kinds) 

14) ถ้ายังไง... tâa-yang-ngai... 

In any case... / Anyway... 

15) ไม่มีทางสู้ / ไม่มีทางสู้...ได้ mâi-mii-taang sûu / mâi-mii-taang sûu... dâi (I)'m no match for... / (I) can't compete with...  

16) กำลังจะเปิดตัว gam-lang jà bpə̀ət-dtua 

(We're) rolling out a new product. / (It's) making its debut. / (It's) going to launch  soon.  

17) (เรา)หาจนทั่ว(แล้ว) (rao) hǎa-jon-tûa (lɛ́ɛo) 

(We)'ve searched everywhere! / (We)'ve looked all over!  

18) ถือซะว่า(เป็นคำชม) tʉ̌ʉ-sá-wâa (bpen kam-chom) 

Consider it (a compliment). / Call it (a compliment).  

19) ไม่ว่าคุณจะไปที่ไหน mâi-wâa kun jà bpai tîi-nǎi 

No matter where you go.  

20) ไปทำอะไรไว้ INFORMAL bpai tam à-rai wái 

What did you do?! / What the hell did you do?!  

Note: This usually implies the person did something negative.  

Super Useful Expressions, Part 101 

(low-intermediate level) 

1) คุณเป็นคนที่ไหน kun bpen kon tîi-nǎi 

Whereabouts are you from? / Where are you from?  

2) ผมเป็นคน(ขอนแก่น) pǒm bpen kon (kɔ̌n-gɛ̀n)

I'm from (Khon Kaen).  

3) ขำอะไร kǎm à-rai 

What are you laughing at?  

4) (ลูก)ทำดีที่สุดแล้ว (lûuk) tam dii-tîi-sùt lɛ́ɛo 

(You) did your best. (as spoken by a parent to his/her child) 

5) เข้ามาก่อน kâo maa gɔ̀ɔn 

Come on in!  

6) ไปด้วยกันไหม bpai dûai-gan mǎi 

Wanna go along? / Do you wanna go with me? / Do you want to join me?  

7) เห็นหมดแล้ว hěn mòt lɛ́ɛo 

(I) saw everything! / (I) saw it all!  

8) (จะ)เอาสีอะไรดี (jà) ao sǐi à-rai dii 

What color do you want? / Which color would you like?  

9) กลับมาก่อนสิ glàp maa gɔ̀ɔn sì 

Come back (here).  

10) ผมมีความสุขดี pǒm mii-kwaam-sùk dii 

I'm doing (really) well. / I'm really happy. (usually said about how one's doing in  life, in a situation or in a relationship) 

11) เรื่องกล้วยๆ rʉ̂ang glûai-glûai 

(It's a) piece of cake! / It's very simple!  

12) ใกล้เสร็จหรือยัง glâi sèt rʉ̌ʉ-yang 

Are (you) almost finished?  

13) (หมด)ทั้งหัวใจ (mòt) táng hǔa-jai 

With all my heart.  

14) ...ทุกครั้งที่เห็น... ...túk-kráng tîi hěn...

...every time (I) see... / ...whenever (I) see... 

15) จะรู้ได้ยังไงว่า... jà rúu dâi yang-ngai wâa... 

How will we know...? / How do I know...? 

16) ไม่ว่าคุณเป็นใคร... mâi-wâa kun bpen krai... 

No matter who you are...  

17) สเปคเลย sà-bpèk ləəi 

She's just my type! / He's exactly my type!  

Note: สเปค (sà-bpèk) is also spelled as สเป็ค (sà-bpék). 

18) ค่าเช่าเดือนละเท่าไหร่ kâa-châo dʉan-lá tâo-rài 

How much is the rent (per month)?  

19) เอาอะไรอีกไหม INFORMAL ao à-rai ìik mǎi 

Do you want anything else? / Anything else?  

20) งานยุ่งมาก ngaan yûng mâak 

(I)'m really busy with work. / (I)'ve been swamped at work. / (I've) been really  busy. 

Super Useful Expressions, Part 102 

(high-intermediate level) 

1) ไร้สาระสิ้นดี rái-sǎa-rá sîn-dii 

(That's) totally ridiculous!  

2) ไม่รู้อะไรเลยหรือ STRONG mâi-rúu à-rai ləəi rʉ̌ʉ 

Don't you know anything?!  

3) ห้ามไม่ได้ hâam mâi-dâi 

You can't stop me. / I can't stop you. 

4) นอนหลับยาก nɔɔn-làp yâak 

(I) have a hard time falling asleep. / I'm a light sleeper.  

5) เขามีความรู้มาก SEMI-FORMAL kǎo mii kwaam-rúu mâak He's very knowledgable.  

6) ไม่เสียตังค์สักบาท mâi sǐa dtang sàk bàat 

It didn't cost a baht! / It didn't cost a thing!  

7) อะไรทำให้คุณคิดแบบนั้น à-rai tam-hâi kun kít bɛ̀ɛp-nán What makes you think that?  

8) เข้ามาทำอะไรใน(ห้องผม) kâo maa tam à-rai nai (hɔ̂ng pǒm) What are you doing in (my room)?!  

9) แค่แวบเดียว kɛ̂ɛ wɛ̂ɛp diao 

Just a glimpse/flash.  

10) อย่ากังวลเรื่อง... มากเกินไป yàa gang-won rʉ̂ang... mâak-gəən-bpai Don't worry too/so much about... 

11) ฝากไปบอก(เขา)ว่า... fàak bpai bɔ̀ɔk (kǎo) wâa... 

Tell (him/her) that... 

12) ออกไปรอข้างนอก ɔ̀ɔk-bpai rɔɔ kâang-nɔ̂ɔk 

(Go) wait outside.  

13) ปล่อยวางเถอะ bplɔ̀i-waang tə̀ 

Let it go.  

14) ฉันเคยผิดหวังในความรัก SEMI-LITERARY chǎn kəəi pìt-wǎng nai  kwaam-rák 

I've been disappointed in love (before). 

15) สาบานกับตัวเอง(ไว้)ว่า... sǎa-baan gàp dtua-eeng (wái) wâa... (I) promised myself (that)... / (I) swore to myself (that)... 

16) ฉันสงสัยมานานแล้ว(ว่า...) chǎn sǒng-sǎi maa naan lɛ́ɛo (wâa...) I've always wondered (...) / I've long suspected(...)  

17) ไม่เข้าใจตรงไหน mâi-kâo-jai dtrong-nǎi 

What part don't you understand?  

18) อีกนานไหมกว่า(จะถึง) ìik naan mǎi gwàa (jà tʉ̌ng) 

How much longer (till we get there)? / Will it be much longer (till we get there)?  

19) เกรงใจกันบ้าง greeng-jai gan bâang 

Be considerate (of others). 

20) (จะ)อะไรกันหนักหนา IDIOMATIC (jà) à-rai gan nàk-nǎa What's the big deal?! / What the heck?!  

Super Useful Expressions, Part 103 

(high-intermediate level) 

1) โคตรเลว INFORMAL/ROUGH kôot leeo 

(He)'s really bad/vile/terrible!  

2) (ก็)ไม่เห็นแปลก (gɔ̂ɔ) mâi hěn bplɛ̀ɛk 

That's not surprising. / I'm not surprised. / I don't think it's so strange.  

3) มันช่างทรมาน(เหลือเกิน) man châng tɔɔ-rá-maan (lʉ̌a-gəən) It's (complete) torture! / It's excruciating!  

4) ฉันว่าน่าจะดีกว่า chǎn wâa nâa-jà dii-gwàa 

I think it would be better. / I think it's better. 

5) ทำไมรู้เรื่อง... เยอะจัง tam-mai rúu-rʉ̂ang... yə́ jang 

How do you know so much about... ? 

6) ถ้าไม่ลำบาก tâa mâi lam-bàak 

If it's not too much trouble. / It's it's not an imposition. 

7) มันมีความหมายสำหรับผมมาก man mii-kwaam-mǎai sǎm-ràp pǒm mâak It means a lot to me.  

8) ...มีอาชีพเป็น(ทนาย) FORMAL ...mii aa-chîip bpen (tá-naai) ...is an (attorney). / ...works as an (attorney).  

9) (เขา)ได้ย้ายไปยัง... (kǎo) dâi yáai bpai yang... 

(He) moved to...  

10) ยังไม่รู้ตัวอีกเหรอ STRONG yang mâi-rúu-dtua ìik rə̌ə Don't you get it?! / Don't you realize...?!  

11) เสียเงินไปตั้งเยอะ sǐa ngən bpai dtâng yə́ 

(I) spent a fortune! / (I) spent a lot of money!  

12) เดี๋ยว(ผม)ไปหยิบมาให้ dǐao (pǒm) bpai yìp maa hâi 

I'll go get it for you. / I'll grab it for you.  

13) เธอมีใจให้แก təə mii jai hâi gɛ̀ɛ 

She has feelings for you. / She likes you.  

14) กลับมาเดี๋ยวนี้ glàp maa dǐao-níi 

Get back here! / Come back (here)! 

15) เรียกร้องความสนใจ rîak-rɔ́ɔng kwaam-sǒn-jai 

try to get attention / trying to get attention  

16) ไม่อยากให้มันเกิดขึ้น mâi-yàak hâi man gə̀ət-kʉ̂n 

(I) don't want it to happen.  

17) ถ้าเกิดว่า... tâa gə̀ət wâa... 

What if... ? / If... ? / What happens if... ?  

18) ไม่อยากให้เป็นแบบนั้น mâi-yàak hâi bpen bɛ̀ɛp-nán

(I) wouldn't want that. / (I) wouldn't want it to be that way. / (I) wouldn't want that  to happen.  

19) (ฉัน)ลองทำทุกอย่างแล้ว (chǎn) lɔɔng tam túk-yàang lɛ́ɛo I tried everything.  

20) จะยากอะไร INFORMAL jà yâak à-rai 

How hard can it be?! / Was that so hard?!  

Super Useful Expressions, Part 104 

(high-intermediate level) 

1) (มึง)นี่โง่จริงๆ STRONG (mʉng) nîi ngôo jing-jing 

(You)'re such an idiot! / (You)'re so stupid!  

2) เล่าต่อ lâo dtɔ̀ɔ 

Tell me more. / Keep going.  

3) (คุณ)ตาฝาด (kun) dtaa-fàat 

You're seeing things! / You're delusional! / You're mistaken. (your eyes are  playing tricks on you) 

4) ...อีกหลายอย่าง ...ìik lǎai yàang 

...(and) many more types/kinds/things.  

5) ...แม้แต่นิดเดียว ...mɛ́ɛ-dtɛ̀ɛ nít-diao 

...not even a little bit. / ...not in the least bit.  

6) ...หรือไม่ก็... ...rʉ̌ʉ mâi gɔ̂ɔ... 

...or... (if not this way, then that way) 

7) คงไม่ได้หรอก kong mâi-dâi rɔ̀ɔk 

I can't. / I'd rather not. / I don't think I can. (giving semi-polite refusal usually  followed by a reason)

8) ในเสี้ยววินาที nai sîao wí-naa-tii 

In a split second / In a fraction of a second  

9) ซวยจริงๆ(เลย) INFORMAL/ROUGH suai jing-jing (ləəi) So unlucky! / It really sucks! / Shit out of luck!  

10) คน(เรา)มีรสนิยมที่(แตก)ต่างกัน kon (rao) mii rót-sà-ní-yom tîi (dtɛ̀ɛk)  dtàang gan 

We all have different tastes. / Everyone has different tastes.  

11) สบายใจได้ sà-baai-jai dâi 

(You) can relax. (don't worry about it) / Set your mind at ease.  

12) (มัน)เป็นระเบียบเรียบร้อย (man) bpen rá-bìap rîap-rɔ́ɔi (It)'s neat and orderly. / (It)'s neat and organized. / (It)'s nice and tidy. 

13) (ฟิน)ที่สุดในสามโลก INFORMAL/PLAYFUL (fiin) tîi-sùt nai sǎam-lôok The most (awesome) in the world! (used for exaggeration) 

14) ดีเสียอีก/ดีซะอีก dii sǐa-iìk / dii sá-ìik 

(That's) even better! 

15) (เขา)ได้ฉายาว่า... FORMAL/LITERARY (kǎo) dâi chǎa-yaa wâa... (He) earned the nickname... / (She) was called...  

16) ได้อุทิศชีวิตของเขาให้กับ... dâi ù-tít chii-wít kɔ̌ɔng kǎo hâi gàp... He dedicated/devoted his life to...  

17) หมดทุกอย่าง mòt túk-yàang 

Everything (used for emphasis) 

18) เป็นผู้เชี่ยวชาญด้าน... bpen pûu-chîao-chaan dâan... (She)'s an expert/master at...  

19) ดูก็รู้(ว่า...) duu gɔ̂ɔ rúu (wâa...) 

One look and you know (...) / It's obvious (he)'s... 

20) มันไม่ต่างอะไรจาก... man mâi dtàang à-rai jàak... It's no different from/than...  

Super Useful Expressions, Part 105 

(high-beginner level) 

1) อย่าหัวเราะ yàa hǔa-rɔ́ 

Don't laugh! 

2) (มัน)แย่มาก (man) yɛ̂ɛ mâak 

It's terrible/horrible/awful. 

3) หนูไม่ชอบตื่นเช้า nǔu mâi-chɔ̂ɔp dtʉ̀ʉn cháao 

I don't like getting up early. / I'm not a morning person.  Note: หนู (nǔu) is used by here by a female to address someone older.  

4) เรื่องอะไร rʉ̂ang à-rai 

About what? / What story? (are you talking about) 

5) (...)ทำให้ผมมีความสุข(...) (...) tam-hâi pǒm mii-kwaam-sùk (...) It makes me happy. / You make me happy.  

6) กินด้วยกันสิ gin dûai-gan sì 

Have some. / Join me. (eating together) 

7) เขานิสัยไม่ดี kǎo ní-sǎi mâi-dii 

He's bad! / He's a bad boy! / He's ill-natured. / He's rude. 

8) ช้าหน่อย cháa nɔ̀i 

Slow down!  

9) ฉันจะมาใหม่ chǎn jà maa mài 

I'll be back. / I'll come back. 

10) อยากไป yàak bpai 

(I) wanna go.  

11) หลับแล้วเหรอ làp lɛ́ɛo rə̌ə 

Are you asleep (already)?  

12) ออกกำลังกายอยู่ ɔ̀ɔk-gam-lang-gaai yùu I'm exercising. / I'm working out.  

13) (ลอง)คิดดู (lɔɔng) kít duu 

Think about it.  

14) คุณมีฉัน kun mii chǎn 

You have me. / You got me.  

15) ผมอธิบายได้ pǒm à-tí-baai dâi 

I can explain.  

16) บอกฉันได้นะ bɔ̀ɔk chǎn dâi ná 

(You) can tell me.  

17) ฉันเจ็บนะ chǎn jèp ná 

You're hurting me! / That hurts!  

18) คุณมาจากไหน kun maa-jàak nǎi 

Where are you from?  

19) คุณมาจากประเทศอะไร kun maa-jàak bprà-têet à-rai What country are you from?  

20) (อยาก)ดื่มอะไรไหม (yàak) dʉ̀ʉm à-rai mǎi Do you want something to drink? 

Super Useful Expressions, Part 106 

(high-intermediate level) 

1) ผมไม่ได้เป็นอะไรกับเขา pǒm mâi-dâi bpen à-rai gàp kǎo There's nothing between us. / She doesn't mean anything to me. 

2) ไม่รบกวนเวลาคุณ mâi róp-guan wee-laa kun 

I won't take up your time/waste your time.  

3) หาเท่าไหร่ก็หาไม่เจอ hǎa tâo-rài gɔ̂ɔ hǎa-mâi-jəə 

(I) couldn't find it anywhere. / (I) looked everywhere but couldn't find it.  

4) นั้นแสดงว่า... nán sà-dɛɛng wâa... 

That shows that... / That means that...  

5) ต้องทำยังไงถึงจะ... dtɔ̂ng tam yang-ngai tʉ̌ng jà... 

What do I need/have to do to...?  

6) มีแต่คุณคนเดียวที่... mii kɛ̂ɛ kun kon-diao tîi... 

You're the only one/person who...  

7) คุณกำลังจะทำอะไร kun gam-lang jà tam à-rai 

What are you gonna do? / What are you doing? (with future meaning) 

8) จะได้เห็นดีกัน SLANG/IDIOMATIC jà dâi hěn dii gan You're gonna get it! / I'll get you!  

9) ทำไมถึงคิดแบบนั้น tam-mai tʉ̌ng kít bɛ̀ɛp-nán 

What makes you say that? / Why do you think that?  

10) โตๆกันแล้ว dtoo-dtoo gan lɛ́ɛo 

You're an adult now. / You're all grown up. / We're adults (now).  

11) หายไปอยู่ไหน BIT STRONG hǎai bpai yùu-nǎi 

Where (the hell) have you been? / Where has it all gone to? / Where has it  disappeared to? 

12) ถ้าเกิดว่า... tâa gə̀ət wâa... 

What if... ? / If... ?  

13) จะไปไหนก็ไป SLANG/STRONG jà bpai nǎi gɔ̂ɔ bpai 

Beat it. / Get out of here! / Just leave! (I'm tired of you and don't want to see you  anymore) 

14) มีความเป็นไปได้สูงที่.../ว่า... mii kwaam-bpen-bpai-dâi sǔung tîi.../wâa.. There's a strong possibility that... / There's a good chance that... / It's highly  probable that... 

15) ผมไม่ได้พูดสักค pǒm mâi-dâi pûuk sàk kam 

I didn't say a word. / I didn't say a thing. 

16) แค่ทำตามหน้าที่เท่านั้น FORMAL kɛ̂ɛ tam-dtaam nâa-tîi tâo-nán (I)'m just doing my job/duty.  

17) ผมไม่เอาด้วยแล้ว pǒm mâi-ao dûai lɛ́ɛo 

I've had enough! / I'm out! / Enough of this!  

18) ฉันชินกับการอยู่คนเดียว chǎn chin gàp gaan-yùu-kon-diao I'm used to living/being alone.  

19) อะไร(ที่)ทำให้คุณคิดว่า... à-rai (tîi) tam-hâi kun kít wâa... What makes you think that...  

20) ไม่ใช่เรื่องเล็ก mâi-châi rʉ̂ang lék 

It's no trivial matter. / It's no small thing. / It's not to be taken lightly. 

Super Useful Expressions, Part 107 

(high-intermediate level)

1) ต้องขอโทษด้วย(นะคะ) dtɔ̂ng kɔ̌ɔ-tôot dûai (ná-ká) 

I must apologize. / I'm sorry. 

2) ต้องขอโทษด้วย(นะคะ)พอดี(ติดงาน) dtɔ̂ng kɔ̌ɔ-tôot dûai (ná-ká) pɔɔ-dii (dtìt-ngaan) 

I must apologize. (I was busy with work.) / I'm sorry. (I was busy with work.) Note: พอดี (pɔɔ-dii) is often used when giving a reason or excuse.  

3) ถามจริงๆเถอะ... STRONG tǎam jing-jing tə̀... 

Be honest... / Tell me the truth... (used when asking a question and  want honesty.) 

4) เปิดไม่ติด bpə̀ət-mâi-dtìt 

It won't turn on. / It doesn't work. / It's not working.  

5) อยากให้เป็นแบบนั้น yàak hâi bpen bɛ̀ɛp-nán 

(I) wish it were true. / (I) want it to be that way. / I'd like that.  

6) เขาไม่มั่นใจในตัวเอง kao mâi mân-jai nai dtua eeng 

He lacks confidence. / She insecure.  

7) ไม่มีตำหนิเลย mâi mii dtam-nì ləəi 

(It)'s flawless. / (It) has no marks on it. / (It)'s in perfect condition. / (It)'s without  blemish. 

8) ผมพูดอะไรผิดไปหรือเปล่า pǒm pûut à-rai pìt bpai rʉ̌ʉ-bplàao Did I say something wrong?  

9) นี่มันเรื่องอะไร nîi man rʉ̂ang à-rai 

What's this about...? / What's going on (here)?  

10) (มือ)ไปโดนอะไรมา (mʉʉ) bpai doon à-rai maa 

What happened to your (hand)? (looking at injury) 

11) ผมพูดอะไรออกไป pǒm pûut à-rai ɔ̀ɔk bpai

What did I say?! / What was I saying?! 

12) ฉันหิวจะแย่แล้ว INFORMAL chǎn hǐu jà yɛ̂ɛ lɛ́ɛo I'm starved! / I'm starving! / I'm sooo hungry!  

13) เราค่อยกลับมาคุยกัน rao kɔ̂i glàp maa kui gan 

We can talk about it later. / We can talk about it again later. 

14) กฎข้อที่ (1)... gòt kɔ̂ɔ tîi (nʉ̀ng)... 

Rule Number (1)... 

15) ผมขอเตือนนะ... pǒm kɔ̌ɔ dtʉan ná... 

I'm warning you... / Let this be a warning...  

16) จะว่าไปแล้ว... jà wâa bpai lɛ́ɛo... 

In fact... / As a matter of fact... / I can tell you this...  

17) ฝากบอกด้วยว่า... fàak bɔ̀ɔk dûai wâa... 

Please tell her/him...  

18) ทำอะไรให้ไม่พอใจหรือเปล่า tam à-rai hâi mâi-pɔɔ-jai rʉ̌ʉ-bplàao Did I do something to upset you?  

19) ไม่อยากเสียคุณไป mâi-yàak sǐa kun bpai 

I don't want to lose you.  

20) คุณเป็นใครกันแน่ STRONG kun bpen krai gan nɛ̂ɛ Who the hell are you? / Who are you really?  

Super Useful Expressions, Part 108 

(high-beginner level) 

1) อย่าร้องไห้ yàa rɔ́ɔng-hâi 

Don't cry. 

2) ยังไม่มี yang mâi-mii 

Not yet. / I don't have one yet. / I don't have any yet.  

3) ไม่ต้องรอ(ฉัน) mâi-dtɔ̂ng rɔɔ (chǎn) 

You don't need to wait (for me). / No need to wait up (for me). 

4) นี่มัน(ห้อง)ผม STRONG nîi man (hɔ̂ng) pǒm This is MY (room)!  

5) อยากกินอีก yàak gin ìik 

I wanna have that again. / I want to eat it again.  

6) จะเอาเท่าไร jà ao tâo-rai 

How much do you want?  

7) ไม่มีเวลา(แล้ว) mâi-mii wee-laa (lɛ́ɛo) 

(We) have no more time. / There's no more time.  

8) วาง(ลง) waang (long) 

Put it down. / Lay it down.  

9) ลืมซื้อมา lʉʉm sʉ́ʉ maa 

(I) forgot to buy it.  

10) อย่าลืมซื้อ... yàa lʉʉm sʉ́ʉ... 

Don't forget to buy...  

11) ครับ(พ่อ) / ค่ะ(พ่อ) kráp (pɔ̂ɔ) / kà (pɔ̂ɔ) Yes, (Dad). (acknowledging, agreeing, etc.) 

12) ยังไม่รู้ yang mâi-rúu 

I don't know yet. / I still don't know.  

13) แค่ครั้งเดียว kɛ̂ɛ kráng-diao 

Only once. / Only one time. 

14) หลับหรือยัง làp rʉ̌ʉ yang 

Are you asleep (yet)? 

15) ได้นอนแค่ (3) ชั่วโมง dâi nɔɔn kɛ̂ɛ (sǎam) chûa-moong I only slept (3) hours. / I only got (3) hours of sleep.  

16) ฉันคุยกับแม่แล้ว chǎn kui gàp (mɛ̂ɛ) lɛ́ɛo 

I spoke with (mom) already. / I already spoke with (my mother).  

17) ไม่รู้เมื่อไร mâi-rúu mʉ̂a-rai 

(I) don't know when.  

18) ตรงไป(ข้างหน้า) dtrong-bpai (kâang-nâa) 

Go straight (ahead).  

19) รถผมเสีย rót pǒm sǐa 

My car broke down.  

20) เขานิสัยดี kǎo ní-sǎi-dii 

He's nice. / He's a nice guy. / He's polite and well-mannered.  

Super Useful Expressions, Part 109 (high-intermediate level) 

1) สะใจเลย sà-jai ləəi 

(I) feel so (damn) satisfied! / (I) enjoyed that! / That was satisfying!  

2) ประสาท SLANG/ROUGH bprà-sàat 

Crazy guy! / Idiot! / You're wacko!  

3) ไม่เลยสักนิด mâi ləəi sàk-nít 

Not in the least bit. / Not at all. / Not a bit.  

4) ฉันคิดถึงแต่เรื่อง(กิน) chǎn kít-tʉ̌ng dtɛ̀ɛ rʉ̂ang (gin)

All I think about is (eating/food). / I think about (eating/food) all the time. / My mind is filled with thoughts of (eating/food).  

5) ไม่ทราบว่าใครโทรมาครับ FORMAL mâi-sâap wâa krai too-maa kráp Who's calling please?  

6) ไม่ทราบว่าเป็นใคร FORMAL mâi-sâap wâa bpen krai I don't know who it is/was.  

7) ตั้งแต่(ผม)เกิดมา... dtâng-dtɛ̀ɛ (pǒm) gə̀ət maa... 

Since I was born... / For the first time in my life... / This is the first time I... 

8) ดูท่าแล้ว... duu tâa lɛ́ɛo... 

It looks like... / It seems like... / Apparently...  

9) ...จะได้ประหยัดเงิน ...jà dâi bprà-yàt ngən 

...so (you) can save money. / ...(you) will save money (economize) 

10) เขาเล่นตัวอยู่ kǎo lên-dtua yùu 

She's playing hard to get.  

11) (...)ก่อนจะสายเกินไป (...) gɔ̀ɔn jà sǎai gəən-bpai 

(...)before it's too late.  

12) จัดไป INFORMAL jàt bpai 

Bring it on! / Hell yeah! (Let's do it!) 

13) (มือ)เย็นเฉียบ(เลย) (mʉʉ) yen chìap (ləəi) 

(Your hands) are as cold as ice/freezing cold/ice cold.  

14) จำทางไม่ได้ jam taang mâi-dâi 

(I)'ve lost my way. / (I) can't remember the way. 

15) ทำให้อยู่ท้อง tam-hâi yùu-tɔ́ɔng 

(It) fills you up. / (It)'s filling.  

16) ฉันทำอะไรลงไป DRAMATIC chǎn tam à-rai long bpai

What did I do?! (assumes something bad was done) 

17) อาการแย่มาก aa-gaan yɛ̂ɛ mâak 

(He)'s in really bad shape. / (He)'s hurt really bad. (of an accident or sickness) 

18) เธอสติไม่ดี təə sà-dtì mâi-dii 

She's not all there. / She's nuts. / She's crazy.  

19) เลิกพูดกันได้แล้ว lə̂ək pûut gan dâi lɛ́ɛo 

Stop talking about it/that!  

20) ติดต่อไม่ได้เลย dtìt-dtɔ̀ɔ mâi-dâi ləəi 

(I) can't get through to her. / (I)'ve been unable to contact her.  

Super Useful Expressions, Part 110 

(low-intermediate level) 

1) ผมเอาใจช่วยนะ pǒm ao-jai-chûai ná 

I hope you are successful. / I wish you success.  

2) มีอะไรไม่สบายใจหรือเปล่า mii à-rai mâi sà-baai-jai rʉ̌ʉ-bplàao Is something wrong? / Is something bothering you?  

3) จะกลับตอนไหน jà glàp dtɔɔn-nǎi 

When will (you) be back? / What time will (you) be back?  

4) ไม่กล้าท mâi glâa tam 

I didn't dare. / You wouldn't dare!  

5) มาหาใคร(ครับ) maa-hǎa krai (kráp) 

Who are you here to see? / Who are you here for? / Who are you looking for?  

6) แกโกหกฉัน gɛɛ goo-hòk chǎn 

You're lying (to me). 

7) เสียงดังเกินไป(แล้ว) sǐang-dang gəən-bpai (lɛ́ɛo) You're being too loud. / It was too loud. / She's too noisy!  

8) อาการดีขึ้น(แล้ว) aa-gaan dii-kʉ̂n (lɛ́ɛo) 

(He)'s feeling better. (His) condition has improved. / (He)'s getting better. 

9) กินอะไรหรือยัง gin à-rai rʉ̌ʉ-yang 

Have you eaten (anything) yet? / Did you eat yet? 

10) เขาไม่ได้สติ kǎo mâi-dâi sà-dtì 

He's unconscious. 

11) แย่แล้ว yɛ̂ɛ lɛ́ɛo 

Oh, shit! / Oh, no! 

12) ใครเป็นคนขโมย(ไป) krai bpen kon kà-mooi (bpai) Who stole it?  

13) ต้องเข้มแข็ง dtɔ̂ng kêm-kɛ̌ng 

Be strong! / Stay strong! (emotionally) 

14) ผมไม่รู้จักใครเลย pǒm mâi-rúu-jàk krai ləəi 

I don't know anyone (here). 

15) ยังปวดหัวอยู่หรือเปล่า yang bpùat-hǔa yùu rʉ̌ʉ-bplàao Do you still have a headache?  

16) ฉันไม่ไว้ใจใคร chǎn mâi-wái-jai krai 

I don't trust anyone.  

17) ผมไม่มีทางเลือก pǒm mâi-mii-taang lʉ̂ak 

I have no choice. / I had no choice.  

18) อย่าทำให้(ผม)ผิดหวัง yàa tam-hâi (pǒm) pìt-wǎng Don't disappoint me. / Don't let me down.  

19) อย่ายุ่ง STRONG yàa yûng

Don't interfere! / Leave me alone! / Don't mess with (me)!  

20) เดี๋ยวก็หาย dǐao gɔ̂ɔ hǎai 

It'll be better soon. / It'll heal soon. / It'll soon pass.  

Super Useful Expressions, Part 111 

(low-advanced level) 

1) ของฝากเล็กๆน้อยๆ kɔ̌ɔng-fàak lék-lék nɔ́ɔi-nɔ́ɔi 

Here's a small souvenir. / I have a little something I bought for you.  

2) ไม่ได้เข้าข้างใคร mâi-dâi kâo-kâang krai 

I'm not siding with anyone. / I'm not taking anyone's side.  

3) คิดดีทำดี kít dii tam dii 

Think good, do good.  

Note: This is an expression from Buddhism meaning if you have good  intentions and do good, you will have good karma. 

4) ขอให้เป็นอย่างนั้น SEMI-FORMAL/LITERARY kɔ̌ɔ-hâi bpen yàang-nán So be it. / I hope so. / I hope it turns out that way.  

5) เป็นคนละเอียดรอบคอบ bpen kon lá-ìat rɔ̂ɔp-kɔ̂ɔp 

He's meticulous/very thorough/very careful.  

6) เหมือนตกนรกทั้งเป็น mʉ̌an dtòk-ná-rók táng-bpen 

It's like hell on earth! / It's like going to hell alive!  

7) (จะ)สบายไปทั้งชาติ (jà) sà-baai bpai táng-châat 

He'd live comfortably for the rest of his life. / He would have it easy his whole life. (not necessary to work due to having excess money, etc.) 

8) ทำจิตใจให้สงบ tam jìt-jai hâi sà-ngòp

Calm your mind. 

9) ยังนับว่าโชคดี yang náp-wâa chôok-dii 

Consider yourself lucky! / You were lucky! / Still, you were fortunate. (that  nothing worse happened to you, etc.) 

10) พบเห็นได้ทั่วไป póp-hěn dâi tûa-bpai 

(It)'s found all over. / (It)'s commonly seen. / (It)'s a common sight.  

11) โชคเข้าข้าง(เราแล้ว) LITERARY chôok kâo-kâang (rao lɛ́ɛo) Luck is on our side. / This is our lucky break.  

12) ฉันไม่เรียกร้องอะไร SEMI-FORMAL chǎn mâi rîak-rɔ́ɔng à-rai I don't ask for anything. / I'm not demanding anything.  

13) ใครจะว่ายังไงก็ช่างเขา INFORMAL krai jà wâa yang-ngai gɔ̂ɔ châng kǎo They can say whatever they want! / I don't care what anyone says!  

14) สมกันดี(นะ) sǒm-gan dii (ná) 

Good match! / How suitable! / (You two) are perfect for each other! / They're a good fit. 

15) ฉันแค่พูดตามความเป็นจริง chǎn kɛ̂ɛ pûut dtaam kwaam-bpen-jing I'm just telling the truth. / I'm just being honest.  

16) ...ว่าไหม ...wâa mǎi 

... ,huh? / ... ,don't you think? / ... ,isn't it? / ... ,don't they? etc.  (This functions similar to a tag question.) 

17) เงินเดือนไม่พอใช้ ngən-dʉan mâi-pɔɔ chái 

(My) salary isn't enough (to live on).  

18) ดูซึมๆ duu sʉm-sʉm 

(She) looks down/depressed/mopey.  

19) มองไม่วางตาเลย mɔɔng mâi waang dtaa ləəi 

(He) kept staring at her. / (He) couldn't take his eyes off her. 

20) ขายดิบขายดี IDIOMATIC kǎai-dìp-kǎai-dii 

(They're) selling like hotcakes! / (They're) selling really well!  

Super Useful Expressions, Part 112 

(high-intermediate level) 

1) มีแต่ได้กับได้ mii dtɛ̀ɛ dâi gàp dâi 

It's a win-win (situation). 

2) คิดเอาเองสิ kít ao eeng sì 

Figure it out! / Think about it yourself.  

3) ต่างกับฉัน dtàang gàp chǎn 

Different from me. / Not the same as me. / Unlike me. 

4) ไม่อยากเห็นหน้าคุณอีก VERY STRONG mâi-yàak hěn nâa kun ìik I don't wanna see you anymore! 

5) ผมพูดมาตลอด(...) pǒm pûut maa dtà-lɔ̀ɔt(...) 

I've always said... / I've been saying (that) all along.  

6) จริงอยู่ว่า... jing yùu wâa... 

True, ... / It's true... 

7) คุณใช้(มัน)เป็นข้ออ้าง kun chái (man) bpen kɔ̂ɔ-âang You're using (it) as an excuse.  

8) สวยวัวตายควายล้ม / หล่อวัวตายควายล้ม IDIOM sǔai wua dtaai kwaai  lóm / lɔ̀ɔ wua dtaai kwaai lóm 

Drop dead gorgeous! / Super handsome or beautiful. 

Note: The literal meaning is that cows would die and water buffalos would fall  over due to one's good looks. 

9) โป๊ะเชะ SLANG bpó-ché 

Nailed it! / Bingo! / Jackpot!  

10) คุณตัดสินใจถูกแล้ว kun dtàt-sǐn-jai tùuk lɛ́ɛo 

You made the right choice.  

11) สู้ตาย SLANG sûu-dtaai 

Fight to the death. / Let's do it! / Let's do this!  

12) อะไรทำให้คุณคิดว่า... à-rai tam-hâi kun kít wâa... 

What makes you think... ?  

13) ทำอะไรไม่ได้นอกจาก... tam à-rai mâi-dâi nɔ̂ɔk-jàak... 

(I) can't do anything other than.../but.../except...  

14) ไม่ใช่ซะหน่อย mâi-châi sá-nɔ̀i 

No, that's not it! / No, it's not! / That's not true at all! 

15) ฉันหาข้อมูลมาแล้ว chǎn hǎa kɔ̂ɔ-muun maa lɛ́ɛo 

I already checked it out/researched it.  

16) เพราะอย่างนี้... SEMI-FORMAL prɔ́-yàang-níi... 

That's why... / That's the reason why...  

17) ดูจากสีหน้า... duu jàak sǐi-nâa... 

Judging from the look on (your) face... / (I can tell) from the look on (your) face...  

18) ให้คำตอบไม่ได้ hâi kam-dtɔ̀ɔp mâi-dâi 

(I) can't answer that. / (I) don't have an answer for that. / (I) can't give an answer.  

19) แกจะไปรู้ได้ยังไง STRONG gɛɛ jà bpai rúu dâi yang-ngai How the hell would you know?! / How could you possibly know?!  

20) ไม่มีอะไรน่าเป็นห่วง mâi mii à-rai nâa-bpen-hùang 

(There's) nothing to worry about. / It's nothing to worry about. 

Super Useful Expressions, Part 113 

(low-intermediate level) 

1) เพิ่งเริมต้น pə̂ng rə̂əm-dtôn 

We're just getting started. / It's just begun. / It's just the beginning. 

2) ฉันจะเรียกตำรวจ chǎn jà rîak dtam-rùat 

I'm gonna call the cops!  

3) เขายังโกรธฉันอยู่ kǎo yang gròot chǎn yùu 

He's still angry with/at me. 

4) ผมรู้ว่าผมผิด(...) pǒm rúu wâa pǒm pìt (...) 

I know I'm wrong(...)  

5) ยังไม่บอกหนู(เลย)(ว่า...) yang mâi bɔ̀ɔk nǔu (ləəi) (wâa...) You haven't told me yet(...)  

6) ฉันดูออก(...) chǎn duu ɔ̀ɔk (...) 

I can tell. / I can see...  

7) ยังสวยเหมือนเดิม yang sǔai mʉ̌an-dəəm 

You're still as pretty/beautiful as ever!  

8) เดี๋ยวตามไป dǐao dtaam bpai 

I'll be right there. / I'll meet you (there) in a minute. / I'll be there soon.  

9) พอใจแล้ว pɔɔ-jai lɛ́ɛo 

I'm satisfied. / I'm content. / It's enough for me.

10) ตื่นเต้นมาก dtʉ̀ʉn-dtên mâak 

I'm so excited. / It's so exciting.  

11) ฉันเชื่อใน(ความรัก) chǎn chʉ̂a nai (kwaam-rák) 

I believe in (love).  

12) มีลูกค้าประจำเยอะ mii lûuk-káa bprà-jam yə́ 

We have a lot of customers who come here regularly. / She has a lot of steady  clients. 

13) ไม่เคยผิดพลาด mâi-kəəi pìt-plâat 

I never make mistakes. / He's never wrong.  

14) ฉันรักที่นี่(มาก) chǎn rák tîi-nîi (mâak) 

I love it here. / I love this place. 

15) อยากกินอะไรอีกไหม yàak gin à-rai ìik mǎi 

Do you wanna have something else (to eat)?  

16) นี่คือคำสั่ง nîi kʉʉ kam-sàng 

That's an order!  

17) เรื่องมันยาว rʉ̂ang man yaao 

It's a long story.  

18) บอกเขาแล้วหรือ bɔ̀ɔk kǎo lɛ́ɛo rʉ̌ʉ 

You told her already?!  

19) มันคุ้มไหม man kúm mǎi 

Is it worth it? / Was it worth it?  

20) ปากหวาน(จริงๆ) bpàak-wǎan (jing-jing) 

Such a sweet talker! / You're so sweet! / You really know how to sweet talk! Super Useful Expressions, Part 114

(low-intermediate level) 

1) เช่นกัน SEMI-FORMAL chên-gan 

Me, too. / You, too. / Same here. / Likewise.  

2) รักษาตัวด้วย SEMI-FORMAL rák-sǎa dtua dûai 

Take care (of yourself).  

3) น่าทึ่งมาก nâa-tʉ̂ng mâak 

(It)'s really amazing/impressive/incredible!  

4) (FYI)ย่อมาจากอะไร (FYI) yɔ̂ɔ maa-jàak à-rai 

What does (FYI) stand for?  

5) ทำแบบนี้ไม่ได้ tam bɛ̀ɛp-níi mâi-dâi 

(You) can't do that. / (You) can't do this.  

6) เขาเป็นคนดี kǎo bpen kon dii 

He's a nice guy. / He's a good person.  

7) ผมจะทำให้ดู pǒm jà tam-hâi duu 

I'll show you. (how to do it, etc.) 

8) จำไว้ให้ดี jam wái hâi dii 

Just remember... / Keep in mind... / Bear in mind...  

9) หลังจากนั้นผม(ก็)... lǎng-jàak nán pǒm (gɔ̂ɔ)... 

After that I... / Then I...  

10) อยากให้คุณรู้ว่า... yàak hâi kun rúu wâa... 

I want you to know...  

11) เขาหน้าตา(เป็น)ยังไง INFORMAL kǎo nâa-dtaa (bpen) yang-ngai What does s/he look like?  

12) อย่าแตะต้อง(อะไร) yàa dtɛ̀-dtɔ̂ng (à-rai)

Don't touch (anything)!  

13) อ่าน(หมด)ทุกเล่ม(เลย) àan (mòt) túk lêm (ləəi) I read them all. / I've read each and every one.  

14) เขายังมีชีวิตอยู่หรือเปล่า kǎo yang mii-chii-wít-yùu rʉ̌ʉ-bplàao Is he still alive/living? 

15) ยังมีชีวิตอยู่ yang mii-chii-wít-yùu 

(I)'m still alive.  

16) ฉันไม่ได้บอกว่า(คุณบ้า) chǎn mâi-dâi bɔ̀ɔk wâa (kun bâa) I didn't say (you were crazy).  

17) มองอะไรวะ STRONG/VULGAR mɔɔng à-rai wá What are ya lookin' it?!  

18) พอใจหรือยัง pɔɔ-jai rʉ̌ʉ-yang 

Are you satisfied (now)? / Satisfied?  

19) จริงรึเปล่า jing rʉ́-bplàao 

Is it true? / Really?  

20) ไม่เคยได้ยิน mâi-kəəi dâi-yin 

Never heard it. / Never heard of him.  

Super Useful Expressions, Part 115 

(high-beginner level) 

1) มีเวลาน้อย mii wee-laa nɔ́ɔi 

(I) don't have much time. / (I) have little time (for)...  

2) ใครเป็นคนท krai bpen kon tam 

Who did it? / Who made it? 

3) ฟังฉันนะ fang chǎn ná 

Listen to me.  

4) หันหลัง(สิ)(ค่ะ) hǎn-lǎng (sì)(kà) 

Turn around.  

5) ไม่ใช่ตอนนี้ mâi-châi dtɔɔn-níi 

Not now!  

6) อายุเท่าไหร่ aa-yú tâo-rài 

How old are you? / How old is she?  

7) มันจบแล้ว man jòp lɛ́ɛo 

It's over. / It's finished. 

8) พอไหม(คะ) pɔɔ mǎi (ká) 

Is that enough? / Will that do?  

9) ตอนนี้เลย dtɔɔn-níi ləəi 

(Right) now!  

10) ไปมาแล้ว bpai maa lɛ́ɛo 

I've already been (there). / I went there already. 

11) เจ็บไหม jèp mǎi 

Does it hurt?  

12) เจ็บตรงไหน jèp dtrong-nǎi 

Where does it hurt? 

13) ฉันชอบ (X) มากกว่า (Y) chǎn chɔ̂ɔp (X) mâak-gwàa (Y) I like (X) more than (Y).  

14) ชอบไหม chɔ̂ɔp mǎi 

Do you like it? / Do you like her?

15) ฟังดูดี fang duu dii 

Sounds good.  

16) ขอบคุณมาก(นะคะ) kɔ̀ɔp-kun mâak (ná-ká) 

Thank you very much. 

17) เขามาทำอะไร kǎo maa tam à-rai 

What was he doing here? / What is she doing here? 

18) หยุดไม่ได้ yùt mâi-dâi 

I can't stop. / I couldn't stop.  

19) ผมรอได้ pǒm rɔɔ dâi 

I can wait.  

20) ไม่ได้บอก(ค่ะ) mâi-dâi bɔ̀ɔk (kà) 

He didn't say. / He didn't tell me.  

Super Useful Expressions, Part 116 

(high-intermediate level) 

1) ตรงเวลาเป๊ะเลย dtrong-wee-laa bpé ləəi 

Right on time! / Just in time!  

2) มันไม่ใช่เรื่องง่ายๆ man mâi-châi rʉ̂ang ngâai-ngâai 

It's not a simple matter. / It's not (so) easy.  

3) ไม่เกินไปหน่อยหรอ STRONG mâi gəən-bpai nɔ̀i rə̌ə 

It's it a bit too much? / Isn't that going too far? / Isn't that overkill? / Isn't that a bit  extreme?  

4) ต้องมีอะไรบ้าง dtɔ̂ng mii à-rai bâang 

There has to be something. / There's gotta be something.  

5) ผมจะอยู่เคียงข้างคุณ pǒm jà yùu kiang-kâang kun

I'll be by your side. / I'll be with you.  

6) เลิกยุ่งกับฉัน(ซะที)(เถอะ) STRONG lə̂ək yûng-gàp chǎn (sá-tii) (tə̀) Leave me alone! / Stop messing with me! / Stop bugging me!  

Note: ซะที (sá-tii) is a casual version of เสียที (sǐa-tii), which is a particle urging that one wants something to happen soon or finally. 

7) เขาออกจากโรงพยาบาลแล้ว kǎo ɔ̀ɔk-jàak roong-pá-yaa-baan lɛ́ɛo She got out of the hospital. / She left the hospital. / She was discharged.  

8) ไม่ว่าเกิดอะไรขึ้น(ก็ตาม)... mâi-wâa gə̀ət à-rai kʉ̂n (gɔ̂ɔ-dtaam)... No matter what happens... / Whatever happens...  

9) ต้องดูก่อน(ว่า...) dtɔ̂ng duu gɔ̀ɔn (wâa)... 

We'll have to see(...) / We'll see(...)  

10) ...กลับมาเหมือนเดิม ...glàp-maa mʉ̌an-dəəm 

...back to normal. / ...back to as before. / ...the same as (it) used to be.  

11) อีกอย่างหนึ่ง... ìik yàang nʉ̀ng... 

And another thing... / Besides... 

12) ลึกๆในใจ... lʉ́k-lʉ́k nai jai... 

Deep down... / Deep inside my heart...  

13) ต้องทำตามสัญญา dtɔ̂ng tam-dtaam sǎn-yaa 

(You) must keep your promise. / (I) have to keep my promise.  

14) เหมือนเคยเจอที่ไหนมาก่อน mʉ̌an kəəi jəə tǐi-nǎi maa-gɔ̀ɔn It's like I've seen (him) somewhere before. 

15) (การผ่าตัด)เป็นไปด้วยดี (gaan-pàa-dtàt) bpen-bpai dûai-dii (The operation) went well/smoothly.  

16) ตลกตายล่ะ SARCASTIC dtà-lòk dtaai (lâ) 

Very funny. (meaning it's not really funny)

17) ไม่มีวันเป็นไปได้ mâi-mii-wan bpen-bpai-dâi 

(It) will never happen. / (It) will never work.  

18) ยอมไม่ได้แน่ๆ yɔɔm mâi-dâi nɛ̂ɛ-nɛ̂ɛ 

(I) can't let that happen! / (I) can't let you do that! / (I) can't allow that!  

19) พูดไปเรื่อย pûut bpai rʉ̂ai 

(You)'re rambling. / (You)'re talking aimlessly. 

20) ฉันไม่ว่า(อะไร) chǎn mâi wâa (à-rai) 

It didn't bother me. / It was fine with me. / I was cool with it. / I didn't mind. (=I didn't say anything because it was fine with me.) 

Super Useful Expressions, Part 117 

(high-intermediate level) 

1) ขออะไรอย่าง(หนึ่ง)ได้ไหม kɔ̌ɔ à-rai yàang (nʉ̀ng) dâi-mǎi Can (I) ask you a favor? / Will you do something for (me)?  

2) มีอะไร(จะพูด)ก็พูดมา(สิ) INFORMAL mii à-rai (jà pûut) gɔ̂ɔ pûut maa (sì) If you have something to say, (just) say it.  

3) ผมต่างหากที่ต้องขอบคุณคุณ pǒm dtàang-hàak tîi dtɔ̂ng kɔ̀ɔp-kun kun I should be the one thanking you. / I should be doing the thanking.  

4) เราเข้าใจตรงกัน rao kâo-jai dtrong-gan 

We're in agreement. / We're on the same page. / We have an understanding.  

5) ไม่เห็นเป็นไรเลย INFORMAL mâi hěn bpen-rai ləəi 

No, it's okay. / It's alright. / It's fine. (=It's not a problem or big deal.) Note: ไร (rai) is a shortened form of อะไร (à-rai).  

6) คืออย่างนี้... kʉʉ yàang-níi 

It's like this... / I mean... (used when explaining something)

7) คืองี้... INFORMAL kʉʉ ngíi 

It's like this... / I mean... (used when explaining something) 

8) คนสมัยนี้... kon sà-mǎi níi... 

People nowadays... / People today... / People these days...  

9) ถ้ามีอะไรผิดปกติ... tâa mii à-rai pìt-bpòk-gà-dtì... 

If there's anything unusual... / If something goes wrong... / If anything is out of  the ordinary...  

Note: ปกติ (bpàk-gà-dtì) is an alternative pronunciation.  

10) มัน(จะ)เกิดขึ้นไม่ได้ man (jà) gə̀ət-kʉ̂n mâi-dâi 

That can't happen. / That's never gonna happen.  

11) เป็นอย่างที่คุณคิด bpen yàang-tîi kun kít 

It was just like you said.  

12) เอาไปไหน ao bpai-nǎi 

Where are you going with that? / Where are you taking that?  

13) โคตรเจ๋งเลย INFORMAL/MAINLY TEENAGE kôot jěng ləəi Totally awesome! / Freakin' cool man! 

14) เราจะ(แกล้ง)ทำเป็นไม่รู้จักกัน rao jà (glɛ̂ɛng) tam-bpen mâi-rúu-jàk gan We'll pretend we don't know each other. 

15) แกรู้มั้ยว่าทำอะไรลงไป gɛɛ rúu mái wâa tam à-rai long bpai Do you know what you've done?!  

16) ชักช้าจริง chák-cháa jing 

(You)'re so slow! (hurry!) / (He)'s driving so slow! (hurry up!) 

17) คุณคิดจะทำอะไร kun kít jà tam à-rai 

What do you think you're doing?! 

18) เขาถูกปฏิเสธ kǎo tùuk bpà-dtì-sèet 

He was rejected. / She was denied. / She was turned down. 

19) ฉันไม่ได้คิดอะไรกับเขา chǎn mâi-dâi kít à-rai gàp kǎo I don't have feelings for him. / I don't have a thing for him.  

20) มันเป็นจริงอย่างที่(ผม)คิด man bpen-jing yàang-tîi (pǒm) kít It's just like/as (I) thought.  

Super Useful Expressions, Part 118 

(high-beginner level) 

1) ฉันสับสน chǎn sàp-sǒn 

I'm confused. 

2) เขาโชคดี kǎo chôok-dii 

He's lucky. 

3) ไม่ได้ mâi-dâi 

No! / (You) can't!  

4) พรุ่งนี้(วันเสาร์)/พรุ่งนี้เป็น(วันเสาร์) prûng-níi (wan-sǎo) / prûng-níi bpen  (wan-sǎo) 

Tomorrow's (Saturday)  

5) ไม่จำเป็น mâi jam-bpen 

It's not necessary. / (There's) no need.  

6) ไม่อายเหรอ mâi aai rə̌ə 

Aren't you embarrassed/ashamed?!  

7) (มัน)อยู่ชั้น(3) (man) yùu chán (sǎam) 

(It)'s on the (3rd) floor. 

8) หนักเท่าไหร่ nàk tâo-rài 

How much does (it) weigh?  

9) เงียบ(ไป) INFORMAL-STRONG ngîap (bpai) Quiet!  

10) โทรไปแล้ว too-bpai lɛ́ɛo 

I called already. / I called him already.  

11) ฉันอยากเจอเขา chǎn yàak jəə kǎo I want to meet him/her.  

12) (8)ชั่วโมงต่อวัน (bpɛ̀ɛt) chûa-moong dtɔ̀ɔ-wan (8) hours a day.  

13) ทำยาก tam yâak 

It's difficult to do. / That's hard.  

14) พูดสิ pûut sì 

Say it. (repeat it after me, etc.) 

15) ฉันไม่กลัว(หรอก) chǎn mâi-glûa (rɔ̀ɔk) I'm not afraid (at all).  

16) ล้างมือก่อน láang mʉʉ gɔ̀ɔn 

Wash your hands (first).  

17) ทุกวัน túk-wan 

Every day.  

18) มันหนักมาก man nàk mâak 

It's very heavy.  

19) นานแค่ไหน naan kɛ̂ɛ-nǎi 

For how long?  

20) ใหญ่มาก yài mâak

It's huge. / That's a big one. 

Super Useful Expressions, Part 119 

(high-intermediate level) 

1) เงียบปาก ngîap bpàak 

Shut your mouth! / Shut (the xxx) up! / Zip it! 

2) เอาเถอะน่า ao tə̀ nâa 

Come on! / Oh, come on!  

3) (ต้อง)ตั้งสติ (dtɔ̂ng) dtâng-sà-dtì 

(You have to) calm down. / (You need to) stay focused. / (You have to) compose  yourself. / Get a grip!  

4) เหลือเวลาแค่(3 อาทิตย์) lʉ̌a wee-laa kɛ̂ɛ (sǎam aa-tít) 

(We) only have (3 weeks) left. / (There's) only (3 weeks) left.  

5) เลือกเอาเอง(นะ) lʉ̂ak ao eeng (ná) 

The choice is yours. / It's your choice. / You choose.  

6) ต้องไปจากที่นี่ dtɔ̂ng bpai jàak tîi-nîi 

We gotta get out of here. / I have to leave. / She has to go away. 

7) คุณทำแบบนี้กับฉันได้ยังไง kun tam bɛ̀ɛp-níi gàp chǎn dâi yang-ngai How can/could you do this to me?!  

8) เป็นแบบนี้แหละ bpen bɛ̀ɛp-níi lɛ̀ 

That's how (I) am! / That's how (it) is!  

9) (...) สองสามวันมานี้ (...) (...) sɔ̌ɔng sǎam wan maa níi (...) The last few days... / ...for the last few days.  

10) (...) โดยที่ไม่คิด (...) (...) dooi-tîi mâi kít (...)

Without thinking (about...) /...without thinking.  

11) อีกไม่ถึง(3 นาที)... ìik mâi-tʉ̌ng (sǎam naa-tii)... 

In less than (3 minutes)...  

12) บ้างก็ว่า... bâang gɔ̂ɔ wâa... 

Some (people) say... 

13) อย่าตกเป็นเหยื่อ(...) yàa dtòk-bpen-yʉ̀a(...) 

Don't fall victim (to...) / Don't fall prey to... / Don't be victimized.  

14) ใครจะรับผิดชอบ krai jà ráp-pìt-chɔ̂ɔp 

Who will be responsible? / Who would be responsible? / Who would take  responsibility? 

15) ดียังไง dii yang-ngai 

How is it good? / What's good about it?  

16) เคยได้ยินที่ไหน kəəi dâi-yin tîi-nǎi 

Where have (I) heard that before?  

17) รู้สึกกดดันไหม rúu-sʉ̀k gòt-dan mǎi 

Do (you) feel pressure/pressured?  

18) เขาช้าตลอด kǎo cháa dtà-lɔ̀ɔt 

She's always late/slow!  

19) ไม่ใช่ประเด็น mâi-châi bprà-den 

(That)'s not the issue/point.  

20) กล้าดียังไง glâa-dii yang-ngai 

How dare you! / You've got a lot of nerve! 

Super Useful Expressions, Part 120 

(high-intermediate level)

1) ไฟดับ(แล้ว) fai-dàp (lɛ́ɛo) 

The power's out. / There's a power outage. 

2) จะรีบไปไหน jà rîip bpai-nǎi 

Where are you rushing off to? / Where are you hurrying to?  

3) อย่าใจร้อน yàa jai-rɔ́ɔn 

Don't (be in a) rush! / Don't be so hot-tempered!  

4) ที่นี่ที่ไหน tîi-nîi tîi-nǎi 

Where are we? / Where is this place?  

5) คงมีเรื่องเข้าใจผิดกัน kong mii rʉ̂ang-kâo-jai-pìt-gan 

There must be a/some misunderstanding.  

6) เขาไม่สนใจอะไรเลยนอกจาก(เงิน) kǎo mâi-sǒn-jai à-rai ləəi nɔ̂ɔk-jàak (ngən) She's not interested in anything except/other than (money).  

7) ดูท่าทาง(ไม่ค่อยสนใจ) duu-tâa-taang (mâi-kɔ̂i sǒn-jai) 

He doesn't seem (so interested). / It looks like (he's not so interested). / It doesn't  look like (he's very interested).  

8) เขาดูมีความสุขมาก(เลย) kǎo duu mii-kwaam-sùk mâak (ləəi) She looks like she's really happy. / She seems really happy.  

9) ไม่บอกก็ไม่เป็นไร mâi bɔ̀ɔk gɔ̂ɔ mâi-bpen-rai 

If you don't tell me, it's okay. / If you aren't going to tell me, no problem.  

10) ไม่มีอะไรสำคัญ(ไป)กว่า(ครอบครัว) mâi mii à-rai sǎm-kan (bpai) gwàa  (krɔ̂ɔp-krua) 

Nothing's more important than (family). / There's nothing more important than  (family.)  

11) ทำอะไรไม่ถูก tam à-rai mâi-tùuk 

I couldn't do anything. / I was helpless. / I was at a loss. 

12) รู้แล้วว่าใคร rúu lɛ́ɛo wâa krai 

(I) know who it was!  

13) รู้แล้วว่าใครท rúu lɛ́ɛo wâa krai tam 

(I) know who did it! 

14) ขอความร่วมมือ(...) FORMAL kɔ̌ɔ kwaam-rûam-mʉʉ (...) (We) ask for your cooperation. / (I)'d like to ask for your cooperation. 

15) เชิญด้านนี้ FORMAL chəən dâan-níi 

This way, please. / Follow me, please.  

16) พูดไม่รู้เรื่อง pûut mâi-rúu-rʉ̂ang 

(She) doesn't make any sense! 

17) ไม่ลำบากอะไรเลย mâi lam-bàak à-rai ləəi 

It was no trouble. / No trouble at all.  

18) (กลัว)ตื่นไม่ทัน (glua) dtʉ̀ʉn mâi-tan 

(I'm afraid) I'll oversleep. / (I'm afraid) I won't wake up in time.  

19) ฉันรู้ความลับของคุณ chǎn rúu kwaam-láp kɔ̌ɔng kun I know your secret!  

20) พูดความจริงเถอะ pûut kwaam-jing tə̀ 

Tell (me) the truth! 

Super Useful Expressions, Part 121 

(high-intermediate level) 

1) ขอแสดงความยินดีด้วย SEMI-FORMAL kɔ̌ɔ sà-dɛɛng kwaam-yin-dii dûai Congratulations! 

2) ขอแสดงความยินดีกับคุณ FORMAL kɔ̌ɔ sà-dɛɛng kwaam-yin-dii gàp kun Allow me to congratulate you. / Congratulations to you.  

3) ไม่รู้จะตอบอะไร mâi-rúu jà dtɔ̀ɔp à-rai 

I don't know how to answer. 

4) จะให้ผมตอบว่ายังไง jà hâi pǒm dtɔ̀ɔp wâa yang-ngai 

How do you want me to answer? / What do you want me to say?  

5) คนอื่นหายไปไหนกันหมด kon-ʉ̀ʉn hǎai-bpai-nǎi gan mòt Where did all the others go? / Where did everyone go?  

6) อยากบอกอะไรฉันไหม yàak bɔ̀ɔk à-rai chǎn mǎi 

Is there something you want to tell me?  

7) ฉันติดธุระ chǎn dtìt tú-rá 

I'm in the middle of something. / I'm all tied up. / I'm busy (with something).  

8) (กระเทียม)มีกลิ่นฉุน (grà-tiam) mii-glìn chǔn 

(Garlic) has a pungent odor. / (Garlic) has a strong smell.  

9) ...ช่วยลดความเสี่ยงที่(จะเป็นโรคหัวใจ) ...chûai lót kwaam-sìang tîi (jà  bpen rôok-hǔa-jai) 

...(it) helps reduce the risk of (heart disease). / ...(it) reduces the risk of (heart  disease).  

10) เจ็บจี๊ด INFORMAL jèp jíit 

It hurts like hell! / That really hurts! (said of a sharp pain) 

11) เราไม่น่าเจอกันเลย rao mâi-nâa jəə-gan ləəi 

We should never have met!  

12) และที่สำคัญ... SEMI-FORMAL lɛ́ tîi sǎm-kan... 

And importantly... / And what's important...  

13) (คนจีน)มีความเชื่อว่า... FORMAL/LITERARY (kon-jiin) mii kwaam-chʉ̂a wâa...

(Chinese) believe that...  

14) อยากออกไปจากที่นี่(เร็วๆ) yàak ɔ̀ɔk-bpai-jàak tîi-nîi (reo-reo) (I) wanna get out of here (now)! / (I) want to get out of here (quickly)! 

15) มีเรื่องต้องจัดการ mii rʉ̂ang dtɔ̂ng jàt-gaan 

There's something (I) have to do. / There are something things (I) have to take  care of.  

16) คุณต่างหากล่ะ kun dtàang-hàak lâ 

It's YOU! / YOU'RE the one! (who is scared, etc. not me!) 

17) ก็จริง INFORMAL gɔ̂ɔ jing 

That's true. / Right.  

18) ยอมรับความจริง yɔɔm-ráp kwaam-jing 

Face the truth! / Admit the truth! / Accept the fact.  

19) ไม่มีผลเสีย mâi-mii pǒn-sǐa 

There's no damage (done). / There's no disadvantage. / It won't hurt us.  

20) เขาไม่เอาไหน STRONG kǎo mâi-ao-nǎi 

He's useless! / He's good for nothing! / He's a loser! 

Super Useful Expressions, Part 122 

(mid-advanced level) 

1) ต้องตัดไฟแต่ต้นลม IDIOM dtɔ̂ng dtàt-fai dtɛ̀ɛ dtôn-lom We have to nip it in the bud. / I have to stop it before it gets out of control. / I gotta eliminate the threat. 

2) ฉลาดเป็นกรด IDIOMATIC chà-làat bpen gròt 

He's very clever. / She's very sharp.

3) ไม่รู้จักเข็ด mâi rúu-jàk kèt 

He hasn't learned his lesson! 

4) ยอมตายแทนกันได้ yɔɔm dtaai tɛɛn gan dâi 

I'd be willing to die in her place. / I would die for him.  

5) ผมเหนื่อยสายตัวแทบขาด pǒm nʉ̀ai sǎai-dtua tɛ̂ɛp kàat I'm dog tired. / I'm absolutely exhausted. (from working hard without a break) 

Note: สายตัว (sǎai-dtua) means 'tendon' or 'sinew'.  

6) ต้องคิดหน้าคิดหลังให้ดี IDIOMATIC dtɔ̂ng kít nâa kít lǎng hâi-dii You have to think about things carefully. (don't do things rashly but think  carefully) 

7) มันหายวับไปกับตา IDIOMATIC man hǎai wáp bpai gàp dtaa It vanished before his very eyes! / It disappeared like magic!  

8) มันยังไงกันแน่/มันยังไงกันวะ IDIOMATIC/INFORMAL/STRONG man yang-ngai gan nɛ̂ɛ / man yang-ngai gan wá 

What's goin' on?! / What's that all about?! / What the hell's going on?!  

9) ...ไม่ไกลเกินฝัน LITERARY ...mâi glai gəən fǎn 

...is within reach. / ...is attainable. / ...isn't beyond your dream. 

10) รู้ตัวอีกที... IDIOMATIC rúu-dtua ìik-tii... 

Before (you) know it... / The next thing you know...  

11) ผมรวบรวมความกล้าที่จะ... pǒm rûap-ruam kwaam-glâa tîi-jà... I gathered up the courage to... 

12) ขออโหสิกรรม(...) LITERARY/RELIGIOUS kɔ̌ɔ à-hǒo-sì-gam(...) Please forgive me. / I apologize (for)... (for wrongdoing or sin) 

13) ผมไม่มีกะจิตกะใจ(จะ)ทำอะไร LITERARY/IDIOMATIC pǒm mâi-mii gà-jìt-gà-jai (jà) tam à-rai

I don't feel like doing anything. / I don't have the motivation to do anything.  (feeling listless or down) 

14) เขารับช่วงต่อ(กิจการของพ่อ) kǎo ráp-chûang-dtɔ̀ɔ (gìt-jà-gaan kɔ̌ɔng pɔ̂ɔ) He's taking over (his father's business). / He's the successor to (his father's  business).  

15) ฉันรู้ก็แล้วกัน chǎn rúu gɔ̂ɔ-lɛ́ɛo-gan 

I just know. / Let's just say I know.  

16) กิ่งทองใบหยก IDIOM gìng-tɔɔng-bai-yòk 

A match made in heaven! / They're so compatible! (said of a married couple) Note: กิ่งทอง (gìng-tɔɔng) is 'gold branch' and ใบหยก (bai-yòk) is 'jade leaf'.  

17) รู้หน้าไม่รู้ใจ IDIOMATIC rúu nâa mâi-rúu jai 

You know him, but you don't really know what's in his heart/mind. 

18) ยิ่ง(หนีความจริง)เท่าไหร่ก็ยิ่ง(เจ็บมาก)เท่านั้น yîng (nǐi kwaam-jing) tâo-rài gɔ̂ɔ yîng (jèp mâak) tâo-nán 

The more (I run from the truth), the more (hurt I get).  

19) ห้ามโน่นห้ามนี่ IDIOMATIC hâam nôon hâam nîi 

Don't do this! Don't do that!  

20) เปลี่ยนจากหน้ามือเป็นหลังมือ IDIOM bplìan jàak nâa-mʉʉ bpen lǎng  mʉʉ 

(She's) changed completely. / (She's) changed 180 degrees. / She's made a  complete turnaround.  

Super Useful Expressions, Part 123 

(high-beginner level) 

1) เขาทิ้งฉัน kǎo tíng chǎn

He left me. / He dumped me.  

2) ทำให้หิว tam-hâi hǐu 

Makes (me) hungry.  

3) (คุณ)อยากทำอะไร (kun) yàak tam à-rai What do you wanna do?  

4) เขาหล่อ kǎo lɔ̀ɔ 

He's handsome. / He's cute. 

5) เขาหน้าตาดี kǎo nâa-dtaa dii She's good looking. / He's handsome.  

6) เขาใจดี(มาก) kǎo jai-dii (mâak) He's (very) kind. / He's (really) nice. 

7) (เรา)เป็นเพื่อนกัน (rao) bpen pʉ̂an gan (We)'re friends.  

8) ไม่เหมือนผม mâi-mʉ̌an pǒm Not like me. / Unlike me.  

9) อยากเลี้ยง(หมา) yàak líang (mǎa) I wanna have a (dog). 

10) รอไม่ได้ rɔɔ mâi-dâi 

(I) can't wait.  

11) ไม่รู้จักเขา mâi rúu-jàk kǎo 

(I) don't know him/her.  

12) ยังไม่หิว yang mâi hǐu 

I'm not hungry (yet). 

13) ฉันไปด้วย chǎn bpai dûai 

I'm going too! / I'm going with you.

14) คุณต้องหยุด kun dtɔ̂ng yùt 

You have to stop. / You have to quit. 

15) อะไรหรือ(ครับ) à-rai rʉ̌ʉ (kráp) 

What (is it)?  

16) พักผ่อนเยอะๆ pák-pɔ̀n yə́-yə́ 

Get a lot of rest. / Rest up.  

17) ไม่รู้(เขา)อยู่ไหน mâi-rúu (kǎo) yùu-nǎi 

I don't know where (he) is.  

18) กลับมาตั้งแต่เมื่อไร glàp-maa dtâng-dtɛ̀ɛ mʉ̂a-rai 

When did (you) get/come back? 

19) ไปก่อนนะ bpai gɔ̀ɔn ná 

Bye. / Gonna go now---bye. / See you later. (one person leaves before the other) 

20) ยังไม่หลับหรือ yang mâi làp rʉ̌ʉ 

Are you still awake? / Not sleeping yet? 

Super Useful Expressions, Part 124 

(high-intermediate level) 

1) ไม่น่าจะใช่นะ mâi-nâa-jà châi ná 

That's unlikely. / I don't think so. 

2) ไม่น่าเกี่ยวนะ mâi-nâa gìao ná 

It's probably not connected. / I doubt if it's related.  

3) ขออนุญาตนะคะ kɔ̌ɔ à-nú-yâat ná-ká 

May I? / Please allow me... / Excuse me...  

4) ดวงดาวเต็มท้องฟ้า duang-daao dtem tɔ́ɔng-fáa

The sky is full of stars. 

5) ผมจะทำตามที่คุณบอก pǒm jà tam dtaam-tîi kun bɔ̀ɔk 

I'll do as you say. / I'll do whatever you want.  

6) มันเป็นอย่างนี้(...) man bpen yàang-níi(...) 

This is how it is(...) / This is how it happened(...)  

7) ...เมื่อวันนั้นมาถึง... ...mʉ̂a wan nán maa-tʉ̌ng... 

When that day comes... / ...when that day comes.  

8) ฝากดูแลเขาด้วย(นะ) fàak duu-lɛɛ kǎo dûai (ná) 

Take care of her (for me).  

9) ฉันเคยผ่านมันมาแล้ว chǎn kəəi pàan man maa lɛ́ɛo 

I've been through that before. / I've experienced that before.  

10) ไม่ใช่คนที่(ฉัน)เคยรู้จัก mâi-châi kon tîi (chǎn) kəəi rúu-jàk (He)'s not the person I used to know. (=He's changed and not like he used to be.) 

11) โดยไม่มีเงื่อนไข dooi mâi-mii-ngʉ̂an-kǎi 

No strings attached. / With no conditions. / Unconditionally. 

12) มีหลายประเภท FORMAL/LITERARY mii lǎai bprà-pêet There are many kinds/types.  

13) ประเภทที่(1)... FORMAL/LITERARY bprà-pêet tîi (nʉ̀ng)... The (first) type/kind is...  

14) ...หรืออะไรแบบนั้น rʉ̌ʉ à-rai bɛ̀ɛp-nán 

...or something like that. 

15) ...จากทั่วทุกมุมโลก... ...jàak tûa túk mum lôok... 

...from all over the world... /...from all around the world... /...from every corner of  the world...  

16) คิดถึงใจจะขาด kít-tʉ̌ng jai jà kàat

(I) miss (him) so bad my heart's gonna break! / (I) miss (him) like crazy!  

17) (หล่อ)ไม่แพ้กัน (lɔ̀ɔ) mâi-pɛ́ɛ-gan 

(He)'s just as (handsome)! / (He)'s equally (handsome)!  

18) คุณจะรู้สึกผิดไปตลอดชีวิต kun jà rúu-sʉ̀k-pìt bpai dtà-lɔ̀ɔt chii-wít You'll feel guilty for the rest of your life.  

19) ผมไม่อยากทำให้เธอเสียใจ pǒm mâi yàak tam-hâi təə sǐa-jai I don't wanna break her heart. / I don't want to hurt her.  

20) กลัวจะเสียเธอไป glua jà sǐa təə bpai 

(I)'m afraid (I)'ll lose her. 

Super Useful Expressions, Part 125 

(low-intermediate level) 

1) ขอบคุณพระเจ้า kɔ̀ɔp-kun prá-jâo 

Thank God! 

2) เจอกันที่(รถ) jəə gan tîi (rót) 

Meet you at (the car). / See you at (the car).  

3) (มัน)ซนมาก (man) son mâak 

He's very naughty/mischievous. 

4) ดื่มเป็นเพื่อนผมหน่อย dʉ̀ʉm bpen-pʉ̂an pǒm nɔ̀i Have a drink with me. / Join me for a drink.  

5) เอาใหม่ ao mài 

Again. / Do it again. / Say it again. / Try again.  

6) เบาหน่อย bao nɔ̀i 

Easy! / Not so loud! 

7) ต้องใช้เงินเยอะ dtɔ̂ng chái-ngən yə́ 

It costs a lot. / You have to spend a lot of money.  

8) ลองกินดู lɔɔng gin duu 

Have a taste. / Try it. / Give it a try. 

9) น่ารังเกียจ(จริงๆ) nâa-rang-gìat (jing-jing) 

That's (really) disgusting/nasty/odious!  

10) (เขา)อาจจะมาสาย (kǎo) àat-jà maa sǎai 

(He) might be late.  

11) กลับมาแล้ว glàp maa lɛ́ɛo 

You're back. / He's back.  

12) อย่าเพิ่งไปนะ/อย่าเพิ่งไปสิ yàa pə̂ng bpai ná / yàa pə̂ng bpai sì Don't go (yet)! / Don't leave!  

13) ใกล้แล้ว glâi lɛ́ɛo 

(We're) close. / Almost there. / (She's) almost here.  

14) ฉันไม่ได้ต้องการ(เงินมากมาย) chǎn mâi-dâi dtɔ̂ng-gaan (ngən mâak mǎai) 

I don't need/want (a lot of money). 

15) เขาไม่มีหัวใจ kǎo mâi-mii hǔa-jai 

He's heartless! / He has no heart!  

16) เขาจมนำ้ตาย kǎo jom-náam dtaai 

He drowned.  

17) ปลอดภัย(ดี)(แล้ว) bplɔ̀ɔt-pai (dii) (lɛ́ɛo) 

(She)'s safe now. / (She)'s okay now.  

(She)'s out of danger now. 

18) ผมรู้มันเป็นยังไง pǒm rúu man bpen yang-ngai

I know what it's like. / I know how it is.  

19) ผมสงสาร(เขา) pǒm sǒng-sǎan (kǎo) 

I feel sorry for (him).  

20) น่าสงสาร nâa-sǒng-sǎan 

Poor thing! / What a pity. 

Super Useful Expressions, Part 126 

(high-intermediate level) 

1) จะมีประโยชน์อะไรล่ะ jà mii bprà-yòot à-rai lâ 

What's the point (in that)? / What good would that do? 

2) ฉันเป็นแฟนพันธุ์แท้(ของ Black Pink) chǎn bpen fɛɛn-pan-tɛ́ɛ (kɔ̌ɔng Black Pink) 

I'm a huge fan (of Black Pink).  

3) พอจะเข้าใจไหม pɔɔ jà kâo-jai mǎi 

Can you understand now? (after I've explained it to you) 

4) อันที่จริง(แล้ว)... LITERARY/FORMAL an-tîi-jing (lɛ́ɛo) In fact... / As a matter of fact...  

5) ทำไมเราถึงควร(จ้างคุณ) tam-mai rao tʉ̌ng kuan (jâang kun) Why should we (hire you)?  

6) ฉันได้มีโอกาสรู้จัก... chǎn dâi mii oo-gàat rúu-jàk... 

I had a chance to get to know... / I had a chance to become familiar with...  

7) ต้องยืดหยุ่น dtɔ̂ng yʉ̂ʉt-yùn 

(You) have to be flexible.  

8) เป็นสาวประเภทสอง bpen sǎao bprà-pêet sɔ̌ɔng

She's a ladyboy. / She's a transgender (woman). 

9) จะเอา(...)ไปทำอะไร jà ao (...) bpai tam à-rai 

What do you need it for? / What are you gonna do with it? / What do you need...  for? 

10) (มัน)แปลตรงตัวว่า... (man) bplɛ̀ɛ dtrong-dtua wâa... 

(It) literally means... / Word for word (it) means...  

11) มันตรงกันข้ามกับ... (man) dtrong-gan-kâam gàp... 

It's the opposite of...  

12) รู้กันทั่วไปว่า... rúu-gan tûa-bpai wâa... 

It's common knowledge that... / It's commonly known that...  

13) ไม่รู้ว่า(ผม)จะทนได้นานแค่ไหน mâi-rúu wâa (pǒm) jà ton dâi naan kɛ̂ɛ-nǎi (I) don't know how long (I) can take/endure this.  

14) ไม่รู้ว่า(ผม)จะทนได้อีกนานแค่ไหน mâi-rúu wâa (pǒm) jà ton dâi ìik naan  kɛ̂ɛ-nǎi 

(I) don't know how longer (I) can take/endure this. 

15) ทานข้าวให้หมด taan-kâao hâi-mòt 

Eat all your food. / Finish your meal. 

16) เราจะไม่ทำให้(คุณ)ผิดหวัง rao jà mâi tam-hâi (kun) pìt-wǎng We won't disappoint you. / We won't let you down. 

17) อย่าลืมติดตาม(นะครับ) yàa lʉʉm dtìt-dtaam (ná-kráp) Don't forget to watch. / Don't forget to follow (along). (said of TV or other series) 

18) แล้วยังไงต่อ lɛ́ɛo yang-ngai dtɔ̀ɔ 

(And) then what? / What happened next? / And then?  

19) มันไม่ดีต่อร่างกาย man mâi-dii dtɔ̀ɔ râang-gaai 

It's bad for you. / It's bad for your health. 

20) ยินดีรับใช้ FORMAL yin-dii ráp-chái 

At your service. / I'm happy to help. 

Super Useful Expressions, Part 127 

(high-intermediate level) 

1) ฉันซื้อ(รถ)ให้ตัวเอง chǎn sʉ́ʉ (rót) hâi dtua-eeng 

I bought myself (a new car)! / I bought (a new car) for myself.  

2) ว่าไงล่ะ INFORMAL wâa ngai lâ 

Well...? / (So,) what do you say? / So, what do you think?  

3) บอกได้แค่นี้ bɔ̀ɔk dâi kɛ̂ɛ-níi 

That's all I can say (now).  

4) ผมเพิ่งมาทำงานที่นี่ pǒm pə̂ng maa tam-ngaan tîi-nîi (I've) just started working here.  

5) (นั่งทำงาน)ตลอดทั้งวัน (nâng tam-ngaan) dtà-lɔ̀ɔt táng-wan (sit working) all day  

6) ฟังอย่างตั้งใจ SEMI-LITERARY fang yàang-dtâng-jai Listen carefully. / Listen hard. / Listen attentively. 

7) (ป้องกันไว้)ย่อมดีที่สุด (bpɔ̂ng-gan wái) yɔ̂m dii-tîi-sùt (Preventing it) is best/is the best way.  

Note: ย่อม (yɔ̂m) here means something like 'naturally', 'inevitably', or 'of  course'.  

8) ไม่ต้องหรูหรา mâi-dtɔ̂ng rǔu-rǎa 

It doesn't need to be so fancy. / I don't need anything extravagant. 

9) ยกตัวอย่างเช่น yók-dtua-yàang-chên 

For example... / For instance...  

10) (ความสำเร็จ)สำหรับฉันคือ... (kwaam-sǎm-rèt)sǎm-ràp chǎn kʉʉ... (Success) to me means... / For me, (success) is... 

11) เตรียมตัวให้พร้อม(...) SEMI-LITERARY dtriam-dtua hâi-prɔ́ɔm(...) Get ready (for/to...) / Be prepared (for/to...)  

12) ไม่มีอะไรเทียบได้(กับ...) mâi-mii-à-rai tîap dâi (gàp...) Nothing compares (to...)  

13) (Vibe)ในที่นี้หมายถึง(ความรู้สึก) (Vibe) nai tîi-níi mǎai-tʉ̌ng (kwaam-rúu sʉ̀k) 

(Vibe) here means (feeling). ('here' means 'in this context') 

14) มีผลต่อสุขภาพ mii-pǒn-dtɔ̀ɔ sùk-kà-pâap 

(It) affects your health. / (It) has an influence on your health. 

15) กลับเป็นปกติ glàp bpen-bpòk-gà-dtì 

Back to normal.  

16) จะ(กำจัด)ด้วยวิธีไหนดี jà (gam-jàt) dûai wí-tii nǎi dii 

How can I (get rid of) it? / What can I do to (get rid of) it? (by what method)  

17) ทำไมถึงอยากได้(งานนี้) tam-mai-tʉ̌ng yàak dâi (ngaan-níi) Why do you want (this job)?  

18) ผมใฝ่ฝันอยากเป็นนักบิน SEMI-FORMAL/LITERARY pǒm fài-fǎn yàak  bpen nák-bin 

I have a dream of being a pilot. / I dreamed of being a pilot.  

19) ฉันรู้สึกอบอุ่นใจ chǎn rúu-sʉ̀k òp-ùn-jai 

It gives me a warm (comfortable) feeling. / It warms my heart.  

20) (มันเป็น)ทุกสิ่งทุกอย่าง (man bpen) túk-sìng-túk-yàang (It's) everything.

Super Useful Expressions, Part 128 

(high-beginner level) 

1) เมาแล้ว mao lɛ́ɛo 

I'm drunk. / You're drunk. 

2) ทำเองหรือ tam eeng rʉ̌ʉ 

Did you make (this/it) yourself?  

3) จะกลับกี่โมง jà glàp gìi-moong 

What time are (you) coming back? / What time are (you) going home? / When are (you) coming back?  

4) วันนี้วันอะไร wan-níi wan à-rai 

What day is it (today)?  

5) มากับใคร maa gàp krai 

Who did you come with?  

6) ไปดูมาแล้ว bpai duu maa lɛ́ɛo 

I saw it already. / I looked already.  

7) อันนี้อะไร an-níi à-rai 

What is this (one)?  

8) หนีไป(สิ) nǐi bpai (sì) 

Run (away)! / Run for it!  

9) ผมจะไปเดินเล่น pǒm jà bpai dəən-lên 

I'm going for a walk. / I'm gonna go out for a walk.  

10) ไปเดินเล่นมา bpai dəən-lên maa 

I went for a walk.  

11) เขาเห็นแก่ตัว kǎo hěn-gɛ̀ɛ-dtua

He's selfish.  

12) เขารวยมาก kǎo ruai mâak 

He's rich/loaded/got lots of money.  

13) หนาวไหม nǎao mǎi 

Are you cold?  

14) ร้อนไหม rɔ́ɔn mǎi 

Are you hot? 

15) ฉันกำลังจะมีลูก chǎn gam-lang-jà mii lûuk 

I'm gonna have a baby.  

Note: กำลังจะ (gam-lang-jà) means 'about to' or 'going to'.  

16) ไปดูหน่อยสิ INFORMAL bpai duu nɔ̀i sì 

Go have a look. / Go and have a look.  

17) เมื่อคืนนอนกี่ทุ่ม mʉ̂a-kʉʉn nɔɔn gìi-tûm 

What time did you go to bed last night?  

18) ฉันหลงรักเขา chǎn lǒng-rák kǎo 

I fell in love with him.  

19) (ก็)งั้นๆ (gɔ̂ɔ) ngán-ngán 

So-so.  

Note: ก็ (gɔ̂ɔ) has various meanings depending on the context, but here it's used as a filler, much like a pause.  

20) (มัน)เป็นของโปรดของผม (man) bpen kɔ̌ɔng-bpròot kɔ̌ɔng pǒm It's my favorite. / That's my favorite.  

Super Useful Expressions, Part 129 

(high-intermediate level)

1) อะไรก็เกิดขึ้นได้ à-rai gɔ̂ɔ gə̀ət-kʉ̂n dâi 

Anything can happen. / Anything could happen.  

2) ไม่มีเหตุผล mâi-mii hèet-pǒn 

There's no reason. / It's illogical. / He's got to reason (to...)  

3) ทำอะไรไม่ได้แล้ว tam à-rai mâi-dâi lɛ́ɛo 

Nothing more can be done. / There's nothing more we can do.  

4) นี่แหละคือ(ความสุข) nîi lɛ̀ kʉʉ (kwaam-sùk) 

THIS is (happiness). / This is what (happiness) is! (shows emphasis) 

5) รู้หมดทุกเรื่องเลย rúu mòt túk rʉ̂ang ləəi 

(You) know (about) everything!  

6) หนีไม่ทัน nǐi mâi-tan 

(She) couldn't escape/get away (in time).  

7) (มัน)ต้องมีสาเหตุ (man) dtɔ̂ng mii sǎa-hèet 

There must be a reason. / He's gotta have a reason.  

8) เปิดเผยไม่ได้ bpə̀ət-pə̌əi mâi-dâi 

(I) can't disclose that. / (I) can't reveal that. / (I) can't tell you that.  

9) เขาปฏิเสธ(ทุก)ข้อกล่าวหา kǎo bpà-dtì-sèet (túk) kɔ̂ɔ-glàao-hǎa He denied (all) charges/allegations.  

10) ผมต้องบอกก่อนว่า... pǒm dtɔ̂ng bɔ̀ɔk gɔ̀ɔn wâa... First of all, I have to tell you/say... / I should first 7 that...  

11) ผมคิดมาตลอด(ว่า)... pǒm kít maa dtà-lɔ̀ɔt (wâa...) I always thought (that...)  

12) วันหนึ่งข้างหน้า... wan-nʉ̀ng kâang-nâa... 

One/some day in the future... 

13) ยังสรุปไม่ได้(ว่า...) yang sà-rùp mâi-dâi (wâa...) 

We can't reach a conclusion yet (...) / I don't know yet. (it's inconclusive) 

14) ดีพอสมควร dii pɔɔ-sǒm-kuan 

Pretty good. / A fair amount. / Reasonably good.  

15) เกิดขึ้นตลอด 24 ชั่วโมง gə̀ət-kʉ̂n dtà-lɔ̀ɔt yîi-sìp-sìi chûa-moong (It) happens 24 hours a day. / (It)'s going on round the clock.  

16) เอาคืนมา ao kʉʉn maa 

Give it back! / Give that back to me! 

17) สาเหตุเพราะอะไร FORMAL sǎa-hèet prɔ́ à-rai 

What was the reason? / Why?  

18) ลองดูก็แล้วกัน lɔɔng duu gɔ̂ɔ-lɛ́ɛo-gan 

Give it a try. / Have a look.  

19) ฉันสัญญากับตัวเองว่าจะไม่ทำอีก chǎn sǎn-yaa gàp dtua-eeng wâa jà mâi tam ìik 

I promised myself that I would never do it/that again.  

20) จงเชื่อในตัวเอง jong chʉ̂a nai dtua-eeng 

Believe in yourself.  

Super Useful Expressions, Part 130 

(high-intermediate level) 

1) เลิกเซ้าซี้ lə̂ək sáo-síi 

Stop pestering (me)! / Stop nagging (me)! 

2) มีเรื่องให้ช่วยหน่อย mii rʉ̂ang hâi chûai nɔ̀i 

I need your help with something. / I have something I need you to help me with.

3) (ฉันก็)จำชื่อไม่ได้แล้วเหมือนกัน (chǎn gɔ̂ɔ) jam chʉ̂ʉ mâi-dâi lɛ́ɛo mʉ̌an-gan I can't remember the/her name either.  

4) ฟื้นขึ้นมาแล้ว fʉ́ʉn-kʉ̂n maa lɛ́ɛo 

He's conscious now. / He's awake now. OR He's been resurrected.  

5) (หน้า)บวมช้ (nâa) buam-chám 

(Her face) is swollen and bruised. 

6) เขาจิตใจดี kǎo jìt-jai dii 

He has a good heart. / He's a very nice person. 

7) ต้องได้รับโทษ dtɔ̂ng dâi-ráp tôot 

(He) must be punished/brought to justice.  

8) วันนี้พอแค่นี้ก่อน wan-níi pɔɔ kɛ̂ɛ-nîi gɔ̀ɔn 

That's enough for today. / That's all for today. / Let's call it a day.  

9) จำแทบไม่ได้ jam tɛ̂ɛp mâi-dâi 

(I) can barely recognize (him). / (I) nearly didn't recognize (you). 

10) ...โดย(ที่)ไม่มีใครเห็น ...dooi (tîi) mâi-mii-krai hěn 

...without anyone seeing (me). / ...unnoticed.  

11) ...ถูกเก็บ(ไว้)ใน(ตู้เซฟ) ...tùuk gèp (wái) nai (dtûu-séep) They're kept in (a safe). / We keep it in (a safe).  

12) (ฉัน)ไม่น่า(...)เลย (chǎn) mâi-nâa (...) ləəi 

(I) shouldn't have(...)  

13) เขาไม่ชอบหน้าผม kǎo mâi chɔ̂ɔp nâa pǒm 

He doesn't like me.  

14) งานต้องมาก่อน ngaan dtɔ̂ng maa gɔ̀ɔn 

Work has to come first. / Work has to take precedent. 

15) ช่วยได้ทัน chûai dâi tan

Saved in the nick of time. / Helped him in time.  

16) หนีไม่รอดหรอก nǐi mâi rɔ̂ɔt rɔ̀ɔk 

(He) can't escape! / (He) won't get away!  

17) ไขกุญแจเข้าไปในห้อง kǎi-gun-jɛɛ kâo-bpai nai hɔ̂ng (He) unlocked the door and went into the room. / (He) opened the door and  walked in the room.  

18) ผมพกติดตัวตลอด(เวลา) pǒm pók dtìt-dtua dtà-lɔ̀ɔt (wee-laa) I always carry it with me. / I take it wherever I go. / I have it on me at all times.  (a phone, umbrella, etc.) 

19) เขาเป็นคนเก่ง kǎo bpen kon-gèng 

He's really good. / He's great. / He's very good at what he does.  

20) (ไอ้)ปัญญาอ่อน VERY STRONG (âi) bpan-yaa-ɔ̀ɔn 

Retard! / Idiot! / Moron! 

Note: Be very careful with this word. It also has a neutral meaning of  being mentally handicapped or having Down's syndrome.  

Super Useful Expressions, Part 131 

(low-intermediate level) 

1) ยังไงก็ขอบคุณ / ยังไงก็ขอบใจ yang-ngai gɔ̂ɔ kɔ̀ɔp-kun / yang-ngai gɔ̂ɔ kɔ̀ɔp-jai 

Thank you anyway. / Thanks anyway. 

2) หนูพยายามทุกอย่าง(แล้ว) nǔu pá-yaa-yaam túk-yàang (lɛ́ɛo) I tried everything. / I've tried everything. 

3) ต้องทำให้ได้ dtɔ̂ng tam hâi-dâi 

(You) gotta do it! / (You) have to do it! / (You) must do it! 

4) (อาการ)ยังไม่ดีขึ้น (aa-gaan) yang mâi dii-kʉ̂n (He)'s not better yet. / (He) hasn't gotten any better yet.  

5) มันเจ็บ(จริงๆ) man jèp (jing-jing) 

It (really) hurts! / That (really) hurts!  

6) ไม่เอาอะไร mâi ao à-rai 

I don't need anything. / I don't want anything.  

7) เขานอนโรงพยาบาล kǎo nɔɔn roong-pá-yaa-baan He's in the hospital. 

8) พยายามฆ่าเขา pá-yaa-yaam kâa kǎo  

He tried to kill/murder her. 

9) มันหวานดี man wǎan dii 

It's nice and sweet! / It's really sweet.  

10) ทำไมแพงจัง(เลย) tam-mai pɛɛng jang (ləəi) Why is it/that so expensive?  

11) หาทั่วแล้ว hǎa tûa lɛ́ɛo 

(I) searched/looked everywhere!  

12) ใช้ได้ chái-dâi 

It's useable. / You can use it. / It works.  

13) อร่อยที่สุดในโลก à-rɔ̀i tîi-sùt nai lôok 

The best in the world! / The most delicious in the world!  

14) ก็เหมือนเดิม gɔ̂ɔ mʉ̌an-dəəm 

The same. / Same as before. / Same as usual. 

15) ฉันต้องไปทำงานต่อนะ chǎn dtɔ̂ng bpai tam-ngaan dtɔ̀ɔ ná I gotta get back to work. 

16) ให้ฉันช่วยนะ hâi chǎn chûai ná 

Let me help you. / I'll give you a hand.  

17) ดูสิใครมา duu sì krai maa 

Look who's here!  

18) หลีกไป STRONG lìik bpai 

Get out of the way! / Move (out)! / Move it!  

19) เป็นเรื่องส่วนตัว bpen rʉ̂ang-sùan-dtua 

It's personal. / It's a private matter. / That's private.  

20) ไม่รับสาย mâi ráp-sǎai 

(He) doesn't answer. / He doesn't pick up the phone. 

Super Useful Expressions, Part 132 

(high-intermediate level) 

1) มาครบแล้ว maa króp lɛ́ɛo 

Everyone's here! / We have everything now. (They've all arrived./All food items  we ordered have come, etc.) 

2) ถูกของคุณ tùuk kɔ̌ɔng kun 

You're right. / What you say is correct. 

3) คุณทำถูก kun tam tùuk 

You did the right thing. / You were right. / What you did was right. 

4) เราต้องทำทุกอย่างให้เสร็จภายใน (2 เดือน) rao dtɔ̂ng tam túk-yàang hâi sèt paai-nai (sɔ̌ɔng dʉan) 

We have to finish everything within (2 months).  

5) มีใครอีกไหม mii krai ìik mǎi 

Are there others? / Anyone else? 

6) (จำเลย)รับสารภาพ (jam-ləəi) ráp sǎa-rá-pâap 

(The defendant) confessed. / (The defendant) pleaded guilty.  

7) (คุณ)คิดอย่างนั้นหรือ(เปล่า) (kun) kít yàang-nán rʉ̌ʉ (bplàao) Is that how you think? / Do you think like that? / Do you think that too?  

8) ด้วยความยินดีครับ FORMAL dûai kwaam-yin-dii kráp 

It's my pleasure. / The pleasure was mine. / You're welcome. 

9) มีพร้อมทุกอย่าง mii prɔ́ɔm túk-yàang 

It has everything. / He has everything. (example: a condo is fully furnished with  other things as well). 

10) มีมาหลายปีแล้ว mii maa lǎai bpii lɛ́ɛo 

(He)'s had it for many years. / (They)'ve been around for years. 

11) ข้อหาอะไร kɔ̂ɔ-hǎa à-rai 

For what? / On what charge? / On what grounds?  

12) วางอาวุธ waang aa-wút 

Drop your weapon! / Put your weapon down! 

13) ยกมือขึ้น yók-mʉʉ kʉ̂n 

Hands up!  

14) ไม่ใช่เรื่องบังเอิญ mâi-châi rʉ̂ang-bang-əən 

It's not a coincidence. / It's no coincidence. 

15) ผมไม่มีที่ไป pǒm mâi-mii tîi bpai 

I have nowhere to go. / I have no place to go. / I have nowhere left to go.  

16) ขอโทษที่ทำให้ตกใจ kɔ̌ɔ-tôot tîi tam-hâi dtòk-jai 

Sorry to startle/scare you.  

17) ต้องเป็นเขา(แน่) dtɔ̂ng bpen kǎo (nɛ̂ɛ) 

It must be him! / It's gotta be him! / It must have been him! 

18) เพื่ออะไร(คะ) pʉ̂a à-rai (ká) 

For what? / For what purpose?  

19) ที่เดิมเวลาเดิม tîi dəəm wee-laa dəəm 

Same place, same time.  

20) ไอ้เวร VULGAR âi-ween 

You bastard! / You asshole! 

Super Useful Expressions, Part 133 

(high-intermediate level) 

1) ฉันคิดออกแล้ว chǎn kít ɔ̀ɔk lɛ́ɛo 

I figured it out! / I got it! / I have an idea! 

2) รู้จักเป็นอย่างดี rúu-jàk bpen yàang-dii 

I know her very well. / I'm very familiar with...  

3) ผมลืมตัวไป(หน่อย/นิด) pǒm lʉʉm-dtua bpai (nɔ̀i/nít) I got (a bit) carried away. / I lost self control.  

4) (เขา)เป็นใครมาจากไหนไม่มีใครรู้ (kǎo) bpen krai maa-jàak-nǎi mâi-mii-krai rúu 

No one knows who he is or where he's from.  

5) ทุกอย่างปกติดี túk-yàang bpòk-gà-dtì dii / túk-yàang bpàk-gà-dtì dii Everything's normal/fine.  

6) ยังทำงานไม่เสร็จ yang tam-ngaan mâi sèt 

(He)'s not finished (with his work/the job) yet. / He isn't done yet.  

7) จะออกไปทำธุระข้างนอก jà ɔ̀ɔk bpai tam tú-rá kâang-nɔ̂ɔk (I)'m going out to run some errands.  

8) มันไม่สมควร man mâi-sǒm-kuan

It's not proper. / That's not appropriate.  

9) ยินดีรับใช้ค่ะ FORMAL yin-dii ráp-chái kà 

At your service. / You're very welcome. / I'm happy to serve (you). 

10) เพื่อนทุกคนต่าง(แปลกใจที่)... pʉ̂an túk-kon dtàang (bplɛ̀ɛk-jai tîi)... All (my) friends (were surprised that)...  

11) ฉันบอกตรงๆเลยนะ... chǎn bɔ̀ɔk dtrong-dtrong ləəi ná... To be honest... / I'm gonna tell it to you straight... / I'm gonna be honest with  you...  

12) ข่าวดีก็คือ... kàao dii gɔ̂ɔ kʉʉ... 

The good news is...  

13) มีข่าวร้าย(...) mii kàao ráai (...) 

(I) have some bad news(...)  

14) มีวิธีอื่นไหม(คะ) mii wí-tii ʉ̀ʉn mǎi (ká) 

Do (you) have another way? / Do (you) have any other idea? (to solve this  problem, etc.) 

15) ผม(ก็)ไม่เข้าใจเหมือนกัน pǒm (gɔ̂ɔ) mâi-kâo-jai mʉ̌an-gan I don't understand either. 

16) ไม่มีประโยชน์ mâi mii bprà-yòot 

It's useless. / It's no use. / There's no point.  

17) มันสมควรตาย ANGRY man sǒm-kuan dtaai 

He deserved/deserves to die. 

18) เต็มไปด้วยเลือด dtem-bpai-dûai lʉ̂at 

covered in blood / blood all over  

19) ผมไม่เคยอายที่(มีแฟนอ้วน) pǒm mâi-kəəi aai tîi (mii fɛɛn ûan) I've never been embarrassed (to have a fat girlfriend). / I've never been ashamed  of (having a fat girlfriend). 

20) ฉันเองก็คิดเหมือนกัน chǎn eeng gɔ̂ɔ kít mʉ̌an-gan That's what I thought! / I thought the same thing myself! 

Super Useful Expressions, Part 134 

(high-intermediate level) 

1) พวกเขาไม่มีอะไรเหมือนกันเลย pûak-kǎo mâi-mii-à-rai mʉ̌an-gan ləəi They have nothing (at all) in common. 

2) ฉันทำเงินได้จำนวนมาก chǎn tam-ngən dâi jam-nuan mâak I made a lot of money!  

3) ต้องมีวิธีอื่น dtɔ̂ng mii wí-tii ʉ̀ʉn 

There must be another way.  

4) ต้องจับมันให้ได้ dtɔ̂ng jàp man hâi-dâi 

(We)'ve got to get him! / (I)'ve gotta arrest her!  

5) ไม่เคยเป็นแบบนี้มาก่อน mâi-kəəi bpen bɛ̀ɛp-níi maa-gɔ̀ɔn (He)'s never (acted) like this before. / (It) didn't used to be that way. / (It)'s never been like that before.  

6) อย่าเรียกฉันว่า(ที่รัก) yàa rîak chǎn wâa (tîi-rák) 

Don't call me (darling/baby/honey).  

7) ลองไปถามเขาดู lɔɔng bpai tǎam kǎo duu 

Try asking him. / Try and ask him. / Go ask him.  

8) ไม่เข้าใจอยู่ดี mâi-kâo-jai yùu-dii 

(I) still don't get it. / (I) still don't understand.  

9) ถ้าสังเกตให้ดี... tâa sǎng-gèet hâi-dii... 

If (you) look at (it) carefully... / If (you) examine (them) carefully... /

If (you) observe carefully...  

10) (มัน)ยากที่จะเดาได้(ว่า...) (man) yâak tîi-jà dao dâi (wâa...) It's difficult to guess (...)  

11) ถ้าฟังไม่ผิด... tâa fang mâi-pìt... 

If I didn't hear (you) wrong... / If I didn't misunderstand...  

12) ขอบคุณสำหรับกำลังใจนะคะ kɔ̀ɔp-kun sǎm-ràp gam-lang-jai ná-ká Thank you for your encouragement/support.  

13) ยินดีบริการครับ FORMAL yin-dii bɔɔ-rí-gaan kráp (We)'re happy to serve you. / (I)'m pleased to serve you.  

14) ลืมตาได้แล้ว lʉʉm-dtaa dâi lɛ́ɛo 

You can open your eyes (now). 

15) ขอบคุณท่านมากครับ FORMAL kɔ̀ɔp-kun tân mâak kráp Thank you very much. (as said to someone with a higher rank/status  such as a monk, etc.) 

16) ลงมือได้เลย long-mʉʉ dâi ləəi 

Do it. / Just do it. / Go ahead and do it.  

17) (มัน)ไม่สมบูรณ์ (man) mâi sǒm-buun  

(It)'s incomplete. / (It)'s imperfect.  

18) มันมากเกินพอแล้ว man mâak-gəən pɔɔ-lɛ́ɛo 

It's more than enough. / That's more than sufficient.  

19) ไม่รู้เรื่องนี้เลย mâi-rúu-rʉ̂ang níi ləəi 

(I) don't know anything about it. / (I) didn't know anything  about that at all. 

20) มาดูเองดีกว่า maa duu eeng dii-gwàa 

(It's better if you) come and have a look for yourself. 

Super Useful Expressions, Part 135 

(mid-advanced level) 

1) เขารับกรรมแล้ว kǎo ráp-gam lɛ́ɛo 

He paid the price. / He suffered the consequences. / He got what was coming to  him. / He got his Karma. / He met his fate. 

2) เป็นคนเจ้าคิดเจ้าแค้น bpen kon jâo-kít-jâo-kɛ́ɛn 

(She)'s very vindictive! / (She)'s revengeful. / (She) likes to hold grudges. 

3) (กินมาม่า)ตั้งแต่เล็กจนโต SEMI-LITERARY (gin maa-mâa) dtâng-dtɛ̀ɛ lèk  jon dtoo 

(I've eaten Mama) from the time I was a kid until now. / (I've eaten Mama) all my  life. / (I've been eating Mama) since I was a kid.  

4) พวกเขาหนีหัวซุกหัวซุน SEMI-LITERARY pûak-kǎo nǐi hǔa-súk-hǔa-sun They ran away in a rush. / They ran away like rats. / They ran like hell. 

5) เปลืองน้ำลาย(เปล่าๆ) bplʉang nám-laai (bplàao-bplàao) (You)'re (just) wasting your breath!  

6) ดึก(ตื่น)ป่านนี้แล้ว(ยังไม่นอนอีกหรือ) dʉ̀k (dtʉ̀ʉn) bpàan-níi lɛ́ɛo (yang mâi nɔɔn ìik rʉ̌ʉ) 

This late at night (and you still haven't gone to bed)? / It's late--- (you're still not in  bed)?  

7) ขอบคุณที่อุตส่าห์มาหา kɔ̀ɔp-kun tîi ùt-sàa maa hǎa 

Thank you for coming all the way to see me. / Thank you for going to the trouble  to meet me. 

8) เขาทำพิธีสะเดาะเคราะห์ kǎo tam pí-tii-sà-dɔ̀-krɔ́ 

He had a ceremony to get rid of his bad luck. 

9) มันคุ้มค่าเหนื่อย man kúm-kâa nʉ̀ai 

It's worth the effort. / The wages are worth it. 

10) ...แต่ไหนแต่ไรแล้ว LITERARY ...dtɛ̀ɛ-nǎi-dtɛ̀ɛ-rai lɛ́ɛo ...from time immemorial. / ... all along. / (It) always...  

11) ...ต้องเจอแบบนี้แหละ ...dtɔ̂ng jəə bɛ̀ɛp-níi lɛ̀ 

That's what you get when you... / This is what you have to go through for...  (shows consequence from doing something bad) 

12) ต้องยกประโยชน์ให้(จำเลย) dtɔ̂ng yók-bprà-yòot hâi (jam-ləəi) You have to give (the defendant) the benefit of the doubt. (He might be lying but  we have to accept what he says for now.) 

13) ไม่รู้ว่าจะเป็นตายร้ายดีอย่างไร IDIOMATIC mâi-rúu wâa jà bpen dtaai ráai  dii yàang-rai 

I don't know if he's dead or alive.  

14) (กำลัง)ตกที่นั่งลำบาก SEMI-LITERARY (gam-lang) dtòk tîi-nâng-lam bàak 

(He)'s in big trouble. / (He)'s in deep shit. / (He)'s in a very difficult situation. 

15) รักษาเนื้อรักษาตัวดีๆ/ด้วย LITERARY rák-sǎa-nʉ̌a rák-sǎa-dtua dii-dii/ dûai 

Take (good) care of yourself.  

16) (มัน)ยังไงกันเนี่ย STRONG/IDIOMATIC (man) yang-ngai gan nîa What the hell (is this)?! / What on earth (is this)?! / What the hell is going on?!  

17) ไม่เป็นอันกินอันนอน LITERARY/IDIOMATIC mai-bpen-an-gin-an-nɔɔn (I) can't eat at all or sleep a wink. (due to anxiety) 

18) ปากกับใจไม่ตรงกัน bpàak gàp jai mâi-dtrong-gan 

What you say is not really how you feel. / Your words don't match your feelings.  

19) ผม(มีส่วน)ผิดด้วย(เหมือนกัน) pǒm (mii sùan) pìt dûai (mʉ̌an-gan) I'm at fault too. / It's (partly) my fault too.  

20) ในเรื่องร้ายยังมีเรื่องดี IDIOM nai rʉ̂ang-ráai yang mii rʉ̂ang-dii Every cloud has a silver lining.

Super Useful Expressions, Part 136 

(low-advanced level) 

1) ฝากความคิดถึงด้วยนะคะ FORMAL fàak kwaam-kít-tʉ̌ng dûai ná-ká Send (her) my regards. / Please send (her) my regards. 

2) เข็ดแล้ว kèt lɛ́ɛo 

I've learned my lesson! (afraid to make the same mistake again) 

3) ไม่เชื่อก็เรื่องของมึง STRONG/ANGRY mâi-chʉ̂a gɔ̂ɔ rʉ̂ang kɔ̌ɔng mʉng If you don't believe me, up to you! / Well, that's your business if you don't believe  me!  

4) เขาเป็นผู้สมรู้ร่วมคิด kǎo bpen pûu-sǒm-rúu-rûam-kít 

He was an accomplice. / He was in on it (too). / He was an accessory. 

5) เวรกรรมมีจริง ween-gam mii-jing 

Karma is real. / What goes around comes around.  

6) เรื่องจริงเป็นไง INFORMAL rʉ̂ang-jing bpen-ngai 

What's the truth? / What really happened?  

7) อะไรจะเกิดมันต้องเกิด à-rai jà gə̀ət man dtɔ̂ng gə̀ət 

Whatever happens, happens. / Whatever will be will be.  

8) รอจังหวะดี(ๆ) rɔɔ jang-wà dii-(dii) 

Wait for the right moment. / Wait until the timing's right. / Wait for a good chance.  

9) ไม่น่าเป็นไปได้ mâi-nâa bpen-bpai-dâi 

That's not possible. / How could that be? / That's unlikely.  

10) หลง(รัก)หัวปักหัวป lǒng (rák) hǔa-bpàk hǔa-bpam 

(Fall) head over heels in love.  

11) ราคาเป็นกันเอง raa-kaa bpen-gan-eeng

The prices are reasonable. (not expensive) 

Note: เป็นกันเอง (bpen-gan-eeng) means friendly or amicable.  

12) (ฝาก)ดูแลเขาแทนฉัน(ด้วย) (fàak) duu-lɛ̀ɛ kǎo tɛɛn chǎn (dûai) Take care of her for me. / Look after him for me.  

13) มันคงเป็นไปได้ยาก man kong bpen-bpai-dâi yâak It will/would be very difficult.  

14) จู่ๆก็โผล่มา jùu-jùu gɔ̂ɔ plòo-maa 

(She) suddenly shows/showed up.  

Note: จู่ๆ (jùu-jùu) is technically pronounced with a long vowel, but in  reality it's often cut to a short vowel.  

15) จะให้บอก(เขา)ว่ายังไง(ดี) jà hâi bɔ̀ɔk kǎo wâa yang-ngai(dii) What do you want me to tell him/her?  

16) จะจำเอาไว้ jà jam ao-wái 

(I)'ll keep that in mind. / I'll remember (what you told me).  

17) ไม่มีเหตุผลอื่น mâi-mii hèet-pǒn ʉ̀ʉn 

There's no other reason. 

18) เงิน(ทอง)มันหายาก ngən(tɔɔng) man hǎa-yâak Money doesn't grow on trees. / It's hard to make money. 

19) ไม่ได้หวังอะไรมาก mâi-dâi wǎng à-rai mâak 

(I) don't expect a lot. / (I)'m not hoping for a lot.  

20) แค่นี้จริงๆ หรือ kɛ̂ɛ-níi jing-jing rʉ̌ʉ 

Is that really all it is? / Is that really all there is?  

Super Useful Expressions, Part 137 

(low-intermediate level)

1) ภาษา(ใต้)เรียกว่าอะไร paa-sǎa (dtâai) rîak-wâa à-rai 

What do you call that in (Southern) Dialect? / How do you say that in (Southern)  Dialect? 

2) พูดมาเลย INFORMAL pûut maa ləəi 

Say it. / Tell me what what you wanna say.  

3) หนูทำอย่างนั้นไม่ได้ nǔu tam yàang-nán mâi-dâi 

I can't do that!  

4) ฉันเข้าใจความรู้สึก(ของ)คุณ chǎn kâo-jai kwaam-rúu-sʉ̀k (kɔ̌ɔng) kun I understand how you feel. / I understand your feelings.  

5) เขาขี้เหนียว kǎo kîi-nǐao 

He's stingy. / He's cheap.  

6) กลั้นหายใจ glân hǎai-jai 

Hold your breath.  

7) ผมกลัวเข็ม pǒm glua kěm 

I'm afraid of needles. 

8) ไม่ยอมไปหาหมอ mâi-yɔɔm bpai-hǎa mɔ̌ɔ 

(He) won't go to the doctor. / (He) refuses to go see a doctor.  

9) (มัน)น่าอาย (man) nâa-aai 

It's embarrassing! / It's shameful! / It's humiliating!  

10) นี่เป็นครั้งแรกที่... nîi bpen kráng-rɛ̂ɛk tîi... 

This is the first time I've...  

11) คิดถึงเหมือนกัน kít-tʉ̌ng mʉ̌an-gan 

I miss you too. / I miss him too.  

12) จะให้ฉันทำอะไร jà hâi chǎn tam à-rai

What do you want me to do? / What am I supposed to do?  

13) น่าเกลียด nâa-glìat 

So ugly! / He's ugly! / That's disgusting!  

14) หนูยุ่งมากเลย nǔu yûng mâak ləəi 

I'm really busy. / I've been really busy. 

15) (งั้น)ฉันวางไว้ตรงนี้(นะ) (ngán) chǎn waang wái dtrong-níi (ná) (Then) I'll leave it right here. / (Okay then) I'll set it over here.  

16) คิดแล้วเศร้า kít lɛ́ɛo sâo 

When I think about it, it makes me sad.  

17) กำลังนิยมกัน gam-lang ní-yom gan 

It's popular/fashionable.  

18) (ความรัก)ไม่มีวันตาย (kwaam-rák) mâi-mii-wan dtaai (Love) never dies. / (Love) is immortal.  

19) ขอเข้าไปได้ไหมครับ kɔ̌ɔ kâo-bpai dâi-mǎi kráp 

May I come in?  

20) จะออกไปไหน jà ɔ̀ɔk bpai-nǎi 

Where are you off to? / Where are you going? (said when seeing someone  about to go out)  

Super Useful Expressions, Part 138 

(high-intermediate level) 

1) รับรอง(ได้เลยว่า)... ráp-rɔɔng (dâi ləəi wâa)... 

I guarantee you... / I assure you... 

2) เป็นลูกบุญธรรม bpen lûuk-bun-tam 

(He)'s adopted. / (He)'s an adopted child. 

3) ลุย(เลย) INFORMAL lui (ləəi) 

Go for it! / Go! / Do it!  

4) ยังไม่รู้มันคืออะไร yang mâi-rúu man kʉʉ à-rai 

(We) still not know what it is. / (We) don't know what it is yet.  

5) มันเป็นความลับสุดยอด man bpen kwaam-láp sùt-yɔ̂ɔt It's top secret.  

6) ห้ามให้ใครรู้เด็ดขาด hâam hâi krai rúu dèt-kàat 

(Definitely) don't let anyone know!!  

Note: เด็ดขาด (dèt-kàat) can be translated here in various ways. The core  meaning is absolute(ly), definite(ly) or decisive(ly).  

7) ตั้งใจทำเต็มที่(แล้ว) dtâng-jai tam dtem-tîi (lɛ́ɛo) 

(I) did my best. 

8) เขาทำผิดอะไร kǎo tam-pìt à-rai 

What did he do (wrong)? / What is he guilty of?  

9) ฉันใช้เวลาส่วนใหญ่กับ(ครอบครัว) chǎn chái-wee-laa sùan-yài gàp (krɔ̂ɔp-krua) 

I spend/spent most of my time with/on (my family).  

10) ถูกใช้อย่างกว้างขวาง tùuk chái yàang-gwâang-kwǎang (It)'s widely used. / (It)'s used extensively.  

11) ถ้าถามก็บอกว่า... tâa tǎam gɔ̂ɔ bɔ̀ɔk wâa... 

If (she) asks, then tell (her)... 

12) (...)ถ้าไม่อยากเดือดร้อน(...) (...) tâa mâi yàak dʉ̀at-rɔ́ɔn (...) (...)if you don't want trouble(...)  

13) ...และ/แล้วในที่สุดก็แต่งงานกัน ...lɛ́/lɛ́ɛo nai-tîi-sùt gɔ̂ɔ dtɛ̀ng-ngaan gan ...and (they) finally got married. / ...and at last (they) wed. 

14) ในสมัยโบราณ... FORMAL/LITERARY nai sà-mǎi-boo-raan... In ancient times... / In olden times... 

15) เขาถูกวางยาพิษ kǎo tùuk waang-yaa-pít 

He was poisoned.  

16) เอาไว้ก่อน ao wái gɔ̀ɔn 

Later. (I'll do it later) / Not yet. (Leave it for later) / Let's do it later.  

17) มันเป็นหนึ่งใน... ที่ใหญ่ที่สุดในโลก man bpen nʉ̀ng nai... tîi yài tîi-sùt nai-lôok 

It's one of the largest... in the world.  

18) เธอไม่มีทางสู้ təə mâi-mii-taang sûu 

She can't fight back. / She's defenseless. / She can't defend herself.  

19) ต้องมีอะไรสักอย่าง dtɔ̂ng mii à-rai sàk-yàang 

Something must be going on. / There's got to be something.  

20) บอกใบ้ให้หน่อย bɔ̀ɔk-bâi hâi nɔ̀i 

Give me a hint/clue. 

Super Useful Expressions, Part 139 

(high-intermediate level) 

1) เกิดเรื่องใหญ่(แล้ว) gə̀ət rʉ̂ang yài (lɛ́ɛo) 

We have big trouble! / We have a big problem!  

2) เกือบไม่ทัน gʉ̀ap mâi-tan 

Almost didn't make it in time!  

3) เร่งมือหน่อย rêng-mʉʉ nɔ̀i 

Speed it up! / Step on it! / Hurry up! (speed up the work you are doing)

4) เขาไม่ได้ทำอะไรหนู(สักหน่อย) kǎo mâi-dâi tam à-rai nǔu (sàk-nɔ̀i) She didn't do anything to you.  

5) (YWCA) เริ่มต้นครั้งแรกที่(ลอนดอน) (waai-em-see-ee) rə̂əm-dtôn kráng rɛ̂ɛk tîi (lɔɔn-dɔɔn) 

(YWCA) originally/first started in (London). 

6) คุณจะเอายังไง INFORMAL kun jà ao yang-ngai 

What will it be? / What are you gonna do? / So, what do you want?  

7) อย่าปล่อยให้หลุดไป yàa bplɔ̀i-hâi lùt bpai 

Don't let it slip (away). / Don't let her get away.  

8) ทำได้อย่างง่ายดาย tam dâi yàang-ngâai-daai 

(It) can be done easily. / (It) can be done with ease. / (It)'s easy to do.  

9) ตายเสียดีกว่า dtaai sǐa dii-gwàa 

It's better to (just) die. / Dying is better.  

10) ฉันโชคดีมากที่ได้มีโอกาส... chǎn chôok-dii mâak tîi dâi mii oo-gàat... I was very lucky to have the opportunity to...  

11) หลายคนไม่เคยรู้/ทราบว่า... lǎai kon mâi-kəəi rúu/sâap wâa... Many people don't know (that)...  

12) ในช่วงเริ่มต้น(ของ)... FORMAL/LITERARY nai chûang-rə̂əm-dtôn (kɔ̌ɔng)... 

In the early period (of)... / In the beginning (of)... 

13) นี่เป็นเหตุผลว่าทำไม... nîi bpen hèet-pǒn wâa tam-mai... This is why... / This is the reason why...  

14) ชั้นมัธยมศึกษาปีที่(2) FORMAL chán mát-tá-yom-sʉ̀k-sǎa bpii tîi (sɔ̌ɔng) The (second) year of secondary school.  

15) นึกภาพไม่ออก nʉ́k-pâap-mâi-ɔ̀ɔk

(I) can't imagine. / (I) can't picture it.  

16) ภาพยนตร์ยอดเยี่ยมแห่งปี FORMAL/LITERARY pâap-pá-yon yɔ̂ɔt-yîam  hɛ̀ng-bpii 

Movie of the Year.  

17) ขอให้มีความสุขกับ(การเดินทาง) FORMAL/LITERARY kɔ̌ɔ-hâi mii kwaam-sùk gàp (gaan-dəən-taang) 

Enjoy (your trip). / May you enjoy (your trip). / Happy (travels).  

18) เขียน(ขึ้น)ในปี(1957) kǐan (kʉ̂n) nai bpii (nʉ̀ng-pan-gâao-rɔ́ɔi-hâa-sìp-jèt) It was written in (1957).  

19) (เขา)พูดจริงทำจริง (kǎo) pûut-jing tam-jing 

(He) does what he says. / Talk the talk, walk the walk. / (He)'s a man of his  word. / (He)'s a man of action.  

20) แกเดาผิด gɛɛ dao-pìt 

You guessed wrong. / You were mistaken. 

Super Useful Expressions, Part 140 

(high-intermediate level) 

1) ผ่านมาพอดี pàan maa pɔɔ-dii 

(I) just happened to be passing by. 

2) จะแวะไปหานะ jà wɛ́ bpai-hǎa ná 

I'll drop by to see you.  

3) เกาะแน่นๆ gɔ̀ nɛ̂n-nɛ̂n 

Hold on tight! (riding on the back of a motorcycle, etc.) 

4) เขาไม่ยอมหรอก kǎo mâi yɔɔm rɔ̀ɔk 

She won't let you! / She won't allow it! 

5) ขายหมดเกลี้ยง kǎai mòt-glîang 

All sold out. / Completely sold out. / Sold them all!  

6) (หวาน)กำลังดี (wǎan) gam-lang-dii 

It's just the right (sweetness). / The (sweetness) is perfect. / It's got the exact  perfect (sweetness).  

7) ผมมีเรื่องปรึกษา(ครับ) pǒm mii rʉ̂ang bprʉ̀k-sǎa (kráp) I want to ask your advice (about something). / I need to consult with you about  something.  

8) ตั้งใจทำให้ดีที่สุด dtâng-jai tam-hâi dii-tîi-sùt 

Try your best. / Do the best you can do. / Focus on doing your best.  

9) ยังเป็นเพื่อนกันได้ใช่ไหม yang bpen pʉ̂an gan dâi châi-mǎi We can still be friends, can't we?  

10) (...)จะได้ไม่เจ็บ (...) jà dâi mâi-jèp 

(...)so you won't get hurt. / (...)so she won't be heartbroken.  

11) เปิดใจยอมรับ... bpə̀ət-jai yɔɔm-ráp... 

Open your mind/heart to...  

12) เขาหวังดี(กับ...) kǎo wǎng-dii (gàp...) 

He means well. / She has good intentions. / He's doing this for your own good.  

13) ไม่ต้องลงทุนอะไรเลย mâi-dtɔ̂ng long-tun à-rai ləəi 

You don't need to invest anything. / It's not necessary to spend anything. (starting a business, etc.) 

14) (มี)กำไรน้อย (mii) gam-rai nɔ́ɔi 

There's not much profit. / You can't make much money. (from a business, etc.) 

15) เงินก้อนโต ngən gɔ̂ɔn-dtoo 

big money / a huge amount of money / wads of cash  

16) มันโง่สิ้นดี STRONG man ngôo sîn-dii

That was so stupid! / He is such an idiot!  

17) ผมแค่ทำตามคำสั่ง pǒm kɛ̂ɛ tam-dtaam kam-sàng I was just following orders. / I was just doing what they told me to do.  

18) ปวดเมื่อยไปทั้งตัว(เลย) bpùat-mʉ̂ai bpai táng-dtua (ləəi) I'm aching all over! / I'm stiff all over!  

19) ขอเวลาคิดก่อน kɔ̌ɔ-wee-laa kít gɔ̀ɔn 

Give me some time to think. / I need some time to think about it.  

20) ฉันไม่เคยรู้สึกกับใครแบบนี้มาก่อน chǎn mâi-kəəi rúu-sʉ̀k gàp krai bɛ̀ɛp-níi maa-gɔ̀ɔn 

I've never felt like this about anyone before.  

Super Useful Expressions, Part 141 

(high-intermediate level) 

1) ขอโทษที่ทำให้ลำบาก kɔ̌ɔ̌-tôot tîi tam-hâi lam-bàak Sorry to cause you trouble. / I'm sorry to bother you. / I'm sorry to have troubled you. 

2) (คุณ)ฟังไม่ผิด (kun) fang mâi-pìt 

You heard (that) right.  

3) เอาไว้เล่าให้ฟัง ao-wái lâo-hâi-fang 

I'll tell you (about it) later.  

4) อดสงสารไม่ได้ òt sǒng-sǎan mâi-dâi 

I can't help but feel sorry for (her). / I can't stop feeling sorry for (him).  

5) ไอ้ควาย VULGAR âi kwaai 

Stupid son of a bitch! / Stupid bitch! / Idiot! 

6) จะไม่เกิดขึ้นอีก jà mâi gə̀ət-kʉ̂n ìik 

(It) won't happen again. 

7) ตอแหล STRONG dtɔɔ-lɛ̌ɛ 

Bullshit! / You liar! 

8) เลิกตอแหลเถอะ STRONG lə̂ək dtɔɔ-lɛ̌ɛ tə̀ 

Stop bullshitting! / Quit shitting me! / Stop lying to me!  

9) ทำเกินไปแล้ว tam gəən-bpai lɛ́ɛo 

You're going too far! / He's gone too far. / She's overdone it. 

10) ไม่มีเศษเงิน mâi-mii sèet-ngən 

(I) don't have any change/coins. / (I) don't have any loose change.  

11) มีแต่แบงค์(1000) mii-dtɛ̀ɛ bɛ́ng (pan) 

(I) only have (1000)s. / (I) only have (1000)-baht bills.  

12) ไม่มีเงินทอน mâi-mii ngən-tɔɔn 

(He) doesn't have any change.  

13) พูดได้แค่นี้แหละ pûut dâi kɛ̂ɛ-níi lɛ̀ 

That's all I can say. / That's all I'm sayin'.  

14) (มัน)ไม่มีอยู่จริง (man) mâi-mii-yùu jing 

(It) doesn't (really) exist. / (It)'s not real. 

15) ต้องทิ้งเอาไว้ dtɔ̂ng tíng ao-wái 

You have to leave it. / You have to set it aside.  

16) ผมจะรีบไปรีบกลับ pǒm jà rîip bpai rîip glàp I'll hurry back. (go there and come back quickly) 

17) พูดถึงก็มาพอดี pûut-tʉ̌ng gɔ̂ɔ maa pɔɔ-dii 

Speak of the Devil! / Look who's here! 

18) เดาถูก dao tùuk

I guessed it (right). / Good guess. 

19) หนีไม่พ้น nǐi mâi-pón 

You can't escape (it). / You can't get away (from her). / There's no escaping (it).  

20) ต้องมั่นใจในตัวเอง dtɔ̂ng mân-jai nai dtua-eeng 

You have to be self-confident. / You have to have self-confidence. / You have to be confident in yourself. 

Super Useful Expressions, Part 142 

(low-intermediate level) 

1) ตื่นสาย dtʉ̀ʉn sǎai 

(I) overslept. / (I) slept in. 

2) ผมรู้สึกผิด pǒm rúu-sʉ̀k pìt 

I feel guilty.  

3) คงอีกไม่นาน kong ìik-mâi-naan 

It shouldn't be that long (from now). / It won't be long.  

4) อดทนอีกหน่อย/อดทนอีกนิด òt-ton ìik nɔ̀i/ òt-ton ìik nít Hang in there a bit longer. / Be patient for a little bit longer. 

5) ผมไม่เสียใจเลย pǒm mâi sǐa-jai ləəi 

I don't regret it at all. / I have no regrets.  

6) เป็นแค่ลูกจ้าง bpen kɛ̂ɛ lûuk-jâang 

I'm just an employee. / I just work here.  

7) (จะ)ใส่อะไรดี (jà) sài à-rai dii 

What should I wear?  

8) ปวดเยี่ยว INFORMAL/ROUGH bpùat yîao

I gotta take a piss! / I gotta piss. 

9) ฉันต้องรีบไป chǎn dtɔ̂ng rîip bpai 

I gotta go. / I gotta rush off. / I gotta run. / I have to get going.  

10) เสียเวลาจริงๆ sǐa-wee-laa jing-jing 

What a waste of time! / That's really a waste of time!  

11) รู้จักเขาได้ยังไง rúu-jàk kǎo dâi yang-ngai  

How do you know him? 

12) เขาเป็นแค่เพื่อน kǎo bpen kɛ̂ɛ pʉ̂an 

He's just a friend.  

13) ยังไม่ได้เป็นแฟน yang mâi-dâi bpen fɛɛn 

He's not my boyfriend (yet).  

14) จะไปนานไหม jà bpai naan mǎi 

Will (you) be gone long? / Will (you) be away for a long time? 

15) ผมเห็นข่าวว่า... pǒm hěn kàao wâa... 

I saw the news (about)...  

16) ฉันจะทำให้(...) chǎn jà tam hâi (...) 

I'll make it for you. / I'll do it for you. / I'll... (for you)  

17) มีคนมาหาค่ะ mii kon maa-hǎa kà 

Someone's here to see you. 

18) ก็เรื่อยๆ gɔ̂ɔ rʉ̂ai-rʉ̂ai 

So-so. / Not bad. (Used for answering 'How are you?' or 'How is your  business doing?' etc.) 

19) อย่านอนดึกนะ yàa nɔɔn dʉ̀k ná 

Don't stay up too late. / Don't sleep too late.  

20) ไม่ต้องทำอะไร mâi-dtɔ̂ng tam à-rai

(You) don't need to do anything.  

Super Useful Expressions, Part 143 

(low-intermediate level) 

1) รีบไปดีกว่า rîip bpai dii-gwàa 

We better get going. / We'd better go. / We'd better hurry. 

2) ขอโทษที่รบกวน kɔ̌ɔ-tôot tîi róp-guan 

(I)'m sorry for bothering/disturbing you.  

3) ช่วยได้เยอะเลย chûai dâi yə́ ləəi 

(You)'ve been quite helpful. / (You) really helped me a lot. / That was really helpful.  

4) ฉันถือเอง(ได้) chǎn tʉ̌ʉ eeng (dâi) 

I'll carry it (myself).  

5) วันไหนดี(คะ) wan nǎi dii (ká) 

What day is good (for you)?  

6) ใกล้สุกแล้ว glâi sùk lɛ́ɛo 

It's almost done. (cooked) / They're nearly ripe.  

7) ใกล้เสร็จแล้ว glâi sèt lɛ́ɛo 

(I)'m almost finished. / (I)'m nearly done.  

8) ได้ยินที่ฉันพูดไหม dâi-yin tîi chǎn pûut mǎi 

Did you hear what I said?  

9) เมื่อคืนกลับดึก mʉ̂a-kʉʉn glàp dʉ̀k 

(I) came home late last night. / (I) got home late last night.  

10) ทำอย่างกับ(ว่า)(ฉันไม่ใช่คน) tam-yàang-gàp (wâa) (chǎn mâi-châi kon)

You act like (I'm not a person)!  

Note: อย่างกับ/ยังกับ/ยังกะ (yàang-gàp/yang-gàp/yang-gà) all mean 'like' or 'as if'.  They are listed in level of formality, with ยังกะ (yang-gà) being the most casual. 

11) กลับแล้วหรือยัง glàp lɛ́ɛo rʉ̌ʉ-yang 

Is (he) back yet? / Did (he) come back yet? / Is (he) home yet?  

12) ได้ยินมาจากไหน dâi-yin maa-jàak nǎi 

Where did you hear that (from)?  

13) ก็เรื่อยๆ gɔ̂ɔ-rʉ̂ai-rʉ̂ai 

So-so. / I'm okay. / Not too shabby.  

14) รับไว้ไม่ได้ ráp wái mâi-dâi 

I can't accept it. / I can't take this. / I can't. (accept a gift, or something  given to you, etc.) 

15) (ยัง)มีอีกเยอะ (yang) mii ìik yə́ 

There's (still) a lot more. / There are (still) a lot more. / There are many more.  

16) ปลูกกันเยอะ(มาก) bplùuk gan yə́ (mâak) 

They grow a lot of them.  

17) รอ(ฉัน)อยู่ตรงนี้ rɔɔ (chǎn) yùu-dtrong-níi 

Wait for me here. / Wait here.  

18) ไปเอามา INFORMAL bpai-ao-maa 

Go get it. / Go get them.  

19) เป็นคนขี้แพ้ bpen kon kîi-pɛ́ɛ 

(He)'s a loser. / (He) gives up easily.  

20) ไม่เปลี่ยนเลย mâi bplìan ləəi 

You haven't changed at all. / It hasn't changed a bit.

Super Useful Expressions, Part 144 

(low-advanced level) 

1) ฉันไม่มีปัญญา(ซื้อ iphone) chǎn mâi-mii-bpan-yaa (sʉ́ʉ ai-foon) I can't afford (an iphone). / (Buying an iphone) is beyond my means.  

2) ไม่เคยเลยแม้แต่ครั้งเดียว mâi-kəəi ləəi mɛ́ɛ-dtɛ̀ɛ kráng-diao Never. Not even once. / Never. Not even one time.  

3) (มัน)(จะ)คุ้มกับการรอ(คอย) (man) (jà) kúm-gàp gaan-rɔɔ (kɔɔi) It's worth waiting for.  

4) อาจไม่เหมาะกับผม àat mâi-mɔ̀-gàp pǒm 

Maybe it's not for me. / It might not be suitable for me.  

5) ไม่มีทางเป็นไปได้ mâi-mii-taang bpen-bpai-dâi 

There's no way to make it happen. / It's impossible. / It's never gonna happen.  

6) ขอโทษสำหรับทุกสิ่งทุกอย่าง SEMI-LITERARY/FORMAL kɔ̌ɔ-tôot sǎm-ràp túk-sìng-túk-yàang 

I'm sorry for everything. / I apologize for everything (that has happened).  

7) เตือนแล้วเตือนอีก dtʉan lɛ́ɛo dtʉan ìik 

He warned me repeatedly. / She warned him over and over again.  

8) ทำตัวเป็นผู้ใหญ่หน่อย(สิ) tam-dtua bpen pûu-yài nɔ̀i (sì) Act like an adult. / Act your age.  

9) กินให้เต็มที่ gin hâi dtem-tîi 

Eat your fill! / Chow down!  

10) แต่ที่แน่ๆ... dtɛ̀ɛ tîi nɛ̂ɛ-nɛ̂ɛ... 

But one thing's for sure...  

11) รู้ดีแก่ใจว่า... SEMI-LITERARY rúu-dii gɛ̀ɛ-jai wâa...

(You) know very well that... / (You) know deep in your heart...  

12) ไม่ว่าใครจะว่ายังไง(ก็ตาม)... mâi-wâa krai jà wâa yang-ngai (gɔ̂ɔ dtaam)... 

No matter what anyone says... / Regardless of what anyone says...  

13) ผมไม่ได้เกิดมาเพื่อ... pǒm mâi-dâi gə̀ət maa pʉ̂a... 

I wasn't born to... / I wasn't destined to...  

14) ไม่คิดดอกเบี้ย mâi kít dɔ̀ɔk-bîa 

Don't charge interest. / Interest free. 

15) อยากอยู่อย่างสงบ yàak yùu yàang-sà-ngòp 

(I) want to live in peace/peacefully. 

16) อาจจะยากในช่วงแรก/ช่วงเริ่มต้น àat-jà yâak nai chûang-rɛ̂ɛk/chûang rə̂əm-dtôn 

It might be hard at first. / In the beginning, it might be difficult.  

17) พูดแล้วหิวเลย(เนี่ย) pûut lɛ́ɛo hǐu ləəi (nîa) 

Just mention it and I'm hungry already! / That makes me hungry! (you talking  about such and such food) 

18) ยังไม่รู้ว่าจะเกิดอะไรขึ้น yang mâi-rúu wâa jà gə̀ət à-rai kʉ̂n (We) don't know (yet) what's going to happen. / There is no telling what will  happen.  

19) เป็น(แค่)ผู้หญิงธรรมดาคนหนึ่ง bpen (kɛ̂ɛ) pûu-yǐng tam-má-daa kon nʉ̀ng I'm (just) an ordinary woman. 

20) ใครกันแน่ที่(ใจร้าย) INFORMAL/STRONG krai gan nɛ̂ɛ tîi (jai-ráai) Who's the one (who's mean)?! / Who's the (mean one)?! (said when someone  accuses you of being mean, but in fact they are meaner than you)  

Super Useful Expressions, Part 145 

(low-intermediate level)

1) ไชโย chai-yoo 

Hooray! / Banzai! / Bravo! / Cheers! / Yah! 

2) มาดูนี่(สิ) maa duu nîi (sì) 

Take a look at this. / Look at this. / Come and have a look at this.  

3) นอนกรนหรือเปล่า nɔɔn gron rʉ̌ʉ-bplàao 

Do you snore?  

4) เขามาจีบฉัน kǎo maa jìip chǎn 

He came to flirt with me. / He tried to pick me up. / He tried to hit on me.  

5) มาทำอะไรแถวนี้ maa tam à-rai tɛ̌ɛo-níi 

What are you doing around here/in this part of town?  

6) พ่อแม่สอนดี pɔ̂ɔ-mɛ̂ɛ sɔ̌ɔn dii 

(His) parents taught him well. 

7) ลองชิมดูสิ lɔɔng chim duu sì 

Have a taste. / Taste it.  

8) กุญแจหาย(ไปแล้ว) gun-jɛɛ hǎai (bpai lɛ́ɛo) 

I lost my key(s)!  

9) ไม่มีข่าว mâi-mii kàao 

There wasn't any news (about it).  

10) ยอมแพ้แล้ว yɔɔm pɛ́ɛ lɛ́ɛo 

They've surrendered. / He gave up.  

11) เรายังเป็นเพื่อนกัน(อยู่) rao yang bpen pʉ̂an gan (yùu) We're still friends.  

12) เป็นไข้ bpen-kâi 

I have a fever. / She has the flu. / He's feverish. 

13) มันดีหรือไม่ดี man dii rʉ̌ʉ mâi-dii 

Is that good or bad? / Is that a good thing or a bad thing?  

14) แย่ลงอีกแล้ว yɛ̂ɛ-long ìik-lɛ́ɛo 

(It) got worse again. 

15) ไม่แพงมากเกินไป mâi-pɛɛng mâak-gəən-bpai (It) wasn't so expensive. / (It)'s not that expensive.  

16) คืนนี้เจอกัน(นะ) kʉʉn-níi jəə gan (ná) 

See you tonight.  

17) เขาตกลงแล้ว kǎo dtòk-long lɛ́ɛo 

She agreed. / She said okay.  

18) มีอะไร INFORMAL mii à-rai 

What? / What is it? / Yes?  

19) เร็วๆ(หน่อย) reo-reo (nɔ̀i) 

Hurry! / Hurry up! / Get a move on!  

20) มันเป็นแค่ความคิด man bpen kɛ̂ɛ kwaam-kít It's just an idea. / It was only a thought. 

Super Useful Expressions, Part 146 (low-intermediate level) 

1) ฉันไม่ได้เมา chǎn mâi-dâi mao 

I'm not drunk! 

2) พรุ่งนี้ต้องตื่นแต่เช้า prûng-níi dtɔ̂ng dtʉ̀ʉn dtɛ̀ɛ-cháao (I) have to get up very early tomorrow (morning).  

3) ใครก็ทำได้ krai gɔ̂ɔ tam-dâi

Anyone can do it! / Anyone can make it.  

4) ขอเวลาแค่(5 นาที) kɔ̌ɔ wee-laa kɛ̂ɛ (hâa naa-tii) 

Just give me (5 minutes). / I just need (5 minutes).  

5) ฉันรู้จักเขาดี chǎn rúu-jàk kǎo dii 

I know him well.  

6) ปวดขี้ INFORMAL bpùat kîi 

I've gotta take a dump. / I have to shit.  

7) ไปไม่นาน(หรอก) bpai mâi naan (rɔ̀ɔk) 

I won't be long (at all). / He won't be gone long.  

8) อิ่มแล้วหรือ ìm lɛ́ɛo rʉ̌ʉ 

Are you full (already)?  

9) อย่าเข้ามาใกล้ฉัน yàa kâo-maa glâi chǎn 

Don't come near me!  

10) อย่าเข้าไปใกล้(...) yàa kâo-bpai glâi (...) 

Don't go near (it)! / Stay away from (him)! / Don't get too close!  

11) โชคดีมากที่... / โชคดีจริงๆที่... chôok-dii mâak tîi... / chôok-dii jing-jing tîi... (You)'re really lucky to/that...  

12) ใส่พอดี(เลย) sài pɔɔ-dii (ləəi) 

It fits perfectly. / It fits just right.  

13) ไปก่อนเลย bpai gɔ̀ɔn (ləəi) 

You go (on) ahead. / Go ahead. / You can go first.  

14) คุณจะทำอะไร kun jà tam à-rai 

What are you gonna do? 

15) ไม่รู้สึกอะไร(เลย) mâi rúu-sʉ̀k à-rai (ləəi) 

(I) don't feel anything. / (I) don't feel a thing. 

16) ไอทั้งคืน ai táng-kʉʉn  

I was coughing all night. / He coughed all night long.  

17) แทบไม่ได้นอนเลย tɛ̂ɛp mâi-dâi nɔɔn ləəi 

(I) barely slept at all.  

18) ฉันไม่มีอะไรท chǎn mâi-mii à-rai tam 

I have nothing to do. / I don't have anything to do.  

19) อย่าโลภ(มาก) yàa lôop (mâak) 

Don't be greedy! 

20) มันเปลืองเงิน man bplʉang ngən 

It's a waste of money. / That's a waste of money. 

Super Useful Expressions, Part 147 

(low-advanced level) 

1) ยิงปืนนัดเดียวได้นกสองตัว IDIOM ying bpʉʉn nát diao dâi nók sɔ̌ɔng dtua Kill two birds with one stone. 

2) เขาโดนใส่ความ kǎo doon sài-kwaam 

He was framed. / He was set up. / He was falsely blamed.  

3) ยังเด็กเกินกว่าที่จะเข้าใจ yang dèk gəən-gwàa tîi-jà kâo-jai (She) was (still) too young to understand.  

4) เรา(มัน)คนละระดับกัน rao (man) kon-lá rá-dàp gan 

We're not in the same class. / We're of a different class/level. (of social standing or wealth) 

5) (มี)นามว่า(การะเกด) LITERARY (mii) naam wâa (gaa-rá-gèet) She was named "'Karaket'. / Her name is 'Karaket'. 

6) กำลังตกอยู่ในอันตราย SEMI-LITERARY gam-lang dtòk-yùu-nai-an-dtà-raai (He)'s in danger. / (He)'s fallen into trouble.  

7) ฉันทำได้ทุกอย่างเพื่อ(ลูก) chǎn tam dâi túk-yàang pʉ̂a (lûuk) I'd do anything for (my kids).  

8) ไม่หลาบจ SEMI-LITERARY mâi làap-jam 

(He) hasn't learned his lesson. / (He)'s incorrigible. 

9) เท่าที่ฟังมา... tâo-tîi fang maa... 

From what I hear... / From what I've heard... / As far as I've heard...  

10) เขาได้รับผลกระทบจาก... FORMAL kǎo dâi-ráp pǒn-grà-tóp jàak... He was affected by... / He was hit by...  

11) มันทำให้ฉันได้รู้ว่า... man tam-hâi chǎn dâi-rúu wâa... 

It made me realize...  

12) เป็นบุญของผมที่... LITERARY bpen-bun kɔ̌ɔng pǒm tîi... I'm fortunate to... / I'm lucky that... / I'm blessed to...  

13) ฉันรอ(คอย)วันนี้มานาน chǎn rɔɔ (kɔɔi) wan-níi maa naan I've been waiting for this day for a long time. / I've waited for this day for ages.  

14) อาจพิการตลอดชีวิต àat pí-gaan dtà-lɔ̀ɔt-chii-wít 

(He) might be crippled/handicapped for the rest of (his) life. 

15) (มัน)เป็นเรื่องที่น่าเสียดายมาก (man) bpen rʉ̂ang tîi nâa-sǐa-daai mâak It's really a pity. / It's really unfortunate.  

16) รถเสียหลัก rót sǐa-làk 

The car lost control.  

17) ลืมไม่ลงเลย SEMI-LITERARY lʉʉm mâi-long ləəi 

I can't forget. / It's unforgettable.

18) มอบตัวเถอะ mɔ̂ɔp-dtua tə̀ 

Turn yourself in! / Surrender!  

19) (หล่อ)เหมือน(พ่อ)ไม่มีผิด (lɔ̀ɔ) mʉ̌an (pɔ̂ɔ) mâi-mii-pìt (Handsome) just like (his father)!  

20) ไปดีมาดี bpai dii maa dii 

Bon voyage. / Have a safe trip. 

Super Useful Expressions, Part 148 (low-intermediate level) 

1) ให้ผมไปส่งนะ hâi pǒm bpai sòng ná 

Let me give you a ride. / I'll walk you there. 

2) ยังจำได้ yang jam dâi 

(I) still remember it. / (I) still remember him. / (I) still remember.  

3) มันไม่สำคัญ man mâi sǎm-kan 

It's not important.  

4) มาที่นี่ได้ยังไง maa tîi-nîi dâi yang-ngai 

What are you doing here?! / How did you get here?!  

5) ผมมีข้อเสนอให้คุณ pǒm mii kɔ̂ɔ-sà-nə̌ə hâi kun I have a proposition/proposal/suggestion for you.  

6) อย่าเพิ่งเปิด yàa pə̂ng bpə̀ət 

Don't open it! / Don't open it yet!  

7) โทษที INFORMAL tôot tii 

Sorry. / Excuse me.  

8) ยังไม่กล้าพอ yang mâi glâa pɔɔ 

I don't have the courage (yet). / I don't have the guts (to...) 

9) ล้อเล่นใช่มั้ย lɔ́ɔ-lên châi-mái 

You're joking, right? / Are you kidding me?  

10) ครั้งที่แล้ว... kráng-tîi-lɛ́ɛo... 

(The) last time...  

11) ถ้าทำอย่างนั้น... tâa tam yàang-nán... If you did that... / If he does that... / If you do...  

12) จะย้ายไปไหน jà yáai bpai nǎi 

Where are you moving to?  

13) ไม่ค่อยชอบคนเยอะ mâi-kɔ̂i chɔ̂ɔp kon yə́ (I) don't like crowds. / (I) don't like crowded places.  

14) อาจจะนานไป(หน่อย) àat-jà naan bpai (nɔ̀i) It might be (a bit) too long. (to wait, to do something, etc.) 

15) ทำไมไม่ตอบ tam-mai mâi dtɔ̀ɔp 

Why don't you answer (me)? / Why aren't you answering?  

16) ไปนอนพักผ่อน bpai nɔɔn pák-pɔ̀n 

Get some rest. / Lie down and get some rest.  

17) คุณน่าจะดีใจ kun nâa-jà dii-jai 

You outta be happy. / You should be glad. 

18) คล้ายกันมาก kláai-gan mâak 

(They)'re very similar! / So similar!  

19) ดีขึ้นหรือยัง dii-kʉ̂n rʉ̌ʉ-yang 

Are you better yet? / Are you feeling better?  

20) ห้ามบอกใคร hâam bɔ̀ɔk krai 

Don't tell anyone! / You're not allowed to tell anybody.

Super Useful Expressions, Part 149 

(low-intermediate level) 

1) ใครโทรมา krai too-maa 

Who's calling (me)? / Who called?  

2) บอกไม่ถูก bɔ̀ɔk-mâi-tùuk 

I can't explain. / It's difficult to say.  

3) เราเพิ่งรู้จักกัน rao pə̂ng rúu-jàk-gan 

We just met. / We've only just met.  

4) ไม่เอาอีกแล้ว mâi-ao ìik-lɛ́ɛo 

I don't want (that) any more! / Never again!  

5) เงยหน้าขึ้น ngəəi-nâa kʉ̂n 

Look up. / Raise up your head. / Lift your head (up).  

6) แค่นี้เองเหรอ kɛ̂ɛ níi eeng rə̌ə 

That's it? / Is that all? / Is that all there is?  

7) อยากจะรู้เหมือนกัน yàak já rúu mʉ̌an-gan 

(I) wanna know too. / (I) want to know that as well. 

8) เขาเป็นคนขี้อวด kǎo bpen kon kîi-ùat 

He's a braggart. / He likes to brag. / She likes to show off. 

9) ไม่เคยสำเร็จ mâi-kəəi sǎm-rèt 

He was never successful. / She never pulled it off. / It's never been done  (successfully).  

10) ไม่ว่าคุณจะเป็นใคร mâi-wâa kun jà bpen krai 

No matter who you are...  

11) เราพบกันครั้งแรกที่... rao póp gan kráng-rɛ̂ɛk tîi... We first met at. / We met each other for the first time at... 

12) บอกผมได้ไหม(ว่า...) bɔ̀ɔk pǒm dâi-mǎi (wâa...) Can you tell me (...)?  

13) รอนานไหมคะ rɔɔ naan mǎi ká 

Have you been waiting long? / Did you wait a long time?  

14) อย่าลืมที่(ฉัน)บอกนะ yàa lʉʉm tîi (chǎn) bɔ̀ɔk ná Don't forget what I told you. 

15) ไม่เคยรู้จักเขา(มาก่อน) mâi-kəəi rúu-jàk kǎo (maa-gɔ̀ɔn) (I)'ve never met him before.  

16) ผมรู้ความจริงแล้ว pǒm rúu kwaam-jing lɛ́ɛo I know the truth. / Now I know the truth.  

17) ไม่กินเผ็ด mâi gin pèt 

(I) don't eat spicy food. / I can't eat spicy food.  

18) ชอบกินอาหารรสจัด chɔ̂ɔp gin aa-hǎan rót-jàt (I) like spicy food. / (I) like heavily seasoned food.  

19) มันจืดไปหน่อย man jʉ̀ʉt bpai nɔ̀i 

It's a bit bland. / It's lacking flavor.  

20) รักเสมอมา rák sà-mə̌ə maa 

I've always loved you. 

Super Useful Expressions, Part 150 (high-intermediate level) 

1) ทำไมต้องโกรธขนาดนั้น tam-mai dtɔ̂ng gròot kà-nàat nán Why do you have to get so angry? 

2) เป็นโอกาสทอง bpen oo-gàat tɔɔng

It's the perfect chance. / It's a golden opportunity. 

3) ต้องมีวิธีที่ดีกว่า dtɔ̂ng mii wí-tii dii gwàa 

There must be a better way.  

4) กลับเองได้ glàp eeng dâi 

I can go back by myself. / I can go back alone.  

5) มีคนมาถามหาคุณ mii kon maa tǎam-hǎa kun 

There was (a guy) here asking about you. / A (guy) came here asking about you.  

6) ได้ทำในสิ่งที่อยากท dâi tam nai sìng tîi yàak tam 

(I) got to do what I wanted (to do). / I was able to do things which I had wanted to do.  

7) (พอจะ)รู้จักคนชื่อ... ไหม(ครับ) (pɔɔ jà) rúu-jàk kon chʉ̂ʉ... mǎi (kráp) Do you know someone named... ?  

8) ผมตั้งใจจะพูดว่า... pǒm dtâng-jai jà pûut wâa... 

I meant to say... / I intended to say...  

9) หน้าด้าน STRONG nâa-dâan 

How dare you! / You have no shame! / You're shameless!  

10) กินยาก็ไม่หาย gin yaa gɔ̂ɔ mâi hǎai 

(I) took (the) medicine but it/I didn't get (any) better.  

11) ไม่แข่งกับใคร mâi kèng gàp krai 

(I)'m not competing with anyone.  

12) ใกล้แค่นี้เอง glâi kɛ̂ɛ-níi eeng 

It's right here. / It's very close. / It's just around the corner.  

13) ได้พักผ่อนเต็มที่ dâi pák-pɔ̀n dtem-tîi 

(I) got a really good rest. / (I) was able to get a really good break. 14) แม้แต่คำเดียว mɛ́ɛ-dtɛ̀ɛ kam diao

Not a single word. / Not a single bite. 

15) ฉันยังไม่แก่ขนาดนั้น chǎn yang mâi gɛ̀ɛ kà-nàat-nán I'm not that old!  

16) ความรักอย่างเดียว(มัน)ไม่พอ kwaam-rák yàang-diao (man) mâi-pɔɔ Love alone isn't enough.  

17) ผมเป็นคนเสนอเอง pǒm bpen kon sà-nə̌ə eeng 

I was the one who suggested it.  

18) มีรอยขีดข่วน mii rɔɔi-kìit-kùan 

It's scratched. / It's got a scratch.  

19) ทำไมขี้ลืมขนาดนี้ tam-mai kîi-lʉʉm kà-nàat-níi 

Why am (I) so forgetful?!  

20) ปฏิเสธได้ยังไง bpà-dtì-sèet dâi yang-ngai 

How could (he) refuse? 

Super Useful Expressions, Part 151 

(low-intermediate level) 

1) ทำไมต้องโกหก(ฉัน) tam-mai dtɔ̂ng goo-hòk (chǎn) 

Why do you have to lie (to me)? 

2) ผมว่าไม่น่านะ(ครับ) pǒm wâa mâi-nâa ná (kráp) 

I don't think so. / I doubt it.  

3) พี่จะซื้อให้ใหม่ pîi jà sʉ́ʉ hâi mài 

I'll buy you a new one.  

4) เป็น(โรงเรียน)ที่มีชื่อเสียง bpen (roong-rian) tîi mii-chʉ̂ʉ-sǐang It's a (famous/well-known/reputable) school. 

5) (ก็)พอได้ INFORMAL (gɔ̂ɔ) pɔɔ-dâi 

Not bad. / Okay. / Passable.  

6) อีกนิดเดียวจะเสร็จ ìik nít-diao jà sèt 

(I)'ll be done in a bit. / (I)'ll be finished in short while.  

7) ฉันคิดถูกแล้ว chǎn kít tùuk lɛ́ɛo 

I was right. (about what I thought) 

8) มันแพงมาก man pɛɛng mâak 

(It)'s so expensive! / (That) was so expensive!  

9) กินยาหรือยัง gin yaa rʉ̌ʉ-yang 

Did you take (any) medicine (yet)? / Have you taken your medicine yet?  

10) ฝุ่น(มัน)เยอะ / มีฝุ่นเยอะ fùn (man) yə́ / mii fùn yə́ It's dusty. / There's a lot of dust.  

11) ไม่ได้เป็นอะไร mâi-dâi bpen-à-rai 

I'm fine. / Nothing's wrong with me.  

12) แดด(มัน)ร้อน dɛ̀ɛt (man) rɔ́ɔn 

The sun's very hot. / It's very hot. 

13) ฟังก่อน STRONG fang gɔ̀ɔn 

Listen! / Listen to me!  

14) ไม่อยากโกหก mâi-yàak goo-hòk 

I don't wanna lie (to you). 

15) ผมได้ยินเสียงร้อง pǒm dâi-yin sǐang-rɔ́ɔng 

I heard screaming. / I heard a scream. 

16) เห็นนี้หรือยัง hěn níi rʉ̌ʉ-yang 

Have (you) seen this (yet)? 

17) ผมได้กลิ่นควัน pǒm dâi-glìn kwan 

I smell smoke.  

18) อย่าประมาท yàa bprà-màat 

Don't be careless.  

19) อย่าพูดอย่างนั้น(สิ) / อย่าพูดแบบนั้น(สิ) yàa pûut yàang-nán (sì) / yàa  pûut bɛ̀ɛp-nán (sì) 

Don't say that! / Don't talk that way!  

20) เขาเป็นสุภาพบุรุษ kǎo bpen sù-pâap-bù-rùt 

He's a gentleman.  

Super Useful Expressions, Part 152 

(low-intermediate level) 

1) นั่งที่ไหนดี nâng tîi-nǎi dii 

Where shall we sit? / Where do you wanna sit? 

2) ทำไมกลับเร็วจัง tam-mai glàp reo jang 

Why are you back so soon? / Why are you home so early? OR You're leaving  so soon? 

3) มีทางเดียว mii taang-diao 

There's only one way.  

4) มันไม่ปลอดภัย man mâi bplɔ̀ɔt-pai 

It's not safe.  

5) รางวัลที่ (1) raang-wan tîi (nʉ̀ng) 

(First) prize.  

6) ปวดตา bpùat dtaa 

(My) eyes hurt. / (My) eyes are tired. 

7) ไม่เหงาเหรอ mâi ngǎo rə̌ə 

Aren't you lonely? / Don't you get lonely?  

8) อย่าไปพูดถึง... yàa bpai pûut-tʉ̌ng... 

Don't talk about... ! / Don't mention... !  

9) ตอนที่คุณไม่อยู่... dtɔɔn-tîi kun mâi-yùu... 

When you were gone... / While you were gone... / When you were away...  

10) ...ถ้าจำเป็นจริงๆ ...tâa jam-bpen jing-jing 

...if it's really necessary. / ...if you really need to.  

11) มันจำเป็นจริงๆ man jam-bpen jing-jing 

It's really necessary. / It's absolutely necessary. / I really must.  

12) น่าจะพอ nâa-jà pɔɔ 

It should be enough. / That should suffice.  

13) นี้ไง níi ngai 

Here it is! (I found it.) 

14) จะไปทำไม jà bpai tam-mai 

Why are you going to go (there)? / Why are you going? 

15) เขาจะคิดยังไง kǎo jà kít yang-ngai 

What will she think (about it)? 

16) ผมจะไปเยี่ยมคุณ pǒm jà bpai yîam kun 

I'll visit you. / I'll come visit you.  

17) ทำไม่ยาก tam mâi yaak 

It's not hard to make.  

18) ฉันอยากตาย chǎn yàak dtaai 

I wanna die!  

19) ต้องทำงานหนัก dtɔ̂ng tam-ngaan nàk

I have to work hard. / He had to work hard.  

20) ไม่รู้จะขอบคุณยังไง mâi-rúu jà kɔ̀ɔp-kun yang-ngai I don't know how to thank you. 

Super Useful Expressions, Part 153 (low-intermediate level) 

1) ทำอะไรบ้างวันนี้ tam à-rai bâang wan-níi 

What did you do today? / What did you make today? 

2) เขางก / เขาเป็นคนขี้งก kǎo ngók / kǎo bpen kon kîi-ngók He's stingy. / He's greedy. / He's a cheapskate.  

3) เขาออกไปข้างนอก kǎo ɔ̀ɔk-bpai kâang-nɔ̂ɔk 

She went out. / She went outside.  

4) ฉันดีใจกับแกนะ chǎn dii-jai-gàp gɛɛ ná 

I'm happy for you.  

5) ท้อง(ฉัน)ร้อง tɔ́ɔng (chǎn) rɔ́ɔng 

My stomach is growling! (hungry) 

6) จำได้ทุกค jam dâi túk kam 

(I) remember every word.  

7) เราเป็นแค่เพื่อน rao bpen kɛ̂ɛ pʉ̂an 

We're just friends. / We're only friends.  

8) ใครบอกว่า... krai bɔ̀ɔk wâa... 

Who says...? / Who said...?  

9) ไม่ได้กินนาน mâi-dâi gin naan 

I haven't had (that) for a long time. / It's been awhile since I ate (that). 

10) ผมไม่มีคนอื่น pǒm mâi-mii kon-ʉ̀ʉn 

I don't have anyone else. / I have no one else. (but you/her) 

11) ถอดรองเท้าด้วย(ครับ) tɔ̀ɔt rɔɔng-táao dûai (kráp) Please take off your shoes.  

12) มันน่าตื่นเต้น man nâa-dtʉ̀ʉn-dtên 

It's exciting.  

13) ยังไม่อยากมี yang mâi-yàak mii 

(I) don't want one yet. 

14) ขายไม่ค่อยดี kǎai mai-kɔ̂i dii 

(They're) not selling very well. / (It's) not selling so well.  

15) เล่ามาสิ INFORMAL lâo maa sì 

Tell me! / Tell me about it!  

16) เชิญใหม่นะครับ / นะคะ chəən mài ná-kráp / ná-ká Please come again. 

17) อยากออกไปเดินเล่น yàak ɔ̀ɔk-bpai dəən-lên (I) wanna go out for a walk. / I want to take a stroll. 

18) มันบาปนะ man bpàap ná 

It's a sin. / That's a sin.  

19) ฉันจำเป็นต้องรู้ chǎn jam-bpen-dtɔ̂ng rúu I have to know. / It's necessary that I know.  

20) พูดอะไรก็ได้ pûut à-rai gɔ̂ɔ-dâi 

Say whatever you want (to say). / Say anything.  

Super Useful Expressions, Part 154 (low-advanced level)

1) (มัน)อาจไม่ใช่ความจริงทั้งหมด (man) àat mâi-châi kwaam-jing táng-mòt (It) might not all be true. / All of it may not be the truth. 

2) จะแย่ยิ่งกว่านี้ jà yɛ̂ɛ yîng-gwàa níi 

It could have been worse. / I could be worse.  

3) ไม่ได้รับการติดต่อกลับ(มาเลย) FORMAL mâi-dâi ráp gaan-dtìt-dtɔ̀ɔ glàp (maa ləəi) 

(I) didn't receive any response. / (I) never got an answer back. 

4) (ท่าทาง)มีพิรุธ (tâa-taang) mii-pí-rút 

He's acting suspicious. / Something's fishy about the way she's behaving. / She's  acting strange. (suspicious) 

5) มันอันตรายถึงชีวิต man an-dtà-raai tʉ̌ng chii-wít 

It's lethal. / It's life-threatening. / It can be fatal. 

6) ความพยายามอยู่ที่ไหน ความสำเร็จอยู่ที่นั่น kwaam-pá-yaa-yaam yùu-tîi nǎi kwaam-sǎm-rèt yùu-tîi-nân 

Where there's a will, there's a way! / If you try hard, you'll surely succeed! 

7) ฝากไว้ให้คิด fàak wái hâi kít 

Food for thought. / Something to think about.  

8) โคตรสะใจ(เลยว่ะ) kôot sà-jai (ləəi) wâ 

(I)'m soooo satisfied! / So fuckin' satisfying!  

9) (และ)เมื่อถึงวันนั้น LITERARY lɛ́ mʉ̂a tʉ̌ng wan-nán 

(And) when that day comes...  

10) (เธอเป็นนางเอก)ขวัญใจของ(ผม) LITERARY (təə bpen naang-èek)  kwǎn-jai kɔ̌ɔng (pǒm) 

(She)'s (my) favorite actress. / (She)'s (my) beloved actress. 

11) ต้องมีระเบียบวินัย FORMAL dtɔ̂ng mii rá-bìap-wí-nai

(You) need to be disciplined. / (You) have to have discipline. 

12) ทําทุกวิถีทางเพื่อ(แก้ไขปัญหา) tam túk wí-tii-taang pʉ̂a (gɛ̂ɛ-kǎi bpan-hǎa) (They) tried everything possible to (fix the problem). / (He) did everything he  could to (fix the problem).  

13) เรากลับไปแก้ไขอะไรไม่ได้(แล้ว) rao glàp bpai gɛ̂ɛ-kǎi à-rai mâi-dâi (lɛ́ɛo) We cannot undo what has been done. / We cannot change anything. (it's done  and in the past) 

14) ไปไม่ถึง(บ้าน) bpai mâi-tʉ̌ng (bâan) 

(He) didn't make it (home). / (He) didn't reach (his home). 

15) ผมมีปัญหามากพออยู่แล้ว pom mii bpan-hǎa mâak pɔɔ yùu lɛ́ɛo I have enough troubles of my own. / I have enough problems already.  

16) ตายคาที่ dtaai kaa-tii 

(He) died on the spot. / (He) died instantaneously. / (He) died at the scene of the  accident.  

17) เรารู้จักกัน(มา)ตั้งแต่ยังเป็นเด็ก rao rúu-jàk gan (maa) dtâng-dtɛ̀ɛ yang bpen dèk 

We've known each other since we were kids. 

18) ตามหามันให้เจอ STRONG dtaam-hǎa man hâi jəə 

Find him! / Go find him! (follow him and find him) 

19) อย่าโยนความผิดให้คนอื่น yàa yoon-kwaam-pìt-hâi kon-ʉ̀ʉn Don't blame others. / Don't put the blame on others. / Stop blaming others.  

20) พูดเองเออเอง pûut eeng əə eeng 

Speak for yourself! / You said it, not me! 

Super Useful Expressions, Part 155 

(high-intermediate level)

1) ทำไมจะไม่ได้ tam-mai jà mâi-dâi 

Why CAN'T you?! / Why not?!  

2) ไม่มีใครพิสูจน์ได้ mâi-mii-krai pí-sùut dâi 

No one can prove it. / You can't prove it. / No one could prove it.  

3) เล่าให้(ฉัน)ฟังก็ได้นะ lâo hâi (chǎn) fang gɔ̂ɔ-dâi ná 

(You) can tell me (about it). 

4) เป็นอะไรตาย INFORMAL bpen-à-rai dtaai 

What did he die of? / What did he die from?  

5) (มัน)มีอยู่จริง (man) mii-yùu jing 

(It) (really) does exist. / (It)'s real.  

6) มันเกิดขึ้นเร็วมาก man gə̀ət-kʉ̂n reo mâak 

It happened so fast.  

7) ผมไม่มีอะไรจะคุย pǒm mâi-mii-à-rai jà kui 

I have nothing to say.  

8) ไม่น่าเป็นห่วง mâi-nâa bpen-hùang 

There's nothing to worry about. / No need to worry (about it). 

9) อย่าไปใส่ใจเลย yàa bpai sài-jai ləəi 

Don't pay (it) any attention. / Don't worry about it. / Just let it go. 

10) ตามธรรมเนียม(จีน)(...) dtaam-tam-nian (jiin) (...) 

It's a (Chinese) tradition. / According to (Chinese) tradition (...) / Following  (Chinese) custom (...)  

11) ต้องมีอะไรบางอย่าง... dtɔ̂ng mii-à-rai baang-yàang... There must be something... / There's gotta be something...  

12) คุณจะไม่ผิดหวัง(อย่างแน่นอน) kun jà mâi pìt-wǎng (yàang-nɛ̂ɛ-nɔɔn)

You (surely) won't be disappointed.  

13) เข้าท่า(ดี) kâo-tâa (dii) 

Good idea! / Sounds good!  

14) เวลาเป็นเงิน(เป็นทอง) wee-laa bpen ngən (bpen-tɔɔng) Time is money. 

15) ขอโทษเรื่องอะไร kɔ̌ɔ-tôot rʉ̂ang à-rai 

What are you sorry for?  

16) ขอโทษอะไรกัน kɔ̌ɔ-tôot à-rai gan 

Sorry for what?!  

17) ทุกปัญหามีทางออก(เสมอ) SEMI-LITERARY túk bpan-hǎa mii taang-ɔ̀ɔk  (sà-mə̌ə) 

Every problem has a solution.  

18) ไม่ต้องพึ่งพาใคร mâi-dtɔ̂ng pʉ̂ng-paa krai 

(You) don't need to depend on anyone. / You can be independent.  

19) เขาเป็นคนบอกผมเอง kǎo bpen kon bɔ̀ɔk pǒm eeng 

He's the one who told me. / He told me himself. 

20) เราสอบไม่ติด rao sɔ̀ɔp mâi-dtìt 

I didn't pass. / I didn't pass the test. 

Super Useful Expressions, Part 156 

(high-intermediate level) 

1) ฉันรู้เรื่องแล้ว chǎn rúu-rʉ̂ang lɛ́ɛo 

I know about it. / I know about... / I know. 

2) ผมเสียใจกับเรื่องที่เกิดขึ้น pǒm sǐa-jai gàp rʉ̂ang tîi gə̀ət-kʉ̂n I'm sorry for what (has) happened. 

3) ฉันโกหกตัวเองไม่ได้ chǎn goo-hòk dtua-eeng mâi-dâi I can't lie to myself.  

4) ผมเบื่อที่จะต้อง(รอ) pǒm bʉ̀a tîi jà dtɔ̂ng (rɔɔ) 

I'm tired of (waiting). / I'm tired of having to (wait).  

5) พูดอะไรอย่างนั้น pûut à-rai yàang-nán 

Why do you say that?! / What are you talking about?!  

6) เราไม่ได้แข่งกับใคร(เลย) rao mâi-dâi kɛ̀ng gàp krai (ləəi) We're not competing with anyone.  

7) ช่วยลดเสียงหน่อยนะคะ POLITE chûai lòt sǐang nɔ̀i ná-ká Please lower your voice. / Please talk more quietly. / Please keep it down.  

8) ผมว่าพาไปโรงพยาบาลดีกว่า pǒm wâa paa bpai roong-pá-yaa-baan dii-gwàai 

I think it's better if we take (him) to the hospital. / I think we should take (him) to the hospital.  

9) กำลังคิดถึงพอดีเลย gam-lang kít-tʉ̌ng pɔɔ-dii ləəi 

(I) was just thinking about/of you.  

10) อดคิดไม่ได้(ว่า...) òt kít-tʉ̌ng mâi-dâi (wâa...) 

(I) can't help but think (...) / (I) can't stop thinking (...)  

11) ผมก็คิดแค่ว่า... pǒm gɔ̂ɔ kít kɛ̂ɛ wâa... 

I just thought... / I only thought that...  

12) อย่าทำตัวเป็นเด็ก / เลิกทำตัวเป็นเด็ก yàa tam-dtua bpen dèk / lə̂ək tam-dtua bpen dèk 

Don't act like a child! / Don't be so childish!  

13) ตกลงจะเอายังไง INFORMAL dtòk-long jà ao yang-ngai So, what'll it be? / So, what do you want (to do)? / So, who do you want? 

14) ทำกับข้าวไม่เก่ง tam-gàp-kâao mâi-gèng 

I'm not a good cook. / I'm not good at cooking. 

15) (พ่อ)ต้องภูมิใจในตัวผม (pɔ̂ɔ) dtɔ̂ng puum-jai-nai-dtua pǒm (My dad) would be proud of me!  

16) รอ(ไป)อีกสักพัก(หนึ่ง) INFORMAL rɔɔ (bpai) ìik sàk-pák (nʉ̀ng) Wait a bit longer. / Wait a while longer.  

17) เป็นเพื่อนที่ดี bpen pʉ̂an tîi dii 

You're a good friend. / He's a good friend.  

18) สักพักแล้ว sàk-pák lɛ́ɛo 

For a while. / It's been awhile. / For a while now.  

19) มันจะมากเกินไป STRONG/ANGRY man jà mâak gəən-bpai You're out of line! / You've said too much!  

20) ยกโทษให้ผมด้วย(นะครับ) yók-tôot-hâi pǒm dûai (ná-kráp) Please forgive me! / Forgive me, please.  

Super Useful Expressions, Part 157 

(high-intermediate level) 

1) ไม่ดีกว่า INFORMAL mâi dii-gwàa 

Better if (we) don't. / Better not. / No (better not). 

2) ไม่รู้หายไปไหน mâi-rúu hǎai bpai-nǎi 

(I) don't know where it's gone to. / (I) don't know what happened to it. / (I) don't  know where it disappeared to. / (I) don't know where they've all gone.  

3) ผมไม่เคยเห็นอะไรแบบนี้(มาก่อน) pǒm mâi-kəəi hěn à-rai bɛ̀ɛp-níi (maa-gɔ̀ɔn) 

I've never seen anything like this before. / I've never seen this kind before.

4) มัวทำอะไรอยู่ STRONG/INFORMAL mua tam à-rai yùu What are you doing!? (implies the person should be doing something else, or  is holding you up, etc.) 

5) ปากเสีย STRONG bpàak-sǐa 

You have a foul mouth! / You're offensive! / You're abusive!  

6) ฉันเข้าใจว่าคุณรู้สึกยังไง chǎn kâo-jai wâa kun rúu-sʉ̀k yang-ngai I understand how you feel. 

7) ให้ฉันเดานะ hâi chǎn dao ná 

Let me guess.  

8) ทำอย่างนั้นไม่ได้ tam yàang-nán mâi-dâi 

(You) can't do that!  

9) ให้ฉันจัดการ hâi chǎn jàt-gaan 

Let me do this. / Let me handle this. / Let me take care of it.  

10) (...)อยู่ใกล้แค่นี้เอง (...) yùu glâi kɛ̂ɛ-níi eeng 

It's really close. / It's so close. / It's right here. (nearby) 

11) ผมไม่กลัวความตาย pǒm mâi-glua kwaam-dtaai 

I'm not afraid of death.  

12) ไม่มีใครหนีพ้น(ความตาย) mâi-mii-krai nǐi-pón (kwaam-dtaai) No one can escape (death).  

13) ฝากด้วยนะ fàak dûai ná 

Please take care of it. / Please do it for me. / I'll leave her with you. (Please take  care of her.) 

14) คุณเป็นผู้ฟังที่ดี kun bpen pûu-fang tîi dii 

You're a good listener. 

15) รักษาหายไหม rák-sǎa hǎai mǎi

Can it be treated/cured/healed? (said of a disease or medical condition) 

16) (เงิน)ทุกบาททุกสตางค์ (ngən) túk bàat túk sà-dtaang every penny / every cent  

Note: สตางค์ (sà-dtaang) is also pronounced as (sà-dtang). 

17) ประจําเดือนไม่มา bprà-jam-dʉan mâi maa 

My period hasn't come. / My period is late.  

18) ชีวิตมันสั้น chii-wít man sân 

Life is short.  

19) ไปเที่ยวให้สนุก bpai-tîao hâi sà-nùk 

Have fun. / Have a good time. / Have a good trip. (said to someone going out  or traveling) 

20) ปรับปรุงตัวเองให้ดีขึ้น bpràp-bprung dtua-eeng hâi dii-kʉ̂n Improve yourself (for the better). 

Super Useful Expressions, Part 158 

(low-intermediate level) 

1) ฉันมีงานต้องท chǎn mii ngaan dtɔ̂ng tam 

I've got work to do. / I have work I gotta get to. 

2) ทำไมเงียบไป(เลยคะ) tam-mai ngîap bpai (ləəi ká) 

Why are you so quiet? (not saying anything) / Why have you been so quiet?  (haven't heard from you for a long time) 

3) พนันกับผมไหม pá-nan gàp pǒm mǎi 

Do you wanna bet?  

4) ฉันพนันกับเพื่อนว่า... chǎn pá-nan gàp pʉ̂an wâa...

I bet my friend (that)...  

5) เอามาให้ผม INFORMAL ao maa hâi pǒm 

Give it to me. / Give me that.  

6) อยู่นี่เอง yùu nîi eeng 

(Oh), there you are! (looking for someone and found them) 

7) ทำแบบนี้ได้ยังไง tam bɛ̀ɛp-níi dâi yang-ngai How can you do that?! / How could you (do that)?!  

8) เขาเจ้าเล่ห์(มาก) kǎo jâo-lêe (mâak) 

He's (very) tricky! / He's a sneaky bugger! / He's (very) sly!  

9) ถ้าเป็นเมื่อก่อน... tâa bpen mʉ̌a-gɔ̀ɔn... 

If it was/were before... / If it had been in the past...  

10) ยื่นมือออกมา yʉ̂ʉn mʉʉ ɔ̀ɔk-maa 

Hold out your hand(s).  

11) (คุณ)จะทำยังไง (kun) jà tam yang-ngai 

What will you do? (if that happens, etc.) 

12) ไม่มีสมอง STRONG mâi mii sà-mɔ̌ɔng 

(He)'s got no brain(s)! / (He)'s got nothing upstairs! / (He)'s stupid!  

13) ผมไม่ได้โง่ pǒm mâi-dâi ngôo 

I'm not stupid! / I'm not a fool!  

14) ต้องใช้เงินเท่าไหร่(คะ) dtɔ̂ng chái-ngən tâo-rài (ká) How much will it cost? / How much is it going to cost me? 

15) มีหลายแบบ mii lǎai bɛ̀ɛp 

We have many kinds. / There are many types.  

16) ทำไมหน้าแดง tam-mai nâa dɛɛng 

Why is your face (so) red?

17) กำลังจะหนีไป gam-lang jà nǐi bpai 

They're getting away! / She's trying to escape! / He's running away!  

18) อย่าไปฟังเขา yàa bpai fang kǎo 

Don't listen to him!  

19) กินไม่ได้นอนไม่หลับ gin mâi-dâi nɔɔn-mâi-làp 

She didn't eat nor sleep. / I can't eat or sleep. 

20) ระวังตัวให้ดี rá-wang-dtua hâi dii 

Be on your guard! / Be very careful. / Better watch out! 

Super Useful Expressions, Part 159 

(high-intermediate level) 

1) อาการไม่ค่อยดี(เลย) aa-gaan mâi-kɔ̂i dii (ləəi) 

(He)'s not doing so well. / (His) condition is poor. / (He)'s in pretty bad shape. 

2) อาการไม่ดีขึ้น aa-gaan mâi dii-kʉ̂n 

(He)'s not getting any better. / (He) hasn't gotten any better.  

3) (นี่)ไม่ใช่เรื่องเล่นๆ (nîi) mâi-châi rʉ̂ang lên-lên 

This is no laughing matter. / This is no joke.  

4) เตรียมใจไว้แล้ว dtriam-jai wái lɛ́ɛo 

(She)'s ready. / (She)'s mentally prepared.  

5) ปัดโธ่ STRONG/ANGRY bpàt-tôo 

Shit! / Damn! / Damn it!  

6) ทนมานานแล้ว ton maa naan lɛ́ɛo 

I've endured this for a long time! / She's put up with it for so long! 

7) ผมจัดการเองได้ pǒm jàt-gaan eeng dâi 

I can handle it (by myself). / I can manage it by myself.  

8) จะให้ฉันพูดอะไร jà hâi chǎn pûut à-rai 

What do you want me to say?  

9) คุณเป็นคนแถวนี้หรือ kun bpen kon tɛ̌ɛo-níi rʉ̌ʉ 

Are you from around here?  

10) จะบ้าหรือไง INFORMAL/STRONG jà bâa rə̌ə ngai 

Are you crazy/nuts?  

11) ยังพอมีเวลา yang pɔɔ mii wee-laa 

There's still time. / You still have time.  

12) เขาถูกใส่ร้าย kǎo tùuk sài-ráai 

 He was set up. / She was slandered. / He was framed.  

13) ทะเลาะอะไรกัน tá-lɔ́ à-rai gan 

What were you arguing about? / What did you have a fight about?  

14) อย่าให้(แม่)รู้นะ yàa hâi (mɛ̂ɛ) rúu ná 

 Don't let (Mom) find out! / Don't let (Mom) know about this! 

15) อย่าน้อยใจ(นะ) yàa nɔ́ɔi-jai (ná) 

Don't be offended. / Don't take offense. / Don't feel like you're being slighted.  

16) ลืมสนิทเลย lʉʉm sà-nìt ləəi 

(I) completely forgot!  

17) ลาออกไม่ได้ laa-ɔ̀ɔk mâi-dâi 

(You) can't quit. (you shouldn't quit your job) 

18) อวยพรหน่อย(สิ) uai-pɔɔn nɔ̀i (sì) 

Give me your blessing. / Wish me good luck.  

19) เธอขี้อิจฉา təə kîi-ìt-chǎa

She's envious. / She's the jealous type. 

20) ผมจะรับผิดชอบเอง pǒm jà ráp-pìt-chɔ̂ɔp eeng 

I'll take responsibility. 

Super Useful Expressions, Part 160 

(high-intermediate level) 

1) (ขอโทษ)ฉันซุ่มซ่ามเอง (kɔ̌ɔ-tôot) chǎn sûm-sâam eeng (I'm sorry.) I was careless. (broke something, etc. due to clumsiness) 

2) ซุ่มซ่ามจริงๆ sûm-sâam jing-jing 

You're so clumsy!  

3) ผมรับปาก(ไป)แล้ว pǒm ráp-bpàak (bpai) lɛ́ɛo 

I promised already. / I promised him already. / I already promised.  

4) ไม่รักษาคำพูด mâi rák-sǎa kam-pûut 

(He) didn't keep his word. / (He) broke his promise.  

5) แทบไม่มีเวลากินข้าว tɛ̂ɛp mâi-mii wee-laa gin-kâao (I) barely had time to eat. (I was so busy) 

6) อย่าให้พลาด yàa hâi plâat 

Don't screw it up! / Don't make a mistake! OR Don't miss it!  

7) ลองคิดดูให้ดี/ลองคิดดูดีๆ lɔɔng kít duu hâi-dii / lɔɔng kít-duu dii-dii Think it over (carefully). 

8) ผมไม่เกี่ยวข้องกับเรื่องนี้ pǒm mâi gìao-kɔ̂ng-gàp rʉ̂ang-níi I had nothing to do with it! / I'm not involved!  

9) มีหลักฐานอะไร mii làk-tǎan à-rai 

What evidence do you have? / Do you have any proof? 

10) เราต้องแจ้งความ rao dtɔ̂ng jɛ̂ɛng-kwaam 

We have to call/inform the police.  

11) ...ตั้งแต่เกิดมา... ...dtâng-dtɛ̀ɛ gə̀ət maa... 

...since I was born... / ...in my entire life... / ...since my birth... / ...for the first  time in my life...  

12) กลัวอย่างเดียว(ว่า...) glua yàang-diao (wâa..) 

(I'm) only afraid of one thing (...)  

13) ฉันบอกได้เลย(นะ)... chǎn bɔ̀ɔk dâi ləəi (ná)... 

I can tell you... / I have to tell you...  

14) ถ้าเป็นอย่างนั้น... tâa bpen-yàang-nán... 

If that's how it is... / If that's the case... 

15) ต้องเดินไปข้างหน้า dtɔ̂ng dəən bpai kâang-nâa 

You have to keep moving on. / You have to keep going. / You have to keep  moving ahead.  

16) เขาจะไม่มีวันให้อภัยผม kǎo jà mâi-mii-wan hâi-à-pai pǒm She will never forgive me.  

17) ไม่ได้มามือเปล่า SEMI-LITERARY mâi-dâi maa mʉʉ-bplàao (He) didn't come empty-handed.  

18) อีควาย VULGAR ii kwaai 

You Buffalo! / You idiot!  

19) โหดเหี้ยมมาก hòot-hîam mâak 

That's really ruthless! / It's really cruel! 

20) ทำไม่ได้จริงๆ tam mâi-dâi jing-jing 

I just can't do it! / I really can't!

Super Useful Expressions, Part 161 

(high-intermediate level) 

1) ซ้ำแล้วซ้ำอีก sám-lɛ́ɛo-sám-ìik 

Over and over (again). / Again and again. / Time after time.  

2) ซ้ำแล้วซ้ำเล่า sám-lɛ́ɛo-sám-lâo 

Over and over (again). / Again and again. / Time after time.  

3) ผมจะพูดเป็นครั้งสุดท้าย STRONG/ANGRY pǒm jà pûut bpen kráng-sùt-táai I'm gonna say it/this for the last time!  

4) แหวะ INFORMAL wɛ̀ 

Gross! / Yuck! / Puke! (used to express disgust) 

5) ผมให้เกียรติเขา pǒm hâi-gìat kǎo 

(I) respect him/her.  

6) ทำอะไรตรงนั้น tam à-rai dtrong-nán 

What are you doing over there? / What were you doing there?  

7) ต้องเผชิญหน้ากับความจริง dtɔ̂ng pà-chəən-nâa-gàp kwaam-jing (You) have to face the truth.  

8) ไม่น่าเกิดขึ้น(เลย) mâi-nâa gə̀ət-kʉ̂n (ləəi) 

This should never happen. / It never should have happened.  

9) ไม่ได้เป็นอะไรมาก mâi-dâi bpen à-rai mâak 

(I)'m not that sick. / (My) condition isn't so bad.  

10) หนูไม่ได้เถียง nǔu mâi-dâi tǐang 

I wasn't arguing (with you)! / I wasn't talking back!  

11) ฉันไม่ได้ฝันไปใช่ไหม chǎn mâi-dâi fǎn bpai châi-mǎi 

I'm not dreaming, am I? 

12) พูดง่ายนี่ INFORMAL pûut ngâai nîi 

(That's) easy for you to say! / You say that so easily!  

13) ผมพอมีเงินเก็บอยู่บ้าง pǒm pɔɔ mii ngən-gèp yùu bâang I have some money saved up. / I have some savings.  

14) ผมไม่มีเงินเยอะขนาดนั้น pǒm mâi-mii ngən yə́ kà-nàat-nán I don't have that kind of money! / I don't have that much money! 

15) ฉันมีวิธีที่ดีกว่านั้น chǎn mii wí-tii dii-gwàa nán 

I have a better way.  

16) ผมคิดอย่างนั้นเหมือนกัน pǒm kít yàang-nán mʉ̌an-gan I was thinking the same thing. / I think so too.  

17) ฉันไม่ได้เอากระเป๋าเงินมา chǎn mâi-dâi ao grà-bpǎo-ngən maa I didn't bring my purse with me.  

18) คิดถึงคนอื่นก่อน kít-tʉ̌ng kon-ʉ̀ʉn gɔ̀ɔn 

Put others first. / Think about others first.  

19) ฉันไม่เคยเล่าเรื่องนี้ให้ใครฟัง chǎn mâi-kəəi lâo rʉ̂ang níi hâi krai fang I've never told anyone (about) this before. (said of story, incident,  experience, etc.) 

20) จะเสียใจไปตลอดชีวิต jà sǐa-jai bpai dtà-lɔ̀ɔt chii-wít (You)'ll regret it for the rest of your life. / (You)'ll be sorry for the rest of your  life. 

Super Useful Expressions, Part 162 

(high-intermediate level) 

1) โลก(มัน)แคบ lôok (man) kɛ̂ɛp 

It's small world! / Small world!

2) ไม่ใช่ความรับผิดชอบของผม mâi-châi kwaam-ráp-pìt-chɔ̂ɔp kɔ̌ɔng pǒm (That)'s not my responsibility. 

3) ใครๆก็ทำแบบนี้ krai-krai gɔ̂ɔ tam bɛ̀ɛp-níi 

Everyone does it! 

4) สภาพพอใช้ได้ sà-pâap pɔɔ-chái-dâi 

It's in fairly good condition. (a used item for sale, etc.) 

5) ขอบคุณล่วงหน้า SEMI-FORMAL kɔ̀ɔp-kun lûang-nâa Thank you in advance.  

6) ไม่ได้รีบร้อนอะไร mâi-dâi rîip-rɔ́ɔn à-rai 

(I)'m not in any big rush. / (I)'m not in a hurry.  

7) มีชีวิตชีวา mii-chii-wít-chii-waa 

It's lively. / It's full of life.  

8) ไม่เข้าใจว่าต้องทำยังไง mâi-kâo-jai wâa dtɔ̂ng tam yang-ngai (I) don't understand what (I'm supposed) to do.  

9) ฉันดีใจกับคุณนะ chǎn dii-jai gàp kun ná 

I'm happy for you.  

10) เท่าที่ผมเห็น... tâo-tîi pǒm hěn... 

As far as I can see... / As far as I can tell...  

11) อีกไม่เกิน (5 วัน)... ìik mâi-gəən (hâa wan)... 

In less than (5 days)... / Within (5 days)... 

12) ไม่เคยคิดเลยว่า... mâi-kəəi kít ləəi wâa... 

(I)'ve never thought (that)...  

13) หลายอย่างร่วมกัน lǎai yàang rûam-gan 

a lot of things mixed together / many things combined together / a variety of  factors 

14) ผมผิดคนเดียว pǒm pìt kon-diao 

It's all my fault. 

15) พังหมด(เลย) pang mòt (ləəi) 

It's screwed up! / It's completely destroyed! / It's ruined!  

16) คิดแต่จะเอาชนะ kít dtɛ̀ɛ jà ao chá-ná 

(He) only thinks about winning/trying to win.  

17) ขอโทษจากใจจริง SEMI-LITERARY kɔ̌ɔ-tôot jàak jai jing I'm truly sorry. / I'm sincerely sorry. 

18) ไม่มีวันจบ(สิ้น) mâi-mii-wan jòp(sîn) 

It's never-ending/endless.  

19) ฉันรู้สึกเหมือนเดิม chǎn rúu-sʉ̀k mʉ̌an-dəəm 

I feel the same (as I always have). / My feelings are the same as before. 

20) ขอให้สำเร็จ kɔ̌ɔ-hâi sǎm-rèt 

I wish you success. 

Super Useful Expressions, Part 163 

(high-intermediate level) 

1) ผมไม่เกี่ยวอะไรด้วย pǒm mâi-gìao à-rai dûai 

I'm not involved. / It has nothing to do with me. / I had nothing to do with it. 

2) ไม่คิดถึงคนอื่น mâi kít-tʉ̌ng kon-ʉ̀ʉn 

(You) don't think about others. / (You) only think about yourself, not others.  

3) อย่างนั้นใช่ไหม(ครับ) yàang-nán châi-mǎi (kráp) 

Is that right? / Is that what you mean? 

4) เก็บ(เอา)ไว้เถอะ gèp (ao) wái tə̀ 

You keep it. / Keep it (it's for you). 

5) ใครๆก็อิจฉา krai-krai gɔ̂ɔ ìt-chǎa 

You'll be the envy of everyone! / Everyone will envy you. / Everyone envies her.  

6) ขอติดรถไปด้วย IDIOMATIC kɔ̌ɔ dtìt-rót bpai dûai 

Can I catch a ride (with you)? / Can I get a lift?  

7) ทำไมหน้าซีด tam-mai nâa-sîit 

Why are so pale? / Why is your face so pale?  

8) รอเวลาที่เหมาะสม rɔɔ wee-laa tîi mɔ̀-sǒm 

Wait for the right/appropriate time.  

9) มีอาการสาหัส mii aa-gaan sǎa-hàt 

(She)'s in critical condition. / (Her) condition is serious.  

10) มีใครบางคนบอก(ผม)ว่า... mii krai baang kon bɔ̀ɔk (pǒm) wâa... Someone told me...  

11) คุณควรจะภูมิใจ(ที่...) kun kuan-jà puum-jai (tîi...) 

You should be proud (that...)  

12) ฉันแค่สงสัยว่าทำไม... chǎn kɛ̂ɛ sǒng-sǎi wâa tam-mai... I was just wondering why... / I just wonder why...  

13) ผมเก็บ(เอา)ไว้นานแล้ว pǒm gèp (ao) wái naan lɛ́ɛo 

I've been saving/keeping it for a long time.  

14) ส่งข่าวบ้างนะ sòng kàao bâang ná 

Let me know how you're doing. (send news) 

15) เลิกเถียงกันได้แล้ว lə̂ək tǐang gan dâi lɛ́ɛo 

Stop arguing! / Stop fighting!  

16) กลับเป็นปกติ glàp bpen-bpòk-gà-dtì

Back to normal.  

Note: ปกติ (bpàk-gà-dtì) is an alternative pronunciation for this word.  

17) จริงหรือไม่ SEMI-LITERARY jing rʉ̌ʉ mâi 

Is that true? / Is it true? / True or not?  

18) มันเป็นหน้าที่ของคุณ man bpen nâa-tîi kɔ̌ɔng kun It's your duty/responsibility.  

19) อยากกินอะไรก็สั่ง(เลย) yàak gin à-rai gɔ̂ɔ sàng (ləəi) Order whatever you like. 

20) ดูแลตัวเองได้ duu-lɛɛ dtua-eeng dâi 

I can take care of myself. / I'm capable of taking care of myself. 

Super Useful Expressions, Part 164 

(low-intermediate level) 

1) มันไม่ใช่ความผิดของ(คุณ) man mâi-châi kwaam-pìt kɔ̌ɔng (kun) It wasn't (your) fault. / It isn't (your) fault. 

2) ไม่ใช่หรอก INFORMAL mâi-châi rɔ̀ɔk 

No, it (isn't). / No, that's not it.  

3) นอนลง nɔɔn long 

Lie down.  

4) ยังไม่กลับมา yang mâi glàp maa 

(He)'s not back yet. / (She) hasn't returned yet.  

5) ปวดฉี่ bpùat chìi 

I gotta pee. / I have to go to the bathroom. 

6) คุณกำลังทำอะไรอยู่ kun gam-lang tam à-rai yùu What are you doing?  

7) ผมเผลอหลับไป pǒm plə̌ə làp bpai 

I nodded off. / I dozed off. / I fell asleep.  

8) อย่าส่งเสียง yàa sòng-sǐang 

Don't make a sound!  

9) เก่งจริงๆ gèng jing-jing 

Great job! / Very good!  

10) ...ทำให้นึกถึง... ...tam-hâi nʉ́k-tʉ̌ng... ...(that) reminds me of... / ...(that) makes me of think of...  

11) เขาเสียเลือดมาก kǎo sǐa lʉ̂at mâak 

He lost a lot of blood. (in an accident, etc.) 

12) อีกสักครู่ ìik sàk-krûu 

In a minute. / Shortly. / In a bit. / In a moment.  

13) มีใครรู้ไหม(ว่า...) mii krai rúu mǎi (wâa...) Does anyone know (...)?  

14) อย่าทะเลาะกันเลยนะ yàa tá-lɔ́-gan ləəi ná Don't argue! / Don't fight! / No fighting! 

15) ไม่เห็นสักคน(เลย) mâi hěn sàk kon (ləəi) (I) didn't see anyone (at all).  

16) เสียหายไปหมด(แล้ว) sǐa-hǎai bpai mòt (lɛ́ɛo) Everything was destroyed. / It was badly damaged. 

17) (ห้อง)กว้างขวางดี (hɔ̂ng) gwâang-kwǎang dii (The room)'s spacious/large.  

18) ทิ้งเขาไม่ได้ tíng kǎo mâi-dâi

(I) can't leave him.  

19) ทำงานได้ดีมาก tam-ngaan dâi dii mâak Great job! / Nice work!  

20) รีบไปกันเถอะ rîip bpai gan tə̀ 

Let's get going! / Let's go! / Let's hurry!  

Super Useful Expressions, Part 165 (high-beginner level) 

1) พอแล้ว pɔɔ lɛ́ɛo 

That's enough. / That's plenty. 

2) (ที่)ไหน INFORMAL (tîi) nǎi 

Where? / Where is it? / Where is she?  

3) ผมอยู่นี่แล้ว pǒm yùu nîi lɛ́ɛo 

I'm here. / I'm here now.  

4) เขามีงานที่ดี kǎo mii ngaan tîi dii 

He has a good job.  

5) ยังไม่สาย yang mâi sǎai 

It's not too late.  

6) ไม่ใช่แค่นี้ mâi-châi kɛ̂ɛ-níi 

Not only this. / Not just this. / That's not all.  

7) ปิดตา(ไว้) bpìt dtaa (wái) 

Close your eyes.  

8) ไม่สนุก(เลย) mâi sà-nùk (ləəi) 

This is no fun! / You're no fun! / It wasn't much fun. 

9) ลองดู(สิ) lɔɔng duu (sì) 

Try it. / Give it a try. / Have a taste. / Taste it.  

10) มีเมียใหม่(แล้ว) mii mia mài (lɛ́ɛo) (He) has a new wife.  

11) มีผัวใหม่(แล้ว) mii pǔa mài (lɛ́ɛo) (She) has a new husband.  

12) มีแฟนใหม่(แล้ว) mii fɛɛn mài (lɛ́ɛo) He has a new girlfriend. / She has a new boyfriend.  

13) ตอบฉันหน่อย INFORMAL dtɔ̀ɔp chǎn nɔ̀i Answer me.  

14) ผมไม่ได้ยิน(คุณ) pǒm mâi dâi-yin (kun) I didn't hear (you). / I couldn't hear (you). 

15) ฉันโตแล้ว chǎn dtoo (lɛ́ɛo) 

I'm grown up now. / I'm a grown-up.  

16) อยากไปไหม yàak bpai mǎi 

Do you wanna go?  

17) ยังไม่พอ yang mâi pɔɔ 

Not enough. / It's still not enough.  

18) เอาอีกไหม ao iìk mǎi 

Do you want more?  

19) เอาอีก(ค่ะ) ao iìk (kà) 

More. / I want more. / Give me more.  

20) มาคนเดียวหรือ maa kon-diao rʉ̌ʉ Did you come alone? / Are you by yourself? 

Super Useful Expressions, Part 166 

(high-intermediate level) 

1) คิดไม่ผิดจริงๆ kít mâi pìt jing-jing 

(I) was right! (my thinking about this). / I wasn't wrong! 

2) ทำใจไม่ได้ tam-jai mâi-dâi 

I can't accept it. / She couldn't put up with it. / I can't stand it. / He can't get over  her.  

3) เอาไปทิ้ง STRONG/INFORMAL ao-bpai tíng 

Throw it away. / Take it and throw it away. / Dump it.  

4) ไปลงนรก(ซะ) STRONG/VULGAR bpai long ná-rók (sá) Go to hell! / To hell with you!  

5) มัน(ก็)ไม่สำคัญแล้ว man (gɔ̂ɔ) mâi sǎm-kan lɛ́ɛo 

It doesn't matter anymore. / It's not important anymore. 

6) ไม่มีใครสู้ได้ mâi-mii-krai sûu dâi 

He's unbeatable! / No one can beat her!  

7) มันจะไม่เกี่ยวได้ยังไง man jà mâi gìao dâi yang-ngai 

How could it not be connected/related?  

8) ไหนคุณบอกว่า(จะช่วยฉัน) nǎi kun bɔ̀ɔk wâa (jà chûai chǎn) I thought you said you (were going to help me)!  

Note: ไหน (nǎi) is used to counter or remind someone who does or says  something contradictory to what s/he formerly said s/he would do.  

9) ออกเวรแล้ว ɔ̀ɔk ween lɛ́ɛo 

(He) finished his shift. / (He)'s off (duty). 

10) อย่าชักช้า yàa chák-cháa

Don't be so slow! / Hurry up! / Don't be late (hurry up)! / Don't dilly dally!  

11) งามเหลือเกิน LITERARY ngaam lʉ̌a-gəən 

It's so beautiful! / She's stunning!  

12) (มัน)ไม่เป็นความจริง FORMAL/LITERARY (man) mâi bpen kwaam-jing It's not true. / That's not true.  

13) ขอเพิ่มอีก(ชาม)(ครับ) POLITE kɔ̌ɔ pə̂əm ìik (chaam)(kráp) May I have another (bowl)? / Can I have some more, please? 

14) ถ้า(ผม)ทำอย่างนั้น... tâa (pǒm) tam yàang-nán... 

If (I) do that... 

15) ไม่ต้องมาขู่ STRONG mâi-dtɔ̂ng maa kùu 

Don't threaten (me)! 

16) (ตอนแรกผมไม่เชื่อ)แต่ตอนนี้เชื่อแล้ว (dtɔɔn-rɛ̂ɛk pǒm mâi-chʉ̂a) dtɛ̀ɛ dtɔɔn-níi chʉ̂a lɛ́ɛo 

(At first I didn't believe it) but now I believe it.  

17) ฉันมีอะไรต้องทำอีกเยอะ chǎn mii à-rai dtɔ̂ng tam ìik yə́ I have a lot left to do. / I have a lot of things to do.  

18) สำนึกแล้ว sǎm-nʉ́k lɛ́ɛo 

(I) regret what I did. / (I) realize what I did was wrong. 

19) เมีย(ผม)มีชู้ mia (pǒm) mii-chúu 

My wife is having an affair/has a secret lover/is being unfaithful.  

20) อย่าหลงเชื่อ yàa long-chʉ̂a 

Don't fall for it! / Don't believe him (he's lying). / Don't be fooled! 

Super Useful Expressions, Part 167 

(high-intermediate level)

1) คุณคิดจะทำอะไร kun kít jà tam à-rai 

What do you think you're doing?! / What do you intend to do? / What are you  gonna do? 

2) ฉันไม่ต้องการพูดถึงมัน chǎn mâi-dtɔ̂ng-gaan pûut-tʉ̌ng man I don't want to talk about it.  

3) เกิดขึ้นได้ยังไง/เกิดขึ้นได้อย่างไร gə̀ət-kʉ̂n dâi yang-ngai/gə̀ət-kʉ̂n dâi yàang-rai 

How could this happen? / How could this have happened? / How did this happen?  

4) คิดว่าต้องเกิดเรื่องร้าย(แน่ๆ) kít-wâa dtɔ̂ng gə̀ət rʉ̂ang-ráai (nɛ̂ɛ-nɛ̂ɛ) (I) think something terrible/bad must have happened.  

5) ยังโชคดีที่(ยังมีงานท) yang chôok-dii tîi (yang mii ngaan tam) Luckily, (I still have job.) / I'm lucky to (still have job.)  

6) ไม่มีใครเชื่อหรอก mâi-mii-krai chʉ̂a rɔ̀ɔk 

No one will/would believe you!  

7) อย่าให้มันหนี(ไปได้) yàa hâi man nǐi (bpai dâi) 

Don't let him get away/escape!  

8) อย่าพูดถึงมัน(เลย) yàa pûut-tʉ̌ng man (ləəi) 

Don't talk about it. / Let's not talk about it.  

9) จับตาดูเขาไว้ jàp-dtaa-duu kǎo wái 

Keep an eye on him. / Watch him carefully.  

10) ใครจะไปเชื่อ(ว่า...) krai jà bpai chʉ̂a (wâa...) 

Who would believe that?! / Who would believe...?!  

11) เอาเถอะ... ao tə... 

All right... / Okay... (acquiescing)

12) ค้นให้ทั่ว(...) kón hâi tûa(...) 

Search everywhere! / Look everywhere! / Find him! (look high and low until you  find him) / Search the... !  

13) เอาอีกแล้ว IDIOMATIC ao ìik lɛ́ɛo 

Not again! / He's at it again! / Here you go again!  

14) บอกแล้วใช่ไหมว่า(อย่ากลับบ้านดึก) bɔ̀ɔk lɛ́ɛo châi-mǎi wâa (yàa glàp bâan dʉ̀k) 

Didn't I tell you (not to come home late)?! / I told you (not to come home late),  didn't I? 

15) เรากำลังคุยเรื่องอะไร rao gam-lang kui rʉ̂ang à-rai 

What are/were we talking about?  

16) (100 บาท)เท่านั้นเอง (rɔ́ɔi bàat) tâo-nán-eeng 

(It's) only (100 baht). / (It's 100 baht), that's all. 

17) มีทั้งข้อดีข้อเสีย mii táng kɔ̂ɔ-dii kɔ̂ɔ-sǐa 

We all have our strengths and weaknesses. / There are both pros and cons. / It  has its advantages and disadvantages.  

18) จะให้(ผม)เชื่อได้อย่างไร jà hâi (pǒm) chʉ̂a dâi yàang-rai How can I believe you?! / How do you expect me to believe you?!  

19) (กำลัง)หิวพอดีเลย (gam-lang) hǐu pɔɔ-dii ləəi 

I'm (just now) getting hungry.  

20) ผมจะพยายามให้ดีที่สุด pǒm jà pá-yaa-yaam hâi dii-tîi-sùt I'll try (and do) my best. / I'll give it my best shot. 

Super Useful Expressions, Part 168 

(low-intermediate level)

1) ไปแล้วนะ bpai lɛ́ɛo ná 

I'm off. / I'm going now. 

2) (...)ใช่ป่ะ INFORMAL (...)châi-bpà 

Right? / Isn't that right? / ...aren't you? / ...isn't it? / ...isn't she? 

Note: ป่ะ (bpà) is a very casual, shortened form of หรือปล่าว (rʉ̌ʉ-bplàao). This is also spelled as ปะ (bpa).  

3) ฉันตกบันได chǎn dtòk ban-dai 

I fell down the stairs.  

4) (รอ)อยู่ตั้งนาน(แล้ว) (rɔɔ) yùu dtâng naan (lɛ́ɛo) 

(I)'ve been (waiting) for a long time.  

5) ผมเพิ่งย้ายมาจาก(อเมริกา) pǒm pə̂ng yáai maa-jàak (à-mee-rí-gaa) I just moved here from (America).  

6) จำหน้าไม่ได้ jam nâa mâi-dâi 

I don't recognize him. / I didn't recognize you.  

7) คุณบ้าไปแล้วหรือ kun bâa bpai lɛ́ɛo rʉ̌ʉ 

Have you lost your mind?! / Are you crazy!? / Have you gone mad?!  

8) เดี๋ยวคุณ(ก็)จะรู้เอง dǐao kun (gɔ̂ɔ) jà rúu eeng 

You'll soon know. / Soon you'll know. / You'll find out soon.  

9) ผมไม่หนี pǒm mâi nǐi 

I'm not running away! / I'm not gonna escape.  

10) ทำได้แค่นี้ tam dâi kɛ̂ɛ-níi 

That's all (I) can do. / That's all (I) could do.  

11) หาไม่เจอ hǎa mâi jəə 

I can't find it. / They couldn't find her. 

12) ฉันจะบอกความจริงให้ chǎn jà bɔ̀ɔk kwaam-jing hâi

I'll tell you the truth. / I'll tell you what really happened.  

13) ฉันยังจำได้ดี(...) chǎn yang jam dâi dii (...) I still remember it well. / I can still clearly remember...  

14) ผมให้สัญญาว่า... pǒm hâi sǎn-yaa wâa... I promise to... / I promised that I... 

15) ทนไม่ได้ที่... ton mâi-dâi tîi... 

I can't stand... / I can't handle... / I can't bear...  

16) ดูดีๆ(สิ) INFORMAL duu dii-dii (sì) 

Look carefully.  

17) ใกล้จะมืดแล้ว glâi jà mʉ̂ʉt lɛ́ɛo 

It's nearly dark. / It's almost dark.  

18) ไม่ทำอะไรหรอก mâi tam à-rai rɔ̀ɔk 

I'm not gonna do anything. OR I'm not gonna hurt you.  

19) อย่าเพิ่งกังวล yàa pə̂ng gang-won 

Don't worry. / Don't you worry. 

20) ฉันไม่สนหรอก INFORMAL/STRONG chǎn mâi-sǒn rɔ̀ɔk I don't care! 

Super Useful Expressions, Part 169 (high-intermediate level) 

1) มีงานให้คุณท mii ngaan hâi kun tam 

I have a job for you. / I have some work for you to do. 

2) เฮ้ย hə̂i/hə́i 

Hey! 

3) ห้ามให้ใครเข้ามา hâam hâi krai kâo-maa 

Don't let anyone (come) in!  

4) (เธอ)อยู่กินกับ(แฟนมา 10 ปี) IDIOMATIC (təə) yùu-gin-gàp (fɛɛn maa sìp  bpii) 

(She)'s been living with (her boyfriend for 10 years).  

5) เขาไม่ไว้ใจใครง่ายๆ kǎo mâi wái-jai krai ngâai-ngâai 

She doesn't trust others/anyone easily. 

6) พี่ไม่ได้ตั้งใจจะพูดแบบนั้น pîi mâi-dâi dtâng-jai jà pûut bɛ̀ɛp-nán I didn't mean to say that.  

7) (จะฆ่ามัน)ด้วยมือของผม(เอง) (jà kâa man) dûai mʉʉ kɔ̌ɔng pǒm (eeng) (I'll kill him) with my own hands!  

8) พรุ่งนี้ค่อยว่ากันใหม่ prûng-níi kɔ̂i wâa gan mài 

Let's call it a day (and finish talking about it tomorrow).  

9) เขาไม่ได้บอกคุณหรือ kǎo mâi-dâi bɔ̀ɔk kun rʉ̌ʉ 

He didn't tell you?  

10) เลิกทะเลาะกันได้แล้ว lə̂ək tá-lɔ́-gan dâi lɛ́ɛo 

Stop fighting/arguing!  

11) (...)เหมือนที่เคยบอก(...) (...) mʉ̌an tîi kəəi bɔ̀ɔk (...) 

(...) like I told you before (...) / (...) as I told you (...) / (...) as I said (...)  

12) ผมจะไม่ยอมให้ใคร... pǒm jà mâi yɔɔm-hâi krai... 

I won't let anyone... / I'm not gonna let anyone...  

13) หลายปีมานี้... lǎai bpii maa níi... 

For many years... / All these years...  

14) ถ้าผมรู้ว่า... tâa pǒm rúu wâa... 

If I had known... / If I knew...

15) (ชอบ)ฉี่รดที่นอน (chɔ̂ɔp) chìi-rót tîi-nɔɔn 

(He) wets the bed.  

16) ยังไม่เข็ดอีกหรือ yang mâi kèt ìik rʉ̌ʉ 

Haven't you learned your lesson?!  

17) ออกมาได้แล้ว ɔ̀ɔk-maa dâi lɛ́ɛo 

(You) can come out now. OR (They) got out of...  

18) เตรียมตัวตาย dtriam-dtua dtaai 

Prepare to die! (angry) OR Get ready for death. (neutral) 

19) ไม่ต้องแย่งกัน mâi-dtɔ̂ng yɛ̂ng-gan 

(You) don't need to fight (over it)! / No need to compete.  

20) ทำบ้าอะไรของมึง ROUGH/ANGRY tam bâa à-rai kɔ̌ɔng mʉng What the hell are you doing? / What the hell did you do? 

Super Useful Expressions, Part 170 

(high-intermediate level) 

1) คิดไม่ตก IDIOMATIC kít mâi dtòk 

(I) can't figure it out. / (I) can't figure out what to do. 

2) (มัน)ไม่ใช่เรื่องใหม่ (man) mâi-châi rʉ̂ang mài 

It's nothing new. / That's not new.  

3) ไม่เหมือนที่เคยกินมา mâi mʉ̌an tîi kəəi gin maa 

(It)'s not like anything I've eaten before. / (It)'s different from anything I've had before.  

4) (คุณ)รู้ไหมว่ามันยากแค่ไหน (kun) rúu mǎi wâa man yâak kɛ̂ɛ-nǎi Do you know how hard it was/is?  

5) ฉันจะทำอะไรกับมันก็ได้ STRONG chǎn jà tam à-rai gàp man gɔ̂ɔ-dâi

I can do whatever I want with it/her/him!  

6) เป็นห่วงเสมอ bpen hùang sà-mə̌ə 

I always worry about you. / She's always worried about you.  

7) (Celebrity) มีที่มาจากคำว่า (celebrate) (Celebrity) mii tîi-maa jàak kam wâa (celebrate). 

(Celebrity) originated from the word (celebrate).  

8) นับวันรอ náp wan rɔɔ 

(I'm) counting the days.  

9) เขาไม่เกี่ยวกับเรื่องนี้ kǎo mâi-gìao gàp rʉ̂ang-níi 

He's got nothing to do with this! / He's not involved!  

10) สิ่งที่(ฉัน)กลัวที่สุดคือ... sìng tîi (chǎn) glua tîi-sùt kʉʉ... The thing I'm most afraid of is... / My worst fear is...  

11) คุณลืมไปแล้วหรือว่า... kun lʉʉm bpai lɛ́ɛo rʉ̌ʉ wâa... Have you forgotten... ? / Did you forget... ?  

12) ถ้าย้อนเวลากลับไปได้ tâa yɔ́ɔn wee-laa glàp bpai dâi If I could turn back time... / If I could go back in time... / If I could turn back the  clock...  

13) ฉันจะไม่ยอมให้ใคร... chǎn jà mâi yɔɔm-hâi krai... 

I won't let anyone... / I won't allow anybody to...  

14) ผมไม่ใช่คนอย่างนั้น pǒm mâi-châi kon yàang-nán 

I'm not that kind of person. / I'm not like that.  

15) เขาเริ่มต้นชีวิตใหม่ kǎo rə̂əm-dtôn chii-wít mài 

He started a new life. / She started her life over again.  

16) (เด็ก)ถูกลักพาตัว (dèk) tùuk lák-paa-dtua 

(A child) was kidnapped/abducted. 

17) ทำตัวไม่ถูก IDIOM tam-dtua mâi-tùuk 

(I) don't know what to do. / (I) don't know how to act.  

18) มีอยู่ทางเดียว mii yùu taang-diao 

There's only one way.  

19) ฉันไม่ได้บอกอะไรเลย chǎn mâi-dâi bɔ̀ɔk à-rai ləəi 

I didn't tell (him) anything.  

20) พอใช้ได้ pɔɔ chái-dâi 

That will work. / It's usable. / It's pretty nice. / It's fairly good. 

Super Useful Expressions, Part 171 

(high-intermediate level) 

1) ทำไมต้องเป็นอย่างนี้ tam-mai dtɔ̂ng bpen yàang-níi 

Why does it have to be like this?! / Why does this have to happen?! 

2) คงไม่มีอะไรหรอก kong mâi-mii-à-rai rɔ̀ɔk 

It's probably nothing. (nothing will happen)  

3) เราอาจจะคิดมากไปเอง rao àat-jà kít mâak bpai eeng 

I might be (just) overthinking it. / I might be thinking too much.  

4) ไม่น่าพูดเลย mâi-nâa pûut ləəi 

(I) shouldn't have said that. / (I) shouldn't have said anything.  

5) ถ้าได้ข่าวอะไร(จะโทรบอกแล้วกัน) tâa dâi-kàao à-rai (jà too bɔ̀ɔk lɛ́ɛo-gan) If I hear anything, (I'll call and let you know.) / If I receive any news, (I'll call and  let you know.)  

6) เราต้องรีบออกไปจากที่นี่ rao dtɔ̂ng rîip ɔ̀ɔk bpai jàak tîi-nîi We have to get out of here! / We have to hurry and leave this place! 

7) ทำไม(ถึง)ต้องทำอย่างนี้ tam-mai (tʉ̌ng) dtɔ̂ng tam yàang-níi What do you have to do this?! / Why are you doing this?!  

8) (ผู้หญิง)มีตั้งมากมาย EMPHASIS or SURPRISE (pûu-yǐng) mii dtâng  mâak-maai 

There are a lot of (women)!  

9) ช่วยไว้ไม่ทัน chûai wái mâi-tan 

He couldn't rescue her in time. / They didn't save him in time.  

10) ...ถ้าคิดอย่างนั้น... ...tâa kít yàang-nán... 

...if that's what you think... / ...if that's how you think...  

11) ตามปกติแล้ว... dtaam-bpòk-gà-dtì lɛ́ɛo...  

Normally... / Usually... / Generally... / Typically...  

Note: ปกติ (bpàk-gà-dtì) is an alternative pronunciation for this word.  

12) (...)ก็เรื่องของคุณ (...) gɔ̂ɔ rʉ̂ang kɔ̌ɔng kun 

(...)That's your problem! / That's your business!  

13) ฉันต้องรู้ให้ได้ว่า... STRONG/URGENT chǎn dtɔ̂ng rúu hâi-dâi wâa... I have to know... / I need to know...  

14) ...ส่วนอีกคนหนึ่งเป็น(หมอ) ...sùan ìik kon nʉ̀ng bpen (mɔ̌ɔ) ...the other is (a doctor). / ...as for the other, she's (a doctor). 

15) (กลัว)ว่าจะมีเรื่องร้ายเกิดขึ้น (glua) wâa jà mii rʉ̂ang-ráai gə̀ət-kʉ̂n (I'm afraid) something terrible will happen. 

16) อีกไม่ไกลแล้ว ìik mâi glai lɛ́ɛo 

It's not much further. / It's not far from here.  

17) เจ็บตรงไหนไหม jèp dtrong-nǎi mǎi 

Did you hurt yourself? / Are you hurt? 

18) ไม่ใช่ไม่เชื่อนะ mâi-châi mâi-chʉ̂a ná 

It's not that I don't believe you.  

19) ผมไม่อยากเสี่ยง pǒm mâi yàak sìang 

I don't want to take the risk. / I don't wanna risk it.  

20) ไม่ได้ง่ายอย่างที่คิด mâi-dâi ngâai yàang tîi kít 

It wasn't as easy as we thought it would be. / It's not as easy as you think. 

Super Useful Expressions, Part 172 

(high-intermediate level) 

1) เมื่อไหร่จะได้เจอกันอีกนะ mʉ̂a-rài jà dâi jəə-gan ìik ná When will we (be able to) see each other again? 

2) ชอบเป็นจุดสนใจ chɔ̂ɔp bpen jùt-sǒn-jai 

(He) likes to be the center of attention. / (She) likes to be in the limelight.  

3) ไม่ต้องมายุ่ง STRONG mâi dtɔ̂ng maa yûng 

Stay out of this! / Don't mess with me! / Leave me alone! / Don't interfere!  

4) (วิ่งเร็ว)ที่สุดเท่าที่จะทำได้ (wîng reo) tîi-sùt tâo-tîi jà tam dâi (Run as fast) as I can/could.  

5) ท้องได้(เจ็ด)เดือน tɔ́ɔng dâi (jèt) dʉan 

(Seven) months pregnant.  

6) รีบเข้าไปช่วย rîip kâo-bpai chûai 

He rushed (in) to help her. / She hurried over to help him.  

7) มันกลายเป็นเรื่องปกติ man glaai-bpen rʉ̂ang bpòk-gà-dtì It's become normal/common/a normal thing. 

Note: ปกติ (bpàk-gà-dtì) is an alternative pronunciation for this word.  

8) ลูกดิ้น lûuk dîn 

The baby kicked/moved. (of pregnant woman) 

9) ฉันมีข่าวมาบอก chǎn mii kàao maa bɔ̀ɔk 

I have some news for you. / I got news for you.  

10) (...)เมื่อวันก่อน(...) (...)mʉ̂a-wan gɔ̀ɔn(...) 

(...) the other day (...)  

11) หลายต่อหลายคน SEMI-LITERARY lǎai-dtɔ̀ɔ-lǎai kon Many people. / So many people. / A lot of people.  

12) หลายต่อหลายครั้ง SEMI-LITERARY lǎai-dtɔ̀ɔ-lǎai kráng Many times. / Numerous times. / Time and time again. 

13) (มัน)คงจะดีไม่น้อยถ้า... (man) kong jà dii mâi-nɔ́ɔi tâa... It would be really great/nice if...  

14) (...)นานเท่าไหร่แล้ว (...)naan tâo-rài lɛ́ɛo 

How long has it been? / How long have you...? 

15) ลำบากแค่ไหนก็... lam-bàak kɛ̂ɛ-nǎi gɔ̂ɔ... 

No matter how hard it is... / No matter how much trouble I have to face...  

16) ตรวจสอบให้แน่ใจว่า... SEMI-FORMAL dtrùat-sɔ̀ɔp hâi nɛ̂ɛ-jai wâa... Check to make sure (that)... / Make sure (that)...  

17) เขาเพิ่งออกจากคุกมา(ได้ไม่นาน) kǎo pə̂ng ɔ̀ɔk-jàak kúk maa (dâi mâi naan) 

He just got out of jail (not long ago).  

18) ได้เจอกันเสียที / ได้เจอกันซะที dâi jəə gan sǐa-tii / dâi jəə gan sá-tii We finally meet! 

Note: ซะที (sá-tii) is a shortened form of เสียที (sǐa-tii), which is a final particle  used to show that you want something to happen soon or completely.  

19) หาทั่วทั้ง(บ้านแต่ไม่เจอ) hǎa tûa-táng (bâan dtɛ̀ɛ mâi jəə) (I) looked all over (the house but I couldn't find her). / (I) searched everywhere (in  the house but I couldn't find it).  

20) ซวยแล้ว suai lɛ́ɛo 

We are screwed! / We're in deep shit! / Damn! / Shit! 

Super Useful Expressions, Part 173 

(low-advanced level) 

1) กินอะไรได้อย่างนั้น gin à-rai dâi yàang-nán 

You are what you eat. 

2) อย่าปล่อยให้หลุดมือไป yàa bplɔ̀i-hâi lùt mʉʉ bpai 

Don't let it slip out of your hand. / Don't let it slip away.  

3) ไม่มีใครยอมใคร mâi-mii-krai yɔɔm krai 

No one is going to give in. / No one's gonna give in to the other. / No one's going  to give an inch.  

4) ถ้าไม่เห็นกับตา(ฉัน)(ก็ไม่เชื่อ) tâa mâi hěn-gàp-dtaa (chǎn) (gɔ̂ɔ mâi chʉ̂a) If I hadn't seen it with my own eyes, (I wouldn't have believed it.)  

5) เขากำลังเดินตามรอยเท้า(ของพ่อ) kǎo gam-lang dəən dtaam rɔɔi-táao  (kɔ̌ɔng pɔ̂ɔ) 

He's following (his father's) footsteps. / He's walking in (his father's) footsteps.  

6) บรรทัดฐานใหม่ ban-tát-tǎan mài 

The new normal. / A new standard.  

7) เขาไม่เป็น(สอง)รองใคร kǎo mâi bpen (sɔ̌ɔng) rɔɔng krai He's second to none!

8) (ห้ามส่งเสียง)ไม่งั้นตาย STRONG (hâam sòng-sǐang) mâi-ngán dtaai (Don't make a sound) or you're dead! / (Don't make a sound) or you'll die!  

9) ท่าไม่ดี IDIOMATIC tâa mâi-dii 

It doesn't look so good. / That's not good. (looks like the situation is getting  dangerous or out of hand) 

10) (นี่)มันเท่ากับว่า... (nîi) man tâo-gàp-wâa... 

That's like... / That's the same as...  

11) ...ต้องมีข้อแลกเปลี่ยน ...dtɔ̂ng mii kɔ̂ɔ-lɛ̂ɛk-bplìan 

...you have to get something in exchange. / ...I have to get something in return. 

12) รู้(สึก)ตัวอีกที... rúu-(sʉ̀k)-dtua ìik-tii... 

Before I knew it... / All of the sudden... / Next thing I knew... 

13) ...สามารถใช้เป็นหลักฐาน(...) ...sǎa-mâat chái bpen làk-tǎan(...) ...can be used as evidence (...) 

14) ...อาจนำไปสู่(มะเร็งตับ) FORMAL ...àat nam-bpai-sùu (má-reng-dtàp) ...might result in (liver cancer). / ...may lead to (liver cancer.) 

15) (งานวิจัย)ชี้ชัด(อยู่)แล้วว่า... (ngaan-wí-jai) chíi chát (yùu) lɛ́ɛo-wâa... (Research) clearly shows... / (Research) confirms... 

16) ตาเถร IDIOM/SLANG dtaa-těen 

Holy Cow! / Shit! / Bejesus! / Holy Mackerel! / Oh my God!  

17) มันออกจะแปลกๆ man ɔ̀ɔk jà bplɛ̀ɛk-bplɛ̀ɛk 

It's rather strange. / That's weird. / It seems rather weird.  

18) เป็นพระคุณ LITERARY bpen-prá-kun 

I'm much obliged. / I'm very grateful. / I really appreciate it.  

19) นี่ไม่ใช่เวลามาเล่นนะ nîi mâi-châi wee-laa maa lên ná This is no time to play around! 

20) ต้องคุยกับเขาให้รู้เรื่อง dtɔ̌ng kui gàp kǎo hâi rúu-rʉ̂ang (I) have to talk some sense into him! / (I) have to talk to him (and make him  understand).  

Super Useful Expressions, Part 174 

(low-intermediate level) 

1) ฉันช่วยถือ chǎn chûai tʉ̌ʉ 

I'll carry that for you. / I'll hold that for you. / I'll help you carry that. 

2) เป็นผัวเมียกัน bpen pǔa mia gan 

They're married. / They're husband and wife. / They're a couple. 

3) เขาอาจตายได้ kǎo àat dtaai dâi 

He might die. / She could die.  

4) เธอไม่ได้มาด้วย təə mâi-dâi maa dûai 

She didn't come. / She didn't come with me.  

5) รออีกสักหน่อยนะ(ครับ) rɔɔ ìik sàk-nɔ̀i ná (kráp) 

Wait a bit more. / Wait a while longer.  

6) รออีกสักพักนะ(ครับ) rɔɔ ìik sàk-pák ná (kráp) 

Wait a bit more. / Wait a while longer.  

7) รออีกสักสองสาม(วัน) rɔɔ ìik sàk sɔ̌ɔng-sǎam (wan) 

Wait a few more (days).  

8) กลิ่น(มัน)แรงไปหน่อย glìn (man) rɛɛng bpai nɔ̀i 

The smell is (a bit) too strong. / It's (a bit) overpowering.  

9) ผม(จะ)หยิบให้ pǒm (jà) yìp hâi 

I'll get it for you. / I'll grab it for you. / I'll get it. 

10) ขอทางหน่อย kɔ̌ɔ-taang nɔ̀i 

Excuse me... / Coming through. / Please move out of the way.  

11) อย่าเพิ่งเลย yàa pə̂ng ləəi 

Not yet! / Not just yet! (Don't do it yet.) 

12) รอดมาได้ rɔ̂ɔt maa dâi 

I survived. / She's still alive. 

13) เคยเจอมาแล้ว kəəi jəə maa lɛ́ɛo 

I've met him before. OR I've had that happen before. / I've been through that.  

14) เราต้องรอให้... rao dtɔ̂ng rɔɔ hâi... 

We have to wait for... / We have to wait until... 

15) เป็นไปได้ bpen-bpai dâi 

It's possible. / That's possible.  

16) อยู่นิ่งๆ yùu nîng-nîng 

Stay still! / Hold still!  

17) ไม่มีพยาน mâi-mii pá-yaan 

There were no witnesses. / He hasn't got any witnesses.  

18) เราต้องไปช่วยเขา rao dtɔ̂ng bpai chûai kǎo 

We have to (go) help him. / We have to help her out.  

19) ขอยืมเงินหน่อย kɔ̌ɔ yʉʉm ngən nɔ̀i 

Can I borrow some money?  

20) หายโกรธหรือยัง hǎai gròot rʉ̌ʉ-yang 

Are you still angry?  

Super Useful Expressions, Part 175 

(high-intermediate level)

1) มันทำให้เสียสมาธิ man tam-hâi sǐa-sà-maa-tí 

It's distracting. / It makes me lose my focus. 

2) ต้องตัดใจจากเขา dtɔ̂ng dtàt-jai jàak kǎo 

You have to let him go! / You have to get over her! / You have to forget him!  

3) ผมมีข่าวมาแจ้งให้ทราบ FORMAL pǒm mii kàao maa jɛ̂ɛng hâi sâap I have some news to tell you. / I have some news for you.  

4) ขอโทษ(นะ)ที่ทำให้(คุณ)เป็นห่วง kɔ̌ɔ-tôot (ná) tîi tam-hâi (kun) bpen-hùang I'm sorry for making you worry. / I'm sorry for causing you to worry.  

5) คุณจะว่าอย่างไร kun jà wâa yàang-rai 

What would you say?  

6) ถอนคำพูด(นะ) tɔ̌ɔn kam-pûut (ná) 

Take it back! (what you said) 

7) (ไม่เคยร้องไห้)ต่อหน้าคนอื่น (mâi-kəəi rɔ́ɔng-hâi) dtɔ̀ɔ-nâa kon-ʉ̀ʉn (I've never cried) in front of others.  

8) ผล(ตรวจ)ออกมาแล้ว pǒn(dtrùat) ɔ̀ɔk-maa lɛ́ɛo 

The results are out. / The test came back.  

9) คุณนั้นแหละ(ที่บ้า) kun nán-lɛ̀ (tîi bâa) 

YOU'RE the one (who's crazy)! / (It's YOU) who's crazy!  

10) เมื่อไม่กี่วันที่ผ่านมา... SEMI-FORMAL mʉ̂a mâi-gìi wan tîi pàan maa... A few days ago...  

11) เมื่อไม่กี่วันก่อน... SEMI-FORMAL mʉ̂a mâi-gìi wan gɔ̀ɔn... A few days ago...  

12) ถ้ามีใครรู้... mâi-mii-krai rúu... 

If anyone finds out... / If anyone knew... 

13) ขอเพียงแค่... kɔ̌ɔ piang-kɛ̂ɛ... 

All (I) ask is... / (I) only ask that...  

14) ไม่มีทางเบื่อแน่นอน mâi-mii-taang bʉ̀a nɛ̂ɛ-nɔɔn 

(I)'d never get bored! / There's no way I'd ever get bored! 

15) ขอโทษนะที่ไม่ได้ติดต่อคุณ kɔ̌ɔ-tôot ná tîi mâi-dâi dtìt-dtɔ̀ɔ kun (I)'m sorry for not having contacted you. / (I)'m sorry that I didn't contact you.  

16) ใกล้จะคลอดแล้ว glâi jà klɔ̂ɔt lɛ́ɛo 

(She)'s about to give birth. / (She)'s going into labor.  

17) เขาถูกประหารชีวิต kǎo tùuk bprà-hǎan-chii-wít 

He was executed. (received the death penalty) 

18) พูดให้มันดีๆหน่อย STRONG pûut hâi man dii-dii nɔ̀i Watch your language! / Speak nicely!  

19) เป็นลางร้าย bpen laang-ráai 

It's a bad omen. / That's ominous. 

20) อย่ามาเสือก STRONG yàa maa sʉ̀ak 

Don't butt in! / Don't intrude!  

Super Useful Expressions, Part 176 

(low-intermediate level) 

1) ผม(ก็)ตกใจ pǒm (gɔ̂ɔ) dtòk-jai 

I was shocked. / I was startled. 

2) เขาไม่พอใจ kǎo mâi-pɔɔ-jai 

He's angry/upset/not satisfied with...  

3) ฉันรู้จักเขาดี chǎn rúu-jàk kǎo dii

I know him well. / I know her well.  

4) เสร็จแล้วหรือ sèt lɛ́ɛo rʉ̌ʉ 

Are you finished? / Are you done?  

5) เสร็จแล้ว sèt lɛ́ɛo 

I'm done. / It's finished.  

6) ไม่ต้อง mâi-dtɔ̂ng 

No (it's not necessary). / No need.  

7) จริงๆ(นะ) jing-jing (ná) 

Really! / It's true!  

8) ยังไม่สุก yang mâi sùk 

It's not ripe (yet). / It's not cooked.  

9) ฉัน(มัน)เลว chǎn (man) leeo 

I'm bad. / I'm so bad. / I'm no good.  

Note: มัน (man) often means 'it' but here it's used in a derogatory way to refer to oneself.  

10) เป็นไปได้ว่า... bpen-bpai-dâi wâa... 

It's possible that... / There's a possibility that...  

11) ออกมาสิ ɔ̀ɔk-maa sì 

Come out.  

12) เขาออกมาแล้ว kǎo ɔ̀ɔk-maa lɛ́ɛo 

He's out. / He came out.  

13) เข้ามาข้างใน kâo-maa kâang-nai 

Come inside.  

14) ดีๆ dii-dii 

Good, good. / Good. / That's good.

15) ตอนนี้ยัง(ค่ะ) dtɔɔn-níi yang (kà) 

Not yet. / No, not yet. / Right now---no.  

16) (ที่นี่)บ้านของใคร (tîi-nîi) bâan kɔ̌ɔng krai 

Whose house is this?  

17) จับไม่ได้ jàp mâi-dâi 

They can't catch him. / You can't catch me. 

18) ผมไม่ไปหรอก pǒm mâi bpai rɔ̀ɔk 

I'm not going. / I'm not going to go. / I'm never gonna go (there). 

19) เหมือนมาก mʉ̌an mâak 

Very similar! / It looks almost the same! / They look very similar!  

20) พอเถอะ pɔɔ tə̀ 

That's enough. / That's plenty. 

Super Useful Expressions, Part 177 

(high-intermediate level) 

1) (หน้าตา)น่าเกลียดน่าชัง (nâa-dtaa) nâa-glìat nâa-chang He's so cute! / She's so adorable! (usually said of a baby) 

Note: Literally, this means the baby looks ugly and despicable. Traditionally, this was said to trick any bad spirits away from the baby.  

2) เธอไม่มีวันยอม təə mâi-mii-wan yɔɔm 

She'll never let you. / She'll never allow it.  

3) ได้รับความเสียหายอย่างหนัก SEMI-FORMAL dâi-ráp kwaam-sǐa-hǎai  yàang-nàk 

(It) was badly damaged. 

4) เราสามารถช่วยเหลือคุณได้ FORMAL rao sǎa-mâat chûai-lʉ̌a kun dâi We can help/assist you.  

5) เธอออกไปรอข้างนอก(ก่อน) təə ɔ̀ɔk-bpai rɔɔ kâang-nɔ̂ɔk (gɔ̀ɔn) (You) go wait outside.  

6) ต้องทำให้ได้ dtɔ̂ng tam hâi-dâi 

I must do it! / You have to do it!  

7) คุณก็รู้อยู่แล้วนี่ kun gɔ̂ɔ rúu yùu lɛ́ɛo nîi 

You know that (already)!  

8) ยุ่งจนไม่มีเวลาทำอะไร yûng jon mâi mii wee-laa tam à-rai (I)'m so busy that I have no time for anything. / (I)'m so busy working that I have  no time to do anything else.  

9) นักโทษแหกคุก nák-tôot hɛ̀ɛk-kúk 

The prisoner escaped from jail. / The prisoners broke out of the prison.  

10) (...)จะได้ไม่เหงา (...) jà-dâi-mâi ngǎo 

(...) so you won't get lonely. / (...) so she won't be lonely.  

11) คิดซะว่าเป็น... / คิดเสียว่าเป็น... kít sá wâa bpen... / kít sǐa wâa bpen... (Just) consider it... / (Just) think of it as...  

12) ฉันเคยได้ยินมาว่า... chǎn kəəi dâi-yin maa wâa... 

I've heard before that... / I hear that...  

13) ที่แท้ก็... tîi tɛ́ɛ gɔ̂ɔ... 

But in fact... / But actually...  

14) จงจำไว้ว่า... LITERARY jong jam wái wâa... 

Remember... / Keep in mind... 

15) เขาถูกจับเป็นเชลย kǎo tùuk jàp bpen chà-ləəi 

He was taken captive/prisoner. 

16) ทำอย่างนี้ไม่ได้ tam yàang-níi mâi-dâi 

You can't do this!  

17) ผมเองก็เช่นกัน FORMAL pǒm eeng gɔ̂ɔ chên-gan And so am I. / And me likewise. / And I am, too.  

18) ไม่ต้องมาแก้ตัว mâi-dtɔ̂ng maa gɛ̂ɛ-dtua 

Don't make excuses! / No excuses! / Don't give me any excuses!  

19) (คุณ)จะไปรู้อะไร INFORMAL/STRONG (kun) jà bpai rúu à-rai What (the hell) do/would you know?!   (implying the listener knows nothing) 

20) เราไม่ถูกกัน rao mâi tùuk-gan 

We don't get along. 

Super Useful Expressions, Part 178 

(low-intermediate level) 

1) รออะไรอยู่(คะ) rɔɔ à-rai yùu (ká) 

What are you waiting for? 

2) ไม่ต้องกังวล mâi-dtɔ̂ng gan-won 

Don't worry. / No need to worry.  

3) เป็นเรื่องปกติ bpen rʉ̂ang bpòk-gà-dtì 

It's common. / It's normal.  

Note: ปกติ (bpàk-gà-dtì) is an alternative pronunciation for this word.  

4) ฉันจะให้โอกาสอีกครั้ง chǎn jà hâi oo-gàat ìik kráng I'll give you another chance.  

5) ผมต้องดูแลเขา pǒm dtɔ̂ng duu-lɛɛ kǎo

I have to take care of her.  

6) ไม่ต้องรู้สึกผิดเลย/หรอก mâi-dtɔ̂ng rúu-sʉ̀k pìt ləəi/rɔ̀ɔk (You) don't need to feel guilty.  

7) กี่เดือนแล้ว gìi dʉan lɛ́ɛo 

How many months pregnant are you? OR How many months are you?  

8) มันไม่ใช่ความรัก man mâi-châi kwaam-rák 

It's not love. / That's not love. / It wasn't love.  

9) นอนพักเถอะ(นะคะ) nɔɔn pák tə̀ (ná-ká) 

(You) get some sleep/rest.  

10) คุณต้องตัดสินใจว่า... kun dtɔ̂ng dtàt-sǐn-jai wâa... You have to decide...  

11) ผมไม่เคยรู้มาก่อน(ว่า...) pǒm mâi-kəəi rúu maa-gɔ̀ɔn (wâa...) I never knew before (that...)  

12) ฉันยังไม่เต็มใจ... chǎn yang mâi dtem-jai... 

I'm not willing... / I'm reluctant to...  

13) ถ้า(คุณ)ไม่ได้ท... tâa (kun) mâi-dâi tam... 

If (you) didn't do it...  

14) (เดี๋ยว)ฉันเช็ดให้นะคะ (dǐao) chǎn chét hâi ná-ká I'll wipe it up (for you). / Let me dry you off. 

15) (เอา)วางไว้ (บนโต๊ะ) (ao) waang-wái (bon dtó) 

Leave it (on the table). / Put it (on the table). / Set it (on the table).  

16) ค่อยๆลุกขึ้น kɔ̂i-kɔ̂i lúk-kʉ̂n 

Get up slowly.  

17) หกหมดเลย hòk mòt ləəi 

It spilled all over (the place)! 

18) มันเสี่ยงมาก man sìang mâak 

It's very risky.  

19) แค่หยอก(เล่น) kɛ̂ɛ yɔ̀ɔk (lên) 

Just teasing! / Just joking!  

20) เขาไม่กล้า(หรอก) kǎo mâi-glâa (rɔ̀ɔk) He wouldn't dare! 

Super Useful Expressions, Part 179 (low-intermediate level) 

1) หายใจลึกๆ hǎai-jai lʉ́k-lʉ́k 

Breathe deeply. / Take a deep breath. 

2) ไม่รู้เรื่องอะไร(เลย) mâi-rúu-rʉ̂ang à-rai (ləəi) You don't know anything! / I don't know anything.  

3) ไม่มีใครช่วยได้ mâi-mii-krai chûai dâi No one can help me. / No one can help.  

4) ใครกัน BIT STRONG or SURPRISED krai gan Who? / Who's that?  

5) คุณมาตั้งแต่เมื่อไหร่ kun maa dtâng-dtɛ̀ɛ mʉ̂a-rài When did you get here?  

6) อย่าให้ใครรู้ yàa hâi krai rúu 

Don't let anyone know!  

7) บอกผมมาเลย INFORMAL bɔ̀ɔk pǒm maa ləəi Tell me. (urging) 

8) เขาเลือกไม่ได้ kǎo lʉ̂ak mâi-dâi

She had no choice. / She couldn't choose.  

9) ไม่อยากทำอย่างนี้ mâi yàak tam yàang-níi 

(I) don't wanna do this.  

10) (มัน)ไม่ได้แปลว่า... (man) mâi-dâi bplɛ̀ɛ-wâa... 

It/that doesn't mean...  

11) ว่าแต่... wâa-dtɛ̀ɛ... 

Speaking of... / By the way... / So...  

12) ไม่รู้หรือว่า... mâi rúu rʉ̌ʉ wâa... 

Don't you know (that)...?  

13) ฉันแปลกใจที่... chǎn bplɛ̀ɛk-jai tîi... 

I'm surprised that...  

14) เปลี่ยนใจทำไม / ทำไม(ถึง)เปลี่ยนใจ bplìan-jai tam-mai / tam-mai (tʉ̌ng)  bplìan-jai 

Why did you change your mind? 

15) ขออุ้มหน่อย(ได้ไหม) kɔ̌ɔ ûm nɔ̀i (dâi-mǎi) 

Can I hold him? / Can I hold her? (of a baby) 

16) ต้องเลี้ยงลูกคนเดียว dtɔ̂ng líang lûuk kon-diao 

(She) had to raise her son/daughter by herself.  

17) ผมไม่อยากอยู่ที่นี่แล้ว pǒm mâi-yàak yùu tîi-nîi lɛ́ɛo 

I don't wanna live/stay here anymore.  

18) ผมจะคุยกับเขาเอง pǒm jà kui-gàp kǎo eeng 

I'll talk to him myself.  

19) ต้องเตรียมไว้ก่อน dtɔ̂ng dtriam wái gɔ̀ɔn 

You gotta be prepared (in advance). / You should be ready.  

20) ต้องเลิกคิดถึงเธอ dtɔ̂ng lə̂ək kít-tʉ̌ng təə

(I) have to get her out of my head! / (I) have to stop thinking about her! 

Super Useful Expressions, Part 180 

(high-intermediate level) 

1) มีอะไรจะฝากไหม mii à-rai jà fàak mǎi 

Is there something you want me to tell him? / Do you want me to tell her anything? 

2) ไหนดูสิ nǎi duu sì 

(Well,) let me see. / Show me. / Let's see then.  

3) จะทำอะไรก็ทำเถอะ jà tam à-rai gɔ̂ɔ tam tə̀ 

Do whatever you want! / Do whatever you want to do!  

4) อย่างนั้นหรือ yàang-nán-rʉ̌ʉ 

Is that so? / Really?  

5) ต้องทำอย่างไรถึงจะ(รวย) dtɔ̂ng tam yàang-rai tʉ̌ng-jà (ruai) What do I have/need to do to (get rich)?  

6) ฉันไม่ได้หมายความว่าอย่างนั้น chǎn mâi-dâi mǎai-kwaam-wâa yàang-nán That's not what I meant. / I didn't mean it that way.  

7) เป็นแม่คนแล้ว IDIOMATIC bpen mɛ̂ɛ kon lɛ́ɛo 

She's a mother now. / I'm a mother now.  

8) จะตั้งชื่อลูกว่าอะไร jà dtâng-chʉ̂ʉ lûuk wâa à-rai 

What are you gonna name your baby?  

9) ไม่รอดแน่ mâi rɔ̂ɔt nɛ̂ɛ 

(He)'ll never make it! / (He) won't survive!  

10) ขอพูดตรงๆนะ... SEMI-FORMAL kɔ̌ɔ pûut dtrong-dtrong ná...

Let me be frank... / Let me speak frankly... / I'm going to be honest with you...  

11) โดยทั่วไปแล้ว... SEMI-FORMAL dooi-tûa-bpai-lɛ́ɛo... Generally... / Normally... / Ordinarily...  

12) ในทางตรงข้าม... FORMAL nai-taang-dtrong-kâam... On the other hand... / Whereas...  

13) ...เป็นเวลานาน ...bpen wee-laa-naan 

...for a long time. / ...for so long.  

14) ใครจะอาสา(...) krai jà aa-sǎa(...) 

Who will volunteer (to...)? 

15) ไม่มีสิทธิ์(ที่จะ)... mâi-mii-sìt (tii-jà)... 

You have no right (to)... !  

16) มีสิทธิ์อะไร... mii-sìt à-rai... 

What right do you have (to)... ?!  

17) ผมจะรีบไปรีบกลับ pǒm jà rîip bpai rîip glàp 

I'll hurry back. / I'll be back soon. / I'll be back as soon as possible. (go quickly  and return quickly) 

18) ผมไม่ได้เป็นคนเริ่มก่อน pǒm mâi-dâi bpen kon rə̂əm gɔ̀ɔn I'm not the one who started it. (the fight, argument, etc.) 

19) อาจจะมากกว่านั้น àat-jà mâak-gwàa nán 

Maybe more (than that). / Maybe longer (than that).  

20) ไม่ต้องพูดอะไรแล้ว mâi-dtɔ̂ng pûut à-rai lɛ́ɛo 

Don't say anything (more). / You don't need to say anything anymore. 

Super Useful Expressions, Part 181 

(high-intermediate level)

1) ต้องลุกขึ้นสู้ dtɔ̂ng lúk-kʉ̂n sûu 

(You) have to fight! / (You) have to stand up and fight! / (You) have to take a  stand! 

2) มันคนละเรื่อง man kon-lá-rʉ̂ang 

That's a different story. / That's another story. / It's a different matter.  

3) เรื่อง(มัน)เกิดขึ้นแล้ว rʉ̂ang (man) gə̀ət-kʉ̂n lɛ́ɛo 

It happened already. / It already happened.  

4) เขาเล่าให้ฟังทุกอย่าง(แล้ว) kǎo lâo-hâi-fang túk-yàang (lɛ́ɛo) He told me everything.  

5) ฉันเผลอหลับไป chǎn plə̌ə-làp bpai 

I dozed off. / I nodded off. / I fell asleep.  

6) ต้องสมัครสมาชิกก่อน dtɔ̂ng sà-màk sà-maa-chík gɔ̀ɔn You have to be a member (first). / You have to join first.  

7) เดี๋ยวตบปาก ANGRY dtǐao dtòp bpàak 

I'm gonna smack you!  

8) มันจะดีหรือไม่ man jà dii rʉ̌ʉ mâi 

Is that a good idea? / Would that be a good? (questioning or cannot make up  one's mind) 

9) ปากเสีย IRRITATED/ANGRY bpàak-sǐa 

You have a foul mouth! / You're offensive! / You're abusive!  

10) คุณทราบหรือเปล่าว่า... FORMAL kun sâap rʉ̌ʉ bplàao wâa... Did you know that... ?  

11) (...)จะไม่ดีกว่าหรือ(...) (...) jà mâi dii-gwàa rʉ̌ʉ (...) 

Wouldn't it be better (...?) / Isn't it better (...?) 

12) ถ้าเกิดอะไรขึ้น... tâa gə̀ət-à-rai-kʉ̂n 

If anything happens... / If something happens...  

13) เขากำลังถูกสอบสวน(...) kǎo gam-lang tùuk sɔ̀ɔp-sǔan(...) He's being investigated (...). / He's being questioned. 

14) เพียงแต่ว่า... SEMI-LITERARY piang-dtɛ̀ɛ-wâa... 

It's just that... 

15) (...)ไม่ใช่วิธีแก้ปัญหา (...) mâi-châi wí-tii gɛ̂ɛ bpan-hǎa It's not a solution. / (...) isn't the way to fix the problem.  

16) เป็นแผลเต็มตัว bpen-plɛ̌ɛ dtem-dtua 

He's got scratches all over his body. / She's bruised all over. / It's got scabs all  over.  

17) ฉันไม่อยาก(จะ)ได้อะไร chǎn mâi yàak (jà) dâi à-rai 

I don't want/need anything. (receive a birthday gift, etc.) 

18) เมื่อกี้(คุณ)ว่าอะไร(นะ) mʉ̂a-gîi (kun) wâa à-rai ná 

What did (you) just say?  

19) ผมก็อยากจะรู้เหมือนกัน pǒm gɔ̂ɔ yàak jà rúu mʉ̌an-gan I wanna know too.  

20) เราไม่คู่ควรกับเขา rao mâi kûu-kuan gàp kǎo 

I don't deserve him/her. 

Super Useful Expressions, Part 182 

(high-beginner level) 

1) เป็นผู้ชาย bpen pûu-chaai 

It's a boy!

2) เป็นผู้หญิง bpen pûu-yǐng 

It's a girl!  

3) อยากได้อะไร yàak dâi à-rai 

What do you want?/ What would you like?  

4) ร้องไห้ทำไม rɔ́ɔng-hâi tam-mai Why are you crying? 

5) จริงหรือ jing rʉ̌ʉ 

Really? / Is that true?  

6) ฉันท้องเสีย chǎn tɔ́ɔng-sǐa 

I have diarrhea. 

7) อย่าช้า yàa cháa 

Hurry. / Don't slow down! (we'll be late) 

8) ไปดูกัน(นะ) bpai duu gan (ná) Let's (go and) have a look. / Let's check it out.  

9) อันนี้(ก็)สวย an-níi (gɔ̂ɔ) sǔai This one's cute/nice/pretty.  

10) มีลูกแล้ว mii lûuk lɛ́ɛo 

I have children. / She has a son/daughter.  

11) ถ้าทำได้ tâa tam dâi 

If you can do it. / If he can.  

12) เห็นไหม hěn mǎi 

Do you see her? / Do you see it?  

13) มันไกลไป man glai bpai 

It's too far (away).  

14) ทำเสร็จแล้ว tam sèt lɛ́ɛo

I finished it. / It's done. / I did it already.  

15) ยังไม่เสร็จ yang mâi sèt 

Not yet. / I'm not finished yet. / It's not done yet.  

16) ทำได้ไหม tam dâi mǎi 

Can you do that? / Can you do it?  

17) รอผมหน่อยนะ rɔɔ pǒm nɔ̀i ná 

Wait for me. 

18) ฉันจำได้ chǎn jam dâi 

I remember (it).  

19) ผมอยู่คนเดียว pǒm yùu kon-diao 

I live alone. OR I'm alone.  

20) ผมดีใจมาก pǒm dii-jai mâak 

I'm so happy. / I was so glad.  

Super Useful Expressions, Part 183 (low-intermediate level) 

1) ฉันไม่อยากบังคับคุณ chǎn mâi yaak bang-káp kun I don't wanna force/pressure you. 

2) มีธุระอะไร(หรือ)ครับ mii tú-rá à-rai rʉ̌ʉ kráp What brings you here? / What can I do for you? / May I help you?  (Literally= What business do you have?) 

3) ทำไม(ถึง)กลับมา tam-mai (tʉ̌ng) glàp-maa 

Why did you come back? / Why are you back?  

4) เขาเป็นเจ้าของ(...) kǎo bpen jâo-kɔ̌ɔng (...) 

She's the owner. / She owns (this place). 

5) ทำไมไม่ฟัง(ผม) tam-mai mâi fang (pǒm) Why didn't you listen (to me)? / Why don't you listen (to me)?  

6) ผมพลาดไป(แล้ว) pǒm plâat bpai (lɛ́ɛo) 

I made a mistake. / I screwed up. / I failed. / I missed it.  

7) เขาว่าอะไร(บ้าง) kǎo wâa à-rai (bâang) 

What did she say?  

8) ทนหน่อยนะ ton nɔ̀i ná 

Hang on. / Hang in there. / Be patient. / Just a bit longer. 

9) ยังเจ็บอยู่หรือ yang jèp yùu rʉ̌ʉ 

Does it still hurt?  

10) ไม่เจ็บแล้ว mâi jèp lɛ́ɛo 

It doesn't hurt anymore.  

11) (พ่อ)ชอบบอกว่า... (pɔ̂ɔ) chɔ̂ɔp bɔ̀ɔk wâa... (Dad) always says... / (Dad) always told me...  

12) ...(ก็)คงจะดี ...(gɔ̂ɔ) kong jà dii 

...would be nice/good.  

13) ผมอยากให้คุณรู้ว่า... pǒm yàak hâi kun rúu wâa... I want you to know (that)...  

14) มันไม่ช่วยอะไร(หรอก) man mâi chûai à-rai (rɔ̀ɔk) It/that doesn't help (at all). / It doesn't solve anything. 

15) อย่าไปไกลนะ yàa bpai glai ná 

Don't go too far. (talking to a child playing outside) 

16) มันสำคัญกว่า(เงิน) man sǎm-kam gwàa (ngən) It's more important than (money). 

17) เล่าให้(ผม)ฟังสิ lâo-hâi (pǒm) -fang sì 

Tell (me) about it.  

18) เขาไม่ได้อยู่ที่นี่ kǎo mâi-dâi yùu tîi-nîi 

He's not here.  

19) คุณไม่รู้อะไร(เลย/หรอก) STRONG kun mâi rúu à-rai (ləəi/rɔ̀ɔk) You don't know anything! / You have no idea!  

20) ทำไม(ถึง)น่ารักขนาดนี้ tam-mai (tʉ̌ng) nâa-rák kà-nàat-níi Why are you so cute?! / Why is she so (damn) cute?! 

Super Useful Expressions, Part 184 

(low-intermediate level) 

1) เจ็บทั้งตัว(เลย) jèp táng dtua (ləəi) 

It hurts all over! / It hurts everywhere! 

2) ผมจะไม่ยอมแพ้ pǒm jà mâi yɔɔm-pɛ́ɛ 

I won't give up! 

3) ห้ามเข้า hâam kâo 

No entry. / Keep out. / No admittance.  

4) (เรา)กำลังจะไปเยี่ยม(ญาติ) (rao) gam-lang jà bpai yîam (yâat) (We)'re going to visit (relatives).  

5) ต้องตั้งสมาธิ dtɔ̂ng dtâng-sà-maa-tí 

You have to concentrate. / Focus.  

6) อยู่เฉยๆไม่ได้ yùu chə̌əi-chə̌əi mâi-dâi 

(I) can't just (sit around and) do nothing! / (I) can't stay still. 7) ผมจะไปตามหาเขา pǒm jà bpai dtaam-hǎa kǎo

I'll go look/search for him.  

8) ยังมีเวลาอีกเยอะ yang mii wee-laa ìik yə́ 

There's still a lot of time. / We still have plenty of time.  

9) ผมเจอนี้ pǒm jəə níi 

I found this. (showing the thing he found) 

10) พอเขารู้(ว่า)... pɔɔ kǎo rúu (wâa)... 

When he found out...  

11) ฉัน(ก็)บอกเขาแล้ว(ว่า...) chǎn (gɔ̂ɔ) bɔ̀ɔk kǎo lɛ́ɛo (wâa...) I told him already (that...)  

12) ผมจะรอวันที่... pǒm jà rɔɔ wan tîi... 

I'll wait for the day (that)... / I look forward to...  

13) นี้เป็นครั้งสุดท้าย(ที่...) níi bpen kráng sùt-táai (tîi...) This is the last time (that...)  

14) ฉันมีเหตุผลที่ดีที่... chǎn mii hèet-pǒn tîi dii tîi... I have (a) good reason to... 

15) ไปไม่ได้จริงๆ bpai mâi-dâi jing-jing 

I really can't go. 

16) ผมเห็นใจคุณ pǒm hěn-jai kun 

I sympathize with you. / I feel for you.  

17) ไม่มีวัน STRONG mâi-mii-wan 

Never! / Ever!  

18) ไปกินอะไรมา bpai gin à-rai maa 

What did you eat? / What did you have to eat?  

19) ฉันหลับไม่ลง chǎn làp mâi-long 

I can't sleep. / I can't fall asleep. 

20) ผมรู้ทุกอย่าง pǒm rúu túk-yàang 

I know everything. / I knew everything.  

Super Useful Expressions, Part 185 

(low-advanced level) 

1) ข้ามศพฉันไปก่อน kâam sòp chǎn bpai gɔ̀ɔn 

Over my dead body! / You'll have to kill me first! 

2) เป็นอย่างที่ผมคิดไว้จริงๆ bpen yàang-tîi pǒm kít wái jing-jing It's just like I thought (all along)!  

3) ไม่เห็นจะ(ดี)ตรงไหน mâi-hěn-jà (dii) dtrong-nǎi 

I don't think it's so (good). / I don't see it as being (good). / It's not good (to me). 

4) เราทุกคนต่าง(มีจุดแข็งและจุดอ่อน) rao-túk-kon-dtàang (mii jùt-kɛ̌ng lɛ́ jùt ɔ̀ɔn) 

We all (have our strong and weak points.) / All of us (have strengths and  weaknesses).  

5) แน่จริงก็เข้ามา(สิ/ดิ) ANGRY/IDIOMATIC nɛ̂ɛ jing gɔ̂ɔ kâo-maa (sì/dì) Bring it on! / Come on! / Come on and get me! / Make a move! (said when  challenging someone and has the feeling of 'If you dare...') 

6) ไอ้เด็กเวร VULGAR âi dèk ween 

Little shit! / Little punk! (said when very angry to a bad boy) 

7) คุณไม่รู้ว่ากำลังเล่นอยู่กับใคร ANGRY kun mâi-rúu wâa gam-lang lên yùu  gàp krai 

You don't know who you're dealing with!  

8) (ทำให้เธอยิ่งหลงรักเขา)มากขึ้นไปอีก (tam-hâi təə yîng lǒng-rák kǎo)  mâak-kʉ̂n-bpai-ìik

(It made her love him) even more/all the more.  

9) อย่ามาขึ้นเสียงกับกู ANGRY yàa maa kʉ̂n-sǐang-gàp guu Don't raise your voice with me! / Don't yell at me!  

10) (...)เพื่อเป็นการตอบแทน(...) (...)pʉ̂a bpen gaan-dtɔ̀ɔp-tɛɛn(...) (...) in return. / In return, (...) / (...) as a reward. / As a reward, (...) / (...) to pay  you back. / To pay you back, (...)  

11) ไม่(แตก)ต่างอะไรกับ... mâi(dtɛ̀ɛk)dtàang à-rai gàp... 

(It)'s no different than/from...  

12) นั่น(ก็)เท่ากับว่า... nân (gɔ̂ɔ) tâo-gàp wâa... 

That means... / That is equal to... / That's the same as...  

13) (...)จนถึงทุกวันนี้(...) LITERARY (...)jon-tʉ̌ng túk-wan-níi(...) (Even) to this day... / Even today...  

14) เขาถูกกล่าวหาว่า(ฆ่าภรรยากับชู้) kǎo tùuk glàao-hǎa wâa (kâa pan-rá yaa gàp chúu) 

He was accused of (killing his wife and her lover). 

15) แปลกอยู่อย่างหนึ่ง bplɛ̀ɛk yùu yàang-nʉ̀ng 

There's something that's strange. / Something is (a bit) odd.  

16) (เป็น)เขตหวงห้าม FORMAL (bpen) kèet hǔang-hâam (It's a) restricted area.  

17) แต่เพียงผู้เดียว SEMI-LITERARY dtɛ̀ɛ piang pûu-diao 

(The) one and only. / He and he alone. 

18) ไม่ยอมรับความจริงเลย mâi yɔɔm-ráp kwaam-jing ləəi 

(He) won't accept the truth. / (He) doesn't face the facts. / (He)'s in denial.  

19) ไอ้(คน)เนรคุณ STRONG/VULGAR âi (kon) nee-rá-kun You ungrateful son-of-a-bitch! 

20) จะเป็นอะไรไป STRONG/ANGRY jà bpen-à-rai bpai What's wrong with that?! / What's the matter with that?! 

Super Useful Expressions, Part 186 

(low-intermediate level) 

1) ผมจำได้ดี pǒm jam dâi dii 

I remember it well. 

2) (มัน)ไม่ใช่ความผิดของคุณ (man) mâi-châi kwaam-pìt kɔ̌ɔng-kun It's not your fault.  

3) หมายถึงใคร mǎai-tʉ̌ng krai 

Who do you mean? / Who are you talking about?  

4) ลงมาเดี๋ยวนี้(เลย) long maa dǐao-níi (ləəi) 

Come down right now! (situation: boy climbed up a tree, etc.) 

5) ไม่ใช่ของผม mâi-châi kɔ̌ɔng pǒm 

It's not mine. / That's not mine.  

6) ถูกขโมยไป(แล้ว) tùuk kà-mooi bpai (lɛ́ɛo) 

It was stolen.  

7) เมื่อคืน(ผม)ฝันร้าย mʉ̂a-kʉʉn (pǒm) fǎn-ráai 

Last night I had a nightmare. / I had a bad dream last night.  

8) แต่งตัวเสร็จหรือยัง dtɛ̀ng-dtua sèt-rʉ̌ʉ-yang 

Are you done getting dressed yet? / Are you dressed yet?  

9) ไม่เจอเลย mâi jəə ləəi 

I haven't seen her. / I haven't found it. 

10) ไปเจอที่ไหน bpai jəə tîi-nǎi 

Where did you find her/it? 

11) ผมแค่ไม่เข้าใจว่า... pǒm kɛ̂ɛ mâi-kâo-jai wâa... I just don't understand...  

12) มีสิทธิ์อะไร(...) mii sìt à-rai (...) 

What right do (you) have (...)?  

13) ฉันไม่ดีพอ(...) chǎn mâi dii pɔɔ (...) 

I'm not good enough (...)  

14) ชอบใครมากกว่ากัน chɔ̂ɔp krai mâak-gwàa gan Who do you like better? / Who do you prefer? 

15) พวกเขาทำอะไรกัน pûak-kǎo tam à-rai gan What are they doing?  

16) ฉันรู้ว่ามันยาก chǎn rúu-wâa man yâak 

I know it's hard.  

17) ฉันจะเล่าให้ฟัง chǎn jà lâo-hâi-fang 

I'll tell you (about it). 

18) ทำไมต้องโกรธด้วย tam-mai dtɔ̂ng gròot dûai Why are you angry? / Why are you angry at me?  

19) เขาไม่สงสัยอะไร(เลย) kǎo mâi sǒng-sǎi à-rai (ləəi) She didn't suspect anything (at all). / He doesn't suspect anything.  

20) ขอกอดหน่อย kɔ̌ɔ gɔ̀ɔt nɔ̀i 

May/Can I have a hug? 

Super Useful Expressions, Part 187 (high-intermediate level) 

1) กินรองท้อง(ก่อน) gin rɔɔng-tɔ́ɔng (gɔ̀ɔn)

Have a bite to eat. / Have a snack to tide you over. 

2) มันต่างกันอย่างไร man dtàang-gan yàang-rai 

How are they different? / What's the difference?  

3) เพิ่งเจอกันวันแรก pə̂ng jəə-gan wan rɛ̂ɛk 

(We) only just met today. / We just met each other today. 

4) (จัด)เรียงตามตัวอักษร (jat)-riang dtaam-dtua-àk-sɔ̌ɔn (It)'s arranged alphabetically. / (It)'s in alphabetical order.  

5) โปรดเมตตาด้วย LITERARY bpròot mêet-dtaa dûai 

Be merciful to me! / Please have mercy!  

6) ไม่เหลือใครแล้ว mâi-lʉ̌a krai lɛ́ɛo 

I have no one left! (I'm all alone now.) 

7) ปาฏิหาริย์มีจริง bpaa-dtì-hǎan mii jing 

Miracles happen. / Miracles are for real.  

8) มันยังน้อยไป man yang nɔ́ɔi bpai 

That's an understatement! / It's not enough!  

9) ต้องโดนตี dtɔ̂ng doon dii 

(You) need to be spanked!  

10) รู้แต่ว่า... rúu dtɛ̀ɛ wâa... 

All I know is... / I only know that...  

11) ฉันได้ยินเขาพูดว่า... chǎn dâi-yin kǎo pûut wâa... 

I heard her say (that)...  

12) ถ้า(เขา)เป็นอะไรไป... tâa (kǎo) bpen à-rai bpai... 

If something happens to him... / If something happened to him... / If anything  should happen to her...  

13) เห็นด้วยหรือไม่เห็นด้วยกับ... hěn-dûai rʉ̌ʉ mâi-hěn-dûai gàp...

Agree or disagree with...  

14) ยังสามารถใช้... FORMAL yang sǎa-mâat chái... 

(You) can also use (it with)... 

15) เขาไม่เคยลืมเลย kǎo mâi-kəəi lʉʉm ləəi 

He's never forgotten. / He never forgets.  

16) เงียบจังเลย ไปไหนกันหมด ngîap jang ləəi bpai-nǎi gan mòt It's so quiet---where is everyone?!  

17) (แล้ว)จะเอา(ยัง)ไงต่อ (lɛ́ɛo) jà ao (yang) ngai dtɔ̀ɔ 

What are you gonna do next?  

18) (มันร้อน)เหมือนจะระเบิด (man rɔ́ɔn) mʉ̌an jà rá-bə̀ət (I'm so hot I feel) like I'm gonna explode.  

19) ใครๆก็เรียกว่า(ลุง) krai-krai gɔ̂ɔ rîak wâa (lung) 

Everyone calls him ('Uncle').  

20) แน่นอนอยู่แล้ว nɛ̂ɛ-nɔɔn yùu lɛ́ɛo 

Absolutely! / Of course! 

Super Useful Expressions, Part 188 

(low-intermediate level) 

1) ฉัน(จะ)ไปเป็นเพื่อน chǎn (jà) bpai bpen-pʉ̂an 

I'll go with you. / I'll go with you and keep you company. / I'll accompany you. 

2) กำลังแต่งตัวอยู่ gam-lang dtɛ̀ng-dtua yùu 

She's getting dressed.  

3) ยังไม่เสร็จหรือ yang mâi sèt rʉ̌ʉ 

Aren't you finished yet? / Isn't it done yet? 

4) อยู่ด้วยกันไม่ได้ yùu dûai-gan mâi-dâi 

We can't live together. / We can't be together.  

5) จะไปทำอะไรที่นั่น jà bpai tam à-rai tîi nân What are (you) gonna do there?  

6) ผม(จะ)ไม่ยอมแพ้ pǒm (jà) mâi yɔɔm-pɛ́ɛ I won't give up! / I'm not giving up!  

7) เธอไม่ยอมช่วย təə mâi yɔɔm chûai 

She wouldn't help me. / She won't help me.  

8) ผมเป็นญาติ(ของ)เขา pǒm bpen yâat (kɔ̌ɔng) kǎo I'm his relative. / I'm related to her.  

9) ห้ามเข้าใกล้ hâam kâo glâi 

Don't come near. / Keep away.  

10) ผมมีแผนที่จะ... pǒm mii pɛ̌ɛn tîi-jà... 

I have a plan to... / I had a plan to...  

11) ฟังดูเหมือน... fang duu mʉ̌an... 

It sounds like... / You sound like...  

12) (ขอ)ถามได้ไหม(คะ)... (kɔ̌ɔ) tǎam dâi-mǎi (ká)... May I ask...? / Can I ask...?  

13) ถ้าใครโทรมา... tâa krai too-maa... 

If anyone calls...  

14) อย่า(มา)ดูถูกฉัน yàa (maa) duu-tùuk chǎn Don't look down on me! / Don't belittle me! / Don't insult me! 

15) (แฟน)ไม่เคยบอกรัก (fɛɛn) mâi-kəəi bɔ̀ɔk rák (My boyfriend) has never told me he loves me. 

16) นั่นยังไม่ดีพอ nân yang mâi dii-pɔɔ 

That's (still) not good enough.  

17) ไม่มีเหตุผลที่ดี mâi-mii hèet-pǒn tîi dii 

There's no good reason. / There isn't a good reason.  

18) คุณไม่ควรท kun mâi-kuan tam 

You shouldn't (do it).  

19) มันสายไปแล้ว man sǎai bpai lɛ́ɛo 

It's too late.  

20) ผมเป็นกำลังใจให้ pǒm bpen-gam-lang-jai-hâi 

I'm rooting for you! / I'm behind you all the way. / You have my full support. / You have my backing.  

Super Useful Expressions, Part 189 

(high-intermediate level) 

1) มันคงเป็นกรรมของฉัน man kong bpen gam kɔ̌ɔng chǎn It must be my fate/karma. 

2) ขอโทษแทนเขาด้วย(ครับ) kɔ̌ɔ-tôot tɛɛn kǎo dûai (kràp) I apologize for her. / I apologize on her behalf.  

3) เขามอบตัวแล้ว kǎo mɔ̂ɔp-dtua lɛ́ɛo 

He turned himself in. / He surrendered.  

4) ฉันจะไปตาม(หมอ) chǎn jà bpai dtaam (mɔ̌ɔ) 

I'll go and find (a doctor). / I'll go and see if I can find (a doctor).  

5) เขาพลัดตกลงไปใน(คลองน้ำเสียชีวิต) kǎo plát dtòk-long bpai nai (klɔɔng nám sǐa-chii-wít) 

He slipped and fell into (the canal and died). / He tripped and fell into (the canal  and died). 

6) หลบไป STRONG lòp bpai 

Move! / Get out of my way! / Move over!  

7) ลูกแม่ lûuk mɛ̂ɛ 

My son! / My boy! (from a mother's perspective) 

8) ลูกพ่อ lûuk pɔ̂ɔ 

My son! / My boy! (from a father's perspective) 

9) ก้าวไปข้างหน้า gâao bpai kâang-nâa 

Move forward. / Get ahead.  

10) จะเห็นได้ว่า... SEMI-FORMAL jà hěn dâi wâa... You can see (that)... / It can be seen (that)...  

11) เป็นวิธีเดียวที่จะ... bpen wí-tii diao tîi-jà... 

It's the only way to...  

12) มัน(ก็)คล้ายกับ... man (gɔ̂ɔ) kláai-gàp... 

It's similar to... / It's like...  

13) ไม่ว่าจะทำอะไร... mâi-wâa jà tam à-rai... 

No matter what (you) do...  

14) เราควรให้เวลา... rao kuan hâi wee-laa 

We should give (her) some time... 

15) อดเป็นห่วง(เขา)ไม่ได้ òt bpen-hùang (kǎo) mâi-dâi I can't help but worry (about him). / I couldn't help worrying (about him).  

16) ฉันเป็นพยานได้ chǎn bpen pá-yaan dâi 

I can attest to that. / I witnessed it.  

17) ค่อยๆคุยกัน kɔ̂i-kɔ̂i kui gan 

Let's settle down and talk calmly. / Let's talk it over calmly. 

18) ชอบแบบไหนมากกว่ากัน chɔ̂ɔp bɛ̀ɛp-nǎi mâak-gwàa gan Which one do you prefer? / Which style do you like better?  

19) ใครก็ได้ช่วยที krai-gɔ̂ɔ-dâi chûai tii 

Someone help me! / Help me---anyone!  

20) ผมเสียใจกับสิ่งที่เกิดขึ้น pǒm sǐa-jai gàp sìng tîi gə̀ət-kʉ̂n I'm sorry for what happened. 

Super Useful Expressions, Part 190 

(high-intermediate level) 

1) ไม่มีทางอื่นแล้วหรือ mâi-mii taang ʉ̀ʉn lɛ́ɛo rʉ̌ʉ 

Is there no other way? / Isn't there any other way? 

2) ทำดีกับผู้อื่น tam-dii-gàp pûu-ʉ̀ʉn 

Be nice/good to others.  

3) คุณรู้ว่าคุณกำลังทำอะไรอยู่ kun rúu-wâa kun gam-lang tam à-rai yùu Do you know what you're doing?  

4) (อยู่กับเพื่อน)มีความสุขที่สุดแล้ว (yùu gàp pʉ̂an) mii-kwaam-sùk tîi-sùt lɛ́ɛo I'm so happy (when I'm with my friends). / I'm the happiest (when I'm with my  friends).  

5) ผมไม่เก่งขนาดนั้น pǒm mâi gèng kà-nàat-nán 

I'm not that good/skilled.  

6) เราควรจะทำอย่างไร(ดี) SEMI-FORMAL/LITERARY rao kuan-jà tam  yàang-rai (dii) 

What should I do? / What am I supposed to do?  

7) เราไม่อดตาย rao mâi òt-dtaai 

We're not gonna starve to death. / We're not starving to death. 8) มาอยู่เป็นเพื่อน maa yùu bpen-pʉ̂an

I've come to keep you company.  

9) ผมจะขึ้นไปข้างบน pǒm jà kʉ̂n bpai kâang-bon 

I'm gonna go upstairs.  

10) กำลังมุ่งหน้าไป(ยัง/ที่)... gam-lang mûng-nâa bpai (yang/tîi)... (They)'re heading for/to...  

11) (...)จะไม่เสียเปล่า (...) jà mâi sǐa-bplàao 

(It) won't be wasted. / (It) won't be a waste. / (It) won't be in vain.  

12) ใครอนุญาตให้... krai à-nú-yâat hâi... 

Who authorized you to... ? / Who gave you permission to... ?  

13) เขาหันมาหา(ฉัน)แล้วถามว่า... kǎo hǎn-maa-hǎa (chǎn) lɛ́ɛo tǎam wâa... He turned to me and asked...  

14) อย่ามากล่าวหา(ว่า...) yàa maa glàao-hǎa (wâa...) 

Don't accuse me (of...) 

15) (เรา)หลงเข้าไปในป่า (rao) lǒng kâo-bpai nai bpàa 

(We) got lost in the forest/woods. 

16) ผมจะให้โอกาส(คุณ)เป็นครั้งสุดท้าย pǒm jà hâi-oo-gàat (kun) bpen  kráng-sùt-táai 

I'm going to give (you) one last chance.  

17) โดนไล่ออก(มา) doon lâi-ɔ̀ɔk (maa) 

He was kicked out. / He was fired.  

18) ฉัน(จะ)ไปเอาเอง chǎn (jà) bpai ao eeng 

I'll get it (myself). / I'll go get it (myself).  

19) หนูจะพยายามเข้มแข็ง nǔu jà pá-yaa-yaam kêm-kɛ̌ng I'll try to be strong.  

20) บ้าไปใหญ่แล้ว STRONG bâa bpai yài lɛ́ɛo

That's insane! / This is crazy! / You're crazy! 

Super Useful Expressions, Part 191 

(high-intermediate level) 

1) เราไม่ได้เจอกันกี่(ปี)แล้ว rao mâi-dâi jəə gan gìi (bpii) lɛ́ɛo How many (years) has it been since we've seen each other? 

2) เราคบกันมาห้าปีแล้ว rao kóp-gan maa (hâa) bpii lɛ́ɛo We've been dating/seeing each other for (5 years).  

3) รายได้ดีไหมคะ raai-dâai dii mǎi ká 

Is the income/pay good?  

4) (ขาย)ให้เท่าไหร่ INFORMAL (kǎai) hâi tâo-rài 

How much do you want for it? (shopping on the street) 

5) เหล้าไม่กิน บุหรี่ไม่สูบ lâo mâi gin bù-rìi mâi sùup 

(He) doesn't drink or smoke.  

Note: เหล้า (lâo) and บุหรี่ (bù-rìi) are moved to the front for emphasis here.  This is also commonly said as ไม่กินเหล้า ไม่สูบบุหรี่ (mâi gin lâo mâi gin bù-rìi). 

6) เคยไปแค่ครั้งเดียว kəəi bpai kɛ̂ɛ kráng-diao 

I've only been once (before).  

7) ทำไมถึงอยากอยู่(เมืองไทย) tam-mai tʉ̌ng yàak yùu (mʉang-tai) Why do/did you want to live in (Thailand)?  

8) ทำเองกับมือ tam-eeng gàp mʉʉ 

I made it myself. / I made it with my own two hands. / It's homemade.  

9) ผมเชื่อว่าอย่างนั้น pǒm chʉ̂a wâa yàang-nán 

I believe so. / I believe that('s the case). 

10) จะให้ไปส่งไหม jà hâi bpai sòng mǎi 

Do you want me to take you home/give you a lift/drop you off/see you off?  

11) เล่าให้ฟังหน่อยได้ไหม(คะ) lâo-hâi-fang nɔ̀i dâi-mǎi (ká) Can you tell me (about it)?  

12) ไม่คิดว่าจะเจอพี่ mâi kít wâa jà jəə pîi 

I didn't expect to see you. / I didn't think I would meet you (here). 

13) จะโมโหทำไม jà moo-hǒo tam-mai 

Why are you so mad? / Why did you get so pissed off?  

14) เรามาถูกทางหรือเปล่า rao maa tùuk taang rʉ̌ʉ-bplàao Are we going the right way? (possibly lost, driving, etc.) 

15) ผมว่าเรากลับดีกว่า pǒm wâa rao glàp dii-gwàa 

I think we should go back/return.  

16) มันน่าจะอยู่แถว(ๆ)นี้ man nâa-jà yùu tɛ̌ɛo(tɛ̌ɛo) níi It should be around/near here. 

17) ต้องเสียค่าธรรมเนียม dtɔ̂ng sǐa kâa-tam-niam 

You have to pay a fee/charge. (for a credit card, etc.) 

18) มันเป็นชะตากรรมของฉัน man bpen chá-dtaa-gam kɔ̌ɔng chǎn It's my fate/destiny.  

19) ฉันจะไม่ไปไหนทั้งนั้น EMPHASIS chǎn jà mâi bpai nǎi táng-nán I'm not going anywhere!  

20) ผมเพิ่งรู้ pǒm pə̂ng rúu 

I just (now) found out. / That's news to me!  

Super Useful Expressions, Part 192 

(high-beginner level)

1) มันสะดวก man sà-dùak 

It's convenient. 

2) จำเป็นไหม jam-bpen mǎi 

Is it/that necessary?  

3) มันเป็นของคุณ man bpen kɔ̌ɔng kun 

It's yours.  

4) มันไม่ใช่ของผม man mâi-châi kɔ̌ɔng pom 

It's not mine.  

5) ขอบคุณสำหรับ(เค้ก)(นะคะ) kɔ̀ɔp-kun sǎm-ràp (kéek) (ná-ká) Thank you for (the cake).  

6) ไม่มีคนเลย mâi-mii kon ləəi 

There's no one (here) at all. / It's dead.  

7) มีลูกกี่คนครับ mii lûuk gìi kon kráp 

How many kids/children do you have?  

8) เราเป็นเพื่อนบ้าน(กัน) rao bpen pʉ̂an-bâan (gan) We're neighbors.  

9) มีแมว(สอง)ตัว mii mɛɛo (sɔ̌ɔng) dtua 

(I) have (two) cats.  

10) เลี้ยวขวา líao kwǎa 

Turn right.  

11) เลี้ยวซ้าย líao sáai 

Turn left.  

12) อยู่(ตรง)หัวมุม yùu (dtrong) hǔa-mum 

It's (right) on the corner. 

13) (...)อยู่ซ้ายมือ (...) yùu sáai-mʉʉ 

It's on the left. / It's to the left.  

14) (...)อยู่ขวามือ (...) yùu kwǎa-mʉʉ 

It's on the right. / It's to the right. 

15) (กำลัง)ขับรถอยู่ (gam-lang) kàp-rót yùu 

(I)'m driving.  

16) นั่งรถเมล์มา nâng rót-mee maa 

(I) came by bus. / (I) took the bus.  

17) เบื่อไหม(คะ) bʉ̀a mǎi (ká) 

Do you get bored? / Are you bored?  

18) รอฉันก่อน(นะ) rɔɔ chǎn gɔ̀ɔn (ná) 

Wait for me.  

19) ไม่ร้อนหรือ(คะ) mâi rɔ́ɔn rʉ̌ʉ (ká) 

Aren't you hot?  

20) ยังไม่เปิด yang mâi bpə̀ət 

It's not open yet. / We're not open yet. 

Super Useful Expressions, Part 193 (high-intermediate level) 

1) เจอกันได้ยังไงคะ jəə gan dâi yang-ngai ká 

How did you (two) meet? 

2) เรื่องจิ๊บๆ rʉ̂ang jíp-jíp 

It's no biggie. / It's no big deal. / It's really easy. / It's a piece of cake.  

3) พ่อแม่เสียหมดแล้ว pɔ̂ɔ-mɛ̂ɛ sǐa mòt lɛ́ɛo 

His parents passed away. / She lost both of her parents. 

4) แก่กว่าฉันอีก INFORMAL gɛ̀ɛ-gwàa chǎn ìik 

(He)'s older than me. / (He)'s even older than I am.  

5) ไม่ได้เจอมาเป็น(เดือน)แล้ว mâi-dâi jəə maa bpen (dʉan) lɛ́ɛo I haven't seen one/him for (a month). / It's been (a month) since I've seen one/him.  

6) ผู้ที่ได้รับผลกระทบ(จาก COVID-19) pûu tîi dâi-ráp-pǒn-grà-tóp (jàak koo-wít síp-gâao) 

Those affected/impacted (by Covid-19).  

7) ได้รับผลกระทบไหม(ครับ) dâi-ráp-pǒn-grà-tóp mǎi (kráp) Where you impacted/affected?  

8) คุณเป็นเจ้าของที่นี่หรือ kun bpen jâo-kɔ̌ɔng tîi-nîi rʉ̌ʉ 

Do you own this place? / Are you the owner of this place? / Are you the owner?  

9) ไม่สบายรึเปล่า mâi-sà-baai rʉ́-bplàao 

Are you sick (or what)? / Is she okay?  

10) คุณไม่เคยสนใจฉัน kun mâi-kəəi sǒn-jai chǎn 

You never pay any attention to me. / You never show any interest in me.  

11) อีกสักพักจะกลับ(แล้ว) ìik-sàk-pák jà glàp (lɛ́ɛo) 

(I)'m gonna go back (home) in a bit. / In a bit, (I)'m gonna go back.  

12) ต้องเก็บรักษาไว้ให้ดี dtɔ̂ng gèp-rák-sǎa wái hâi-dii 

You have to take good care of it. (an old antique, a password, etc.) 

13) คงไม่ใช่แล้ว(ล่ะ) kong mâi-châi lɛ́ɛo (lâ) 

Probably not. / I guess not. / Not anymore. (said when something turns out  differently than one had expected) 

14) อายุเยอะแล้ว aa-yú yə́ lɛ́ɛo 

(He)'s old (already).

15) เมื่อไหร่จะเลิกเรียกผมว่า(ลุง)(ซักที) MILDLY IRRITATED mʉ̂a-rài jà lə̂ək rîak pǒm wâa (lung) (sák-tii) 

When are you gonna stop calling me 'Uncle'?!  

16) ผม(ไป)พูดตอนไหน IRRITATED/ROUGH pǒm (bpai) pûut dtɔɔn-nǎi When did I say that?!  

17) เชิญด้านในได้เลยค่ะ FORMAL chəən dâan-nǎi dâi ləəi kà Come in and have a look. / Welcome---have a look inside.  

18) (ราคา)ถูกเหลือเชื่อ (raa-kaa) tùuk lʉ̌a-chʉ̂a 

It's incredibly cheap! / The price is unbelievably low!  

19) จะหาเงินจากไหน INFORMAL jà hǎa ngən jàak nǎi 

Where am I gonna get the money?!  

20) ผมทำงานอยู่ต่างจังหวัด pǒm tam-ngaan yùu dtàang-jang-wàt I work in another province. 

Super Useful Expressions, Part 194 

(low-intermediate level) 

1) บรรยากาศดี ban-yaa-gàat dii 

Nice atmosphere! / The atmosphere is nice. 

2) ฉันไม่ดื่มเหล้า chǎn mâi dʉ̀ʉm lâo 

I don't drink.  

3) ยังแข็งแรง yang kɛ̀ng-rɛɛng 

It's still in good shape. / He's still strong.  

4) เหมือนเดิมครับ mʉ̌an-dəəm kráp 

I'll have the same as usual. / Same as before. / My usual. (when ordering your  regular coffee order at a cafe, etc.)

5) คุณจะรออะไร kun jà rəə à-rai 

What are you waiting for?! (suggesting you should go ahead and do it) 

6) ต้องรีบตัดสินใจ dtɔ̂ng rîip dtàt-sǐn-jai 

We have to hurry and make a decision. / I have to hurry and decide.  

7) สถานีนี้ชื่อ(ว่า)อะไร sà-tǎan-nii níi chʉ̂ʉ (wâa) à-rai What's the name of this (train) station?  

8) ขอกอดทีหนึ่ง kɔ̌ɔ gɔ̀ɔ tii nʉ̀ng 

Give me a hug.  

9) ยังไม่ได้บอก(เลย) yang mâi-dâi bɔ̀ɔk (ləəi) 

I haven't told you yet. / He didn't tell me yet.  

10) ใช้เวลาแค่(5 นาที)(เอง) chái-wee-laa kɛ̂ɛ (hâa naa-tii) (eeng) It only takes (5 minutes). / It takes just (5 minutes).  

11) ขอดูอันนี้ได้ไหมครับ kɔ̌ɔ duu an-níi dâi-mǎi kráp May I see this one? / Can I see this one? (at a store) 

12) หน้าคุ้นจัง nâa kún jang 

She sure looks familiar!  

13) (มัน)กว้างใหญ่มาก (man) gwâang-yài mâak 

It's immense/vast/massive/enormous.  

14) แค่นั้นเองเหรอ kɛ̂ɛ-nán eeng rə̌ə 

Is that it? / Is that all? 

15) จับไว้ jàp wái 

Grab it. / Hold it. / Hold him. / Hold on.  

16) เกิดวันที่เท่าไหร่ gə̀ət wan-tîi tâo-rài 

What day were you born on? 

17) ไม่ค่อยมีนะ mâi-kɔ̂i mii ná 

Not so much. / Not so many.  

18) ผมโกรธตัวเอง pǒm gròot dtua-eeng 

I'm angry at/with myself.  

19) ฉันหลงทาง chǎn lǒng-taang 

I'm lost. / I've lost my way.  

20) น่าทานมาก(ค่ะ) SEMI-FORMAL nâa-taan mâak (kà) It/that looks really delicious! 

Super Useful Expressions, Part 195 

(low-advanced level) 

1) เดี๋ยวตบปาก ANGRY dǐao dtòp bpàak 

I'm gonna smack you! 

2) อย่าอยู่ห่าง(จาก)(ฉัน) yàa yùu hàang (jàak) chǎn 

Stay close to (me).  

3) (ผม)อยู่ในเหตุการณ์ SEMI-FORMAL (pǒm) yùu nai hèet-gaan (I) was at the scene. / (I) was there when it happened. / (I) was there when the  incident took place. 

4) ต้องคิดทบทวนให้ดี SEMI-FORMAL dtɔ̂ng kít-tóp-tuan hâi-dii (You) have to think it over carefully.  

5) ทำให้เกิดความขัดแย้ง tam-hâi gə̀ət kwaam-kàt-yɛ́ɛng (It) causes/creates a conflict.  

6) จะได้ไม่ต้อง(รอ) jà-dâi-mâi-dtɔ̂ng (rɔɔ) 

So you won't/wouldn't have to (wait). 

7) ผมไม่ได้มีเจตนาที่จะ(แหกคุกหนี) FORMAL/LITERARY pǒm mâi-dâi mii  jèet-dtà-naa tîi-jà (hɛ̀ɛk-kúk nǐi) 

I had no intention (of escaping from prison). / I wasn't trying to (escape).  

8) แกจะต้องชดใช้ STRONG/ANGRY gɛɛ jà dtɔ̂ng chót-chái You'll pay for this! 

9) จะต้องไม่ตาย jà dtɔ̂ng mâi dtaai 

You're not gonna die! / You can't die!  

10) คิดไม่ถึงเลยว่า... kít-mâi-tʉ̌ng ləəi wâa... 

I had no idea that... / I didn't think that... / I couldn't imagine that... (used when  something unexpected happens)  

11) แต่ต่างกันตรงที่... dtɛ̀ɛ dtàang-gan dtrong-tîi... 

(It)'s different in that... / The difference is...  

12) มีพระราชโองการ(...) ROYAL mii prá-râat-chá-oong-gaan (...) By the King's royal order (...) / By His Majesty's order... 

13) มีบุญจริงๆที่... SEMI-LITERARY mii bun jing-jing tîi... 

(You)'re really blessed/lucky to...  

14) (...)ไม่ได้ประโยชน์อะไร (...) mâi-dâi bprà-yòot à-rai  

We get no benefit whatsoever. / You get nothing for it. 

15) เวลาอยู่ต่อหน้าคนอื่น(ก็กลัวคนมอง) wee-laa yùu dtɔ̀ɔ-nâa kon-ʉ̀ʉn (gɔ̂ɔ glua mɔɔng) 

When I'm around others (I'm afraid they will look at me). / When I'm in front of  others (I'm afraid they will look at me). 

16) เขามีท่าทีแปลกๆ kǎo mii tâa-tii bplɛ̀ɛk-bplɛ̀ɛk 

She was acting strangely. / He was acting weird.  

17) เราแยกย้ายกันไป rao yɛ̂ɛk-yáai gan bpai 

We went our separate ways. 

18) ตัดใจซะเถอะ dtàt-jai sá tə̀ 

Try to stop thinking about her. / Get over it. / Get over him.  

19) มันเป็นเรื่องใกล้ตัว man bpen rʉ̂ang glâi-dtua 

It's close to home. / It's something you're familiar with.  

20) ไม่รู้จักพอ mâi-rúu-jàk pɔɔ 

He can't get enough! / Insatiable! 

Super Useful Expressions, Part 196 

(low-intermediate level) 

1) น่าอร่อยจัง nâa-à-rɔ̀i jang 

That looks really good/delicious/tasty! 

2) ไม่นานเลย(นะครับ) mâi-naan ləəi (ná-kráp) 

Not so long. / Not that long.  

3) ใครเรียก INFORMAL krai rîak 

Who called (me)?  

4) ระวังติดคอ rá-wang dtìt kɔɔ 

Be careful or it'll get stuck in your throat.  

5) มีอะไรติดคอ mii à-rai dtìt kɔɔ 

Something's stuck in my throat.  

6) ไม่ได้ทำบ่อย(นะ) mâi-dâi tam bɔ̀i (ná) 

(I) don't do it often.  

7) มันมีประโยชน์(นะ) man mii bprà-yòot (ná) 

It's useful/beneficial.  

8) (คุณ)มาจากเมืองไหน / (คุณ)มาจากเมืองอะไร (kun) maa-jàak mʉang nǎi /  (kun) maa-jàak mʉang à-rai

What city are you from?  

9) ชอบกินอาหาร(เกาหลี)ไหม chɔ̂ɔp gin aa-hǎan (gao-lǐi) mǎi Do you like (Korean) food?  

10) ต้องโทรไปบอก(พ่อแม่) dtɔ̂ng too-bpai bɔ̀ɔk (pɔ̂ɔ-mɛ̂ɛ) (I) have to call and tell (my parents).  

11) อันนี้(ปลา)อะไรคะ an-níi (bplaa) à-rai ká 

What kind of (fish) is this?  

12) ไม่เลยหรือ mâi ləəi rʉ̌ʉ 

Not at all? / Not even a little?  

13) เลือกอะไรดี lʉ̂ak à-rai dii 

Which (one) should I choose?  

14) เขาเป็นลูกครึ่ง kǎo bpen lûuk-krʉ̂ng 

He's half (Thai) half (German). 

15) พร้อมหรือยัง prɔ́ɔm rʉ̌ʉ yang 

Are you ready (yet)?  

16) เศรษฐกิจไม่ดี sèet-tà-gìt mâi-dii 

The economy's bad.  

17) โรแมนติกจังเลย roo-mɛɛn-dtìk jang ləəi 

So romantic!  

18) ไม่ได้กำไร(เลย) mâi-dâi gam-rai (ləəi) 

I'm not making any profit. / We didn't turn a profit. / We're in the red.  

19) ขอคิดก่อนนะ POLITE kɔ̌ɔ kít gɔ̀ɔn ná 

Let me think about that. / Let me think.  

20) ไปดูหน่อยได้ไหมครับ bpai duu nɔ̀i dâi-mǎi kráp May I go have a look? / Can I go and see it?

Super Useful Expressions, Part 197 

(low-intermediate level) 

1) คุ้มหรอ INFORMAL kúm rɔ̌ɔ 

Is it worth it? 

2) ไม่คุ้ม mâi kúm 

It's not worth it.  

3) ขายไปแล้ว kǎai bpai lɛ́ɛo 

It's been sold. / They sold it already. / I sold it.  

4) ไม่รู้ว่าเขาชื่ออะไร mâi-rúu wâa kǎo chʉ̂ʉ à-rai 

I don't know what her name is.  

5) ชินรึยัง chin rʉ́-yang 

Are you used to it?  

6) ผมเคยเตือน(คุณ)แล้ว pǒm kəəi dtʉan (kun) lɛ́ɛo 

I've warned (you) before. / I already warned (you).  

7) ดีใจนะที่คุณชอบ dii-jai ná tîi kun chɔ̂ɔp 

I'm glad you like it.  

8) ผมไม่อยากบังคับคุณ pǒm mâi-yàak bang-káp kun 

I don't want to force/pressure you.  

9) ไปคุยกันข้างนอก bpai kui gan kâang-nɔ̂ɔk 

Let's (go) talk outside.  

10) ตอนเด็กๆอยากเป็น(นักกีฬาฟุตบอล) dtɔɔn dèk-dèk yàak bpen (nák-gii-laa fút-bɔn) 

When I was a kid, I wanted to be a (soccer player/footballer).  

11) (คุณพ่อ)ทําอาชีพอะไรคะ FORMAL (kun-pɔ̂ɔ) tam aa-chîip à-rai ká What kind of work does your (father) do? / What's your (father)'s occupation? 

12) (ผม)อยากซื้อบ้าน (pǒm) yàak sʉ́ʉ bâan 

I wanna buy a house.  

13) (ฉัน)อยากซื้อบ้านหลังนี้ (chǎn) yàak sʉ́ʉ bâan lǎng níi I want to buy this house.  

14) อาจจะมี(นะ) àat-jà mii (ná) 

Perhaps there are some. / There may be some. 

15) เป็นของโบราณ bpen kɔ̌ɔng-boo-raan 

It's an antique. / It's an (ancient) artifact.  

16) ไม่มีชีพจร mâi-mii chîip-pá-jɔɔn 

(She) has no pulse.  

17) ขอหอมหน่อย kɔ̌ɔ hɔ̌ɔm nɔ̀i 

Let me give you a kiss (a sniff kiss). / Let me kiss you.  

18) เขาไม่มีประกัน kǎo mâi-mii bprà-gan 

She doesn't have insurance. / He's uninsured.  

19) (ราคา)(ยัง)ไม่รวมภาษี (raa-kaa) (yang) mâi-ruam paa-sǐi (The price) doesn't include tax.  

20) คุณได้อะไร kun dâi à-rai 

What do you get (out of it)? / What did you get? 

Super Useful Expressions, Part 198 (high-intermediate level) 

1) มีของมาฝากด้วย(ค่ะ) mii kɔ̌ɔng maa fàak dûai (kà) I have something for you. / I brought you something. 

2) ฉันอยากใช้ชีวิตที่เรียบง่าย chǎn yàak mii chii-wít rîap-ngâai

I wanna live a simple life.  

3) ถ้าไม่อยากบอกก็ไม่เป็นไร tâa mâi yàak bɔ̀ɔk gɔ̂ɔ mâi-bpen-rai If you don't want to tell me, no problem. / If you don't wanna say, it's okay.  

4) ต้องแต่งตัวยังไง(ดี) dtɔ̂ng dtɛ̀ng-dtua yang-ngai (dii) How should I dress? / What should I wear? (going to party or ceremony) 

5) ต้องแต่งตัวให้เรียบร้อย dtɔ̂ng dtɛ̀ng-dtua hâi rîap-rɔ́ɔi (You) have to dress up/wear proper attire/dress nicely.  

6) ฉันคงตาฝาดไป chǎn kong dtaa-fàat bpai 

I must have been seeing things.  

7) ผมไม่รู้จะอธิบายยังไงดี pǒm mâi-rúu jà à-tí-baai yang-ngai dii I don't know how to explain (it).  

8) ไม่ใช่อย่างนั้น(ค่ะ) mâi-châi yàang-nán (kà) 

It's not that. / That's not it. (that's not what I meant or how it is) 

9) ถือสายรอ(สักครู่)(ครับ) tʉ̌ʉ-sǎai rɔɔ (sàk-krûu) (kráp) (Please) hold on a moment/second. (on the phone) 

10) เพิ่งมาใหม่ pə̂ng maa mài 

(This) just came in. (a product at store) / (I)'m new (here).  

11) ออกไปจากบ้านฉัน ANGRY ɔ̀ɔk-bpai jàak bâan chǎn Get out of my house!  

12) (ยัง)ไม่ได้ยินข่าวอะไรเลย (yang) mâi-dâi-yin kàao à-rai ləəi We haven't heard anything yet. / I haven't heard any news yet.  

13) คงอึดอัดแย่ kong ʉ̀t-àt yɛ̂ɛ 

I'd feel very/really uncomfortable.  

14) มีคนวิ่งตัดหน้ารถ(ผม) mii kon wîng dtàt-nâa rót (pǒm)

Someone cut in front of (me). / Someone cut (me) off. / Someone ran right in  front of me.  

15) ทุกอย่างเหมือนเดิม túk-yàang mʉ̌an-dəəm 

Everything's the same (as before/usual).  

16) คิดเงินด้วย(ครับ) kít-ngən dûai (kráp) 

Ring me up. / Check please.  

17) ไม่เห็นได้ยินเลย URGING mâi-hěn dâi-yin ləəi 

I didn't hear it (at all).  

18) ไม่รู้จะติดต่อใคร mâi-rúu jà dtìt-dtɔ̀ɔ krai 

(I) don't know who to contact. / (I) don't know who I should contact.  

19) ขออนุญาตถาม(หน่อย)นะคะ POLITE/FORMAL kɔ̌ɔ à-nú-yâat tǎam (nɔ̀i)  ná-ká 

If I may (ask)... / May I ask you...?  

20) เขาป่วยเป็นอะไรคะ kǎo bpùai bpen à-rai ká 

What's wrong with him? / What kind of illness did she have? 

Super Useful Expressions, Part 199 

(low-intermediate level) 

1) ดีใจที่ได้คุยกับคุณ(นะ)(ครับ) dii-jai tîi dâi kui gàp kun (ná) (kráp) It was nice talking with you. / I'm glad I was able to talk with you. 

2) พอจะรู้จักไหมครับ pɔɔ-jà rúu-jàk mǎi kráp 

Do (you) happen to know? / Do (you) happen to know him? / Do (you) know it?  

3) ฝันไปรึเปล่า fǎn bpai rʉ́-bplàao 

Am (I) dreaming?!  

4) ผมต้องการเช่ารถครับ POLITE pǒm dtɔ̂ng-gaan châo rót kráp

I'd like to rent a car. / I need to rent a car.  

5) (เรียน)จบที่ไหน(คะ) (rian) jòp tîi-nǎi (ká) 

Where did you graduate from? / What school did you go to?  

6) ทำไมไม่รับสาย tam-mai mâi ráp-sǎai 

Why doesn't he pick up? / Why don't you answer (your phone)?  

7) ควรทำยังไง(ดี) kuan tam yang-ngai (dii) 

What should I do? / What am I supposed to do?  

8) ทำไมไม่โทร(มา)หาฉัน tam-mai mâi too (maa) hǎa chǎn Why didn't you call me?  

9) น้องๆ nɔ́ɔng nɔ́ɔng 

Waiter! / Excuse me! (calling attention to younger person at restaurant, etc.) 

10) ฉันเลือกเอง chǎn lʉ̂ak eeng 

I picked it out myself. (a present, etc. ) OR I will choose it myself.  

11) อยากได้สีอะไร(คะ) yàak dâi sǐi à-rai (ká) 

What color would you like? / What color do you want?  

12) มีหลากหลายสี(ให้เลือก) mii làak-lǎai sǐi (hâi lʉ̂ak) 

We have many colors (to choose from). / There are a variety of colors (to choose  from). 

13) ถูกใจผมมาก tùuk-jai pǒm mâak 

I really like it! / It's really to my liking! / It's really my type!  

14) เธอไม่ได้ไปไหน təə mâi-dâi bpai-nǎi 

She didn't go anywhere. 

15) คุณจะอยู่ที่นี่กี่วัน kun jà yùu tîi-nîi gìi wan 

How many days are you staying (here)?  

16) ห้องเต็ม(แล้ว) hɔ̂ng dtem (lɛ́ɛo)

The rooms are all full/booked.  

17) คิดได้ยังไง kít dâi yang-ngai 

How can you think that?! / What are you thinking?!  

18) ทานด้วยกันไหมคะ FORMAL taan dûai-gan mǎi ká Would you like to have (dinner) together?  

19) เดายาก dao yâak 

It's hard to guess.  

20) น่าขยะแขยง nâa-kà-yà-kà-yɛ̌ɛng 

That's disgusting! / How disgusting! 

Super Useful Expressions, Part 200 (low-intermediate level) 

1) (แฟน)ขี้หึงมาก (fɛɛn) kîi-hʉ̌ng mâak 

(My girlfriend)'s really jealous/the jealous type. 

2) อยากกินอะไร(คะ) yàak gin à-rai (ká) 

What do you wanna eat? / What would you like to eat?  

3) ให้ผมช่วยไหมครับ hâi pǒm chûai mǎi kráp Do you need some help? / Do you want me to help you?  

4) ใครสอน(คุณเต้น) krai sɔ̌ɔn (kun dtên) 

Who taught you (how to dance)?  

5) ยังไม่ได้กินข้าวเลย yang mâi-dâi gin-kâao ləəi I haven't eaten anything yet. / I haven't had anything to eat at all.  

6) ผมไม่มีรายได้ pǒm mâi mii raai-dâai 

I have no income. 

7) เขาตกงาน kǎo dtòk-ngaan 

He lost his job. / She's unemployed. / She's out of work.  

8) ผมไม่กลัวใคร(เลย) pǒm mâi glua krai (ləəi) 

I'm not afraid of anyone!  

9) ขอดูรูปหน่อยได้ไหม(ครับ) kɔ̌ɔ duu rûup nɔ̀i dâi-mǎi (kráp) Can I see a picture (of her/it)?  

10) ฉันจำได้แค่ว่า... chǎn jam-dâi kɛ̂ɛ wâa... 

I only remember... / The only thing I remember is...  

11) (...)พอรึยัง/(...)พอหรือยัง (...) pɔɔ rʉ́-yang / (...) pɔɔ rʉ̌ʉ-yang Is it enough? / Do we have enough yet? / ...enough?  

12) ขอถาม(คุณ)หน่อยได้ไหม(คะ) FORMAL kɔ̌ɔ tǎam (kun) nɔ̀i dâi-mǎi (ká) May I ask you (a question)? / May I ask you something? / Do you mind if I ask  you something?  

13) ผมไม่ได้ฝันไป pǒm mâi-dâi fǎn bpai 

I wasn't dreaming. / It wasn't a dream. / I wasn't imagining things.  

14) จะไปยังไง jà bpai yang-ngai 

How are you gonna get there? / How are you going? 

15) (ดู)เหมือนฝนจะตก (duu) mʉ̌an fǒn jà dtòk 

Looks like it's gonna rain. 

16) ฉันทำเองได้(ค่ะ) chǎn tam eeng dâi (kà) 

I can do it myself. / I got it. / I can handle it.  

17) วันนี้เป็นวันพระ wan-níi bpen wan-prá 

Today's a Buddhist holy day.  

18) คุณดูหน้าซีด kun duu nâa-sîit 

You look pale. 

19) คิดถึงเสมอ kít-tʉ̌ng sà-mə̌ə 

Miss you always. / I always miss him. / I always miss this place.  

20) จอดตรงนี้ jɔ̀ɔt dtrong-níi 

Pull over here. / Stop right here.
