    Bộ Y tế phân bổ vắc xin Covid-19 đợt 5, HCDC nhiều nhất với 786.000 liều

    Ngoài Trung tâm kiểm soát bệnh tật TP.HCM được phân bổ 786.000 liều vắc xin
    Covid-19, một số đơn vị tại TP.HCM cũng được phân bổ thêm vắc xin, trong đó
    có lực lượng Công an TP.HCM.
    Bộ Y tế yêu cầu các cơ quan, đơn vị nhận vắc xin phải tổ chức tiêm ngay ///
    ĐẬU TIẾN ĐẠT Bộ Y tế yêu cầu các cơ quan, đơn vị nhận vắc xin phải tổ chức
    tiêm ngay - ĐẬU TIẾN ĐẠT
    Bộ Y tế yêu cầu các cơ quan, đơn vị nhận vắc xin phải tổ chức tiêm ngay
    ĐẬU TIẾN ĐẠT
    Ngày 17.6, Bộ Y tế có quyết định số 2971/QĐ-BYT, phân bổ đợt 5 vắc xin 
    Covid-19 của AstraZeneca cho các đơn vị, địa phương. Riêng Trung tâm Kiểm
    soát bệnh tật TP.HCM (HCDC) được phân bổ nhiều nhất, với 786.000 liều.

    Bộ Y tế phân bổ 1 triệu liều vắc xin Covid-19 đợt 5 cho gần 40 đơn vị

    Tại quyết định số 2971/QĐ-BYT của Bộ Y tế, ngoài HCDC được phân bổ vắc xin,
    còn có 37 đơn vị khác được phân bổ vắc xin đợt này gồm Trung tâm kiểm soát
    bệnh tật (CDC) một số tỉnh, thành phố; phân bổ cho các viện, bệnh viện,
    trường Cao đẳng; Phân bổ cho các tổng Công ty; công ty; trung tâm pháp y
    các vùng miền... và lực lượng công an TP.HCM và Cục Quân y, Bộ Quốc phòng.
    Theo đó, trong số các bệnh viện được phân bổ có Bệnh viện Bạch Mai nhiều
    nhất với 33.000 liều; Bệnh viện Nhi Trung ương 25.000 liều; Bệnh viện E là
    18.000 liều; Bệnh viện Hữu Nghị 5.000 liều; Bệnh viện Việt Đức 3.000
    liều...
    Ở lần phân bổ này, Viện Pasteur TP.HCM được phân bổ 7.000 liều; Dự án tiêm
    chủng quốc gia là 3.000 liều.
    Cục Quân Y Bộ Quốc phòng được phân bổ 35.000 liều; Lực lượng công an tại
    TP.HCM (bảo quản tại kho của HCDC) là 20.000 liều...
    Theo quyết định phân bổ đợt này, Bộ Y tế yêu cầu Dự án Tiêm chủng mở rộng
    quốc gia - Viện Vệ sinh dịch tễ T.Ư thực hiện tiếp nhận, bảo quản, gửi mẫu
    kiểm định chất lượng và vận chuyển ngay vắc xin tới Dự án Tiêm chủng mở
    rộng khu vực để phân bổ ngay cho các địa phương, đơn vị theo danh sách đã
    ban hành.

    Giám đốc HCDC: “Biến chủng Delta 3 ngày tạo ra một chu kỳ lây Covid-19 ở
    TP.HCM”

    Đối với Sở Y tế TP.HCM, sẽ là cơ quan chỉ đạo HCDC tiếp nhận, bảo quản và
    tổ chức triển khai tiêm chủng ngay số vắc xin được phân bổ cho các đối
    tượng theo đúng Nghị quyết số 21/NQ-CP ngày 26/02/2021 của Chính phủ trên
    địa bàn (bao gồm cả các cơ quan trung ương, bộ ngành, tập đoàn, tổng công
    ty, tổ chức quốc tế, cơ quan ngoại giao,...) và triển khai tiêm cho các đối
    tượng nguy cơ là công nhân các khu công nghiệp, khu chế xuất theo chỉ đạo
    của Thủ tướng Chính phủ.
    Sở Y tế TP.HCM phối hợp với Cục Y tế, Bộ Công an để hỗ trợ và tổ chức tiêm
    vắc xin cho lực lượng công an trên địa bàn TP.HCM.
