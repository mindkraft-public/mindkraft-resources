# Stu's Dotfiles - for OSX

Created this repository to keep all the dotfiles / config files that I can in one place. 

Use this repo and symlink to the appropriate dotfile and aliases

```
ln -s filename-to-link simlinkfile
```

More to come
